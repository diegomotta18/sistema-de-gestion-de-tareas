
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('send-notice', require('./components/SendNotice.vue'));
Vue.component('comment-list', require('./components/CommentTask.vue'));
Vue.component('select-show-user', require('./components/SelectShowUser.vue'));
Vue.component('tag-form', require('./components/TagForm.vue'));
Vue.component('area-project', require('./components/AreaProject.vue'));
Vue.component('change-state', require('./components/ChangeState.vue'));
Vue.component('comment-event', require('./components/CommentEvent.vue'));
Vue.component('change-statesub', require('./components/ChangeStatusSub.vue'));
Vue.component('project-form', require('./components/ProjectForm.vue'));
Vue.component('license-observation', require('./components/LicenseObservation.vue'));

const app = new Vue({
    el: '#app',
});
