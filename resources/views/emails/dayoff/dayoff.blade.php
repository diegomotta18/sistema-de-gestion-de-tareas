<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8"/>
    <title>Misiones Online</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <style type="text/css">
        body{
            margin: 0;
            background-color: #f9f9f9;
        }
        .header{
            text-align: center;
            background: white;
            padding: 10px;
            border-bottom: 1px solid #b1aeae;
        }

        .header img{
            width: 300px;
        }

        .content{
            margin: 45px auto 0;
            width: 500px;
            background:  white;
            padding: 2px 20px;
            border: 1px solid #efefef;
        }
        .btn {
            display: inline-block;
            margin-bottom: 0;
            font-weight: normal;
            text-align: center;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            white-space: nowrap;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            border-radius: 4px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .btn:focus,
        .btn:active:focus,
        .btn.active:focus,
        .btn.focus,
        .btn:active.focus,
        .btn.active.focus {
            outline: 5px auto -webkit-focus-ring-color;
            outline-offset: -2px;
        }
        .btn:hover,
        .btn:focus,
        .btn.focus {
            color: #333333;
            text-decoration: none;
        }
        .btn:active,
        .btn.active {
            outline: 0;
            background-image: none;
            -webkit-box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
            box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
        }
        .btn.disabled,
        .btn[disabled],
        fieldset[disabled] .btn {
            cursor: not-allowed;
            opacity: 0.65;
            filter: alpha(opacity=65);
            -webkit-box-shadow: none;
            box-shadow: none;
        }
        a.btn.disabled,
        fieldset[disabled] a.btn {
            pointer-events: none;
        }
        .btn-default {
            color: #333333;
            background-color: #ffffff;
            border-color: #cccccc;
        }

        .btn-success {
            color: #ffffff;
            background-color: #5cb85c;
            border-color: #4cae4c;
        }
        .btn-success:focus,
        .btn-success.focus {
            color: #ffffff;
            background-color: #449d44;
            border-color: #255625;
        }
        .btn-success:hover {
            color: #ffffff;
            background-color: #449d44;
            border-color: #398439;
        }
        .btn-success:active,
        .btn-success.active,
        .open > .dropdown-toggle.btn-success {
            color: #ffffff;
            background-color: #449d44;
            border-color: #398439;
        }
        .btn-success:active:hover,
        .btn-success.active:hover,
        .open > .dropdown-toggle.btn-success:hover,
        .btn-success:active:focus,
        .btn-success.active:focus,
        .open > .dropdown-toggle.btn-success:focus,
        .btn-success:active.focus,
        .btn-success.active.focus,
        .open > .dropdown-toggle.btn-success.focus {
            color: #ffffff;
            background-color: #398439;
            border-color: #255625;
        }
        .btn-success:active,
        .btn-success.active,
        .open > .dropdown-toggle.btn-success {
            background-image: none;
        }
        .btn-success.disabled:hover,
        .btn-success[disabled]:hover,
        fieldset[disabled] .btn-success:hover,
        .btn-success.disabled:focus,
        .btn-success[disabled]:focus,
        fieldset[disabled] .btn-success:focus,
        .btn-success.disabled.focus,
        .btn-success[disabled].focus,
        fieldset[disabled] .btn-success.focus {
            background-color: #5cb85c;
            border-color: #4cae4c;
        }
        .btn-success .badge {
            color: #ffffff;
            background-color: #5cb85c;
            border-color: #4cae4c;
            font-size: 14px;
            font-weight: bold;
            letter-spacing: 0.7px;
        }
        a:link{
            text-decoration:none;
        }
        a.btn{
            margin-bottom: 10px;
        }
    </style>
</head>
<body>

<div class="header">
    <img src="{{asset('img/mol/logomol.png')}}" alt="logo-mol"  title="Misiones Online"/>
</div>

<div class="content">


    <h3>Día franco</h3>

    <p>
        Nombre: <strong>{{$data['user']}}</strong>
    </p>

    <p>
        Fecha del franco: <strong>{{$data['date_taken']}}</strong>
    </p>

    @if(!is_null($data['observations']))
        <h3>Observaciones</h3>

        <p>{!! $data['observations'] !!}</p>

    @endif
    <p>
        <i>Para consultas comunicarse con el personal de recursos humanos.</i>
    </p>

    <p>
        <strong>{{$auth->name}}</strong>
    </p>

</div>


</body>
</html>

