@if($errors->has('typeUser')){!! Form::label('', 'Elija a quien enviar la novedad', array('class'=>'error')) !!} @else {!! Form::label('description', 'Elija a quien enviar la novedad') !!} @endif
<send-notice></send-notice>
@if($errors->has('typeUser')) <p class="help-block" style="color: #a94442;">Debe seleccionar una opción</p> @endif

<br>

{!! Field::text('title', isset($notice) ? $notice->title : '') !!}

{!! Field::textarea('description', isset($notice) ? $notice->description : '',array('id' =>'summernote', 'class'=>'summernote form-control' )) !!}
@if($errors->has('description')) <p class="help-block" style="color: #a94442;">{{ $errors->first('description') }}</p> @endif

{{--<textarea id="summernote" name="description" class="summernote form-control"></textarea>--}}

{{--{!! Field::email('email', isset($user) ? $user->email : '') !!}--}}

{{--{!! Field::select('role', $roles, isset($user) ? $user->roles()->first()->name : '' ,['empty' => __('Seleccionar Rol')]) !!}--}}

{{--{!! Field::select('area', $areas, isset($user) ? $user->areas : '' ,['name'=>'areas[]','multiple','class'=>'js-states form-control','empty' => __('Asignar areas')]) !!}--}}

<button type="submit" class="btn btn-primary waves-effect waves-classic">
    <i class="fa fa-send" aria-hidden="true"></i>
    Enviar
</button>