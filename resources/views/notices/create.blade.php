@extends('layouts.app')
@push('styles')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
@endpush
@section('content')
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Agregar novedad
                </h5>

                <div class="ibox-tools hidden-xs">
                    <div class="btn-group">
                        <a style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;" href="{{route('notices.index')}}" class="btn btn-default">
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                            Volver
                        </a>
                    </div>
                </div>
                <div class="ibox-tools hidden-lg hidden-sm hidden-md pull-right">
                    <div class="btn-group">
                        <a style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;" href="{{route('notices.index')}}" class="btn btn-xs btn-default">
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                            Volver
                        </a>
                    </div>
                </div>

            </div>

            <div class="ibox-content">
                <form method="POST" action="{{ route('notices.store') }}">
                    @csrf

                    @include('notices.partials.form')
                </form>
            </div>
        </div>
    </div>
@endsection



@push('scripts')
<script src="{{ asset('plugins/select2/saaelect2.min.js') }}"></script>
<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>

<script src="{{ asset('plugins/summernote/lang/summernote-es-ES.js') }}"></script>

<script>
    $(document).ready(function () {
        $('.summernote').summernote({
            height: 300,
            lang: 'es-ES', // default: 'en-US'
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ]
        });
        $('.note-btn').removeAttr('title');
        $('#user_select').select2();
        $('#user_all').select2();
        $('.dropdown-toggle').dropdown();

    });
</script>

@endpush

