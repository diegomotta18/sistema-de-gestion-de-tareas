@extends('layouts.app')

@section('content')

    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Novedad <label class="badge badge-primary">{{$notice->title}}</label>

                </h5>

                <div class="ibox-tools hidden-xs">
                    <div class="btn-group">
                        <a style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;" href="{{route('notices.index')}}" class="btn btn-default">
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                            Volver
                        </a>
                    </div>
                </div>
                <div class="ibox-tools hidden-lg hidden-sm hidden-md pull-right">
                    <div class="btn-group">
                        <a style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;" href="{{route('notices.index')}}" class="btn btn-xs btn-default">
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                            Volver
                        </a>
                    </div>
                </div>

            </div>
            <div class="ibox-content">
                <h5>Usuarios</h5>
                <div class="form">
                    @foreach( $notice->users()->get()->chunk(3) as $users)
                        @foreach( $users as $user)
                            <label class="badge badge-primary">{{$user->name}}</label>
                        @endforeach
                    @endforeach
                </div>
            </div>
            <div class="ibox-content">
                <h5>Descripción</h5>

                <p>{!! $notice->description !!}</p>
            </div>

        </div>
    </div>

@endsection
@push('scripts')
<script>
    $('.dropdown-toggle').dropdown();

</script>
@endpush