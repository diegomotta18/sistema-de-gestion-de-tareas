@extends('layouts.app')

@section('content')
    <div class="alert alert-info" role="alert"><p><i class="fa fa-info-circle"></i>
            <strong>Importante</strong></p>
        <p>En esta sección se puede enviar comunicados a todos los usuario, usuarios por área o usuarios seleccionados.
            Desde el botón <strong>Crear novedad</strong>, se puede crear una nueva nueva ingresando un título,
            descripción y selección de usuarios a enviar la novedad. Una vez enviado la novedad, se informa al usuario
            mediante una notificación sobre el sistema y por email.</p>
    </div>
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Novedades
                </h5>
                <div class="ibox-tools hidden-xs">
                    <div class="btn-group">
                        @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('god'))
                            <div class="btn-group">

                                <a href="{{ route('notices.create') }}" class="btn btn-primary pull-right">
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                    Crear novedad
                                </a>
                            </div>
                        @endif
                        <a class="btn btn-default  pull-right"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           @if(auth()->user()->hasArea('Dirección')) href="{{ route('direccion.index') }}"
                           @else href="{{route('home')}}" @endif><i class="fa fa-arrow-circle-left"></i>Volver</a>
                    </div>
                </div>
                <div class="ibox-tools hidden-md hidden-lg hidden-sm pull-right">
                    <div class="btn-group">

                        @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('god'))

                            <a href="{{ route('notices.create') }}" class="btn btn-xs btn-primary">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                Crear novedad
                            </a>
                        @endif
                        <a class="btn btn-xs btn-default  pull-right"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           @if(auth()->user()->hasArea('Dirección')) href="{{ route('direccion.index') }}"
                           @else href="{{route('home')}}" @endif><i class="fa fa-arrow-circle-left"></i>Volver</a>
                    </div>

                </div>
            </div>

            <div class="ibox-content">
                <table class="table table-hover"
                       data-toggle="table"
                       data-show-columns="true"
                       data-pagination="true"
                       data-search="true">
                    <thead>
                    <tr>
                        <th data-visible="false">ID</th>
                        <th>Titulo</th>
                        <th>Descripción</th>
                        <th>Usuarios</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($notices as $notice)
                        <tr>
                            <td>{{ $notice->id }}</td>
                            <td>{{ $notice->title }}</td>
                            <td>{!! $notice->description  !!}</td>
                            <td>
                                @foreach($notice->users()->get() as $users)
                                    <label class="badge badge-primary">{{$users->name}}</label>
                                @endforeach
                            </td>
                            <td>
                                <div class="btn-group">
                                    <a href="{{route('notices.show',$notice->id)}}" data-toggle="tooltip"
                                       title="Ver novedad"
                                       class="btn btn-info">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                    @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('god'))
                                        <button data-id="{{ $notice->id }}" data-toggle="tooltip" title="Quitar novedad"
                                                class="btn btn-danger  btn-destroy">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </button>
                                    @endif
                                </div>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
@routes

<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();

        $('.btn-destroy').on('click', function () {
            var id = $(this).data('id');
            var url = route('notices.destroy', {id: id});

            swal({
                title: 'Confirmar',
                text: "Una vez confirmada no es posible revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar y borrar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url, {data: {id: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Eliminado!',
                                    'La noticia ha sido eliminada',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });

    });
</script>
@endpush