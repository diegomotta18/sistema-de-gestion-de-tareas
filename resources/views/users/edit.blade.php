@extends('layouts.app')

@section('content')
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Editar Usuario
                </h5>
                <div class="ibox-tools">
                    <div class="btn-group">
                        <div class="pull-right">
                            <a href="{{route('users.index')}}"
                               class="btn btn-default">
                                <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                                Volver
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="ibox-content">
                <form method="POST" action="{{ route('users.update', $user->id) }}">
                    @csrf
                    @method('PUT')

                    @include('users.partials.form')
                </form>
            </div>
        </div>
@endsection

@push('styles')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<script src="{{ asset('plugins/select2/saaelect2.min.js') }}"></script>
<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>

<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();

        $('#areas').select2();
    });
</script>
@endpush