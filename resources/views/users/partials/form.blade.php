{!! Field::text('name', isset($user) ? $user->name : '') !!}

{!! Field::email('email', isset($user) ? $user->email : '') !!}

{!! Field::select('role', $roles, isset($user) ? $user->roles()->first()->name : '' ,['empty' => __('Seleccionar Rol')]) !!}

{!! Field::select('area', $areas, isset($user->area) ? $user->area->id : ''  ,['empty' => ('Asignar área principal') ])!!}

{!! Field::select('areas', $areas, isset($user) ? $user->areas : '' ,['name'=>'areas[]','multiple','class'=>'js-states form-control','empty' => ('Asignar areas')]) !!}

{!! Field::checkbox('chief', 'value', isset($user) ? $user->chief :'' ) !!}
<button type="submit" class="btn btn-primary waves-effect waves-classic">
    <i class="fa fa-save" aria-hidden="true"></i>
    Guardar
</button>