@extends('layouts.app')

@section('content')
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Usuarios
                </h5>

                <div class="ibox-tools">
                    <div class="btn-group">
                        <a href="{{ route('users.create') }}" class="btn btn-primary pull-right">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                            Crear usuario
                        </a>
                    </div>
                </div>
            </div>

            <div class="ibox-content">
                <table id="table-users" class="table table-hover"
                       data-toggle="table"
                       data-show-columns="true"
                       data-pagination="true"
                       data-search="true">
                    <thead>
                    <tr>
                        <th data-visible="false">ID</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Rol</th>
                        <th>Area principal</th>
                        <th>Areas</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->getRoleNames()->implode(', ') }} @if($user->chief) (Jefe de área) @endif</td>
                            <td>@if(isset($user->area)) {{$user->area->name}} @endif</td>
                            <td>{{ $user->areas->pluck('name')->implode(', ') }}</td>
                            <td>
                                <div class="btn-group" style="display: flex;text-align: center">

                                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-warning"
                                       title="Editar usuario">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </a>

                                    <button data-id="{{ $user->id }}" title="Quitar usuario"
                                            class="btn btn-destroy btn-danger">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
@endsection

@push('scripts')
@routes

<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();


        $('#table-users').on('click','.btn-destroy', function () {
            var id = $(this).data('id');
            var url = route('users.destroy', {id: id});

            swal({
                title: 'Confirmar',
                text: "Una vez confirmada no es posible revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar y borrar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url, {data: {id: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Eliminado!',
                                    'El usuario ha sido eliminado',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });
    });
</script>
@endpush