@extends('layouts.app')

@section('content')
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Registro de ingreso de usuarios
                </h5>
            </div>

            <div class="ibox-content">
                <table class="table table-hover table-bordered table-striped table-responsive">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Ingreso</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->start_date }}</td>
                            <td>{{ $user->user->name }}</td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
                <div class="text-center">
                    @if(!is_null($users))
                        {{ $users->links() }}
                    @endif
                </div>
            </div>
        </div>
@endsection

