@extends('layouts.app')

@section('content')
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Etiquetas
                </h5>
            </div>
        </div>
    </div>
    <tag-form></tag-form>
@endsection

@push('scripts')
@routes

<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();


    });
</script>
@endpush