<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Sistema de Gestión Interno de Misiones Online') }}</title>
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote.css') }}">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/bootstrap-table/bootstrap-table.min.css') }}" rel="stylesheet">
    <link href="{{ asset('js/plugins/gritter/jquery.gritter.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/bootstrap-fileinput/css/fileinput.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/bootstrap-fileinput/themes/explorer-fa/theme.css') }}" rel="stylesheet">



    @stack('styles')
    <style>
        .swal2-popup {
            font-size: 1.6rem !important;
        }

        @media (max-width: 767px) {

            .dropdown-menu-center {
                left: 50% !important;
                right: auto !important;
                transform: translate(-50%, 0) !important;
            }
        }
    </style>
</head>
<body class="body-small fixed-sidebar pace-done">

<div id="app">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                @include('layouts.partials.sidemenu')
            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i
                                    class="fa fa-bars"></i>
                        </a>
                            {{--<form role="search" class="navbar-form-custom" action="search_results.html">--}}
                                {{--<div class="form-group">--}}
                                    {{--<input type="text" placeholder="Search for something..." class="form-control"--}}
                                           {{--name="top-search" id="top-search">--}}
                                {{--</div>--}}
                            {{--</form>--}}
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-exclamation-triangle"></i> <span
                                        class="label label-danger">{{auth()->user()->unreadNotifications->whereIn('type',array(\App\Notifications\ExpiredNotification::class))->count()}}</span>
                            </a>
                            <ul class="dropdown-menu dropdown-messages dropdown-menu-rigth" style="width: 250px;padding: 1px;">
                                @if(!is_null(auth()->user()->unreadNotifications))
                                    @foreach(auth()->user()->unreadNotifications->whereIn('type',array(\App\Notifications\ExpiredNotification::class))->sortByDesc('id')->slice(0, 5) as $notification)
                                        <li style="">
                                            <div class="dropdown-messages-box"
                                                 @if($notification->unread())  style="margin-top: 7px; background-color: #f5f5f5;" @endif >
                                                <div class="media-body" style="padding: 5px;">
                                                    <a style="color:#888888;padding: 0px;"
                                                       href="{{$notification->data['link']}}">
                                                        <div>{!! $notification->data['message'] !!}.</div>
                                                        <strong>{{  $notification->data['title']}}</strong>
                                                        <br>
                                                        <small class="text-muted">{{$notification->created_at}}</small>
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="divider"></li>
                                    @endforeach

                                    <li>
                                        <div class="text-center link-block">
                                            <a href="{{route('notifications.expireds')}}">
                                                <i class="fa fa-exclamation-triangle"></i> <strong>Ver todas las tareas
                                                    demoradas</strong>
                                            </a>
                                        </div>
                                    </li>

                                @endif
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-envelope"></i> <span
                                        class="label label-warning">{{auth()->user()->unreadNotifications->where('type',\App\Notifications\MessageNotification::class, \App\Notifications\MsgThreadNotification::class)->count()}}</span>
                            </a>
                            <ul class="dropdown-menu dropdown-messages dropdown-menu-center" style="width: 250px;padding: 1px;">
                                @if(!is_null(auth()->user()->unreadNotifications))
                                    @foreach(auth()->user()->unreadNotifications->whereIn('type',array(\App\Notifications\MessageNotification::class, \App\Notifications\MsgThreadNotification::class))->sortByDesc('id')->slice(0, 5) as $notification)
                                        <li style="">
                                            <div class="dropdown-messages-box"
                                                 @if($notification->unread())  style=" margin-top: 7px;background-color: #f5f5f5;" @endif >
                                                <div class="media-body" style="padding: 5px;">
                                                    <a style="color:#888888;padding: 0px;"
                                                       href="{{$notification->data['link']}}">
                                                        <div>{!! $notification->data['title'] !!}.</div>
                                                        <strong>{{  $notification->data['subject']}}</strong>

                                                        <br>
                                                        <small class="text-muted">{{$notification->created_at}}</small>
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="divider"></li>
                                    @endforeach

                                    <li>
                                        <div class="text-center link-block">
                                            <a href="{{route('threads.index')}}">
                                                <i class="fa fa-envelope"></i> <strong>Ver todas los mensajes</strong>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="divider"></li>

                                    <li>
                                        <div class="text-center link-block">
                                            <a href="{{route('threads.create')}}">
                                                <i class="fa fa-send"></i> <strong>Redactar mensaje</strong>
                                            </a>
                                        </div>
                                    </li>
                                @endif
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-bell"></i> <span
                                        class="label label-primary">{{auth()->user()->unreadNotifications->whereIn('type',array(\App\Notifications\TaskNotification::class,App\Notifications\InformatedNotification::class,App\Notifications\CommentNotification::class,App\Notifications\NovedadNotification::class,App\Notifications\TaskProcessNotification::class,App\Notifications\TaskFinishNotification::class, App\Notifications\TaskRespondedNotification::class, App\Notifications\HolidayCreatedNotification::class))->count()}}</span>
                            </a>
                            <ul class="dropdown-menu dropdown-messages dropdown-menu-center" style="width: 250px;padding: 1px;">
                                @if(!is_null(auth()->user()->unreadNotifications))
                                    @foreach(auth()->user()->unreadNotifications->whereIn('type',array(\App\Notifications\TaskNotification::class,App\Notifications\InformatedNotification::class,App\Notifications\CommentNotification::class,App\Notifications\NovedadNotification::class,App\Notifications\TaskProcessNotification::class,App\Notifications\TaskFinishNotification::class,App\Notifications\TaskRespondedNotification::class,App\Notifications\HolidayCreatedNotification::class))->sortByDesc('created_at')->slice(0, 5) as $notification)
                                        <li style="">
                                            <div class="dropdown-messages-box"
                                                 @if($notification->unread())  style="margin-top: 7px; background-color: #f5f5f5;" @endif >
                                                <div class="media-body" style="padding: 5px;">
                                                    <a style="color:#888888;padding: 0px;"
                                                       href="{{$notification->data['link']}}">
                                                        <div>{!! $notification->data['message'] !!}.</div>
                                                        <strong>{{  $notification->data['title']}}</strong>
                                                        <br>
                                                        <small class="text-muted">{{$notification->created_at}}</small>
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="divider"></li>
                                    @endforeach

                                    <li>
                                        <div class="text-center link-block">
                                            <a href="{{route('notifications.index')}}">
                                                <i class="fa fa-bell"></i> <strong>Ver todas las notificaciones</strong>
                                            </a>
                                        </div>
                                    </li>
                                @endif
                            </ul>
                        </li>
                        <li class="pull-right">
                            <a class="dropdown-item" role="menuitem" href="{{ route('logout') }}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <i class="icon md-power" aria-hidden="true"></i> <i class="fa fa-sign-out"></i> Salir
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>

                </nav>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="wrapper wrapper-content">
                        <div class="row">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- Scripts -->

<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ asset('js/jquery-2.1.1.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('plugins/sweetalert2/sweetalert.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-table/bootstrap-table.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-table/bootstrap-table-es-AR.min.js') }}"></script>
<script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('plugins/summernote/summernote.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-fileinput/js/plugins/piexif.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-fileinput/js/plugins/purify.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-fileinput/js/plugins/sortable.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-fileinput/js/fileinput.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-fileinput/themes/fa/theme.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-fileinput/themes/explorer-fa/theme.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-fileinput/js/locales/es.js') }}"></script>
<script src="{{ asset('js/plugins/peity/jquery.peity.min.js') }}"></script>
<script src="{{ asset('js/plugins/peity/jquery.peity.min.js') }}"></script>
<script src="{{ asset('js/demo/peity-demo.js') }}"></script>
<script src="{{ asset('js/inspinia.js') }}"></script>


@include('layouts.partials.notifications')

@stack('scripts')
</body>
</html>
