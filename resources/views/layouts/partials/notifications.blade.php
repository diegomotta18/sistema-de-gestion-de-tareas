<script>
    @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch (type) {
        case 'info':
            swal("Info", "{{ Session::get('message') }}", "info");
            break;

        case 'warning':
            swal("Warning", "{{ Session::get('message') }}", "warning");
            break;

        case 'success':
            swal("Ok", "{{ Session::get('message') }}", "success");
            break;

        case 'error':
            swal("Oops", "{{ Session::get('message') }}", "error");
            break;
    }
    {{Session::forget('message')}}
    {{Session::forget('alert-type')}}

    @endif

</script>