<ul class="nav metismenu" id="side-menu">
    <li class="nav-header">
        <div class="dropdown profile-element text-center" style="    margin-top: -35px;">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <span class="clear">
                    <span class="block m-t-xs">
                        @if(isset(auth()->user()->area))
                            <h3> <strong class="font-bold">{{auth()->user()->area->name}}</strong></h3>
                        @endif
                    </span>
                </span>

                <span>
                    <img alt="image" class="img-circle" style="max-width: 40px"
                         src="{{ asset('img/default-user.png') }}"/>
                </span>
            </a>

            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <span class="clear">
                    <span class="block m-t-xs">
                        <strong class="font-bold">{{ auth()->user()->name }}
                            <b class="caret"></b>
                        </strong>
                    </span>
                </span>
            </a>

            {{-- <ul class="dropdown-menu animated fadeInRight m-t-xs">
                <li>
                    <a href="{{ route('user.profile') }}">
                        Mi perfil
                    </a>
                </li>
            </ul> --}}
        </div>

        <div class="logo-element">
            I+
        </div>
    </li>

    @if(!auth()->user()->hasArea('Dirección'))
        <li class="{{ set_active('/') }}">
            <a href="{{ route('home') }}">
                <i class="fa fa-th-large"></i>
                <span class="nav-label">Mi inicio</span>
            </a>
        </li>
    @else
        <li class="{{ set_active('/') }}">
            <a href="{{ route('direccion.index') }}">
                <i class="fa fa-th-large"></i>
                <span class="nav-label">Mi inicio</span>
            </a>
        </li>
    @endif
    
    @if(auth()->user()->hasRole('god'))
        <li class="{{ set_active('areas') }}">
            <a href="#">
                <i class="fa fa-fw fa-cogs"></i>
                <span class="nav-label">Parámetros</span>
                <span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse" style="height:0px">
                <li class="">
                    <a href="{{ route('areas.index') }}">
                        <i class="fa fa-area-chart"></i>
                        <span class="nav-label">Áreas</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{ route('statuses.index') }}">
                        <i class="fa fa-check-circle"></i>
                        <span class="nav-label">Estados</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{ route('typelicense.index') }}">
                        <i class="fa fa-certificate"></i>
                        <span class="nav-label">Tipos de licencias</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="{{ set_active('usuarios')}}">
            <a href="#">
                <i class="fa fa-fw fa-lock"></i>
                <span class="nav-label">Seguridad</span>
                <span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse" style="height:0px">
                <li class="{{ set_active('usuarios')}}">
                    <a href="{{ route('users.index') }}">
                        <i class="fa fa-users"></i>
                        <span class="nav-label">Usuarios</span>
                    </a>
                </li>

                <li class="{{ set_active('/usuarios/logins')}}">
                    <a href="{{route('users.logins')}}">
                        <i class="fa fa-sign-in"></i>
                        <span class="nav-label">Registro de ingresos</span></a>
                </li>
                <li class="{{ set_active('/exceptions')}}">
                    <a href="{{route('exceptions.index')}}">
                        <i class="fa fa-exclamation-circle"></i>
                        <span class="nav-label">Excepciones y errores</span></a>
                </li>
            </ul>
        </li>
    @endif
    @if(auth()->user()->hasAreaUnique('RRHH'))
    <li class="{{ set_active('usuarios')}}">
        <a href="{{ route('users.index') }}">
            <i class="fa fa-users"></i>
            <span class="nav-label">Usuarios</span>
        </a>
    </li>
    @endif
    @prensa
    <li class="">
        <a href="#">
            <i class="fa fa-fw fa-file-o"></i>
            <span class="nav-label">Prensa</span>
            <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" style="height:0px">
            <li class="{{ set_active('proposeds') }}">
                <a href="{{route('proposeds.index')}}">
                    <i class="fa fa-fw fa-calendar-times-o"></i>
                    <span class="nav-label">Propuestas</span>
                </a>
            </li>
            <li class="{{ set_active('events') }}">
                <a href="{{route('events.index')}}">
                    <i class="fa fa-fw fa-calendar-times-o"></i>
                    <span class="nav-label">Coberturas</span>
                </a>
            </li>
            <li class="{{ set_active('parrilla') }}">
                <a href="{{route('parrilla.index')}}">
                    <i class="fa fa-fw fa-calendar-times-o"></i>
                    <span class="nav-label">Parrilla</span>
                </a>
            </li>
        </ul>
    </li>
    @endprensa

    @if((auth()->user()->hasAreaUnique('RRHH') && auth()->user()->isAdmin() ))

        <li class="{{ set_active('holiday') }}">
            <a href="{{ route('holiday.index') }}">
                <i class="fa fa-calendar"></i>
                <span class="nav-label">Vacaciones</span>
            </a>
        </li>
        <li class="{{ set_active('licenses') }}">
            <a href="{{ route('licenses.index') }}">
                <i class="fa fa-calendar-check-o"></i>
                <span class="nav-label">Licencias</span>
            </a>
        </li>
        <li class="{{ set_active('freedays') }}">
            <a href="{{ route('freedays.index') }}">
                <i class="fa fa-calendar-times-o"></i>
                <span class="nav-label">Francos</span>
            </a>
        </li>

    @endif

    @if( auth()->user()->isGod())
        <li>

            <a href="#">
                <i class="fa fa-fw fa-users"></i>
                <span class="nav-label">RRHH</span>
                <span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse" style="height:0px">
                <li class="{{ set_active('holiday') }}">
                    <a href="{{ route('holiday.index') }}">
                        <i class="fa fa-calendar"></i>
                        <span class="nav-label">Vacaciones</span>
                    </a>
                </li>
                <li class="{{ set_active('licenses') }}">
                    <a href="{{ route('licenses.index') }}">
                        <i class="fa fa-calendar-check-o"></i>
                        <span class="nav-label">Licencias</span>
                    </a>
                </li>
                <li class="{{ set_active('freedays') }}">
                    <a href="{{ route('freedays.index') }}">
                        <i class="fa fa-calendar-times-o"></i>
                        <span class="nav-label">Francos</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif

    <li class="{{ set_active('box/*') }}">
        <a href="#">
            <i class="fa fa-fw fa-envelope"></i>
            <span class="nav-label">Mensajería</span>
            <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" style="height:0px">
            <li class="{{ set_active('box/threads') }}">
                <a href="{{ route('threads.index') }}">
                    <i class="fa fa-at"></i>
                    <span class="nav-label">Mensajes <span
                                class="label label-warning">{{auth()->user()->unreadNotifications->where('type',\App\Notifications\MessageNotification::class, \App\Notifications\MsgThreadNotification::class)->count()}}</span></span>
                </a>
            </li>

            <li class="{{ set_active('box/sends')}}">
                <a href="{{route('sends.index')}}">
                    <i class="fa fa-send"></i>
                    <span class="nav-label">Enviados</span></a>
            </li>

        </ul>
    </li>

    <li class="{{ set_active('actions') }}">
        <a href="{{ route('actions.index') }}">
            <i class="fa fa-list"></i>
            <span class="nav-label">Acciones diarias</span>
        </a>
    </li>

    <li class="{{ set_active('notices') }}">
        <a href="{{ route('notices.index') }}">
            <i class="fa fa-newspaper-o"></i>
            <span class="nav-label">Novedades</span>
        </a>
    </li>

    @if(auth()->user()->hasAreaUnique('Sistemas'))
        <li class="{{ set_active('tickets') }}">
            <a href="{{route('tickets.index')}}">
                <i class="fa fa-exclamation-triangle"></i>
                <span class="nav-label">Tickets</span>
            </a>
        </li>
    @else
        <li class="{{ set_active('tickets') }}">
            <a href="{{route('tickets.create')}}">
                <i class="fa fa-exclamation-triangle"></i>
                <span class="nav-label">Reportar error</span>
            </a>
        </li>
    @endif

    <li class="">
        <a href="http://testturnos.misionesonline.net" target="_blank">
            <i class="fa fa-list-alt"></i>
            <span class="nav-label">Turnos</span>
        </a>
    </li>

    <li>
        <a href="http://yt.misionesonline.net" target="_blank">
            <i class="fa fa-play-circle"></i>
            <span class="nav-label">Videos</span>
        </a>

    </li>
</ul>