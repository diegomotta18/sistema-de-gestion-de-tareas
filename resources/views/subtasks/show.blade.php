@extends('layouts.app')

@section('content')
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title" style="background-color:#1ab394;">
                <h5 style="color:white;">
                    @if(!is_null($area)){{$area->name}}@endif
                    @if(!is_null($project))/ {{$project->name}} @endif

                </h5>

            </div>
            <div class="ibox-title">
                <h5 class="hidden-xs">
                    <strong style="text-align: center">Detalle de la subtarea</strong>

                </h5>
                <div class="ibox-tools hidden-xs">
                    <change-statesub auth_finish="{{auth()->user()->can('finishtask',$subtask)}}"
                                     task_id="{{$subtask->id}}"
                                     status="{{$subtask->status()->first()}}"></change-statesub>
                    <div class="btn-group">
                        <a style="color: #676a6c;" href="{{ route('task.home.show',['task'=> $subtask->task_id]) }}"
                           class="btn btn-default"
                           title="Ver tarea">
                            <i class="fa fa-sign-in"
                               aria-hidden="true"></i> Tarea principal
                        </a>
                        @direccion
                        <a class="btn btn-default"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           href="{{route('direccion.index')}}"><i class="fa fa-table"></i> Tablero</a>
                        @enddireccion
                        @inicio
                        <a class="btn btn-default"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           href="{{route('home')}}"><i class="fa fa-table"></i> Inicio</a>
                        @endinicio
                        <a class="btn btn-default"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           onclick="window.history.back();"><i class="fa fa-arrow-circle-left"></i>Volver</a>
                    </div>
                </div>
                <div class="ibox-tools hidden-md hidden-lg hidden-sm">

                    <div class="btn-group">
                        @can('changestatus', $task)
                            @if(!is_null($subtask->status()->first()))
                                <change-statesub task_id="{{$subtask->id}}"
                                                 status="{{$subtask->status()->first()}}"></change-statesub>
                            @endif
                        @endcan
                        {{--<a style="color: #676a6c;" href="{{ route('task.home.show',['task'=> $subtask->task_id]) }}"--}}
                        {{--class="btn btn-default btn-xs"--}}
                        {{--title="Ver tarea">--}}
                        {{--<i class="fa fa-sign-in"--}}
                        {{--aria-hidden="true"></i> Tarea principal--}}
                        {{--</a>--}}
                        @direccion
                        <a class="btn btn-default  btn-xs"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           href="{{route('direccion.index')}}"><i class="fa fa-table"></i> Tablero</a>
                        @enddireccion
                        @inicio
                        <a class="btn btn-default btn-xs"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           href="{{route('home')}}"><i class="fa fa-table"></i> Inicio</a>
                        @endinicio
                        <a class="btn btn-default  btn-xs"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           onclick="window.history.back();"><i class="fa fa-arrow-circle-left"></i>Volver</a>
                    </div>
                </div>
            </div>

            <div class="ibox-title">
                {{$subtask->title}}
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-12">
                        <p>{!! $subtask->description !!}</p>

                    </div>
                </div>
                @if(count($subtask->files()->get()) > 0)
                    <div class="row">
                        <div class="col-md-12">
                            <label>Archivos adjuntos</label>
                            <ul style="list-style-type: none;margin-left: 0px;    -webkit-padding-start: 0px;">
                                @foreach($subtask->files()->get() as $file)
                                    <li>
                                        <a href="{{route('tasks.download',$file->id)}}">
                                            {{$file->title}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
                <comment-list task_id="{{$subtask->id}}" user_auth_id="{{auth()->user()->id}}"></comment-list>

            </div>

        </div>
    </div>
@endsection



@push('scripts')
<script src="{{ asset('plugins/summernote/lang/summernote-es-ES.js') }}"></script>

@routes

<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();
    });
</script>


@endpush