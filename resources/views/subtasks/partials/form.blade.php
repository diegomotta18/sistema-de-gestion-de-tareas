{!! Field::text('title', isset($subtask) ? $subtask->title : '') !!}

<div class="form-group @if($errors->has('description')) has-error @endif">

    <label class="control-label form-group @if($errors->has('description')) text-danger @endif">Descripción</label>
    <textarea id="summernote" class="summernote form-control"
              name="description">{{isset($subtask) ? $subtask->description : old('description')}}</textarea>
    {{--{!! Field::textarea('description', isset($task) ? $task->description : '',array('id' =>'summernote', 'class'=>'summernote form-control' )) !!}--}}
    @if($errors->has('description')) <p class="">{{ $errors->first('description') }}</p> @endif
</div>

<div class="form-group">
    <label >Archivos</label>
    <input id="files" type="file" name="files[]" multiple  />
</div>

{!! Field::text('date_init', isset($subtask) ? $subtask->date_init->format('d/m/Y') : '',array('id'=>'date_init')) !!}

{!! Field::text('date_finish',isset($subtask) ? $subtask->date_finish->format('d/m/Y') : '', array('id'=>'date_finish')) !!}{{--@if(auth()->user()->hasRole('admin'))--}}
    {{--<select-show-user></select-show-user>--}}
    {{--{!! Form::checkbox('hide', 'hide') !!}--}}
    {{--{!! Form::label('label', 'Seleccionar usuarios que pueden visualizar la tarea', ['class' => 'form-check-label'])!!}--}}
{{--@endif--}}


{!! Field::select('assigneds', $assigneds, isset($subtask) ? $subtask->assigneds : '' ,['name'=>'assigneds[]','multiple','class'=>'js-states form-control','empty' => ('Asignar usuarios')]) !!}
{!! Field::select('informeds', $replacements, isset($task) ? $task->informeds : '' ,['name'=>'informeds[]','multiple','class'=>'js-states form-control','empty' => ('Asignar informados')]) !!}


{!! Field::select('replacements', $replacements, isset($subtask) ? $subtask->replacements : '' ,['name'=>'replacements[]','multiple','class'=>'js-states form-control','empty' => ('Asignar replacements')]) !!}


<button type="submit" class="btn btn-primary waves-effect waves-classic">
    <i class="fa fa-save" aria-hidden="true"></i>
    Guardar
</button>

