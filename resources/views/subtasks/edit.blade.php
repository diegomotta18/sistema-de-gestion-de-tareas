@extends('layouts.app')
@push('styles')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}" rel="stylesheet">

@endpush
@section('content')
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <h5>
                <label class="badge badge-primary">{{$subtask->title}}</label>
            </h5>
            <div class="ibox-title">
                <h5>
                    Editar subtarea
                </h5>
                <div class="ibox-tools">
                    <div class="btn-group">
                        <a class="btn btn-default"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           onclick="window.history.back();"><i class="fa fa-arrow-circle-left"></i>Volver</a>
                    </div>
                </div>
            </div>

            <div class="ibox-content">
                <form method="POST"
                      @if( !is_null($project)) action="{{ route('subtasks.update', [$subtask->id,$task->id,$area->name,$project->id]) }}"
                      @else action="{{ route('tasks.update', [$task->id,$area->name,null]) }}" @endif>
                    @csrf
                    @method('PUT')

                    @include('subtasks.partials.form')
                </form>
            </div>
        </div>
    </div>
@endsection



@push('scripts')
<script src="{{ asset('plugins/select2/saaelect2.min.js') }}"></script>
<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('plugins/summernote/lang/summernote-es-ES.js') }}"></script>

<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();

        $('.summernote').summernote({
            height: 300,
            lang: 'es-ES', // default: 'en-US'
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['link']]

            ]
        });
        $('.note-btn').removeAttr('title');
        $('#replacements').select2();
        $('#assigneds').select2();
        $('#informeds').select2();
        $("#files").fileinput({
            dropZoneEnabled: false,
            theme: 'explorer-fa',
            language: "es",
            uploadUrl: "/file-upload-single",
            overwriteInitial: false,
            validateInitialCount: true,
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: false,
            showUpload: false,
            maxFileCount: 5,
            initialPreview: [
                @foreach($subtask->files()->get() as  $file)
                    "<img class='kv-preview-data file-preview-image' src='{{route('tasks.download',$file->id)}}'>",
                @endforeach
            ],
            initialPreviewConfig: [
                @foreach($subtask->files()->get() as  $file)
                {
                    caption: "{{$file->title}}",
                    width: "120px",
                    @if(!is_null($project))
                    url: '{{route('file.destroy',[$area->name,$project->id,$file->id])}}',
                    @else
                    url: '{{route('file.destroy',[$area->name,null,$file->id])}}',

                    @endif
                    key: '{{$file->id}}'
                },
                @endforeach
            ],
            append: true,
            preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
            previewFileIcon: '<i class="fa fa-file"></i>',
            previewFileIconSettings: { // configure your icon file extensions
                'doc': '<i class="fa fa-file-word-o text-primary"></i>',
                'xls': '<i class="fa fa-file-excel-o text-success"></i>',
                'ppt': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                'htm': '<i class="fa fa-file-code-o text-info"></i>',
                'txt': '<i class="fa fa-file-text-o text-info"></i>',
                'mov': '<i class="fa fa-file-movie-o text-warning"></i>',
                'mp3': '<i class="fa fa-file-audio-o text-warning"></i>',

            },

            preferIconicZoomPreview: true,
            allowedFileExtensions: ["jpg", "png", "pdf","jpeg","xlsx"]

        })
    });


    $('input[name="date_init"]').daterangepicker({
        singleDatePicker: true,
        autoUpdateInput: false,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10),
        "locale": {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "fromLabel": "From",
            "toLabel": "To",
            "customRangeLabel": "Custom",
            "weekLabel": "W",
            "daysOfWeek": [
                "Dom",
                "Lun",
                "Mar",
                "Mie",
                "Jue",
                "Vie",
                "Sab"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],

            "firstDay": 1
        },
    });
    $('input[name="date_init"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY'));
    });
    $('input[name="date_finish"]').daterangepicker({
        autoUpdateInput: false,
        singleDatePicker: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10),
        "locale": {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "fromLabel": "From",
            "toLabel": "To",
            "customRangeLabel": "Custom",
            "weekLabel": "W",
            "daysOfWeek": [
                "Dom",
                "Lun",
                "Mar",
                "Mie",
                "Jue",
                "Vie",
                "Sab"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],

            "firstDay": 1
        },
    });

    $('input[name="date_finish"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY'));
    });
</script>
@endpush