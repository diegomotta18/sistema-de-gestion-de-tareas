@extends('layouts.app')
@push('styles')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}" rel="stylesheet">

@endpush
@section('content')
    <div class="col-xs-12">
        <div class="ibox float-e-margins">

            <div class="ibox-title" style="background-color:#1ab394;">

                <h5 style="color:white;">
                    @if(!is_null($area))
                        @if(!is_null($project))
                            {{$area->name}} / {{$project->name}} / {{$task->title}}
                        @else
                            {{$area->name}}  / {{$task->title}}
                        @endif
                    @endif
                </h5>

            </div>
            <div class="ibox-title">
                <h5> Agregar subtarea</h5>


                <div class="ibox-tools">

                    <div class="btn-group">
                        @if(!is_null($area))
                            @if(!is_null($project))
                                <a href="{{route('task.project.show',[$area->name,$project->id, $task->id])}}"
                                   class="btn btn-default" style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;">
                                    <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                                    Volver
                                </a>
                            @else
                                <a href="{{route('areas.show.tasks',$area->name)}}" class="btn btn-default" style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;">
                                    <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                                    Volver
                                </a>
                            @endif
                        @else
                            <a href="{{route('home')}}" class="btn btn-default" style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;">
                                <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                                Volver
                            </a>
                        @endif
                    </div>

                </div>
            </div>

            <div class="ibox-content">
                @if(!is_null($area))

                    <form method="POST"
                          @if( isset($project)) action="{{ route('subtasks.store', [$area->name,$project->id,$task->id]) }}"
                          @else action="{{ route('subtasks.store', [$area->name,null,$task->id]) }}" @endif>
                        @csrf

                        @include('subtasks.partials.form')
                    </form>

                @else

                    <form method="POST" action="{{ route('subtasks.store', [null,null,$task->id]) }}">
                        @csrf

                        @include('subtasks.partials.form')
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection



@push('scripts')
<script src="{{ asset('plugins/select2/saaelect2.min.js') }}"></script>
<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('plugins/summernote/lang/summernote-es-ES.js') }}"></script>


<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();

        $('.summernote').summernote({
            height: 300,
            lang: 'es-ES', // default: 'en-US'
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['link']]

            ]
        });
        $('.note-btn').removeAttr('title');
        $('#replacements').select2();
        $('#assigneds').select2();
        $('#informeds').select2();
        $("#files").fileinput({
            dropZoneEnabled: false,
            theme: 'explorer-fa',
            language: "es",
            uploadUrl: "/file-upload-single",
            validateInitialCount: true,
            initialPreviewAsData: true,
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: false,
            maxFileCount: 5,
            showUpload: false,
            allowedFileExtensions: ["jpg", "png", "pdf", "jpeg", "xlsx"]

        });


    });

    $('input[name="date_init"]').daterangepicker({
        singleDatePicker: true,
        autoUpdateInput: false,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10),
        "locale": {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "fromLabel": "From",
            "toLabel": "To",
            "customRangeLabel": "Custom",
            "weekLabel": "W",
            "daysOfWeek": [
                "Dom",
                "Lun",
                "Mar",
                "Mie",
                "Jue",
                "Vie",
                "Sab"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],

            "firstDay": 1
        },
    });
    $('input[name="date_init"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY'));
    });
    $('input[name="date_finish"]').daterangepicker({
        autoUpdateInput: false,
        singleDatePicker: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10),
        "locale": {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "fromLabel": "From",
            "toLabel": "To",
            "customRangeLabel": "Custom",
            "weekLabel": "W",
            "daysOfWeek": [
                "Dom",
                "Lun",
                "Mar",
                "Mie",
                "Jue",
                "Vie",
                "Sab"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],

            "firstDay": 1
        },
    });

    $('input[name="date_finish"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY'));
    });
</script>
@endpush