{!! Field::text('name', isset($status) ? $status->name : '') !!}


<div class="form-group">
    <label class="control-label">Color</label>
    <input type="color" class="form-control" name="color" value="{{isset($status) ? $status->color : ''}}">
</div>
<button type="submit" class="btn btn-primary waves-effect waves-classic">
    <i class="fa fa-send" aria-hidden="true"></i>
    Aceptar
</button>