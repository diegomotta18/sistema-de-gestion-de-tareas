@extends('layouts.app')

@section('content')
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Crear estado
                </h5>
                <div class="ibox-tools">
                    <div class="btn-group">
                        <a style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           href="{{route('statuses.index')}}"
                           class="btn btn-default hidden-xs pull-right">
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                            Volver
                        </a>
                        <a style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           href="{{route('statuses.index')}}"
                           class="btn btn-xs btn-default hidden-md hidden-sm hidden-lg">
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                            Volver
                        </a>
                    </div>
                </div>

            </div>

            <div class="ibox-content">
                <form method="POST" action="{{ route('statuses.store') }}">
                    @csrf
                    @include('statuses.partials.form')
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();
    });
</script>
@endpush