@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="ibox float-e-margins">
                <div class="alert alert-info" role="alert"><p><i class="fa fa-info-circle"></i>
                        <strong>Importante</strong></p>
                    <p>En esta sección se visualiza todas los estados que se asignan a las tareas y eventos </p>
                </div>

                <div class="ibox-title">
                    <h5 >
                        Estados

                    </h5>
                    <div class="ibox-tools hidden-xs">
                        <div class="btn-group">
                            <a
                               href="{{ route('statuses.create') }}" class="btn btn-primary ">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                Crear estado
                            </a>
                            <a class="btn btn-default pull-right"
                               style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                               @if(auth()->user()->hasArea('Dirección')) href="{{route('direccion.index')}}"  @else href="{{route('home')}}" @endif><i class="fa fa-arrow-circle-left"></i>Volver</a>
                        </div>
                    </div>
                    <div class="ibox-tools  pull-right hidden-sm hidden-md hidden-lg">
                        <div class="btn-group">
                            <a
                               href="{{ route('statuses.create') }}" class="btn btn-xs btn-primary ">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                Crear estado
                            </a>
                            <a class="btn btn-xs btn-default   pull-right"
                               style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                               @if(auth()->user()->hasArea('Dirección')) href="{{route('direccion.index')}}"  @else href="{{route('home')}}" @endif><i class="fa fa-arrow-circle-left"></i>Volver</a>
                        </div>
                    </div>
                </div>

                <div class="ibox-content">
                    <table class="table table-hover table-bordered table-striped table-responsive">
                        <thead>
                        <tr class="text-center">
                            {{--<th data-visible="false">ID</th>--}}
                            <th class="text-center">Estado</th>
                            <th class="text-center">Color</th>

                            <th class="text-center">Acciones</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($statuses as $status)
                            <tr>
                                <td>{{ $status->name }}</td>
                                <td><i class="fa fa-circle" style="color: {{$status->color}}"></i></td>

                                <td>
                                    <div class="btn-group">
                                        <a href="{{route('statuses.edit',$status->id)}}" data-toggle="tooltip"
                                           class="btn btn-warning">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>
                                        <button data-id="{{ $status->id }}" data-toggle="tooltip"
                                                class="btn btn-danger  btn-destroy">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
@routes
<script>

    $(document).ready(function () {


        $('.dropdown-toggle').dropdown();

        $('.btn-destroy').on('click', function () {
            var id = $(this).data('id');
            var url = route('statuses.destroy', {id: id});

            swal({
                title: 'Confirmar',
                text: "Una vez confirmada no es posible revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar y borrar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url, {data: {id: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Eliminado!',
                                    'El estado ha sido eliminado',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });
    });
</script>
@endpush