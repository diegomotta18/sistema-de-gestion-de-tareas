@extends('layouts.app')

@section('content')
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                Vacaciones finalizadas
            </h5>

            <div class="ibox-tools">
                <div class="btn-group">
                    <a href="{{route('holiday.index')}}"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       class="btn btn-default hidden-xs pull-right">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                    <a href="{{route('holiday.index')}}"
                       class="btn btn-xs btn-default hidden-md hidden-lg hidden-sm pull-right"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                </div>
            </div>
        </div>

        <div class="ibox-content">
            <table id="table-holidays" class="table table-hover"
                   data-show-columns="true"
                   data-pagination="true"
                   data-search="true">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Inicio</th>
                    <th>Finalización</th>
                    <th>Reincorporación</th>
                    <th>Observaciones</th>
                    <th>Reemplazos</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($holidays as $holiday)
                    <tr>
                        <td>@if(!is_null($holiday->user)){{ $holiday->user->name }}@endif</td>
                        <td>@if(!is_null($holiday->date_init)){{ $holiday->date_init->format('d/m/Y') }} @endif</td>
                        <td>@if(!is_null($holiday->date_finish)){{ $holiday->date_finish->format('d/m/Y') }}@endif</td>
                        <td>@if(!is_null($holiday->date_reinstate)){{$holiday->date_reinstate->format('d/m/Y') }}@endif</td>
                        <th>@if(!is_null($holiday->observations)) {!! $holiday->observations !!}@else Sin observaciones @endif</th>
                        <td> @foreach($holiday->replacements as $replacement) <label class="badge badge-primary"> {{$replacement->name}} </label>@endforeach
                        </td>
                        <td>
                            <div class="btn-group" style="display: flex;text-align: center">
                                <a href="{{ route('holiday.show', $holiday->id) }}" class="btn btn-info"
                                   title="Ver vacaciones">
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                <a href="{{ route('holiday.edit', $holiday->id) }}" class="btn btn-warning"
                                   title="Editar vacaciones">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                                <button data-id="{{ $holiday->id }}" title="Iniciar vacaciones"
                                        class="btn btn-finish btn-primary">
                                    <i class="fa fa-power-off" aria-hidden="true"></i>
                                </button>
                                <button data-id="{{ $holiday->id }}" title="Eliminar vacaciones"
                                        class="btn btn-destroy btn-danger">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
@routes

<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();

        $('#table-holidays').on('click','.btn-destroy', function () {
            var id = $(this).data('id');
            var url = route('holiday.destroy', {holiday: id});

            swal({
                title: 'Confirmar',
                text: "Una vez confirmada no es posible revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar y borrar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url, {data: {holiday: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Eliminado!',
                                    'Las vacaciones ha sido eliminad',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });

        $('#table-holidays').on('click','.btn-finish', function () {
            var id = $(this).data('id');
            var url = route('holiday.notfinish', {holiday: id});

            swal({
                title: 'Iniciar vacaciones',
                text: "",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Iniciar'
            }).then(function (result) {
                if (result.value) {
                    axios.put(url, {data: {holiday: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Vacaciones iniciadas!',
                                    'Las vacaciones han sido iniciadas',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });
    });
</script>
@endpush