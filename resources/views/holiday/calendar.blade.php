@extends('layouts.app')
@push('styles')
<link rel="stylesheet" href="{{asset('plugins/fullcalendar/fullcalendar.css')}}">

@endpush
@section('content')
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                Calendario de vacaciones
            </h5>
            <div class="ibox-tools">
                <div class="btn-group">
                    <a href="{{route('holiday.index')}}"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       class="btn btn-default hidden-xs pull-right">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                    <a href="{{route('holiday.index')}}"
                       class="btn btn-xs btn-default hidden-md hidden-lg hidden-sm pull-right"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                </div>
            </div>
        </div>

        <div class="ibox-content">
            {!! $calendar->calendar() !!}
        </div>
    </div>
@endsection

@push('scripts')
@routes
<script src="{{asset('plugins/fullcalendar/moment.min.js')}}"></script>
<script src="{{asset('plugins/fullcalendar/fullcalendar.js')}}"></script>
<script src="{{asset('plugins/fullcalendar/lang-all.js')}}"></script>

@if(isset($calendar))
{!! $calendar->script() !!}
@endif
<script>

    $(document).ready(function () {

        $('.dropdown-toggle').dropdown();

    });


</script>
@endpush