<div class="col-md-12">


    {!! Field::select('user', $users, isset($holiday->user) ? $holiday->user->id : '' ,['name'=>'user','class'=>'js-states form-control','empty' => ('Seleccionar usuario')]) !!}

</div>

<div class="col-md-12">

    {!! Field::select('replacements', $replacements, isset($holiday) ? $holiday->replacements : '' ,['name'=>'replacements[]','multiple','class'=>'js-states form-control','empty' => ('Asignar informados')]) !!}
</div>
<div class="col-md-4">

    <div id="field_place" class="form-group">
        <label class="{{$errors->has('date_init')? 'text-danger' : '' }}" style="margin-left: 16px;">Fecha de inicio</label>

        <input type="hidden" id="date_init" name="date_init" value="{{isset($holiday) ? $holiday->date_init->format('d/m/Y'):old('date_init') }}">
        @if($errors->has('date_init')) <p class="help-block"
                                          style="color: #a94442;">{{ $errors->first('date_init') }}</p> @endif
    </div>


</div>
<div class="col-md-4">

    <div id="field_place" class="form-group">
        <label class="{{$errors->has('date_finish')? 'text-danger' : '' }}" style="margin-left: 16px;">Fecha de finalización</label>

        <input type="hidden" id="date_finish" name="date_finish" value="{{isset($holiday) ? $holiday->date_finish->format('d/m/Y'):old('date_finish')}}">
        @if($errors->has('date_finish')) <p class="help-block" style="color: #a94442;">{{ $errors->first('date_finish') }}</p> @endif
    </div>
</div>

<div class="col-md-4">

    <div id="field_place" class="form-group">
        <label class="{{$errors->has('date_reinstate')? 'text-danger' : '' }}" style="margin-left: 16px;">Fecha de reincoporación</label>

        <input type="hidden" id="date_reinstate" name="date_reinstate" value="{{isset($holiday) ? $holiday->date_reinstate->format('d/m/Y'):old('date_reinstate')}}">
        @if($errors->has('date_reinstate')) <p class="help-block" style="color: #a94442;">{{ $errors->first('date_reinstate') }}</p> @endif
    </div>
</div>

<div class="col-md-12">

    <div class="form-group @if($errors->has('observations')) has-error @endif">

        <label class="control-label form-group @if($errors->has('observations')) text-danger @endif">Observaciones</label>
        <textarea id="summernote" class="summernote form-control"
                  name="observations">{{isset($holiday) ? $holiday->observations : old('observations')}}</textarea>
        @if($errors->has('observations')) <p class="">{{ $errors->first('observations') }}</p> @endif
    </div>
</div>
@if(isset($holiday))
    <div class="col-md-12">
        {!! Field::checkbox('finish_holiday', 'value', isset($holiday) ? $holiday->holiday_finish :'' ) !!}
    </div>
@endif

<div class="form-group">
<button type="submit" class="btn btn-primary waves-effect waves-classic" style="margin-left: 15px!important;">
    <i class="fa fa-save" aria-hidden="true"></i>
    Guardar
</button>

</div>
