@extends('layouts.app')
@push('styles')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">

@endpush
@section('content')
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Modificar vacaciones
                </h5>
                <div class="ibox-tools">
                    <div class="btn-group">
                        <a href="{{route('holiday.index')}}"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           class="btn btn-default hidden-xs pull-right">
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                            Volver
                        </a>
                        <a href="{{route('holiday.index')}}"
                           class="btn btn-xs btn-default hidden-md hidden-lg hidden-sm pull-right"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;">
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                            Volver
                        </a>
                    </div>
                </div>
            </div>

            <div class="ibox-content">
                <form method="POST" action="{{ route('holiday.update',$holiday->id) }}">
                    @csrf
                    @method('PUT')

                    @include('holiday.partials.form')
                </form>
            </div>
        </div>
@endsection

@push('scripts')
@routes
<script src="{{ asset('plugins/select2/saaelect2.min.js') }}"></script>
<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('plugins/summernote/lang/summernote-es-ES.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('plugins/moment/es.js') }}"></script>


<script>
    $(document).ready(function () {

        $('.dropdown-toggle').dropdown();
        $('#user').select2();
        $('#replacements').select2();

        $('.summernote').summernote({
            height: 200,
            lang: 'es-ES', // default: 'en-US'
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['link']]

            ]
        });


    });
    $('#date_init').datetimepicker({
        inline: true,
        locale: 'es',
        format: "L",
    });

    $('#date_finish').datetimepicker({
        inline: true,
        locale: 'es',
        format: "L",
    });

    $('#date_reinstate').datetimepicker({
        inline: true,
        locale: 'es',
        format: "L",
    });

</script>
@endpush