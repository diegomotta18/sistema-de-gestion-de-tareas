@extends('layouts.app')
@push('styles')
<link rel="stylesheet" href="{{asset('plugins/fullcalendar/fullcalendar.css')}}">

@endpush
@section('content')

    <div class="ibox float-e-margins">
        <div class="ibox-title" style="background-color:#1ab394;">
            <h5 style="color:white;">
                Detalle de las vacaciones de <b style="font-size: 20px;">{{$holiday->user->name}}</b>
            </h5>
            <div class="ibox-tools">
                <div class="btn-group">
                    <a href="{{route('holiday.index')}}"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       class="btn btn-default hidden-xs pull-right">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                    <a href="{{route('holiday.index')}}"
                       class="btn btn-xs btn-default hidden-md hidden-lg hidden-sm pull-right"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                </div>
            </div>

        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <h4>Fecha de inicio</h4>
                    <p>{{$holiday->date_init->format('d/m/Y')}}</p>
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <h4>Fecha de finalización</h4>
                    <p>{{$holiday->date_init->format('d/m/Y')}}</p>
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <h4>Fecha de reincoporación</h4>
                    <p>{{$holiday->date_reinstate->format('d/m/Y')}}</p>
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <div class="row">

                <div class="col-md-12 col-xs-12">
                    <h4>Observaciones</h4>
                    <div style="word-wrap: break-word !important;">{!! $holiday->observations!!}</div>
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <div class="row">
                <h4 style="text-align: center;">Calendario</h4>
                <div class="col-md-offset-2 col-md-8">
                    {!! $calendar->calendar() !!}

                </div>

            </div>
        </div>
    </div>

@endsection

@push('scripts')
@routes
<script src="{{asset('plugins/fullcalendar/moment.min.js')}}"></script>
<script src="{{asset('plugins/fullcalendar/fullcalendar.js')}}"></script>
<script src="{{asset('plugins/fullcalendar/lang-all.js')}}"></script>

@if(isset($calendar))

    {!! $calendar->script() !!}

@endif

<script>

    $(document).ready(function () {

        $('.dropdown-toggle').dropdown();

    });


</script>
@endpush