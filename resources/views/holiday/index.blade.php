@extends('layouts.app')
@push('styles')
<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">

@endpush
@section('content')
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Vacaciones
                </h5>

                <div class="ibox-tools">
                    <div class="btn-group">


                        <a style="color: white;" href="{{ route('holiday.calendar') }}" class="btn btn-success pull-right">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            Calendario
                        </a>
                        <a style="color: white;" href="{{ route('holiday.finished') }}" class="btn btn-info pull-right">
                            <i class="fa fa-check-circle" aria-hidden="true"></i>
                            Finalizadas
                        </a>
                        <a href="{{ route('holiday.create') }}" class="btn btn-primary pull-right">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                            Crear vacaciones
                        </a>
                    </div>
                </div>
            </div>
            <div class="ibox-title">

                <div class="ibox-tools">
                    <form class="form-inline" method="GET" action="{{route('holiday.index')}}">
                        <div class="input-group">
                            <div class="input-group-btn">
                                <label class="btn btn-default">
                                    <b> Fecha desde</b>
                                </label>
                            </div>
                            <input type="text" id="date_init" class="form-control" name="date_init"
                                   value="{{old('date_init')}}">
                            <div class="input-group-btn">
                                <label class="btn btn-default">
                                    <b>Hasta</b>
                                </label>
                            </div>
                            <input type="text" id="date_finish" class="form-control" name="date_finish"
                                   value="{{old('date_finish')}}">
                            <span class="input-group-btn">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>

                                            </button>
                                        </span>
                        </div>
                        @if(auth()->user()->isGod() || auth()->user()->isAdmin())
                            <div class="input-group mb-3">
                                <div class="input-group-btn">
                                    <label class="btn btn-default">
                                        <b> Usuario</b>
                                    </label>
                                </div>
                                <select name="user" id="" class="form-control">
                                    <option value="" selected="selected">Seleccionar usuario
                                    </option>@foreach($users as $key=> $user  )
                                        <option value="{{$key}}" {{ (old("user") == $key ? "selected":"") }}>{{$user}}</option>@endforeach
                                </select>
                                <span class="input-group-btn">
                            <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>

                            </button>
                          <a href="{{route('holiday.index')}}"
                             style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                             class="btn btn-default " type="submit"><i class="fa fa-refresh"></i></a>
                        </span>
                            </div>
                        @endif
                    </form>

                </div>
            </div>

            <div class="ibox-content">
                <table id="table-holidays" class="table table-hover table-bordered table-striped table-responsive">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Inicio</th>
                        <th>Finalización</th>
                        <th>Reincorporación</th>
                        <th>Observaciones</th>
                        <th>Reemplazos</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($holidays as $holiday)
                        <tr>
                            <td>@if(!is_null($holiday->user)){{ $holiday->user->name }}@endif</td>
                            <td>@if(!is_null($holiday->date_init)){{ $holiday->date_init->format('d/m/Y') }} @endif</td>
                            <td>@if(!is_null($holiday->date_finish)){{ $holiday->date_finish->format('d/m/Y') }}@endif</td>
                            <td>@if(!is_null($holiday->date_reinstate)){{$holiday->date_reinstate->format('d/m/Y') }}@endif</td>
                            <td>@if(!is_null($holiday->observations)){!! $holiday->observations !!}@else Sin observaciones @endif</td>
                            <td> @foreach($holiday->replacements as $replacement) <label class="badge badge-primary"> {{$replacement->name}} </label>@endforeach</td>
                            <td>
                                <div class="btn-group" style="display: flex;text-align: center">
                                    <a href="{{ route('holiday.show', $holiday->id) }}" class="btn btn-info"
                                       title="Ver vacaciones">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                    <a href="{{ route('holiday.edit', $holiday->id) }}" class="btn btn-warning"
                                       title="Editar vacaciones">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </a>
                                    <button data-id="{{ $holiday->id }}" title="Finalizar vacaciones"
                                            class="btn btn-finish btn-primary">
                                        <i class="fa fa-check-circle" aria-hidden="true"></i>
                                    </button>
                                    <button data-id="{{ $holiday->id }}" title="Eliminar vacaciones"
                                            class="btn btn-destroy btn-danger">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $holidays->links() !!}
            </div>
        </div>
@endsection

@push('scripts')
@routes
<script src="{{ asset('plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('plugins/moment/es.js') }}"></script>
<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();

        $('#table-holidays').on('click','.btn-destroy', function () {
            var id = $(this).data('id');
            var url = route('holiday.destroy', {holiday: id});

            swal({
                title: 'Confirmar',
                text: "Una vez confirmada no es posible revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar y borrar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url, {data: {holiday: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Eliminado!',
                                    'Las vacaciones ha sido eliminada',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });

        $('#table-holidays').on('click','.btn-finish', function () {
            var id = $(this).data('id');
            var url = route('holiday.finish', {holiday: id});

            swal({
                title: 'Finalizar vacaciones',
                text: "",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Finalizar'
            }).then(function (result) {
                if (result.value) {
                    axios.put(url, {data: {holiday: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Finalizado!',
                                    'Las vacaciones han sido finalizadas',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });
        $('#date_init').datetimepicker({
            format: "L",
            locale: 'es',

        });

        $('#date_finish').datetimepicker({
            format: "L",
            locale: 'es',
        });
    });
</script>
@endpush