@extends('layouts.app')
@push('styles')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">

@endpush
@section('content')
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Agregar url
                </h5>

                <div class="ibox-tools">
                    <div class="btn-group">
                        <a class="btn btn-default hidden-xs"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           href="{{ URL::previous() }}"><i class="fa fa-arrow-circle-left"></i>Volver</a>

                    </div>
                </div>
                <div class="ibox-tools pull-right hidden-sm hidden-md hidden-lg">
                    <div class="btn-group">
                        <a class="btn btn-xs btn-default"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           href="{{ URL::previous() }}"><i class="fa fa-arrow-circle-left"></i>Volver</a>

                    </div>
                </div>
            </div>

            <div class="ibox-content">
                <form method="POST" action="{{ route('task.addurl',$task->id) }}">
                    @csrf
                    @method('PUT')

                    {!! Field::text('urlnotice', isset($task) ? $task->urlnotice : '') !!}

                    <button type="submit" class="btn btn-primary waves-effect waves-classic btn-save"
                            style="">
                        <i class="fa fa-save" aria-hidden="true"></i>
                        Guardar
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();


    });
</script>

@endpush

