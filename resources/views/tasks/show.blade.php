@extends('layouts.app')

@section('content')

    <div class="ibox float-e-margins">
        @if(!is_null($area))
            <div class="ibox-title" style="background-color:#1ab394;">
                <h5 style="color:white;">
                    <a style="color:white;"
                       href="{{route('areas.show',$area->name)}}">{{$area->name}}</a> @if(!is_null($project))/ <a
                            style="color:white;"
                            href="{{route('project.agileboard',$project->id)}}">{{$project->name}}</a>  @endif
                    / {{$task->title}}
                </h5>
            </div>
        @endif
        <div class="ibox-title">

            <div class="ibox-tools hidden-xs">
                <div class="btn-group">
                    @if(!is_null($task->status))
                        <change-state auth_finish="{{auth()->user()->can('finishtask',$task)}}"
                                      task_id="{{$task->id}}"
                                      status="{{$task->status}}"></change-state>
                    @endif
                    @can('update', $task)
                        @if(!is_null($task->project) && !is_null($task->area))

                            <a href="{{ route('task.project.edit',['area' => $task->area->name, 'project'=>$task->project_id,'task'=> $task->id]) }}"
                               class="btn btn-default"
                               style="background-color: #f8ac59;border-color: #f8ac59;color:white;"
                               title="Editar tarea">
                                <i class="fa fa-pencil-square-o"
                                   aria-hidden="true"></i> Editar
                            </a>
                        @elseif(!is_null($task->area))
                            <a href="{{ route('task.area.edit',['area' => $task->area->name,'task'=> $task->id]) }}"
                               style="background-color: #f8ac59;border-color: #f8ac59;color:white;"
                               class="btn  btn-default"
                               title="Editar tarea">
                                <i class="fa fa-pencil-square-o"
                                   aria-hidden="true"></i> Editar
                            </a>
                        @endif
                    @endcan
                    @if(!is_null($area))
                        @if(!is_null($project))
                            <a style="color:white;"
                               href="{{ route('subtasks.create',[$area->name,$project->id,$task->id]) }}"
                               class="btn btn-info hidden-xs">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                Subtarea
                            </a>
                        @endif
                    @endif
                    <a class="btn btn-default"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       href="{{URL::previous()}} "><i class="fa fa-arrow-circle-left"></i>Volver</a>
                </div>
            </div>
            <div class="ibox-tools hidden-sm hidden-lg hidden-md">
                <div class="btn-group">
                    @if(!is_null($task->status))
                        <change-state auth_finish="{{auth()->user()->can('finishtask',$task)}}"
                                      task_id="{{$task->id}}"
                                      status="{{$task->status}}"></change-state>
                    @endif
                    @can('update', $task)
                        @if(!is_null($task->project) && !is_null($task->area()->first()))

                            <a href="{{ route('task.project.edit',['area' => $task->area->name, 'project'=>$task->project_id,'task'=> $task->id]) }}"
                               class="btn btn-xs btn-default"
                               style="background-color: #f8ac59;border-color: #f8ac59;color:white;"
                               title="Editar tarea">
                                <i class="fa fa-pencil-square-o"
                                   aria-hidden="true"></i> Editar
                            </a>
                        @elseif(!is_null($task->area))
                            <a href="{{ route('task.area.edit',['area' => $task->area->name,'task'=> $task->id]) }}"
                               style="background-color: #f8ac59;border-color: #f8ac59;color:white;"
                               class="btn btn-xs btn-default"
                               title="Editar tarea">
                                <i class="fa fa-pencil-square-o"
                                   aria-hidden="true"></i> Editar
                            </a>
                        @endif
                    @endcan
                    @if(!is_null($area))
                        @if(!is_null($project))
                            <a style="color:white;"
                               href="{{ route('subtasks.create',[$area->name,$project->id,$task->id]) }}"
                               class="btn btn-xs btn-info hidden-xs">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                Subtarea
                            </a>
                        @endif
                    @endif
                    <a class="btn btn-xs btn-default"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       href="{{URL::previous()}} "><i class="fa fa-arrow-circle-left"></i>Volver</a>
                </div>
            </div>
        </div>

        <div class="ibox-title">

            <h3 class="pull-right"> @if(!is_null($task->date_init)) {{$task->date_init->format('d/m/Y') }} @else Sin
                fechainicio @endif
                - @if(!is_null($task->date_finish)) {{$task->date_finish->format('d/m/Y') }} @else Sin fecha de
                finalización @endif  @if(!is_null($task->hora))| {{$task->hora }} hs @endif</h3>
            <br>

            <h2><b>#{{$task->id}} -  {{$task->title}}</b></h2>

        </div>

        <div class="ibox-content">
            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <h5>
                        Usuario creador
                    </h5>
                    @if(!is_null($task->user))
                        <label class="badge">{{$task->user->name}}</label>
                    @endif
                </div>
                <div class="col-md-4 col-xs-12">
                    <h5>
                        Usuario informado

                    </h5>
                    @if(!is_null($task->informeds))
                        @foreach($task->informeds as $informed)
                            <label class="badge badge-info">{{$informed->name}}</label>
                        @endforeach
                    @endif
                </div>
                <div class="col-md-4 col-xs-12">
                    <h5>
                        Usuario asignado

                    </h5>
                    @if(!is_null($task->assigneds))
                        @foreach($task->assigneds as $assigned)
                            <label class="badge badge-primary">{{$assigned->name}}</label>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <label>Descripción</label>
                    <div style="word-wrap: break-word !important;">{!! $task->description !!}</div>

                </div>
                @if(!is_null($task->files()->get()) && ($task->files()->count() > 0))
                    <div class="col-md-12">
                        <label>Archivos adjuntos</label>
                        <hr>
                        @foreach($task->files()->get()->chunk(4) as $files)
                            <div class="row">
                                @foreach($files as $f)
                                    <div class="col-md-3 text-center">
                                        <div class="well">
                                            <a href="{{route('tasks.download',$f->id)}}">
                                                @if($f->extension == 'image/png' ||$f->extension == 'image/jpeg' )
                                                    <div class="row">
                                                        <img style="width: 90px;height: 90px;"
                                                             src="{{Storage::url($f->filename)}}">
                                                    </div>

                                                    <span style="word-wrap: break-word !important;">{{$f->title}}</span>
                                                @else

                                                    <div>
                                                        <div class="row">
                                                            <i class="fa fa-file fa-5x"
                                                               style="color:black;width: 70px;height: 72px;"></i>

                                                        </div>

                                                        <span class="text-center"
                                                              style="word-wrap: break-word !important;">{{$f->title}}</span>

                                                    </div>
                                                @endif
                                            </a>
                                        </div>

                                    </div>
                                @endforeach
                            </div>

                        @endforeach
                    </div>
                @endif
            </div>

        </div>
        @if(count($task->subtasks()->get()) > 0)
            <div class="ibox-title">
                <h5>
                    Subtareas
                </h5>
            </div>
            <div class="ibox-content">

                <div class="row">
                    <div class="col-md-12">

                        <table class="table table-hover table-responsive"
                               data-toggle="table"
                               data-show-columns="true"
                               data-pagination="true"
                               data-search="true">
                            <thead>
                            <tr>
                                <th data-visible="false">ID</th>
                                <th>Nombre</th>
                                <th>Rango de fechas</th>
                                {{--<th>Descripcion</th>--}}
                                <th>Asignado</th>
                                <th>Informados</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($task->subtasks()->get() as $subtask)
                                <tr>
                                    <td>{{ $subtask->id }}</td>
                                    <td>{{ $subtask->title }}</td>
                                    <td>{{ $subtask->date_init->format('d/m/Y')}}
                                        - {{$subtask->date_finish->format('d/m/Y')}}</td>
                                    {{--<td>{!! $subtask->description !!}</td>--}}
                                    <td>
                                        <ul style="list-style-type: none;margin-left: 0px;text-align: center;    -webkit-padding-start: 0px;">
                                            @if(!is_null($subtask->assigneds))
                                                @foreach($subtask->assigneds as $assigned)
                                                    <li>
                                                        <label class="badge badge-primary">{{$assigned->name}}</label>
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </td>
                                    <td>
                                        <ul style="list-style-type: none;margin-left: 0px;text-align: center;    -webkit-padding-start: 0px;">
                                            @if(!is_null($subtask->informeds))
                                                @foreach($subtask->informeds as $informeds)
                                                    <li>
                                                        <label class="badge badge-info">{{$informeds->name}}</label>
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </td>
                                    <td>
                                        <div style="list-style-type: none;margin-left: 0px;text-align: center;    -webkit-padding-start: 0px;">

                                            @if(!is_null($task->status))

                                                <label class="badge"
                                                       style="background-color: {{$subtask->status->color}}; color: white;">{{$subtask->status->name}}</label>
                                            @endif
                                        </div>
                                    </td>

                                    <td>
                                        <div class="btn-group" style="display: flex;">
                                            <a href="{{ route('subtasks.show',['area'=>$area->name,'project'=>$project->id, 'task'=> $task->id,'subtask' =>$subtask->id]) }}"
                                               class="btn btn-info"
                                               title="ver tarea">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>
                                            <a href="{{ route('subtasks.editwithareaproject',['area'=>$area->name,'project'=>$project->id, 'task'=> $task->id, 'subtask' =>$subtask->id]) }}"
                                               class="btn btn-warning"
                                               title="Editar tarea">
                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                            </a>

                                            <button data-id="{{ $subtask->id }}" title="Eliminar tarea"
                                                    class="btn btn-destroy btn-danger">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </button>
                                            <div id="dropdown" class="btn-group show-on-hover">
                                                <button type="button" class="btn btn-default dropdown-toggle"
                                                        data-toggle="dropdown" aria-expanded="true">
                                                    <i class="fa fa-list" aria-hidden="true"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right"
                                                    aria-labelledby="dropdownMenu1">

                                                    <li>
                                                        <a href="#" data-id="{{$subtask->id}}"
                                                           class="init-task">
                                                            <i class="fa fa-info" aria-hidden="true"></i>
                                                            Informado
                                                        </a>
                                                    </li>
                                                    <li>

                                                        <a href="#" data-id="{{$subtask->id}}"
                                                           class="start-task">
                                                            <i class="fa fa-tasks" aria-hidden="true"></i>
                                                            En proceso
                                                        </a>
                                                    </li>
                                                    @can('denegatetask', $subtask)

                                                        <li>
                                                            <a href="#" data-id="{{$subtask->id}}"
                                                               class="denegado">
                                                                <i class="fa fa-close" aria-hidden="true"></i>
                                                                Denegado
                                                            </a>
                                                        </li>
                                                    @endcan
                                                    @can('finishtask', $subtask)

                                                        <li>
                                                            <a href="#" class="finish-task"
                                                               data-id="{{$subtask->id}}">
                                                                <i class="fa fa-flag-checkered"
                                                                   aria-hidden="true"></i>
                                                                Finalizado
                                                            </a>
                                                        </li>
                                                    @endcan
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>
                <comment-list task_id="{{$task->id}}" user_auth_id="{{auth()->user()->id}}"></comment-list>
            </div>
        @else
            <div class="ibox-content">

                <comment-list task_id="{{$task->id}}" user_auth_id="{{auth()->user()->id}}"></comment-list>
            </div>
        @endif
    </div>
@endsection

@push('scripts')
@routes

<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();

        $('.btn-destroy').on('click', function () {
            var id = $(this).data('id');
            var url = route('subtasks.destroy', {id: id});

            swal({
                title: 'Confirmar',
                text: "Una vez confirmada no es posible revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar y borrar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url, {data: {id: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Eliminado!',
                                    'La tarea ha sido eliminada',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        }).catch(function (res) {
                        swal("No tiene los privilegios para realizar esta acción!", '', 'error');
                        window.location.reload();
                    });
                }
            });
        });


        $('.finish-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('subtask.finishtask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({
                            title: "Subtarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 1500
                        });
                        window.location.reload();
                    } else if (res.data.status == 405) {
                        window.location.reload();
                    }
                    else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                })
                .catch(function (res) {
                    swal("No tiene los privilegios para realizar esta acción!", '', 'error');
                    window.location.reload();

                });
        });
        $('.init-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('subtask.inittask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({
                            title: "Subtarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                })

        });
        $('.start-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('subtask.processtask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({
                            title: "Subtarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 1500
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });


        $('.denegate').on('click', function () {
            var id = $(this).data('id');
            var url = route('subtask.denegatetask', {id: id});
            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({
                            title: "Tarea denegada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });

    });
</script>


@endpush