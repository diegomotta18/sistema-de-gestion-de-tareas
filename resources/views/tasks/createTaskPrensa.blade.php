@extends('layouts.app')

@push('styles')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">

@endpush
@section('content')

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                Agregar tarea
            </h5>
            <div class="ibox-tools">
                <div class="btn-group">

                    <a class="btn btn-default hidden-xs pull-right"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       href="{{ URL::previous() }}"><i class="fa fa-arrow-circle-left"></i>Volver</a>
                    <a class="btn btn-xs btn-default hidden-md hidden-sm hidden-lg pull-right"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       href="{{ URL::previous() }}"><i class="fa fa-arrow-circle-left"></i>Volver</a>
                </div>

            </div>
        </div>

        <div class="ibox-content">

            <form method="POST" enctype="multipart/form-data" action="{{ route('tasks.mainstore',$area) }}">
                @csrf

                <area-project  area_id="{{auth()->user()->area->id}}"  ></area-project>
                <div class="col-md-12">

                    @if($errors->has('area')) <p class="help-block"
                                                 style="color: #a94442;">{{ $errors->first('area') }}</p> @endif
                </div>

                <div class="col-md-12">
                    {!! Field::text('title', isset($task) ? $task->title : '') !!}
                </div>
                <div class="col-md-12">

                    <div class="form-group @if($errors->has('description')) has-error @endif">

                        <label class="control-label form-group @if($errors->has('description')) text-danger @endif">Descripción</label>
                        <textarea id="summernote" class="summernote form-control"
                                  name="description">{{isset($task) ? $task->description : old('description')}}</textarea>
                        @if($errors->has('description')) <p class="">{{ $errors->first('description') }}</p> @endif
                    </div>
                </div>
                <div class="col-md-12">

                    <div class="form-group">
                        <label>Archivos</label>
                        <input id="files" type="file" name="files[]" multiple/>
                    </div>
                </div>

                <div class="row">
                    <div class='col-md-4'>
                        <div class="form-group">
                            <label class="{{$errors->has('hora')? 'text-danger' : '' }}"
                                   style="margin-left: 16px;">Hora</label>

                            <input type="hidden" id="hora" name="hora"
                                   value="{{isset($task) ? $task->hora:old('hora') }}">
                            @if($errors->has('hora')) <p class="help-block"
                                                         style="color: #a94442;">{{ $errors->first('hora') }}</p> @endif
                        </div>
                    </div>
                    <div class="col-md-4">

                        <div id="field_place" class="form-group">
                            <label class="{{$errors->has('date_init')? 'text-danger' : '' }}"
                                   style="margin-left: 16px;">Fecha de inicio</label>

                            <input type="hidden" id="date_init" name="date_init"
                                   value="{{isset($task) ? $task->date_init->format('d/m/Y'):old('date_init') }}">
                            @if($errors->has('date_init')) <p class="help-block"
                                                              style="color: #a94442;">{{ $errors->first('date_init') }}</p> @endif
                        </div>


                    </div>
                    <div class="col-md-4">

                        <div id="field_place" class="form-group">
                            <label class="{{$errors->has('date_finish')? 'text-danger' : '' }}"
                                   style="margin-left: 16px;">Fecha de finalización</label>

                            <input type="hidden" id="date_finish" name="date_finish"
                                   value="{{isset($task) ? $task->date_finish->format('d/m/Y'):old('date_finish')}}">
                            @if($errors->has('date_finish')) <p class="help-block"
                                                                style="color: #a94442;">{{ $errors->first('date_finish') }}</p> @endif
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    {!! Field::select('informeds', $replacements, isset($task) ? $task->informeds : '' ,['name'=>'informeds[]','multiple','class'=>'js-states form-control','empty' => ('Asignar informados')]) !!}
                </div>

                <div class="form-group">

                    <button type="submit" style="margin-left: 15px !important;"
                            class="btn btn-primary waves-effect waves-classic btn-save">
                        <i class="fa fa-save" aria-hidden="true"></i>
                        Guardar
                    </button>
                </div>
            </form>
        </div>
    </div>

@endsection

@push('scripts')
@routes
<script src="{{ asset('plugins/select2/saaelect2.min.js') }}"></script>
<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('plugins/summernote/lang/summernote-es-ES.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('plugins/moment/es.js') }}"></script>


<script>
    $(document).ready(function () {


        $('.summernote').summernote({
            height: 200,
            lang: 'es-ES', // default: 'en-US'
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['link']]

            ]
        });
        $('.note-btn').removeAttr('title');
        $('#area').select2();
        $('#project').select2();
        $('#replacements').select2();
        $('#informeds').select2();

        $('#assigneds').select2();
        $('#users_show').select2();
        $('.dropdown-toggle').dropdown();
        $("#files").fileinput({
            dropZoneEnabled: false,
            theme: 'explorer-fa',
            language: "es",
            uploadUrl: "/file-upload-single",
            validateInitialCount: true,
            initialPreviewAsData: true,
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: false,
            maxFileCount: 5,
            showUpload: false,
            allowedFileExtensions: ["jpg", "png", "pdf", "jpeg", "xlsx","gif"]

        });

    });

    $('#date_init').datetimepicker({
        inline: true,
        locale: 'es',
        format: "L",
    });
    $('#date_finish').datetimepicker({
        inline: true,
        locale: 'es',
        format: "L",
    });

    $('#hora').datetimepicker({
        inline: true,
        format: 'LT',
        useCurrent: false,

    });

</script>
@endpush