@extends('layouts.app')

@push('styles')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">

@endpush
@section('content')
    <div class="ibox float-e-margins">
        <div class="ibox-title" style="background-color:#1ab394;padding: 10px;">

            <h5 style="color:white;">
                @if(!is_null($area) && !is_null($project))
                    {{$area->name}} / {{$project->name}}
                @else
                    {{$area->name}}
                @endif
            </h5>

        </div>
        <div class="ibox-title">
            <h5>
                Editar tarea
            </h5>
            <div class="ibox-tools">
                <div class="btn-group">
                    <a class="btn btn-default hidden-xs"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;" href="{{ URL::previous() }}"><i class="fa fa-arrow-circle-left"></i>Volver</a>
                    <a class="btn btn-xs btn-default hidden-md hidden-sm hidden-lg"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       href="{{ URL::previous() }}"><i class="fa fa-arrow-circle-left"></i>Volver</a>
                </div>
            </div>
        </div>

        <div class="ibox-content">
            <form method="POST" enctype="multipart/form-data"
                  @if( !is_null($project)) action="{{ route('tasks.update', [$task->id,$area->name,$project->id]) }}"
                  @elseif(!is_null($area)) action="{{ route('tasks.update', [$task->id,$area->name,null])}}"
                  @else  action="{{ route('tasks.update', [$task->id,null,null])}}" @endif
            >
                @csrf
                @method('PUT')

                @if(!is_null($area) && !is_null($project))
                    <area-project area_id="{{$area->id}}" project_id="{{$project->id}}"></area-project>

                @elseif(!is_null($area))

                    <area-project area_id="{{$area->id}}"></area-project>
                @endif
                <div class="col-md-12">
                    @if($errors->has('area')) <p class="help-block"
                                                 style="color: #a94442;">{{ $errors->first('area') }}</p> @endif

                </div>
                @if(auth()->user()->hasRole('user') && auth()->user()->hasArea('Prensa') )
                    @include('tasks.partials.formprensa')
                @else
                    @include('tasks.partials.form')
                @endif
            </form>
        </div>
    </div>
@endsection



@push('scripts')
@routes

<script src="{{ asset('plugins/select2/saaelect2.min.js') }}"></script>
<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('plugins/summernote/lang/summernote-es-ES.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('plugins/moment/es.js') }}"></script>

<script>

    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.summernote').summernote({
            height: 200,
            lang: 'es-ES', // default: 'en-US'
            language: "es",
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['link']]

            ]
        });
        $('.note-btn').removeAttr('title');
        $('#replacements').select2();
        $('#assigneds').select2();
        $('#informeds').select2();

        $('.dropdown-toggle').dropdown();
        $("#files").fileinput({
            dropZoneEnabled: false,
            theme: 'explorer-fa',
            language: "es",
            uploadUrl: "/file-upload-single",
            overwriteInitial: false,
            validateInitialCount: true,
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: false,
            showUpload: false,
            maxFileCount: 5,
            initialPreview: [
                @foreach($task->files()->get() as  $file)
                    "<img class='kv-preview-data file-preview-image' src='{{route('tasks.download',$file->id)}}'>",
                @endforeach
            ],
            initialPreviewConfig: [
                    @foreach($task->files()->get() as  $file)
                {
                    caption: "{{$file->title}}",
                    width: "120px",
                    @if(!is_null($project))
                    url: '{{route('file.destroy',[$area->name,$project->id,$file->id])}}',
                    @else
                    url: '{{route('file.destroy',[$area->name,null,$file->id])}}',

                    @endif
                    key: '{{$file->id}}'
                },
                @endforeach
            ],
            append: true,
            preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
            previewFileIcon: '<i class="fa fa-file"></i>',
            previewFileIconSettings: { // configure your icon file extensions
                'doc': '<i class="fa fa-file-word-o text-primary"></i>',
                'xls': '<i class="fa fa-file-excel-o text-success"></i>',
                'ppt': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                'htm': '<i class="fa fa-file-code-o text-info"></i>',
                'txt': '<i class="fa fa-file-text-o text-info"></i>',
                'mov': '<i class="fa fa-file-movie-o text-warning"></i>',
                'mp3': '<i class="fa fa-file-audio-o text-warning"></i>',

            },

            preferIconicZoomPreview: true,
            allowedFileExtensions: ["jpg", "png", "pdf", "jpeg", "xlsx","gif","docx"]

        })
    });

    $('#date_init').datetimepicker({
        inline: true,
        locale: 'es',
        format: "L",
    });
    $('#date_finish').datetimepicker({
        inline: true,
        locale: 'es',
        format: "L",
    });

    $('#hora').datetimepicker({
        inline: true,
        format: 'LT',
        useCurrent: false,

    });
</script>
@endpush