<div class="col-md-12">

    {!! Field::text('title', isset($task) ? $task->title : '') !!}
</div>
<div class="col-md-12">

    <div class="form-group @if($errors->has('description')) has-error @endif">

        <label class="control-label form-group @if($errors->has('description')) text-danger @endif">Descripción</label>
        <textarea id="summernote" class="summernote form-control"
                  name="description">{{isset($task) ? $task->description : old('description')}}</textarea>
        {{--{!! Field::textarea('description', isset($task) ? $task->description : '',array('id' =>'summernote', 'class'=>'summernote form-control' )) !!}--}}
        @if($errors->has('description')) <p class="">{{ $errors->first('description') }}</p> @endif
    </div>
</div>
<div class="col-md-12">

    <div class="form-group">
        <label>Archivos</label>
        <input id="files" type="file" name="files[]" multiple/>
    </div>
</div>
@if(!auth()->user()->hasArea('Prensa'))
    <div class="row">
        <div class="col-md-6">

            <div id="field_place" class="form-group">
                <label class="{{$errors->has('date_init')? 'text-danger' : '' }}" style="margin-left: 16px;">Fecha de
                    inicio</label>

                <input type="hidden" id="date_init" name="date_init"
                       value="{{isset($task) ? $task->date_init->format('d/m/Y'):old('date_init') }}">
                @if($errors->has('date_init')) <p class="help-block"
                                                  style="color: #a94442;">{{ $errors->first('date_init') }}</p> @endif
            </div>


        </div>
        <div class="col-md-6">

            <div id="field_place" class="form-group">
                <label class="{{$errors->has('date_finish')? 'text-danger' : '' }}" style="margin-left: 16px;">Fecha de
                    finalización</label>

                <input type="hidden" id="date_finish" name="date_finish"
                       value="{{isset($task) ? $task->date_finish->format('d/m/Y'):old('date_finish')}}">
                @if($errors->has('date_finish')) <p class="help-block"
                                                    style="color: #a94442;">{{ $errors->first('date_finish') }}</p> @endif
            </div>
        </div>
    </div>
@else
    <div class="row">
        <div class='col-md-4'>
            <div class="form-group">
                <label class="{{$errors->has('hora')? 'text-danger' : '' }}"
                       style="margin-left: 16px;">Hora</label>

                <input type="hidden" id="hora" name="hora"
                       value="{{isset($task) ? $task->hora:old('hora') }}">
                @if($errors->has('hora')) <p class="help-block"
                                             style="color: #a94442;">{{ $errors->first('hora') }}</p> @endif
            </div>
        </div>
        <div class="col-md-4">

            <div id="field_place" class="form-group">
                <label class="{{$errors->has('date_init')? 'text-danger' : '' }}" style="margin-left: 16px;">Fecha de
                    inicio</label>

                <input type="hidden" id="date_init" name="date_init"
                       value="{{isset($task) ? $task->date_init->format('d/m/Y'):old('date_init') }}">
                @if($errors->has('date_init')) <p class="help-block"
                                                  style="color: #a94442;">{{ $errors->first('date_init') }}</p> @endif
            </div>


        </div>
        <div class="col-md-4">

            <div id="field_place" class="form-group">
                <label class="{{$errors->has('date_finish')? 'text-danger' : '' }}" style="margin-left: 16px;">Fecha de
                    finalización</label>

                <input type="hidden" id="date_finish" name="date_finish"
                       value="{{isset($task) ? $task->date_finish->format('d/m/Y'):old('date_finish')}}">
                @if($errors->has('date_finish')) <p class="help-block"
                                                    style="color: #a94442;">{{ $errors->first('date_finish') }}</p> @endif
            </div>
        </div>
    </div>
@endif

<div class="col-md-12">

    {!! Field::select('assigneds', $assigneds, isset($task) ? $task->assigneds : '' ,['name'=>'assigneds[]','multiple','class'=>'js-states form-control','empty' => ('Asignar usuarios')]) !!}
</div>


<div class="col-md-12">

    {!! Field::select('informeds', $replacements, isset($task) ? $task->informeds : '' ,['name'=>'informeds[]','multiple','class'=>'js-states form-control','empty' => ('Asignar informados')]) !!}
</div>
<div class="col-md-12">

    {!! Field::select('replacements', $replacements, isset($task) ? $task->replacements : '' ,['name'=>'replacements[]','multiple','class'=>'js-states form-control','empty' => ('Asignar reemplazos')]) !!}

</div>

<div class="form-group">

    <button type="submit" class="btn btn-primary waves-effect waves-classic btn-save"
            style="margin-left: 15px !important;">
        <i class="fa fa-save" aria-hidden="true"></i>
        Guardar
    </button>
</div>