@extends('layouts.app')

@push('styles')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">

@endpush
@section('content')

    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Agregar tarea
                </h5>
                <div class="ibox-tools">
                    <div class="btn-group">
                        <a class="btn btn-default hidden-xs"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           href="{{ URL::previous() }}"><i class="fa fa-arrow-circle-left"></i>Volver</a>
                        <a class="btn btn-xs btn-default hidden-md hidden-sm hidden-lg"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           href="{{ URL::previous() }}"><i class="fa fa-arrow-circle-left"></i>Volver</a>
                    </div>
                </div>
            </div>

            <div class="ibox-content">
                @if(!is_null($area))

                    <form method="POST" id="form-task-create" enctype="multipart/form-data"
                          @if( isset($project)) action="{{ route('tasks.store', [$area->name,$project->id]) }}"
                          @else action="{{ route('tasks.store', [$area->name,null]) }}" @endif>
                        @csrf
                        @if(auth()->user()->isUser('user') && auth()->user()->hasArea('Prensa'))
                        @include('tasks.partials.formprensa')

                        @else
                            @include('tasks.partials.form')
                        @endif
                    </form>

                @else
                    <form method="POST" enctype="multipart/form-data" action="{{ route('tasks.store', [null,null]) }}">
                        @csrf
                        {{var_dump(auth()->user()->isUser('user') && auth()->user()->hasArea('Prensa'))}}
                        @if(auth()->user()->isUser('user') && auth()->user()->hasArea('Prensa')){
                        @include('tasks.partials.formprensa')

                        @else
                            @include('tasks.partials.form')
                        @endif
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection

@push('scripts')
@routes
<script src="{{ asset('plugins/select2/saaelect2.min.js') }}"></script>
<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('plugins/summernote/lang/summernote-es-ES.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('plugins/moment/es.js') }}"></script>


<script>
    $(document).ready(function () {


        $('.summernote').summernote({
            height: 200,
            lang: 'es-ES', // default: 'en-US'
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['link']]

            ]
        });
        $('.note-btn').removeAttr('title');
        $('#replacements').select2();
        $('#informeds').select2();

        $('#assigneds').select2();
        $('#users_show').select2();
        $('.dropdown-toggle').dropdown();
        $("#files").fileinput({
            dropZoneEnabled: false,
            theme: 'explorer-fa',
            language: "es",
            uploadUrl: "/file-upload-single",
            validateInitialCount: true,
            initialPreviewAsData: true,
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: false,
            maxFileCount: 5,
            showUpload: false,
            allowedFileExtensions: ["jpg", "png", "pdf", "jpeg", "xlsx","gif","docx"]
        });

    });

    $('#date_init').datetimepicker({
        inline: true,
        locale: 'es',
        format: "L",
    });
    $('#date_finish').datetimepicker({
        inline: true,
        locale: 'es',
        format: "L",
    });


</script>
@endpush