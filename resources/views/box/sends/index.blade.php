@extends('layouts.app')
@section('content')
    <div class="alert alert-info" role="alert"><p><i class="fa fa-info-circle"></i>
            <strong>Importante</strong></p>
        <p>En esta sección se puede visualizar todos los mensajes enviados por el usuario autenticado. Si se selecciona
            un mensaje del listado, redirecciona al hilo de conversación al que pertenece el mensaje. Desde el botón
            <strong>Redactar mensaje</strong> se puede crear un nuevo mensaje.</p>
    </div>
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Enviados
                </h5>
                <div class="ibox-tools hidden-xs">
                    <div class="btn-group">
                        <a href="{{ route('threads.create') }}"
                           class="btn btn-primary">
                            <i class="fa fa-send" aria-hidden="true"></i>
                            Redactar mensaje
                        </a>
                        <a href="{{route('threads.index')}}"
                           class="btn btn-default"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;">
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                            Volver
                        </a>
                    </div>
                </div>
                <div class="ibox-tools hidden-sm hidden-lg hidden-md">
                    <div class="btn-group">
                        <a href="{{ route('threads.create') }}"
                           class="btn btn-xs btn-primary">
                            <i class="fa fa-send" aria-hidden="true"></i>
                            Redactar mensaje
                        </a>
                        <a href="{{route('threads.index')}}"
                           class="btn btn-xs btn-default"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;">
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                            Volver
                        </a>
                    </div>
                </div>
            </div>
            <div class="ibox-content">
                <table class="table table-no-bordered table-condensed table-responsive  "
                       data-toggle="table"
                       data-show-columns="true"
                       data-pagination="true"
                       data-search="true">
                    <thead>
                    <tr>
                        <th>Asunto</th>
                        <th>Descripción</th>
                        <th>Fecha</th>
                        <th>Usuario</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($messages as $message)
                        <tr class="clickable-row" data-href='{{route('threads.show',$message->thread_id)}}'>
                            <td>{{$message->thread->title}}</td>

                            <td>{!! $message->message !!}</td>
                            <td>{{$message->created_at }}</td>

                            <td>
                                <ul style="list-style-type: none;margin-left: 0px;    -webkit-padding-start: 0px;">
                                    @if(!is_null($message->thread->members))

                                        @foreach($message->thread->members as $members)
                                            @if($members->id != auth()->user()->id)
                                                <li>
                                                    <label class="badge badge-primary">{{$members->name}}</label>
                                                </li>
                                            @endif
                                        @endforeach
                                    @endif
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
@routes

<script>

    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();

        $('.btn-destroy').on('click', function () {
            var id = $(this).data('id');
            var url = route('messages.destroy', {id: id});

            swal({
                title: 'Confirmar',
                text: "Una vez confirmada no es posible revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar y borrar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url, {data: {id: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Eliminado!',
                                    'El mensaje ha sido eliminado',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });


        $(".clickable-row").click(function () {
            window.location = $(this).data("href");
        });
    });


</script>
@endpush