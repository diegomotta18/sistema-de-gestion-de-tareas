@extends('layouts.app')
@push('styles')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">

@endpush
@section('content')

    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Redactar mensaje
                </h5>
                <div class="ibox-tools hidden-xs">

                    <a href="{{route('threads.index')}}"
                       class="btn btn-default" style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                </div>
                <div class="ibox-tools pull-right hidden-md hidden-lg hidden-sm">

                    <a href="{{route('threads.index')}}"
                       class="btn btn-xs btn-default" style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="panel-body">
                    <form method="POST" enctype="multipart/form-data"  action="{{ route('threads.store') }}">
                        @csrf

                        {!! Field::select('users', $users,['name'=>'users[]','multiple','class'=>'js-states form-control']) !!}
                        {!! Field::text('subject', '') !!}
                        <div class="form-group @if($errors->has('description')) has-error @endif">

                            <label class="control-label form-group @if($errors->has('description')) text-danger @endif">Mensaje</label>
                            <textarea id="summernote" class="summernote form-control"
                                      name="description">{{old('description')}}</textarea>
                            {{--{!! Field::textarea('description', isset($task) ? $task->description : '',array('id' =>'summernote', 'class'=>'summernote form-control' )) !!}--}}
                            @if($errors->has('description')) <p class="">{{ $errors->first('description') }}</p> @endif
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-4">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary waves-effect waves-classic pull-left">
                                        <i class="fa fa-send" aria-hidden="true"></i>
                                        Enviar
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-4 ">

                                <div class="form-group">

                                    <input id="files" type="file" name="files[]" multiple  />
                                </div>
                            </div>
                        </div>


                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
@routes
<script src="{{ asset('plugins/select2/saaelect2.min.js') }}"></script>
<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>
<script src="{{ asset('plugins/summernote/lang/summernote-es-ES.js') }}"></script>

<script>
    $(document).ready(function () {
        $('.summernote').summernote({
            height: 200,
            lang: 'es-ES', // default: 'en-US'
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['link']]

            ]
        });
        $('.note-btn').removeAttr('title');

        $('.dropdown-toggle').dropdown();
        $('#users').select2();
        $("#files").fileinput({
            dropZoneEnabled: false,
            theme: 'explorer-fa',
            width: 150,
            language: "es",
            uploadUrl: "/file-upload-single",
            validateInitialCount: true,
            initialPreviewAsData: true,
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: false,
            maxFileCount: 5,
            showUpload: false,
            maxFileSize: 2000,
            allowedFileExtensions: ["jpg", "png", "pdf", "jpeg", "xlsx","gif","docx"]
        });
    });
</script>
@endpush