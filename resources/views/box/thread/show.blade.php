@extends('layouts.app')
@push('styles')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">

@endpush
@section('content')

    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    {{$thread->title}}
                </h5>
                <div class="ibox-tools hidden-xs">

                    <a href="{{route('threads.index')}}"
                       class="btn btn-default" style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                </div>
                <div class="ibox-tools pull-right hidden-md hidden-lg hidden-sm">

                    <a href="{{route('threads.index')}}"
                       class="btn btn-xs btn-default" style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                </div>
            </div>
            @if(!is_null($messages))
                @foreach($messages as $message)

                    <div class="ibox-content">
                        <label class="badge badge-info">{{$message->user->name}}</label>
                        <div style="word-wrap: break-word !important;">{!! $message->message !!}</div>

                        @if(isset($message->files) && $message->files->count() > 0)
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Archivos adjuntos</label>
                                    <hr>
                                    @foreach($message->files()->get()->chunk(4) as $files)
                                        <div class="row">
                                            @foreach($files as $f)
                                                <div class="col-md-3 text-center">
                                                    <div class="well">
                                                        <a href="{{route('threads.download',$f->id)}}">
                                                            @if($f->extension == 'image/png' ||$f->extension == 'image/jpeg' )
                                                                <div class="row">
                                                                    <img style="width: 90px;height: 90px;"
                                                                         src="{{Storage::url($f->filename)}}">
                                                                </div>

                                                                <span >{{$f->title}}</span>

                                                            @else

                                                                <div>
                                                                    <div class="row">
                                                                        <i class="fa fa-file fa-5x" style="color:black;"></i>

                                                                    </div>

                                                                    <span class="text-center">{{$f->title}}</span>

                                                                </div>
                                                            @endif
                                                        </a>
                                                    </div>

                                                </div>
                                            @endforeach
                                        </div>

                                    @endforeach
                                </div>
                            </div>
                        @endif

                    </div>
                @endforeach
            @endif
            <div class="ibox-content">
                <div class="panel-body">
                    <form method="POST" enctype="multipart/form-data" action="{{ route('threads.reply',$thread->id) }}">
                        @csrf

                        <div class="row">
                            <div class="col-md-12">
                                {!! Field::textarea('description',array('id' =>'summernote', 'class'=>'summernote form-control' )) !!}
                                @if($errors->has('description')) <p class="help-block" style="color: #a94442;">{{ $errors->first('description') }}</p> @endif
                            </div>

                        </div>


                        <div class="row">
                            <div class="col-md-6 col-xs-4">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary waves-effect waves-classic">
                                        <i class="fa fa-send" aria-hidden="true"></i>
                                        Responder
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-4 ">

                                <div class="form-group">

                                    <input id="files" type="file" name="files[]" multiple  />
                                </div>
                            </div>
                        </div>



                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
@routes
<script src="{{ asset('plugins/select2/saaelect2.min.js') }}"></script>
<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>
<script src="{{ asset('plugins/summernote/lang/summernote-es-ES.js') }}"></script>

<script>
    $(document).ready(function () {
        $('.summernote').summernote({
            height: 200,
            lang: 'es-ES', // default: 'en-US',
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['link']]

            ]
        });
        $('.note-btn').removeAttr('title');

        $('.dropdown-toggle').dropdown();
        $('#users').select2();
        $("#files").fileinput({
            dropZoneEnabled: false,
            theme: 'explorer-fa',
            width: 150,
            language: "es",
            uploadUrl: "/file-upload-single",
            validateInitialCount: true,
            initialPreviewAsData: true,
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: false,
            maxFileCount: 5,
            showUpload: false,
            maxFileSize: 2000,
            allowedFileExtensions: ["jpg", "png", "pdf", "jpeg", "xlsx","gif","docx"]

        });
        $("html, body").animate({scrollTop: $(document).height()}, 1000);

    });

</script>
@endpush