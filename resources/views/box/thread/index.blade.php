@extends('layouts.app')
@section('content')
    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-info-circle"></i>
            <strong>Importante
            </strong></p>
        <p>En esta sección, se lista todos los mensajes. Cada mensaje es una conversación con otro usuario o con varios
            usuarios, dentro de cada hilo de conversación se puede ver un título y descripción del mensaje. Para
            responder un mensaje solo es necesario seleccionar el hilo de conversación, para redirigir al detalle del
            mensaje.
        </p>
        <p>
            Desde el botón <strong>Redactar mensaje</strong>, se puede redactar un mensaje nuevo agregando un título,
            descripción y seleccionar un usuario o varios usuarios, además se puede adjuntar archivos de tipo <strong>.png,
                .jpeg, .pdf y .xlsx</strong>.
        </p>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                Mensajes
            </h5>
            <div class="ibox-tools hidden-xs">
                <div class="btn-group">
                    <a href="{{ route('threads.create') }}"
                       class="btn btn-primary">
                        <i class="fa fa-send" aria-hidden="true"></i>
                        Redactar mensaje
                    </a>
                    <a class="btn btn-default"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       @if(auth()->user()->hasArea('Dirección')) href="{{route('direccion.index')}}"
                       @else href="{{route('home')}}" @endif><i class="fa fa-arrow-circle-left"></i> Volver</a>
                </div>
            </div>
            <div class="ibox-tools pull-right hidden-md hidden-lg hidden-sm">
                <div class="btn-group">
                    <a href="{{ route('threads.create') }}"
                       class="btn btn-xs btn-primary">
                        <i class="fa fa-send" aria-hidden="true"></i>
                        Redactar mensaje
                    </a>
                    <a class="btn btn-xs btn-default"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       @if(auth()->user()->hasArea('Dirección')) href="{{route('direccion.index')}}"
                       @else href="{{route('home')}}" @endif><i class="fa fa-arrow-circle-left"></i> Volver</a>
                </div>
            </div>
        </div>

        <div class="ibox-content">
            <div class="btn-group show-on-hover pull-right" style="margin-top: 10px;">
                <button type="button" class="btn btn-default dropdown-toggle"
                        data-toggle="dropdown" aria-expanded="true">
                    <i class="fa fa-plus" aria-hidden="true"></i> Acciones
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">

                    <li>
                        <a class="btn" id="btn-all-read">
                            <i class="fa fa-check-circle" aria-hidden="true"></i>
                            Marcar todo como leido
                        </a>
                    </li>

                </ul>
            </div>
            <table class="table table-no-bordered table-condensed"
                   data-toggle="table"
                   data-show-columns="false"
                   data-pagination="true"
                   data-search="true">
                <thead>
                <tr>
                    {{--<th data-visible="false">ID</th>--}}
                    <th>Asunto</th>
                    <th class="">Leidos</th>
                    <th class="">Usuario</th>
                    <th class="">Fecha</th>
                </tr>
                </thead>
                <tbody>
                @foreach($threads as $thread)
                    <tr class="clickable-row {{$thread->unreadNotifications->where('type',\App\Notifications\MsgThreadNotification::class)->count() > 0 ? 'active' : '' }}"
                        data-href='{{route('threads.show',$thread->id)}}'>
                        {{--<td>{{$thread->id}}</td>--}}
                        <td>@if($thread->checkFile()) <i
                                    class="fa fa-paperclip"></i> @endif{{$thread->thread->title}}</td>
                        <td>
                            <label class="badge badge-primary">{{$thread->notifications->where('type',\App\Notifications\MsgThreadNotification::class)->count()}}
                                /{{$thread->unreadNotifications->where('type',\App\Notifications\MsgThreadNotification::class)->count()}}</label>
                        </td>
                        <td class="">
                            <ul style="list-style-type: none;margin-left: 0px;    -webkit-padding-start: 0px;">
                                @if(!is_null($thread->thread->members))

                                    @foreach($thread->thread->members as $members)
                                        @if($members->id != auth()->user()->id)
                                            <li>
                                                <label class="badge badge-primary">{{$members->name}}</label>
                                            </li>
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                        </td>
                        <td>{{$thread->lastMessage()->created_at}}
                            <button onclick="event.stopPropagation();" data-id="{{ $thread->id }}"
                                    title="Eliminar mensajes"
                                    class="btn btn-destroy btn-danger pull-right">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
@routes

<script>

    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();

        $('.btn-destroy').on('click', function () {
            var id = $(this).data('id');
            var url = route('threads.destroy', {id: id});

            swal({
                title: 'Confirmar',
                text: "Una vez confirmada no es posible revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar y borrar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url, {data: {id: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Eliminado!',
                                    'Mensajes eliminados',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });

        $(".clickable-row").click(function () {
            window.location = $(this).data("href");
        });


        $('#btn-all-read').on('click', function () {
            var url = route('notifications.allreadbox');
            axios.put(url)
                .then(function (res) {
                    if (res.status == 200) {
                        swal("Todos los mensajes fueron leídos!", '', 'success');
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });

    });


</script>
@endpush