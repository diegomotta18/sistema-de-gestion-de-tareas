@extends('layouts.app')

@section('content')
    <div class="alert alert-info" role="alert"><p><i class="fa fa-info-circle"></i>
            <strong>Importante</strong></p>
        <p>En esta sección se muestra la lista de temas a tratar, que requiere una conversación y más información antes
            de cubrir el tema, sin fecha de finalización determinada.</p>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                Tema de parrilla
            </h5>
            <div class="ibox-tools hidden-xs">
                <div class="btn-group">
                    <a href="{{route('parrilla.create')}}" style="color: white;" class="btn btn-primary"><i
                                class="fa fa-plus-circle"></i> Crear tema parrilla</a>
                    <div class="btn-group">
                        <a class="btn btn-default pull-right"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           @if(auth()->user()->hasArea('Dirección')) href="{{route('direccion.index')}}"  @else href="{{route('home')}}" @endif><i class="fa fa-arrow-circle-left"></i>Volver</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="ibox-title">

            <div class="ibox-tools pull-right hidden-md hidden-lg hidden-sm">
                <div class="btn-group">
                    <a href="{{route('parrilla.create')}}" style="color: white;" class="btn btn-xs btn-primary"><i
                                class="fa fa-plus"></i> Crear tema parrilla</a>
                    <div class="btn-group">
                        <a class="btn btn-xs btn-default"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           @if(auth()->user()->hasArea('Dirección')) href="{{route('direccion.index')}}"  @else href="{{route('home')}}" @endif><i class="fa fa-arrow-circle-left"></i>Volver</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="ibox-content">

            <table class="table table-hover table-bordered table-striped table-responsive">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th class="hidden-xs">Proyecto</th>
                    <th class="hidden-xs">Creador</th>
                    <th class="hidden-xs">Asignado</th>
                    <th class="hidden-xs">Fecha de inicio
                    </th>
                    <th class="hidden-xs">Fecha de finalización
                    </th>
                    <th>Estado</th>
                </tr>
                </thead>
                <tbody>
                @if(!is_null($tasks))
                    @foreach($tasks as $task)

                        <tr>
                            <td>{{ $task->title }}</td>
                            <td class="hidden-xs">@if(isset($task->project)){{$task->project->name}}@else
                                    <label class="badge badge-warning"> Sin
                                        asignar </label> @endif
                            </td>
                            <td class="hidden-xs">
                                @if(isset($task->user))
                                    <label class="badge badge-info">{{$task->user->name}}</label>
                                @endif
                            </td>
                            <td class="hidden-xs">
                                @if($task->assigneds->isEmpty())
                                    <label class="badge badge-primary"> Sin
                                        asignar </label>
                                @else
                                    @foreach($task->assigneds as $assigned)
                                        <label class="badge badge-primary"> {{$assigned->name}} </label>
                                    @endforeach
                                @endif
                            </td>

                            <td class="hidden-xs"> @if(!is_null($task->date_init)){{ $task->date_init->format('d/m/Y')}}@endif</td>
                            <td class="hidden-xs">@if(!is_null($task->date_finish)){{$task->date_finish->format('d/m/Y')}} @endif</td>
                            <td>
                                @if(!is_null($task->status))
                                    <label class="label"
                                           style="background-color: {{$task->status->color}}; color: white;">{{$task->status->name}}</label>
                                @endif
                                @if($task->isExpired())
                                    <label class="label label-danger"
                                           style="color: white;">Demorado</label>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7">
                                <div class="btn-group"
                                     style="margin: auto;display: flex;flex-direction: row;justify-content: center;">

                                    @can('show', $task)

                                        <a href="{{ route('parrilla.show', $task->id) }}"
                                           class="btn btn-info"
                                           title="Ver tarea">
                                            <i class="fa fa-eye"
                                               aria-hidden="true"></i>
                                        </a>

                                    @endcan
                                    @can('update', $task)
                                        <a href="{{ route('parrilla.edit',$task->id) }}"
                                           class="btn btn-warning"
                                           title="Editar tarea">
                                            <i class="fa fa-pencil-square-o"
                                               aria-hidden="true"></i>
                                        </a>
                                    @endcan
                                    @can('destroy', $task)

                                        <button data-id="{{ $task->id }}"
                                                title="Eliminar tarea"
                                                class="btn  btn-destroy btn-danger">
                                            <i class="fa fa-trash-o"
                                               aria-hidden="true"></i>
                                        </button>
                                    @endcan
                                    <div class="btn-group show-on-hover">
                                        <button type="button"
                                                style="margin-top: -1px;height: 32px;"
                                                class="btn btn-default dropdown-toggle"
                                                data-toggle="dropdown"
                                                aria-expanded="true">
                                            <i class="fa fa-list"
                                               aria-hidden="true"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right"
                                            aria-labelledby="dropdownMenu1">

                                            <li>
                                                <a href="#"
                                                   data-id="{{$task->id}}"
                                                   class="init-task">
                                                    <i class="fa fa-info"
                                                       aria-hidden="true"></i>
                                                    Informado
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#"
                                                   data-id="{{$task->id}}"
                                                   class="reply-task">
                                                    <i class="fa fa-send"
                                                       aria-hidden="true"></i>
                                                    Respondido
                                                </a>
                                            </li>
                                            @can('processtask', $task)
                                                <li>
                                                    <a href="#"
                                                       data-id="{{$task->id}}"
                                                       class="start-task">
                                                        <i class="fa fa-tasks"
                                                           aria-hidden="true"></i>
                                                        En proceso
                                                    </a>
                                                </li>
                                            @endcan
                                            @can('denegatetask', $task)
                                                <li>
                                                    <a href="#"
                                                       class="denegado"
                                                       data-id="{{$task->id}}">
                                                        <i class="fa fa-close"
                                                           aria-hidden="true"></i>
                                                        Denegar
                                                    </a>
                                                </li>
                                            @endcan
                                            @can('finishtask', $task)
                                                <li>
                                                    <a href="#"
                                                       class="finish-task"
                                                       data-id="{{$task->id}}">
                                                        <i class="fa fa-flag-checkered"
                                                           aria-hidden="true"></i>
                                                        Finalizar
                                                    </a>
                                                </li>
                                            @endcan
                                            @can('destroy', $task)
                                                <li>
                                                    <a href="#"
                                                       class="pigeonhole"
                                                       data-id="{{$task->id}}">
                                                        <i class="fa fa-file-archive-o"
                                                           aria-hidden="true"></i>
                                                        Archivar
                                                    </a>
                                                </li>
                                            @endcan
                                        </ul>
                                    </div>
                                </div>

                            </td>

                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            <div class="text-center">
                @if(!is_null($tasks))
                    {{ $tasks->links() }}
                @endif
            </div>
        </div>
    </div>


@endsection
@push('scripts')
@routes

<script src="{{ asset('plugins/select2/saaelect2.min.js') }}"></script>
<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>

<script>

    $(document).ready(function () {

        $('#users').select2();

        $(".collapse").on('click', function () {
            if ($(".collapse").hasClass('collapse in')) {
                $('.collapse').collapse('hide');

            }

        });
        $('.dropdown-toggle').dropdown();

        $('.btn-destroy').on('click', function () {
            var id = $(this).data('id');
            var url = route('tickets.delete', {ticket: id});

            swal({
                title: 'Confirmar',
                text: "Una vez confirmada no es posible revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar y borrar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url, {data: {ticket: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Eliminado!',
                                    'La tarea ha sido eliminada',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });

        $('.finish-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.finishtask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.data.status == 200) {
                        swal({
                            title: "Tarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else if (res.data.status == 405) {
                        window.location.reload();

                    }
                    else {
                        swal({
                            title: "Tarea no modificada!",
                            text: 'Tiene subtareas pendientes a finalizar',
                            showConfirmButton: false,
                            type: 'error',
                            timer: 3000
                        });
                    }
                });
        });

        $('.init-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.inittask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {

                    if (res.status == 200) {
                        swal({
                            title: "Tarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });
        $('.start-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.processtask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({
                            title: "Tarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });

        $('.reply-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.replytask', {id: id});
            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({
                            title: "Tarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });

        $('.denegado').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.denegatetask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({
                            title: "Tarea denegada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });


        $('.pigeonhole').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.pigeonhole', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({
                            title: "Tarea archivada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });

        $(".clickable-row").click(function () {
            window.location = $(this).data("href");
        });


    });
</script>
@endpush