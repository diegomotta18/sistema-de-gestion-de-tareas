@extends('layouts.app')

@section('content')
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                Detalle de la tarea parrilla
            </h5>
            <div class="ibox-tools hidden-xs">
                <div class="btn-group">
                    @if(!is_null($task->status))
                        <change-state auth_finish="{{auth()->user()->can('finishtask',$task)}}"
                                      auth_pingehole="{{auth()->user()->can('destroy',$task)}}" task_id="{{$task->id}}"
                                      status="{{$task->status}}"></change-state>
                    @endif
                    @can('update', $task)
                        <a href="{{ route('parrilla.edit', $task->id) }}"
                           class="btn btn-default"
                           style="background-color: #f8ac59;border-color: #f8ac59;color:white;"
                           title="Editar tarea">
                            <i class="fa fa-pencil-square-o"
                               aria-hidden="true"></i> Editar
                        </a>
                    @endcan
                    <a class="btn btn-default"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       href="{{route('parrilla.index')}}"><i class="fa fa-arrow-circle-left"></i>Volver</a>
                </div>
            </div>
        </div>
        <div class="ibox-title  hidden-sm hidden-lg hidden-md">

            <div class="ibox-tools pull-right hidden-sm hidden-lg hidden-md">
                <div class="btn-group ">
                    @if(!is_null($task->status))
                        <change-state auth_finish="{{auth()->user()->can('finishtask',$task)}}"
                                      auth_pingehole="{{auth()->user()->can('destroy',$task)}}" task_id="{{$task->id}}"
                                      status="{{$task->status}}"></change-state>
                    @endif
                    @can('update', $task)
                        <a href="{{ route('parrilla.edit', $task->id) }}"
                           class="btn btn-xs btn-default"
                           style="background-color: #f8ac59;border-color: #f8ac59;color:white;"
                           title="Editar tarea">
                            <i class="fa fa-pencil-square-o"
                               aria-hidden="true"></i> Editar
                        </a>
                    @endcan
                    <a class="btn btn-xs btn-default pull-right"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       href="{{route('parrilla.index')}}"><i class="fa fa-arrow-circle-left"></i>Volver</a>
                </div>
            </div>
        </div>
        <div class="ibox-title">


            <small> @if(!is_null($task->date_init)) {{$task->date_init->format('d/m/Y') }} @else Sin fecha inicio @endif
                - @if(!is_null($task->date_finish)) {{$task->date_finish->format('d/m/Y') }} @else Sin fecha de
                finalización @endif</small>
            <br>

            <h2><b>{{$task->title}}</b></h2>

        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <h5>
                        Usuario creador
                    </h5>
                    @if(!is_null($task->user()->first()))
                        <label class="badge">{{$task->user->name}}</label>
                    @endif
                </div>
                <div class="col-md-4 col-xs-12">
                    <h5>
                        Usuario informado

                    </h5>
                    @if(!is_null($task->informeds()->get()))
                        @foreach($task->informeds()->get() as $informed)
                            <label class="badge badge-info">{{$informed->name}}</label>
                        @endforeach
                    @endif
                </div>
                <div class="col-md-4 col-xs-12">
                    <h5>
                        Usuario asignado

                    </h5>
                    @if(!is_null($task->assigneds()->get()))
                        @foreach($task->assigneds()->get() as $assigned)
                            <label class="badge badge-primary">{{$assigned->name}}</label>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <label>Descripción</label>
                    <div style="word-wrap: break-word !important;">{!! $task->description !!}</div>

                </div>
                @if(!is_null($task->files()->get()) && ($task->files()->count() > 0))
                    <div class="col-md-12">
                        <label>Archivos adjuntos</label>
                        <hr>
                        @foreach($task->files()->get()->chunk(4) as $files)
                            <div class="row">
                                @foreach($files as $f)
                                    <div class="col-md-3 text-center">
                                        <div class="well">
                                            <a href="{{route('tasks.download',$f->id)}}">
                                                @if($f->extension == 'image/png' ||$f->extension == 'image/jpeg' )
                                                    <div class="row">
                                                        <img style="width: 90px;height: 90px;"
                                                             src="{{Storage::url($f->filename)}}">
                                                    </div>

                                                    <span style="word-wrap: break-word !important;">{{$f->title}}</span>

                                                @else

                                                    <div>
                                                        <div class="row">
                                                            <i class="fa fa-file fa-5x"
                                                               style="color:black;width: 70px;height: 72px;"></i>

                                                        </div>

                                                        <span class="text-center"
                                                              style="word-wrap: break-word !important;">{{$f->title}}</span>

                                                    </div>
                                                @endif
                                            </a>
                                        </div>

                                    </div>
                                @endforeach
                            </div>

                        @endforeach
                    </div>
                @endif
            </div>

        </div>
        <div class="ibox-content">
            <comment-list task_id="{{$task->id}}" user_auth_id="{{auth()->user()->id}}"></comment-list>
        </div>
    </div>
@endsection
@push('scripts')
@routes

<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();
    });
</script>

@endpush