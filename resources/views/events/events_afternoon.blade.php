@extends('layouts.app')
@push('styles')
<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">

@endpush
@section('content')
    <div class="alert alert-info" role="alert"><p><i class="fa fa-info-circle"></i>
            <strong>Importante</strong></p>
        <p>En esta sección se muestra la lista de coberturas de la tarde, en cada cobertura indica la persona o las personas que
            estan asignados a realizarlo y se puede cargar nuevas
            coberturas desde el botón <strong>Crear cobertura</strong></p>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                Coberturas por la tarde
            </h5>
            <div class="ibox-tools hidden-xs">
                <div class="btn-group">

                    <a href="{{route('tasks.maincreate')}}" class="btn  btn btn-primary"><i
                                class="fa fa-plus-circle"></i> Proponer tema</a>
                    <a class="btn btn-default hidden-xs"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;" href="{{route('events.index')}}"><i class="fa fa-arrow-circle-left"></i> Volver</a>

                </div>
            </div>
            <div class="ibox-tools pull-right hidden-md hidden-lg hidden-md">
                <div class="btn-group">

                    <a href="{{route('tasks.maincreate')}}" class="btn  btn-xs btn-primary"><i
                                class="fa fa-plus-circle"></i> Proponer tema </a>
                    <a class="btn btn-xs btn-default hidden-md hidden-sm hidden-lg"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;" href="{{route('events.index')}}"><i class="fa fa-arrow-circle-left"></i> Volver</a>
                </div>
            </div>
        </div>
        <div class="ibox-title">
            <div class="ibox-tools">
                <form action="{{route('events.afternoon')}}" method="GET" style="margin-top: -9px;">
                    <div class="col-md-6  pull-right" style="margin-right: -18px;">
                        <div class="input-group ">
                            <input type='text' name="fecha" placeholder="Buscar por fecha de creación de la cobertura" class="form-control" id='datetimepicker4'/>
                            <span class="input-group-btn">

                            <button class="btn btn-primary"
                                    type="submit"><i
                                        class="fa fa-filter"></i>
                                Buscar
                            </button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="ibox-content">
            <table class="table table-hover table-bordered table-striped table-responsive">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th class="hidden-xs">Proyecto</th>
                    <th class="hidden-xs">Creador</th>
                    <th class="hidden-xs">Asignado</th>
                    <th class="hidden-xs text-center">Fecha de creacion de la cobertura</th>
                    <th class="hidden-xs">Fecha de inicio
                    </th>
                    <th class="hidden-xs">Fecha de finalización
                    </th>
                    <th>Estado</th>

                </tr>
                </thead>
                <tbody>
                @if(!is_null($tasks))
                    @foreach($tasks as $task)
                        <tr @if($task->isExpired())style="background-color: #f2dede; border-color: #ebccd1; border-style: solid;border-width: 3px;" @endif>
                            <td>{{ $task->title }} @if(!is_null($task->hora)) <label class="label label-info">{{$task->hora}} hs</label> @endif</td>
                            <td class="hidden-xs">@if(isset($task->project)){{$task->project->name}}@else
                                    <label class="badge badge-warning"> Sin
                                        asignar </label> @endif
                            </td>
                            <td class="hidden-xs">
                                @if(isset($task->user))
                                    <label class="badge badge-info">{{$task->user->name}}</label>
                                @endif
                            </td>
                            <td class="hidden-xs">
                                @if($task->assigneds->isEmpty())
                                    <label class="badge badge-primary"> Sin
                                        asignar </label>
                                @else
                                    @foreach($task->assigneds as $assigned)
                                        <label class="badge badge-primary"> {{$assigned->name}} </label>
                                    @endforeach
                                @endif
                            </td>
                            <td class="hidden-xs text-center" >@if(!is_null($task->created_at)){{ $task->created_at->format('d/m/Y  H:i A')}} @endif

                            <td class="hidden-xs text-center">@if(!is_null($task->date_init)){{ $task->date_init->format('d/m/Y')}} @endif

                            </td>

                            <td class="hidden-xs text-center">@if(!is_null($task->date_finish)){{$task->date_finish->format('d/m/Y')}} @endif
                            </td>
                            <td>
                                @if(!is_null($task->status))
                                    <label class="label"
                                           style="background-color: {{$task->status->color}}; color: white;">{{$task->status->name}}</label>
                                @endif
                                @if($task->isExpired())
                                    <label class="label label-danger"
                                           style="color: white;">Demorado</label>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td @if(auth()->user()->hasArea('Prensa'))  colspan="8" @else colspan="7"
                                @endif  @if($task->isExpired())style="background-color: #f2dede; border-color: #ebccd1; border-style: solid;border-width: 3px;" @endif>
                                <div class="btn-group"
                                     style="margin: auto;display: flex;flex-direction: row;justify-content: center;">
                                    @direccion
                                    @if(!is_null($task->urlnotice))
                                        <a href="{{$task->urlnotice}}"
                                           data-toggle="tooltip"
                                           title="Ir a la url de la noticia "
                                           class="btn btn-default">
                                            <i class="fa fa-share"
                                               aria-hidden="true"></i>
                                        </a>
                                    @endif
                                    @enddireccion
                                    @prensa
                                    @if(!is_null($task->urlnotice))
                                        <a href="{{$task->urlnotice}}"
                                           data-toggle="tooltip"
                                           title="Ir a la url de la noticia "
                                           class="btn btn-default">
                                            <i class="fa fa-share"
                                               aria-hidden="true"></i>
                                        </a>
                                    @endif
                                    @endprensa
                                    @prensa
                                    @can('update', $task)

                                        <a href="{{route('task.changeUrl', $task->id)}}"
                                           data-toggle="tooltip"
                                           title="Agregar url "
                                           class="btn btn-default"
                                           style="background-color: rgb(49, 112, 143);color: white;">
                                            <i class="fa fa-link"
                                               aria-hidden="true"></i>
                                        </a>
                                    @endcan
                                    @endprensa

                                    @can('show', $task)
                                        <a href="{{ route('task.home.show',['task'=> $task->id]) }}"
                                           class="btn btn-info"
                                           title="Ver tarea">
                                            <i class="fa fa-eye"
                                               aria-hidden="true"></i>
                                        </a>
                                    @endcan
                                    @can('update', $task)
                                        @if(!is_null($task->project) && !is_null($task->area))

                                            <a href="{{ route('task.project.edit',['area' => $task->area->name, 'project'=>$task->project_id,'task'=> $task->id]) }}"
                                               class="btn btn-warning"
                                               title="Editar tarea">
                                                <i class="fa fa-pencil-square-o"
                                                   aria-hidden="true"></i>
                                            </a>
                                        @elseif(!is_null($task->area))
                                            <a href="{{ route('task.area.edit',['area' => $task->area->name,'task'=> $task->id]) }}"
                                               class="btn btn-warning"
                                               title="Editar tarea">
                                                <i class="fa fa-pencil-square-o"
                                                   aria-hidden="true"></i>
                                            </a>

                                        @endif
                                    @endcan
                                    @can('destroy', $task)

                                        <button data-id="{{ $task->id }}"
                                                title="Eliminar tarea"
                                                class="btn  btn-destroy btn-danger">
                                            <i class="fa fa-trash-o"
                                               aria-hidden="true"></i>
                                        </button>
                                    @endcan
                                    <div class="btn-group show-on-hover">
                                        <button type="button"
                                                style="margin-top: -1px;height: 32px;"
                                                class="btn btn-default dropdown-toggle"
                                                data-toggle="dropdown"
                                                aria-expanded="true">
                                            <i class="fa fa-list"
                                               aria-hidden="true"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right"
                                            aria-labelledby="dropdownMenu1">

                                            <li>
                                                <a href="#"
                                                   data-id="{{$task->id}}"
                                                   class="init-task">
                                                    <i class="fa fa-info"
                                                       aria-hidden="true"></i>
                                                    Informado
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#"
                                                   data-id="{{$task->id}}"
                                                   class="reply-task">
                                                    <i class="fa fa-send"
                                                       aria-hidden="true"></i>
                                                    Respondido
                                                </a>
                                            </li>
                                            @can('processtask', $task)
                                                <li>
                                                    <a href="#"
                                                       data-id="{{$task->id}}"
                                                       class="start-task">
                                                        <i class="fa fa-tasks"
                                                           aria-hidden="true"></i>
                                                        En proceso
                                                    </a>
                                                </li>
                                            @endcan
                                            @can('denegatetask', $task)

                                                <li>
                                                    <a href="#" data-id="{{$task->id}}" class="denegado">
                                                        <i class="fa fa-close" aria-hidden="true"></i>
                                                        Denegado
                                                    </a>
                                                </li>
                                            @endcan
                                            @can('finishtask', $task)
                                                <li>
                                                    <a href="#"
                                                       class="finish-task"
                                                       data-id="{{$task->id}}">
                                                        <i class="fa fa-flag-checkered"
                                                           aria-hidden="true"></i>
                                                        Finalizar
                                                    </a>
                                                </li>
                                            @endcan
                                            @can('destroy', $task)
                                                <li>
                                                    <a href="#"
                                                       class="pigeonhole"
                                                       data-id="{{$task->id}}">
                                                        <i class="fa fa-file-archive-o"
                                                           aria-hidden="true"></i>
                                                        Archivar
                                                    </a>
                                                </li>
                                            @endcan
                                            {{--@endif--}}

                                        </ul>
                                    </div>
                                </div>


                            </td>

                        </tr>
                    @endforeach
                @endif

                </tbody>
            </table>
            <div class="text-center">
                @if(!is_null($tasks))
                    {{ $tasks->links() }}
                @endif
            </div>
        </div>
    </div>

@endsection

@push('scripts')
@routes
<script src="{{ asset('plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('plugins/moment/es.js') }}"></script>
<script>
    $(document).ready(function () {


        $('.dropdown-toggle').dropdown();

        $('#datetimepicker4').datetimepicker({
            locale: 'es',
            format: 'DD/MM/YYYY'
        });


        $('.table-responsive').on('show.bs.dropdown', function (e) {
            $(e.relatedTarget).next('div[aria-labelledby="dropdownMenuButton"]').appendTo("body");
        });

        $('body').on('hide.bs.dropdown', function (e) {
            $(this).find('div[aria-labelledby="dropdownMenuButton"]').appendTo($(e.relatedTarget).parent());
        });


        $('.btn-destroy').on('click', function () {
            var id = $(this).data('id');
            var url = route('tasks.destroy', {id: id});

            swal({
                title: "{{ "Confirmar" }}",
                text: "{{ "Una vez confirmada no es posible revertir esta acción" }}",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "{{ "Cancelar" }}",
                confirmButtonClass: "btn-warning",
                confirmButtonText: "{{ "Confirmar y borrar" }}",
                closeOnConfirm: !1
            }, function () {
                axios.delete(url, {data: {id: id}})
                    .then(function (res) {
                        if (res.status === 200) {
                            swal("Evento eliminado!", '', 'success');
                            window.location.reload();
                        } else {
                            swal("{{ __("An error has ocurred") }}", '', 'error');
                        }
                    });
            });
        });

        $('.finish-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.finishtask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.data.status == 200) {
                        swal({
                            title: "Tarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else if (res.data.status == 405) {
                        window.location.reload();

                    }
                    else {
                        swal({
                            title: "Tarea no modificada!",
                            text: 'Tiene subtareas pendientes a finalizar',
                            showConfirmButton: false,
                            type: 'error',
                            timer: 3000
                        });
                    }
                });
        });

        $('.init-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.inittask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {

                    if (res.status == 200) {
                        swal({
                            title: "Tarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });
        $('.start-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.processtask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({
                            title: "Tarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });

        $('.reply-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.replytask', {id: id});
            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({
                            title: "Tarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });


        $('.pigeonhole').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.pigeonhole', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({
                            title: "Tarea archivada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });

        $('.denegado').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.denegatetask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({
                            title: "Tarea denegada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });


    });
</script>
@endpush