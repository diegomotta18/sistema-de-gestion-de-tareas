@extends('layouts.app')

@section('content')
    <div class="alert alert-info" role="alert"><p><i class="fa fa-info-circle"></i>
            <strong>Importante</strong></p>
        <p>En esta sección se muestra la lista de errores y excepciones que ocurren sobre sistema </p>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                Errores y excepciones
            </h5>
            <div class="ibox-tools hidden-xs">
                <div class="btn-group">

                    <a class="btn btn-default hidden-xs"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       @if(auth()->user()->hasArea('Dirección')) href="{{route('direccion.index')}}"
                       @else href="{{route('home')}}" @endif><i class="fa fa-arrow-circle-left"></i> Volver</a>

                </div>
            </div>
            <div class="ibox-tools pull-right hidden-md hidden-lg hidden-md">
                <div class="btn-group">

                    <a class="btn btn-xs btn-default hidden-md hidden-sm hidden-lg"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       @if(auth()->user()->hasArea('Dirección')) href="{{route('direccion.index')}}"
                       @else href="{{route('home')}}" @endif><i class="fa fa-arrow-circle-left"></i> Volver</a>
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <div class="btn-group show-on-hover pull-right" style="margin-top: 10px;">
                <button type="button" class="btn btn-default dropdown-toggle"
                        data-toggle="dropdown" aria-expanded="true">
                    <i class="fa fa-plus" aria-hidden="true"></i> Acciones
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li>
                        <a class="btn" id="btn-all-destroy">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                            Eliminar todas las excepciones
                        </a>
                    </li>
                </ul>
            </div>
            <table class="table table-hover table-bordered table-striped table-responsive">
                <thead>
                <tr>
                    <th class="text-center">Detalle</th>
                    <th class="text-center" style="width: 90px;">Fecha</th>
                    <th class="text-center"> Acciones</th>
                </tr>
                </thead>
                <tbody>
                @if(!is_null($exceptions))
                    @foreach($exceptions as $exception)
                        <tr >
                            <td> <b>Mensaje </b><b>{{$exception->message}}</b><br>
                                <b>Archivo </b>{{$exception->file}}<br>
                                <b>Linea </b>{{$exception->line}}<br>
                                <b>Status </b>{{$exception->code}}
                            </td>
                            <td>{{$exception->created_at->format('d/m/Y H:i A')}}</td>
                            <td>
                                <div class="btn-group" style="display: flex;">
                                    <a href="{{route('exceptions.show',$exception->id)}}" data-toggle="tooltip"
                                       title="Ver excepción"
                                       class="btn btn-info">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                        <button data-id="{{ $exception->id }}" data-toggle="tooltip" title="Eliminar excepción"
                                                class="btn btn-danger  btn-destroy">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            <div class="text-center">
                @if(!is_null($exceptions))
                    {{ $exceptions->links() }}
                @endif
            </div>
        </div>
    </div>

@endsection

@push('scripts')
@routes

<script>
    $(document).ready(function () {


        $('.dropdown-toggle').dropdown();


        $('#btn-all-destroy').on('click', function () {
            var url = route('excepcions.destroyall');
            axios.delete(url)
                .then(function (res) {
                    if (res.status == 200) {
                        swal("Todos las excepciones fueron eliminadas!", '', 'success');
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });


        $('.btn-destroy').on('click', function () {
            var id = $(this).data('id');
            var url = route('exceptions.destroy', {id: id});

            swal({
                title: 'Confirmar',
                text: "Una vez confirmada no es posible revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar y borrar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url, {data: {id: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Eliminado!',
                                    'La excepción ha sido eliminada',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });

    });
</script>
@endpush