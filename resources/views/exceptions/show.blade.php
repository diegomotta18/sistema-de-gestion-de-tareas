@extends('layouts.app')

@section('content')

    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Detalle de la excepción

                </h5>

                <div class="ibox-tools hidden-xs">
                    <div class="btn-group">

                        <a class="btn btn-default hidden-xs"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           href="{{route('exceptions.index')}}"><i class="fa fa-arrow-circle-left"></i> Volver</a>

                    </div>
                </div>
                <div class="ibox-tools pull-right hidden-md hidden-lg hidden-md">
                    <div class="btn-group">

                        <a class="btn btn-xs btn-default hidden-xs"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           href="{{route('exceptions.index')}}"><i class="fa fa-arrow-circle-left"></i> Volver</a>
                    </div>
                </div>

            </div>

            <div class="ibox-content">
                <h5>Mensaje</h5>

                <p>{{ $exception->message }}</p>
            </div>
            <div class="ibox-content">
                <h5>Codigo</h5>

                <p>{{ $exception->code }}</p>
            </div>
            <div class="ibox-content">
                <h5>Archivo</h5>

                <p>{{ $exception->file }}</p>
            </div>
            <div class="ibox-content">
                <h5>Linea</h5>

                <p>{{ $exception->line }}</p>
            </div>
            <div>
                <h5>Content</h5>
                {!! $exception->content !!}
            </div>

        </div>
    </div>

@endsection

@push('scripts')
<script>
    $('.dropdown-toggle').dropdown();

</script>
@endpush