@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="ibox float-e-margins">
                <div class="alert alert-info" role="alert"><p><i class="fa fa-info-circle"></i>
                        <strong>Importante</strong></p>
                    <p>En esta sección se visualiza todas las áreas a la que pertenece, puede dirigirse a un área
                        realizando click sobre el nombre del área </p>
                </div>

                <div class="ibox-title">
                    <h5 >
                        Áreas

                    </h5>
                    <div class="ibox-tools hidden-xs">
                        <div class="btn-group">
                            <a
                               href="{{ route('areas.create') }}" class="btn btn-primary">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                Crear área
                            </a>
                            <a class="btn btn-default  pull-right"
                               style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                               @if(auth()->user()->hasArea('Dirección')) href="{{route('direccion.index')}}"  @else href="{{route('home')}}" @endif><i class="fa fa-table"></i> Volver</a>
                        </div>
                    </div>
                    <div class="ibox-tools  pull-right hidden-lg hidden-sm hidden-sm">
                        <div class="btn-group">
                            <a
                               href="{{ route('areas.create') }}" class="btn btn-xs btn-primary">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                Crear área
                            </a>
                            <a class="btn btn-default btn-xs   pull-right"
                               style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                               @if(auth()->user()->hasArea('Dirección')) href="{{route('direccion.index')}}"  @else href="{{route('home')}}" @endif><i class="fa fa-table"></i> Volver</a>
                        </div>
                    </div>
                </div>

                <div class="ibox-content">
                    <table class="table table-hover"
                           data-toggle="table"
                           data-show-columns="true"
                           data-pagination="true"
                           data-search="true">
                        <thead>
                        <tr class="text-center">
                            {{--<th data-visible="false">ID</th>--}}
                            <th class="text-center">Área</th>
                            <th class="text-center">Acciones</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($areas as $area)
                            <tr class="clickable-row" style="cursor:pointer;"
                                data-href='{{route('areas.show',$area->name)}}'>
                                {{--<td>{{ $area->id }}</td>--}}
                                <td>{{ $area->name }}</td>
                                <td>

                                    <div class="btn-group">
                                        <a href="{{route('areas.show',$area->name)}}" class="btn btn-info"> <i
                                                    class="fa fa-list" aria-hidden="true"></i> Proyectos </a>
                                        <a href="{{route('areas.edit',$area->name)}}" data-toggle="tooltip"
                                           title="Editar"
                                           class="btn btn-warning">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>
                                        <button data-id="{{ $area->id }}" data-toggle="tooltip" title="Quitar"
                                                class="btn btn-danger  btn-destroy">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
@routes
<script>
    $(document).ready(function () {


        $('.dropdown-toggle').dropdown();

        $('.btn-destroy').on('click', function () {
            var id = $(this).data('id');
            var url = route('areas.destroy', {id: id});

            swal({
                title: 'Confirmar',
                text: "Una vez confirmada no es posible revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar y borrar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url, {data: {id: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Eliminado!',
                                    'El área ha sido eliminado',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });
    });

</script>
@endpush