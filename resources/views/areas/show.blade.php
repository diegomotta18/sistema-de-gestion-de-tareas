@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="ibox float-e-margins">
                <div class="alert alert-info" role="alert"><p><i class="fa fa-info-circle"></i>
                        <strong>Importante</strong></p>
                    <p>En esta sección, se lista todos los proyectos del area <strong>{{$area->name}}</strong>. En la
                        tabla de <strong>Proyectos</strong>, en la esquina superior derecha, el 1º botón <strong>Crear
                            proyecto</strong>, puede crear un proyecto nuevo. El 2º botón <strong>Tareas del
                            área</strong>, redirige a la lista de tareas sueltas que tiene el área, dichas tareas
                        <strong>no están asignadas a ningún proyecto</strong>. El botón <a href="#"
                                                                                           class="btn btn-xs btn-info"
                                                                                           title="Ver Tareas">
                            <i class="fa fa-list-alt" aria-hidden="true"></i>
                        </a>, redirige a la lista de tareas de un proyecto particular. El bóton
                        <a href="#"
                           class="btn btn-xs btn-warning"
                        >
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </a>, permite editar los datos del proyecto seleccionado y el botón

                        <button
                                class="btn btn-xs btn-danger">
                            <i style="color: white;" class="fa fa-trash-o" aria-hidden="true"></i>
                        </button>
                        , permite eliminar el proyecto seleccionado.
                    </p>
                </div>

                <div class="ibox-title" style="background-color:#1ab394;">
                    <h5 style="color:white;">
                        {{$area->name}}
                    </h5>
                </div>
                <div class="ibox-title">
                    <h5>
                        Proyectos
                    </h5>

                    <div class="ibox-tools hidden-xs">
                        <div class="btn-group">
                            @admin
                            <a href="{{ route('projects.create',$area->name) }}" class="btn btn-primary"
                               style="color: white;">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                Crear proyecto
                            </a>
                            @endadmin
                            <a style="color: white !important;" href="{{ route('areas.show.tasks',$area->name) }}" class="btn btn-info">

                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                Tareas del área
                            </a>
                            <a style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                               href="{{route('areas.index')}}"
                               class="btn btn-default">
                                <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                                Volver
                            </a>
                        </div>
                    </div>
                    <div class="ibox-tools hidden-md hidden-sm hidden-lg">
                        <div class="btn-group">

                            <a href="{{ route('projects.create',$area->name) }}" class="btn btn-xs btn-default">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                Crear proyecto
                            </a>
                            <a href="{{ route('areas.show.tasks',$area->name) }}" class="btn btn-xs btn-primary">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                Tareas del área
                            </a>
                            <a style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                               href="{{route('areas.index')}}"
                               class="btn btn-xs btn-default">
                                <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                                Volver
                            </a>z
                        </div>
                    </div>

                </div>

                <div class="ibox-content">
                    <table class="table table-hover"
                           data-toggle="table"
                           data-show-columns="true"
                           data-pagination="true"
                           data-search="true">
                        <thead>
                        <tr>
                            <th data-visible="false">ID</th>
                            <th>Nombre</th>
                            <th>Responsable</th>
                            <th>Descripcion</th>
                            <th class="text-center">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($projects as $project)
                            <tr>
                                <td>{{ $project->id }}</td>
                                <td>{{ $project->name }}</td>
                                <td>@if(!is_null($project->user)) {{ $project->user->name }} @endif</td>
                                <td>{!! $project->description !!}
                                <td class="text-center">
                                    <div class="btn-group" style="display: flex;">
                                        <a href="{{ route('project.agileboard',$project->id) }}"
                                           class="btn btn-primary" title="Ver tablero ágil" >
                                            <i class="fa fa-table" aria-hidden="true"></i>
                                        </a>
                                        <a href="{{ route('projects.show',['area'=>$area->name,'project'=>$project->id]) }}"
                                           class="btn btn-info"
                                           title="Ver Tareas">
                                            <i class="fa fa-list-alt" aria-hidden="true"></i>
                                        </a>
                                        <a href="{{ route('projects.edit',['area'=>$area->name,'project'=>$project->id]) }}"
                                           class="btn btn-warning"
                                           title="Editar proyecto">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </a>

                                        <button data-id="{{ $project->id }}" title="Eliminar proyecto"
                                                class="btn btn-danger btn-destroy btn-link">
                                            <i style="color: white;" class="fa fa-trash-o" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
@routes
<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();

        $('.btn-destroy').on('click', function () {
            var id = $(this).data('id');
            var url = route('projects.destroy', {id: id});
            swal({
                title: 'Confirmar',
                text: "Una vez confirmada no es posible revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar y borrar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url, {data: {id: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Eliminado!',
                                    'El proyecto ha sido eliminado',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });
    });
</script>
@endpush