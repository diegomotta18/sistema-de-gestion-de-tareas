@extends('layouts.app')

@section('content')


        <div class="ibox float-e-margins">
            <div class="alert alert-info" role="alert"><p><i class="fa fa-info-circle"></i> <strong>Importante</strong>
                </p>
                <p>La lista de tareas en este formulario, no se encuentran asignadas a ningún proyecto, se consideran
                    tareas sueltas. Estas tareas si pueden estar asignados a usuarios y a informados que realizan un
                    seguimiento de la misma.</p></div>
            <div class="ibox-title" style="background-color:#1ab394;">
                <h5 style="color:white;">
                    {{$area->name}}

                </h5>
            </div>
            <div class="ibox-title">
                <h5>
                    Tareas del área
                </h5>

                <div class="ibox-tools hidden-xs">
                    <div class="btn-group">
                        <a href="{{ route('tasks.area.create',$area->name) }}" class="btn  btn-primary">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                            Crear tarea
                        </a>
                        <a style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;" href="{{route('areas.show',$area->name)}}" class="btn  btn-default pull-right">
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                            Volver
                        </a>
                    </div>

                </div>
                <div class="ibox-tools hidden-md hidden-sm hidden-lg pull-right">
                    <div class="btn-group">
                        <a href="{{ route('tasks.area.create',$area->name) }}" class="btn btn-xs btn-primary">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                            Crear tarea
                        </a>
                        <a style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;" href="{{route('areas.show',$area->name)}}" class="btn btn-xs btn-default">
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                            Volver
                        </a>


                    </div>

                </div>
            </div>

            <div class="ibox-content">
                <table class="table table-hover table-responsive"
                       data-toggle="table"
                       data-show-columns="true"
                       data-pagination="true"
                       data-search="true">
                    <thead>
                    <tr>
                        <th data-visible="false">ID</th>
                        <th class="">Titulo</th>
                        <th class="">Asignados</th>
                        <th class="">Informados</th>
                        <th class="">Estado</th>
                        <th class="">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tasks as $task)
                        <tr>
                            <td>{{ $task->id }}</td>
                            <td>{{ $task->title }}</td>
                            <td>
                                @if(!is_null($task->assigneds()))
                                    <ul style="list-style-type: none;margin-left: 0px;text-align: center; -webkit-padding-start: 0px;">
                                        @foreach($task->assigneds()->get() as $assigned)
                                            <li><label class="badge badge-primary">{{$assigned->name }}</label></li>
                                        @endforeach
                                    </ul>
                                @endif
                            </td>
                            <td>
                                <ul style="list-style-type: none;margin-left: 0px;text-align: center;    -webkit-padding-start: 0px;">
                                    @if(!is_null($task->informeds))
                                        @foreach($task->informeds as $informeds)
                                            <li>
                                                <label class="badge badge-info">{{$informeds->name}}</label>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </td>
                            <td>
                                <div style="display: flex;">
                                    @if(!is_null($task->status))
                                        <label class="label"
                                               style="background-color: {{$task->status->color}}; color: white;">{{$task->status->name}}</label>
                                    @endif

                                </div>
                            </td>
                            <td>

                                <div class="btn-group" style="display: flex;text-align: center">
                                    <a href="{{ route('task.area.show', ['area'=>$area->name,'task' =>$task->id]) }}"
                                       class="btn  btn-info"
                                       title="Visualizar tarea">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                    <a href="{{ route('task.area.edit', ['area'=>$area->name,'task' =>$task->id]) }}"
                                       class="btn  btn-warning"
                                       title="Editar tarea">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </a>

                                    <button data-id="{{ $task->id }}" title="Eliminar tarea"
                                            class="btn btn-destroy btn-danger">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                    <div class="btn-group show-on-hover">
                                        <button type="button" class="btn btn-default dropdown-toggle"
                                                data-toggle="dropdown" aria-expanded="true">
                                            <i class="fa fa-list" aria-hidden="true"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu1">

                                            <li>
                                                <a href="#" data-id="{{$task->id}}" class="init-task">
                                                    <i class="fa fa-info" aria-hidden="true"></i>
                                                    Informado
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" data-id="{{$task->id}}" class="start-task">
                                                    <i class="fa fa-tasks" aria-hidden="true"></i>
                                                    En proceso
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="finish-task" data-id="{{$task->id}}">
                                                    <i class="fa fa-flag-checkered" aria-hidden="true"></i>
                                                    Finalizar
                                                </a>
                                            </li>
                                            <li>

                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

@endsection

@push('scripts')
@routes

<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();

        $('.btn-destroy').on('click', function () {
            var id = $(this).data('id');
            var url = route('tasks.destroy', {id: id});

            swal({
                title: 'Confirmar',
                text: "Una vez confirmada no es posible revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar y borrar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url, {data: {id: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Eliminado!',
                                    'La tarea ha sido eliminada',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });

        $('.finish-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.finishtask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.data.status == 200) {
                        swal({title: "Tarea modificada!", showConfirmButton: false, type: 'success', timer: 3000});
                        window.location.reload();

                    } else if (res.data.status == 405) {
                        window.location.reload();
                    }
                    else {
                        swal({
                            title: "Tarea no modificada!",
                            text: 'Tiene subtareas pendientes a finalizar',
                            showConfirmButton: false,
                            type: 'error',
                            timer: 3000
                        });
                    }
                });
        });

        $('.init-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.inittask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {

                    if (res.status == 200) {
                        swal({title: "Tarea modificada!", showConfirmButton: false, type: 'success', timer: 3000});
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });
        $('.start-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.processtask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({title: "Tarea modificada!", showConfirmButton: false, type: 'success', timer: 3000});
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });
    });
</script>
@endpush