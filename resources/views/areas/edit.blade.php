@extends('layouts.app')

@section('content')
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Editar área
                </h5>
                <div class="ibox-tools hidden-xs">
                    <div class="btn-group">
                            <a style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;" href="{{route('areas.index')}}"
                               class="btn btn-default pull-right">
                                <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                                Volver
                            </a>
                    </div>
                </div>
                <div class="ibox-tools hidden-lg hidden-sm hidden-md pull-right">
                    <div class="btn-group">
                        <a style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;" href="{{route('areas.index')}}"
                           class="btn btn-xs pull-right btn-default hidden-md hidden-sm hidden-lg">
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                            Volver
                        </a>
                    </div>
                </div>
            </div>

            <div class="ibox-content">
                <form method="POST" action="{{ route('areas.update', $area->id) }}">
                    @csrf
                    @method('PUT')
                    @include('areas.partials.form')
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();

        $('#areas').select2();
    });
</script>
@endpush