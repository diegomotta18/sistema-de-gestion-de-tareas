{!! Field::text('name', isset($project) ? $project->name : '') !!}

{{--{!! Field::text('description', isset($project) ? $project->description : '') !!}--}}

<div class="form-group @if($errors->has('description')) has-error @endif">

    <label class="control-label">Descripción</label>
    <textarea id="summernote" class="summernote form-control"
              name="description">{{isset($project) ? $project->description : ''}}</textarea>
    {{--{!! Field::textarea('description', isset($task) ? $task->description : '',array('id' =>'summernote', 'class'=>'summernote form-control' )) !!}--}}
    @if($errors->has('description')) <p class="help-block"
                                        style="color: #a94442;">{{ $errors->first('description') }}</p> @endif
</div>
{{--<textarea id="summernote" name="description" class="summernote form-control"></textarea>--}}

{{--{!! Field::email('email', isset($user) ? $user->email : '') !!}--}}

{{--{!! Field::select('role', $roles, isset($user) ? $user->roles()->first()->name : '' ,['empty' => __('Seleccionar Rol')]) !!}--}}

{{--{!! Field::select('area', $areas, isset($user) ? $user->areas : '' ,['name'=>'areas[]','multiple','class'=>'js-states form-control','empty' => __('Asignar areas')]) !!}--}}

<button type="submit" class="btn btn-primary waves-effect waves-classic">
    <i class="fa fa-save" aria-hidden="true"></i>
    Guardar
</button>