@extends('layouts.app')
@section('content')
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Agregar proyecto
                </h5>

                <div class="ibox-tools">
                    <a class="btn btn-default hidden-xs"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;" href="{{ URL::previous() }}"><i class="fa fa-arrow-circle-left"></i>Volver</a>
                    <a class="btn btn-xs btn-default hidden-md hidden-lg hidden-sm"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;" href="{{ URL::previous() }}"><i class="fa fa-arrow-circle-left"></i>Volver</a>
                </div>
            </div>

            <div class="ibox-content">
                <form method="POST" action="{{ route('projects.store', $area->name) }}">
                    @csrf

                    @include('projects.partials.form')
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ asset('plugins/summernote/lang/summernote-es-ES.js') }}"></script>

<script>
    $(document).ready(function () {
        $('.summernote').summernote({
            height: 300,
            lang: 'es-ES', // default: 'en-US'
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ]
        });
        $('.note-btn').removeAttr('title');
        $('.dropdown-toggle').dropdown();

    });
</script>

@endpush