@extends('layouts.app')
@section('content')
    @push('styles')
    <link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
    @endpush
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Agregar proyecto
                </h5>

                <div class="ibox-tools">
                    <a class="btn btn-default hidden-xs"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;" href="{{ URL::previous() }}"><i class="fa fa-arrow-circle-left"></i>Volver</a>
                    <a class="btn btn-xs btn-default hidden-md hidden-lg hidden-sm"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;" href="{{ URL::previous() }}"><i class="fa fa-arrow-circle-left"></i>Volver</a>
                </div>
            </div>

            <div class="ibox-content">
                <form method="POST" action="{{ route('projects.storemain',$area) }}">
                    @csrf
                    {!! Field::select('areas', $areas, isset($user) ? $user->areas : '' ,['name'=>'areas','class'=>'js-states form-control','empty' => ('-- Asignar area --')]) !!}

                    {!! Field::text('name', isset($project) ? $project->name : '') !!}

                    {{--{!! Field::text('description', isset($project) ? $project->description : '') !!}--}}

                    <div class="form-group @if($errors->has('description')) has-error @endif">

                        <label class="control-label">Descripción</label>
                        <textarea id="summernote" class="summernote form-control"
                                  name="description">{{isset($project) ? $project->description : ''}}</textarea>
                        {{--{!! Field::textarea('description', isset($task) ? $task->description : '',array('id' =>'summernote', 'class'=>'summernote form-control' )) !!}--}}
                        @if($errors->has('description')) <p class="help-block"
                                                            style="color: #a94442;">{{ $errors->first('description') }}</p> @endif
                    </div>
                    <button type="submit" class="btn btn-primary waves-effect waves-classic">
                        <i class="fa fa-save" aria-hidden="true"></i>
                        Guardar
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ asset('plugins/summernote/lang/summernote-es-ES.js') }}"></script>
<script src="{{ asset('plugins/select2/saaelect2.min.js') }}"></script>
<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#areas').select2();

        $('.summernote').summernote({
            height: 300,
            lang: 'es-ES', // default: 'en-US'
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ]
        });
        $('.note-btn').removeAttr('title');
        $('.dropdown-toggle').dropdown();

    });
</script>

@endpush