@extends('layouts.app')
@push('styles')
<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">

@endpush
@section('content')
    <div class="ibox float-e-margins">
        <div class="alert alert-info" role="alert"><p><i class="fa fa-info-circle"></i>
                <strong>Importante</strong></p>
            <p>

            </p>
        </div>
        <div class="ibox-title" style="background-color: #1ab394">
            <h5><b>
                    <a style="color:white;"
                       href="{{route('areas.show',$area->name)}}">{{$area->name}}</a> @if(!is_null($project))
                        <b style="color:white;">/</b>
                        <a style="color:white;"
                           href="{{route('project.agileboard',$project->id)}}">{{$project->name}}</a>  @endif
                </b>
            </h5>
        </div>
        <div class="ibox-title">
            <div class="ibox-tools hidden-xs">
                <div class="row">
                    <div class="col-md-6">
                        <form method="GET" action="{{route('project.agileboard',$project->id)}}">
                            <div class="row form-inline">
                                {{--<div id="field_place" class="form-group">--}}
                                {{--<label style="margin-left: 16px;">Fecha desde</label>--}}
                                {{--<input type="text" id="date_init" class="form-control" name="date_init">--}}
                                {{--</div>--}}
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <label class="btn btn-default">
                                            <b> Fecha desde</b>
                                        </label>
                                    </div>
                                    <input type="text" id="date_init" class="form-control" name="date_init" value="{{old('date_init')}}">
                                    <div class="input-group-btn">
                                        <label class="btn btn-default">
                                            <b>Hasta</b>
                                        </label>
                                    </div>
                                    <input type="text" id="date_finish" class="form-control" name="date_finish" value="{{old('date_finish')}}">
                                    <span class="input-group-btn">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>

                                            </button>
                                        </span>
                                </div>

                            </div>
                        </form>
                    </div>
                    <div class="col-md-3">
                        <form method="GET" action="{{route('project.agileboard',$project->id)}}">
                            <div class="row form-inline">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <label class="btn btn-default">
                                            <b> Titulo</b>
                                        </label>
                                    </div>
                                    <input type="text"  class="form-control" name="title" value="{{old('title')}}">
                                    <span class="input-group-btn">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>

                                            </button>
                                        </span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="btn-group">
                        <a href="{{ route('tasks.project.create',[$area->name,$project->id]) }}"
                           class="btn  btn-primary">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                            Crear tarea
                        </a>
                        <a href="{{route('areas.show',$area->name)}}"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           class="btn  btn-default">
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                            Volver
                        </a>
                    </div>
                </div>

            </div>
            <div class="ibox-tools pull-right hidden-md hidden-lg hidden-sm">
                <div class="btn-group">

                    <a href="{{ route('tasks.project.create',[$area->name,$project->id]) }}"
                       class="btn btn-xs  btn-primary">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        Crear tarea
                    </a>
                    <a href="{{route('areas.show',$area->name)}}"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       class="btn btn-xs   btn-default">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                </div>
            </div>
        </div>
        <div class="ibox-title">

            <div class="row">
                <div class="col-lg-4">
                    <div class="ibox">
                        <div class="ibox-content" style="background-color: #f3f3f4 !important;">
                            <h3>Por hacer</h3>
                            {{--<p class="small"><i class="fa fa-hand-o-up"></i> Drag task between list</p>--}}
                            <ul class="sortable-list connectList agile-list" id="todo">
                                @foreach($informados as $informado)
                                    <li data-id="{{ $informado->id }}" class="task warning-element"
                                        id="{{$informado->id}}">
                                        <a style="cursor: pointer;color: black;"
                                           href="{{ route('task.home.show',['task'=> $informado->id]) }}">#{{$informado->id}}
                                            - {{$informado->title}}</a>

                                        <div class="agile-detail">

                                            @can('denegatetask',$informado)
                                                <a data-id="{{$informado->id}}"
                                                   class="pull-right btn btn-xs btn-danger denegado">Denegar</a>
                                            @endcan
                                            @can('processtask',$informado)

                                                <a data-id="{{ $informado->id }}"
                                                   class="pull-right btn btn-xs btn-info start-task">En proceso</a>
                                            @endcan
                                            <i class="fa fa-clock-o"></i> {{$informado->created_at->format('d/m/Y H:i A')}}
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="ibox">
                        <div class="ibox-content" style="background-color: #f3f3f4 !important;">
                            <h3>En progreso</h3>
                            {{--<p class="small"><i class="fa fa-hand-o-up"></i> Drag task between list</p>--}}
                            <ul class="sortable-list connectList agile-list" id="inprogress">
                                @foreach($procesos as $proceso)
                                    <li class="task info-element" id="{{$proceso->id}}">
                                        <a style="cursor: pointer;color: black;"
                                           href="{{ route('task.home.show',['task'=> $proceso->id]) }}">#{{$proceso->id}}
                                            - {{$proceso->title}}</a>
                                        <div class="agile-detail">
                                            @can('finishtask',$proceso)
                                                <a data-id="{{ $proceso->id }}"
                                                   class="pull-right btn btn-xs btn-primary finish-task">Finalizar</a>
                                            @endcan
                                            <i class="fa fa-clock-o"></i> {{$proceso->created_at->format('d/m/Y H:i A')}}
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="ibox">
                        <div class="ibox-content" style="background-color: #f3f3f4 !important;">
                            <h3>Finalizado</h3>
                            {{--<p class="small"><i class="fa fa-hand-o-up"></i> Drag task between list</p>--}}
                            <ul class="sortable-list connectList agile-list" id="completed">
                                @foreach($finalizados as $finalizado)

                                    <li class="task success-element"
                                        id="{{$finalizado->id}}">
                                        <a style="cursor: pointer;color: black;"
                                           href="{{ route('task.home.show',['task'=> $finalizado->id]) }}">#{{$finalizado->id}}
                                            - {{$finalizado->title}}</a>
                                        <div class="agile-detail">
                                            @can('pingeholetask',$finalizado)
                                                <a data-id="{{ $finalizado->id }}"
                                                   class="pull-right btn btn-xs btn-warning pigeonhole">Archivar</a>
                                            @endcan
                                            <i class="fa fa-clock-o"></i> {{$finalizado->created_at->format('d/m/Y H:i A')}}
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection

@push('scripts')
@routes
<script src="{{ asset('plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('plugins/moment/es.js') }}"></script>

<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();

        $('.finish-task').on('click', function () {

            var id = $(this).data('id');
            var url = route('task.finishtask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.data.status == 200) {
                        swal({
                            title: "Tarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else if (res.data.status == 405) {
                        window.location.reload();

                    }
                    else {
                        swal({
                            title: "Tarea no modificada!",
                            text: 'Tiene subtareas pendientes a finalizar',
                            showConfirmButton: false,
                            type: 'error',
                            timer: 3000
                        });
                    }
                });
        });

        $('.init-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.inittask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {

                    if (res.status == 200) {
                        swal({
                            title: "Tarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });
        $('.start-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.processtask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({
                            title: "Tarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });

        $('.pigeonhole').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.pigeonhole', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({
                            title: "Tarea archivada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });

        $('.denegado').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.denegatetask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({
                            title: "Tarea denegada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });
        $('#date_init').datetimepicker({
            format: "L",
            locale: 'es',

        });
        $('#date_finish').datetimepicker({
            format: "L",
            locale: 'es',
        });
    });


</script>
@endpush

