@extends('layouts.app')

@section('content')
    <div class="ibox float-e-margins">
        <div class="alert alert-info" role="alert"><p><i class="fa fa-info-circle"></i>
                <strong>Importante</strong></p>
            <p>
                En esta sección se lista las tareas del proyecto <strong>{{$project->name}}</strong>. En la tabla
                <strong>Tareas</strong>, en la esquina superior derecha el botón <strong>Crear tarea</strong>,
                permite crear tareas nueva para el proyecto. El botón
                <button type="button" class="btn btn-xs btn-default dropdown-toggle"
                        data-toggle="dropdown" aria-expanded="true">
                    <i class="fa fa-list" aria-hidden="true"></i>
                </button>
                , permite cambiar el estado de cada tarea. El botón
                <a href="#"
                   class="btn btn-xs btn-info">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                </a>, permite ver los detalles de la tarea. El botón
                <a href="#"
                   class="btn btn-xs btn-warning">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </a>, permite editar los datos de la tarea. El botón

                <button
                        class="btn btn-xs btn-danger">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                </button>
                , permite eliminar la tarea seleccionada.
            </p>
        </div>
        <div class="ibox-title" style="background-color: #1ab394;">

            <h5 style="color:white;">
                <a style="color:white;" href="{{route('areas.show',$area->name)}}">{{$area->name}}</a>  @if(!is_null($project))/ <a style="color:white;" href="{{route('project.agileboard',$project->id)}}">{{$project->name}}</a>  @endif
            </h5>
        </div>
        <div class="ibox-title">
            <h5>
                Tareas

            </h5>
            <div class="ibox-tools hidden-xs ">
                <div class="btn-group">
                    <a href="{{ route('tasks.project.create',[$area->name,$project->id]) }}"
                       class="btn btn-primary">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>


                        @if(auth()->user()->hasPrensa() && auth()->user()->area->name !== 'Dirección' && (auth()->user()->isUser() || auth()->user()->isAdmin()) )
                            Proponer
                            tema @elseif( auth()->user()->area->name === 'Dirección' && $area->name === 'Prensa')
                            Proponer tema   @else Crear tarea @endif


                    </a>
                    <a href="{{route('areas.show',$area->name)}}"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       class="btn btn-default">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                </div>
            </div>
            <div class="ibox-tools hidden-md hidden-lg hidden-sm pull-right">
                <div class="btn-group">

                    <a href="{{ route('tasks.project.create',[$area->name,$project->id]) }}"
                       class="btn btn-xs btn-primary">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        Crear tarea
                    </a>
                    <a href="{{route('areas.show',$area->name)}}"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       class="btn btn-xs btn-default">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <table class="table table-hover"
                   data-toggle="table"
                   data-show-columns="true"
                   data-pagination="true"
                   data-search="true">
                <thead>
                <tr>
                    <th data-visible="false">ID</th>
                    <th>Nombre</th>
                    <th class="hidden-xs">Rango de fechas</th>
                    <th class="hidden-xs">Asignado</th>
                    <th class="hidden-xs">Informados</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tasks as $task)
                    <tr>
                        <td>{{ $task->id }}</td>
                        <td>#{{$task->id}} - {{ $task->title }}
                            <ul style="list-style-type: none;margin-left: 0px;text-align: center;    -webkit-padding-start: 0px;">
                                @if(!is_null($task->subtasks))
                                    @foreach($task->subtasks as $key => $subtask)
                                        <li><label class="badge"
                                                   style="background-color: {{$subtask->status->color}}; color: white;font-size: 8px;">{{$key +1}}</label>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </td>
                        <td>{{ $task->date_init->format('d/m/Y') }} - {{$task->date_finish->format('d/m/Y') }}</td>
                        {{--<td>{!! $task->description !!}</td>--}}
                        <td>
                            @if(!is_null($task->assigneds))
                                <ul style="list-style-type: none;margin-left: 0px;text-align: center; -webkit-padding-start: 0px;">
                                    @foreach($task->assigneds as $assigned)
                                        <li><label class="badge badge-primary">{{$assigned->name }}</label></li>
                                    @endforeach
                                </ul>
                            @endif
                        </td>
                        <td>
                            <ul style="list-style-type: none;margin-left: 0px;text-align: center;    -webkit-padding-start: 0px;">
                                @if(!is_null($task->informeds))
                                    @foreach($task->informeds as $informeds)
                                        <li>
                                            <label class="badge badge-info">{{$informeds->name}}</label>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </td>
                        <td>
                            @if(!is_null($task->status))
                                <label class="badge"
                                       style="background-color: {{$task->status->color}}; color: white;">{{$task->status->name}}</label>
                            @endif
                        </td>

                        <td>
                            <div class="btn-group" style="display: flex;">
                                <a href="{{ route('task.project.show',['area'=>$area->name,'project'=>$project->id, 'task'=> $task->id]) }}"
                                   class="btn btn-info"
                                   title="ver tarea">
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                <a href="{{ route('task.project.edit',['area'=>$area->name,'project'=>$project->id, 'task'=> $task->id]) }}"
                                   class="btn btn-warning"
                                   title="Editar tarea">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>

                                <button data-id="{{ $task->id }}" title="Eliminar tarea"
                                        class="btn btn-destroy btn-danger">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                <div class="btn-group show-on-hover">
                                    <button type="button" class="btn btn-default dropdown-toggle"
                                            data-toggle="dropdown" aria-expanded="true">
                                        <i class="fa fa-list" aria-hidden="true"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu1">

                                        <li>
                                            <a href="#" data-id="{{$task->id}}" class="init-task">
                                                <i class="fa fa-info" aria-hidden="true"></i>
                                                Informado
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" data-id="{{$task->id}}" class="start-task">
                                                <i class="fa fa-tasks" aria-hidden="true"></i>
                                                En proceso
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="finish-task" data-id="{{$task->id}}">
                                                <i class="fa fa-flag-checkered" aria-hidden="true"></i>
                                                Finalizar
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="pigeonhole" data-id="{{$task->id}}">
                                                <i class="fa fa-file-archive-o" aria-hidden="true"></i>
                                                Archivar
                                            </a>
                                        </li>
                                        <li>

                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>
    </div>
@endsection

@push('scripts')
@routes

<script>
    $(document).ready(function () {

        $('.dropdown-toggle').dropdown();

        $('.btn-destroy').on('click', function () {
            var id = $(this).data('id');
            var url = route('tasks.destroy', {id: id});

            swal({
                title: "{{ "Confirmar" }}",
                text: "{{ "Una vez confirmada no es posible revertir esta acción" }}",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "{{ "Cancelar" }}",
                confirmButtonClass: "btn-warning",
                confirmButtonText: "{{ "Confirmar y borrar" }}",
                closeOnConfirm: !1
            }, function () {
                axios.delete(url, {data: {id: id}})
                    .then(function (res) {
                        if (res.status == 200) {
                            swal("Tarea eliminada!", '', 'success');
                            window.location.reload();
                        } else {
                            swal("{{ __("An error has ocurred") }}", '', 'error');
                        }
                    })
                    .catch(function (res) {
                        swal("No tiene los privilegios para realizar esta acción!", '', 'error');
                        window.location.reload();

                    });
            });
        });

        $('.finish-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.finishtask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.data.status == 200) {
                        swal({title: "Tarea modificada!", showConfirmButton: false, type: 'success', timer: 3000});
                        window.location.reload();

                    } else if (res.data.status == 405) {
                        window.location.reload();
                    }
                    else {
                        swal({
                            title: "Tarea no modificada!",
                            text: 'Tiene subtareas pendientes a finalizar',
                            showConfirmButton: false,
                            type: 'error',
                            timer: 3000
                        });
                    }
                });
        });

        $('.init-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.inittask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {

                    if (res.status == 200) {
                        swal({title: "Tarea modificada!", showConfirmButton: false, type: 'success', timer: 3000});
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });
        $('.start-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.processtask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({title: "Tarea modificada!", showConfirmButton: false, type: 'success', timer: 3000});
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });

        $('.pigeonhole').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.pigeonhole', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({title: "Tarea archivada!", showConfirmButton: false, type: 'success', timer: 3000});
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });


    })
    ;
</script>
@endpush

