@extends('layouts.app')

@section('content')
    <div class="alert alert-info" role="alert"><p><i class="fa fa-info-circle"></i>
            <strong>Importante</strong></p>
        <p>En esta sección se muestra la lista de proyectos y visualizar las tareas que contiene cada proyecto. Si es
            coordinador de área tendrá acceso para crear, modificar y eliminar proyectos.
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                Proyectos
            </h5>
            <div class="ibox-tools">
                <div class="btn-group hidden-xs">

                    @admin
                    <a href="{{route('projects.createmain',$area)}}" class="btn btn-primary">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        Crear proyecto
                    </a>
                    @endadmin
                    <a class="btn btn-default  pull-right"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       @if(auth()->user()->hasArea('Dirección')) href="{{ route('direccion.index') }}"
                       @else href="{{route('home')}}" @endif><i class="fa fa-arrow-circle-left"></i>Volver</a>

                </div>
                <div class="btn-group hidden-md hidden-lg hidden-sm">

                    @admin
                    <a href="{{route('projects.createmain',$area)}}" class="btn btn-xs btn-primary">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        Crear proyecto
                    </a>
                    @endadmin
                    <a class="btn btn-xs btn-default  pull-right"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       @if(auth()->user()->hasArea('Dirección')) href="{{ route('direccion.index') }}"
                       @else href="{{route('home')}}" @endif><i class="fa fa-arrow-circle-left"></i>Volver</a>

                </div>
            </div>
        </div>
        <div class="ibox-content">
            <table class="table table-hover table-responsive"
                   data-toggle="table"
                   data-show-columns="true"
                   data-pagination="true"
                   data-search="true">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Creador</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($projects as $project)
                    <tr>
                        <td>{{ $project->name }}</td>
                        <td>@if(!is_null($project->user)) {{$project->user->name}} @endif</td>
                        <td>
                            @if(!is_null($project->area))
                                <div class="btn-group">

                                    <a href="{{ route('project.agileboard',$project->id) }}"
                                       class="btn btn-primary" title="Ver tablero ágil">
                                        <i class="fa fa-table" aria-hidden="true"></i>
                                    </a>
                                    <a href="{{ route('projects.show',['area'=>$project->area->name,'project'=>$project->id]) }}"
                                       class="btn btn-info"
                                       title="Ver Tareas">
                                        <i class="fa fa-list-alt" aria-hidden="true"></i>
                                    </a>
                                </div>

                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{--<div class="text-center">--}}
            {{--@if(!is_null($projects))--}}
            {{--{{ $projects->links() }}--}}
            {{--@endif--}}
            {{--</div>--}}
        </div>
    </div>
@endsection

@push('scripts')
@routes

<script>
    $(document).ready(function () {


        $('.dropdown-toggle').dropdown();
        $('.table-responsive').on('show.bs.dropdown', function (e) {
            $(e.relatedTarget).next('div[aria-labelledby="dropdownMenuButton"]').appendTo("body");
        });

        $('body').on('hide.bs.dropdown', function (e) {
            $(this).find('div[aria-labelledby="dropdownMenuButton"]').appendTo($(e.relatedTarget).parent());
        });


        $('.btn-destroy').on('click', function () {
            var id = $(this).data('id');
            var url = route('projects.destroy', {id: id});
            swal({
                title: 'Confirmar',
                text: "Una vez confirmada no es posible revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar y borrar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url, {data: {id: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Eliminado!',
                                    'El proyecto ha sido eliminado',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });

    });
</script>
@endpush