@extends('layouts.app')
@push('styles')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">

@endpush
@section('content')

    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Enviar mensajes
                </h5>
                <div class="ibox-tools">

                    <a href="{{route('messages.index')}}"
                       class="btn btn-default">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="panel-body">
                    <form method="POST" action="{{ route('messages.store') }}">
                        @csrf

                        {!! Field::select('users', $users,['name'=>'users[]','multiple','class'=>'js-states form-control']) !!}
                        {!! Field::text('subject', '') !!}
                        {!! Field::textarea('description',array('id' =>'summernote', 'class'=>'summernote form-control' )) !!}
                        @if($errors->has('description')) <p class="help-block" style="color: #a94442;">{{ $errors->first('description') }}</p> @endif
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary waves-effect waves-classic btn-block">
                                <i class="fa fa-send" aria-hidden="true"></i>
                                Enviar
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
@routes
<script src="{{ asset('plugins/select2/saaelect2.min.js') }}"></script>
<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>
<script src="{{ asset('plugins/summernote/lang/summernote-es-ES.js') }}"></script>

<script>
    $(document).ready(function () {
        $('.summernote').summernote({
            height: 300,
            lang: 'es-ES' // default: 'en-US'
        });
        $('.note-btn').removeAttr('title');

        $('.dropdown-toggle').dropdown();
        $('#users').select2();

    });
</script>
@endpush