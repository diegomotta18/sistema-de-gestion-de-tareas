@extends('layouts.app')

@section('content')
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="ibox-tools">
                    <h5>Detalle del mensaje</h5>
                    <a href="{{route('messages.index')}}" class="btn btn-xs btn-default">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                </div>
                <div class="row" style="margin: 10px;">
                    <small>Asunto</small>
                    <small class="pull-right">Enviado por <strong>{{$message->user->name}} </strong>{{$message->created_at}}</small>

                </div>
                <div class="row" style="margin: 10px;">
                    <h5>
                        <strong>{{$message->subject}}</strong>
                    </h5>

                </div>

            </div>


            <div class="ibox-content">

                <div class="row" style="margin: 10px;">
                    <small>Descripción</small>
                    <p></p>
                    {!! $message->description !!}

                </div>

            </div>
        </div>

@endsection

