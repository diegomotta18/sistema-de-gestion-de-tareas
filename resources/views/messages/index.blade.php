@extends('layouts.app')
@section('content')
    <div class="col-xs-12">


        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Mensajes
                </h5>
                <div class="ibox-tools">
                    <a href="{{ route('messages.create') }}"
                       class="btn btn-xs btn-primary">
                        <i class="fa fa-send" aria-hidden="true"></i>
                        redactar mensaje
                    </a>
                    <a href="{{route('messages.index')}}"
                       class="btn btn-xs btn-default">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <table class="table table-hover"
                       data-toggle="table"
                       data-show-columns="true"
                       data-pagination="false"
                       data-search="true">
                    <thead>
                    <tr>
                        <th data-visible="false">ID</th>
                        <th>Asunto</th>
                        <th>Usuario</th>
                        <th>Fecha</th>

                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!is_null($notifications))
                        @foreach($notifications as $notification)
                            <tr class='clickable-row {{$notification->unread() ? 'active': ''}}'
                                data-href='{{$notification->data['link']}}'>
                                <td>{{$notification->id}}></td>

                                <td><strong>{{$notification->data['title']}}
                                        : </strong>{{$notification->data['subject']}}</td>
                                <td>{{$notification->data['user']}}</td>
                                <td>{{$notification->data['date']}}</td>

                                <td class="text-center">
                                    @if($notification->unread())
                                        <div class="btn-group">
                                            <button data-id="{{ $notification->id }}" title="leer mensaje"
                                                    class="btn btn-xs btn-read btn-primary">
                                                <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                            </button>
                                            @endif
                                            <button data-id="{{ $notification->id }}" title="Quitar mensaje"
                                                    class="btn btn-xs btn-destroy btn-danger">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection

@push('scripts')
@routes

<script>

    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();

        $('.btn-destroy').on('click', function () {
            var id = $(this).data('id');
            var url = route('messages.destroy', {message: id});

            swal({
                title: "{{ "Confirmar" }}",
                text: "{{ "Una vez confirmada no es posible revertir esta acción" }}",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "{{ "Cancelar" }}",
                confirmButtonClass: "btn-warning",
                confirmButtonText: "{{ "Confirmar y borrar" }}",
                closeOnConfirm: !1
            }, function () {
                axios.delete(url)
                    .then(function (res) {
                        if (res.status == 200) {
                            swal("Mensaje eliminado!", '', 'success');
                            window.location.reload();
                        } else {
                            swal("{{ __("An error has ocurred") }}", '', 'error');
                        }
                    });
            });
        });

        $('.btn-read').on('click', function () {
            var id = $(this).data('id');
            var url = route('messages.read', {message: id});

            axios.put(url)
                .then(function (res) {
                    if (res.status == 200) {
                        swal("Mensaje fue leido!", '', 'success');
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });

        });

        $(".clickable-row").click(function () {
            window.location = $(this).data("href");
        });
    });
</script>
@endpush