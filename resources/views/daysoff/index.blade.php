@extends('layouts.app')
@push('styles')
<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">

@endpush
@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="ibox float-e-margins">
                <div class="alert alert-info" role="alert"><p><i class="fa fa-info-circle"></i>
                        <strong>Importante</strong></p>
                    <p>En esta sección se visualiza todas los francos asignados a usuarios </p>
                </div>

                <div class="ibox-title">
                    <h5 >
                        Francos

                    </h5>
                    <div class="ibox-tools hidden-xs">
                        <div class="btn-group">
                            <a
                               href="{{ route('freedays.create') }}" class="btn btn-primary ">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                Crear franco
                            </a>
                            <a class="btn btn-default pull-right"
                               style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                               @if(auth()->user()->hasArea('Dirección')) href="{{route('direccion.index')}}"  @else href="{{route('home')}}" @endif><i class="fa fa-arrow-circle-left"></i>Volver</a>
                        </div>
                    </div>
                    <div class="ibox-tools  pull-right hidden-sm hidden-md hidden-lg">
                        <div class="btn-group">
                            <a
                               href="{{ route('freedays.create') }}" class="btn btn-xs btn-primary ">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                Crear franco
                            </a>
                            <a class="btn btn-xs btn-default   pull-right"
                               style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                               @if(auth()->user()->hasArea('Dirección')) href="{{route('direccion.index')}}"  @else href="{{route('home')}}" @endif><i class="fa fa-arrow-circle-left"></i>Volver</a>
                        </div>
                    </div>

                </div>
                <div class="ibox-title">

                    <div class="ibox-tools">
                        <form class="form-inline" method="GET" action="{{route('freedays.index')}}">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <label class="btn btn-default">
                                        <b> Fecha desde</b>
                                    </label>
                                </div>
                                <input type="text" id="date_init" class="form-control" name="date_init"
                                       value="{{old('date_init')}}">
                                <div class="input-group-btn">
                                    <label class="btn btn-default">
                                        <b>Hasta</b>
                                    </label>
                                </div>
                                <input type="text" id="date_finish" class="form-control" name="date_finish"
                                       value="{{old('date_finish')}}">
                                <span class="input-group-btn">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>

                                            </button>
                                        </span>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-btn">
                                    <label class="btn btn-default">
                                        <b> Usuario</b>
                                    </label>
                                </div>
                                <select name="user" id="" class="form-control">
                                    <option value="" selected="selected">Seleccionar usuario
                                    </option>@foreach($users as $key=> $user  )
                                        <option value="{{$key}}" {{ (old("user") == $key ? "selected":"") }}>{{$user}}</option>@endforeach
                                </select>
                                <span class="input-group-btn">
                            <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>

                            </button>
                          <a href="{{route('licenses.index')}}" style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;" class="btn btn-default " type="submit"><i class="fa fa-refresh"></i></a>
                        </span>

                            </div>
                        </form>

                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-hover table-bordered table-striped table-responsive">
                        <thead>
                        <tr class="text-center">
                            <th class="text-center">Usuario</th>
                            <th class="text-center">Fecha</th>
                            <th class="text-center">Observaciones</th>
                            <th class="text-center">Acciones</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($daysoff as $dayoff)
                            <tr>
                                <td>{{ $dayoff->user->name }}</td>
                                <td>{{$dayoff->date_taken->format('d/m/Y')}}</td>
                                <td>{!! $dayoff->observations !!}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{route('freedays.edit',$dayoff->id)}}" data-toggle="tooltip"
                                           class="btn btn-warning">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>
                                        <button data-id="{{ $dayoff->id }}" data-toggle="tooltip"
                                                class="btn btn-danger  btn-destroy">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
@routes
<script src="{{ asset('plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('plugins/moment/es.js') }}"></script>
<script>

    $(document).ready(function () {


        $('.dropdown-toggle').dropdown();

        $('.btn-destroy').on('click', function () {
            var id = $(this).data('id');
            var url = route('freedays.destroy', {dayoff: id});

            swal({
                title: 'Confirmar',
                text: "Una vez confirmada no es posible revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar y borrar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url, {data: {dayoff: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Eliminado!',
                                    'El franco ha sido eliminado',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });

        $('#date_init').datetimepicker({
            format: "L",
            locale: 'es',

        });
        $('#date_finish').datetimepicker({
            format: "L",
            locale: 'es',
        });
    });
</script>
@endpush