<div class="col-md-6">


    {!! Field::select('user', $users, isset($dayoff->user) ? $dayoff->user->id : '' ,['name'=>'user','class'=>'js-states form-control','empty' => ('Seleccionar usuario')]) !!}

</div>

<div class="col-md-6">

    <div id="field_place" class="form-group">
        <label class="{{$errors->has('date_taken')? 'text-danger' : '' }}" style="margin-left: 16px;">Fecha de franco</label>

        <input type="hidden" id="date_taken" name="date_taken" value="{{isset($dayoff) ? $dayoff->date_taken->format('d/m/Y'):'' }}">
        @if($errors->has('date_init')) <p class="help-block"
                                          style="color: #a94442;">{{ $errors->first('date_taken') }}</p> @endif
    </div>

</div>
<div class="col-md-12">

    <div class="form-group @if($errors->has('observations')) has-error @endif">

        <label class="control-label form-group @if($errors->has('observations')) text-danger @endif">Observaciones</label>
        <textarea id="summernote" class="summernote form-control"
                  name="observations">{{isset($dayoff) ? $dayoff->observations : old('observations')}}</textarea>
        @if($errors->has('observations')) <p class="">{{ $errors->first('observations') }}</p> @endif
    </div>
</div>

<button type="submit" class="btn btn-primary waves-effect waves-classic">
    <i class="fa fa-send" aria-hidden="true"></i>
    Aceptar
</button>