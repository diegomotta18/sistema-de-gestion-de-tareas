@extends('layouts.app')
@push('styles')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">

@endpush
@section('content')
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Editar franco
                </h5>
                <div class="ibox-tools">
                    <div class="btn-group">
                        <a style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           href="{{route('freedays.index')}}"
                           class="btn btn-default hidden-xs pull-right">
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                            Volver
                        </a>
                        <a style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           href="{{route('freedays.index')}}"
                           class="btn btn-xs btn-default hidden-md hidden-sm hidden-lg">
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                            Volver
                        </a>
                    </div>
                </div>
            </div>

            <div class="ibox-content">
                <form method="POST" action="{{ route('freedays.update', $dayoff->id) }}">
                    @csrf
                    @method('PUT')
                    @include('daysoff.partials.form')
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ asset('plugins/select2/saaelect2.min.js') }}"></script>
<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('plugins/summernote/lang/summernote-es-ES.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('plugins/moment/es.js') }}"></script>
<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();
        $('.dropdown-toggle').dropdown();


        $('#user').select2();
        $('.summernote').summernote({
            height: 200,
            lang: 'es-ES', // default: 'en-US'
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['link']]

            ]
        });

        $('#date_taken').datetimepicker({
            inline: true,
            locale: 'es',
            format: "L",
            useCurrent: false,
        });
    });
</script>
@endpush