@extends('layouts.app')
@push('styles')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">

@endpush
@section('content')
    <div class="  animated fadeInRight">

        <div class="ibox ">
            @if(!auth()->user()->hasPrensa())

                <button id="collapseb" class="btn btn-info" type="button" data-toggle="collapse"
                        data-target="#collapseExample"
                        aria-expanded="false" aria-controls="collapseExample">
                    <i class="fa fa-info-circle"></i> <strong>Importante</strong>
                </button>
            @endif
            @if(!is_null($area))
                <div class="widget style1 navy-bg">
                    <div class="row">

                        <h2><b>{{$area->name}}</b>

                            <div class="btn-group pull-right">
                                <a class="btn btn-default"
                                   style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                                   href="{{route('direccion.index')}} "><i
                                            class="fa fa-arrow-circle-left"></i>Volver</a>
                            </div>
                        </h2>

                    </div>
                </div>
            @endif
            @if(!auth()->user()->hasPrensa() )

                <div class="collapse" id="collapseExample" aria-expanded="false">
                    <br>
                    <div class="alert alert-info" role="alert">
                        <p><i class="fa fa-info-circle"></i>
                            <strong>Importante
                                <button class="pull-right btn btn-danger btn-xs"><i class="fa fa-close"></i></button>
                            </strong></p>
                        <p>En esta sección, se lista todos los proyectos y todas la tareas a las que un usuario es
                            asignado
                            como también las ultimas novedades. La acciones que se puede realizar son:
                        </p>
                        <p><i class="fa fa-check-circle"></i> Desde el bóton <strong>Crear tarea</strong>, se puede
                            crear
                            nuevas tareas.</p>
                        <p><i class="fa fa-check-circle"></i> Desde el bóton <strong>Crear proyecto</strong>, se puede
                            crear
                            nuevos proyectos.</p>
                        <p><i class="fa fa-check-circle"></i> En la solapa "Proyectos" se visualiza todos los proyectos
                            en
                            que un usuario participa.</p>
                        <p><i class="fa fa-check-circle"></i> En la solapa "Todas las tareas" se visualiza todas la
                            tareas
                            asignadas al usuario ya sea <label class="badge badge-info"> Informado</label>,<label
                                    class="badge badge-primary"> En proceso</label> y <label class="badge badge-danger">
                                Finalizado</label>.
                        </p>
                        <p><i class="fa fa-check-circle"></i>
                            El botón
                            <button type="button" class="btn btn-xs btn-default dropdown-toggle"
                                    data-toggle="dropdown" aria-expanded="true">
                                <i class="fa fa-list" aria-hidden="true"></i>
                            </button>
                            , permite cambiar el estado de cada tarea. El botón
                            <a href="#"
                               class="btn btn-xs btn-info">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                            </a>, permite ver los detalles de la tarea. El botón
                            <a href="#"
                               class="btn btn-xs btn-warning">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>, permite editar los datos de la tarea. El botón

                            <button
                                    class="btn btn-xs btn-danger">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                            , permite eliminar la tarea seleccionada.
                        </p>
                    </div>
                </div>

            @endif
            @if(auth()->user()->hasPrensa() && auth()->user()->isUser())
                <div class="alert alert-info hidden-xs" role="alert">
                    <p><i class="fa fa-info-circle"></i>
                        <strong>Importante
                            <button class="pull-right btn btn-danger btn-xs"><i class="fa fa-close"></i></button>
                        </strong></p>
                    <p>En esta sección, se visualiza las propuestas creadas por el usuario como también temas que
                        propone a realizar. La acciones que se puede realizar son:
                    </p>
                    <p><i class="fa fa-check-circle"></i> Desde el bóton <strong>Proponer tema</strong>, se puede
                        crear nuevos temas.</p>
                    <p><i class="fa fa-check-circle"></i> Desde el bóton <strong>Coberturas</strong>, se puede
                        visualizar las coberturas asignadas que tiene cada usuario del área de prensa.</p>
                    <p><i class="fa fa-check-circle"></i> Desde el bóton <strong>Propuestas</strong>, se puede
                        visualizar las propuestas del dia. Las propuestas no tiene usuarios asignados.</p>
                    <p><i class="fa fa-check-circle"></i> Desde el boton <strong>Proyectos</strong> se visualiza todos
                        los proyectos
                        en que un usuario del área de prensa participa. El coordinador del área de prensa visualiza
                        todos los proyectos.</p>
                    <p><i class="fa fa-check-circle"></i> Desde el boton <strong>Crear proyecto</strong> el usuario
                        coordinador puede crear nuevos proyectos.</p>
                    <p><i class="fa fa-check-circle"></i> En el listado que visualiza en este sección los temas pueden
                        tener los siguientes cambios de estados:
                        <label class="badge badge-info"> Informado</label>,<label class="badge badge-warning">
                            Respondido</label>,<label
                                class="badge badge-primary"> En proceso</label> y <label class="badge badge-danger">
                            Finalizado</label>.
                    </p>
                    <p><i class="fa fa-check-circle"></i>
                        En cada fila muestra los datos que contiene un tema y abajo de cada fila hay botones de
                        acciones, donde cada botón ejecuta una acción distinta. El botón
                        <button type="button" class="btn btn-xs btn-default dropdown-toggle"
                                data-toggle="dropdown" aria-expanded="true">
                            <i class="fa fa-list" aria-hidden="true"></i>
                        </button>
                        , permite cambiar el estado de cada tarea. El botón
                        <a href="#"
                           class="btn btn-xs btn-info">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>, permite ver los detalles de la tarea. El botón
                        <a href="#"
                           class="btn btn-xs btn-warning">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </a>, permite editar los datos de la tarea. El botón

                        <button
                                class="btn btn-xs btn-danger">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </button>
                        , permite eliminar la tarea seleccionada. El botón
                        <a href="#"
                           class="btn btn-xs btn-default">
                            <i class="fa fa-link" aria-hidden="true"></i>
                        </a>, permite agregar la url de la noticia. El botón
                        <a href="#" style="background-color: rgb(49, 112, 143);color: white;"
                           class="btn btn-xs btn-default">
                            <i class="fa fa-share" aria-hidden="true"></i>
                        </a>, permite ir a la url de la noticia.


                    </p>
                    <p><i class="fa fa-check-circle"></i>
                        Si no visualiza los botones
                        <button
                                class="btn btn-xs btn-danger">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </button>
                        <a href="#"
                           class="btn btn-xs btn-warning">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </a>, es porque no tiene los permisos para realizar acciones sobre esa tarea. Consultar con el
                        <strong>Creador</strong> de la tarea.
                    </p>
                </div>
            @endif
        </div>
        <div class="row">

            <div class="@if((auth()->user()->isAdmin()) || auth()->user()->isGod()))  col-md-12 @else col-md-9 @endif col-xs-12">
                <div class="ibox ">
                    <div class="ibox-content" style="margin-top: -20px;">
                        <div class="row">
                            <h2 class="hidden-xs">
                                <div class="btn-group"
                                     style="margin: auto;display: flex;flex-direction: row;justify-content: center;">
                                    <a href="{{route('projects.index',$area)}}" class="btn btn-default"><i
                                                class="fa fa-bookmark"></i> Proyectos</a>
                                    @prensa

                                    <a href="{{route('proposeds.index')}}" class="btn"
                                       style="background-color:#31708f;color: white;"><i
                                                class="fa fa-bookmark"></i> Propuestas</a>

                                    <a href="{{route('events.index')}}" class="btn btn-info"><i
                                                class="fa fa-bookmark"></i> Coberturas</a>

                                    @endprensa
                                    @direccion
                                    @if(!is_null($area) && $area->name == 'Prensa' && !auth()->user()->isGod())
                                        <a href="{{route('proposeds.index')}}" class="btn"
                                           style="background-color:#31708f;color: white;"><i
                                                    class="fa fa-bookmark"></i> Propuestas</a>

                                        <a href="{{route('events.index')}}" class="btn btn-info"><i
                                                    class="fa fa-bookmark"></i> Coberturas</a>
                                    @endif
                                    @enddireccion
                                    <a href="{{route('tasks.maincreate',$area)}}" class="btn btn-primary"><i
                                                class="fa fa-plus-circle"></i> @if(auth()->user()->hasPrensa() && auth()->user()->area->name !== 'Dirección' && (auth()->user()->isUser() || auth()->user()->isAdmin()) )
                                            Proponer
                                            tema @else Crear tarea @endif</a>
                                    @if(auth()->user()->isAdmin() || auth()->user()->isGod())
                                        <a href="{{route('projects.createmain',$area)}}" class="btn btn-default"><i
                                                    class="fa fa-list-alt"></i> Crear
                                            proyecto</a>
                                    @endif
                                </div>
                            </h2>
                            <h2 class=" hidden-md hidden-lg hidden-sm">
                                <div class="btn-group"
                                     style="margin: auto;display: flex;flex-direction: row;justify-content: center;">
                                    <a href="{{route('projects.index',$area)}}" class="btn btn-xs btn-default"><i
                                                class="fa fa-bookmark"></i> Proyectos</a>
                                    @prensa

                                    <a href="{{route('proposeds.index')}}" class="btn btn-xs"
                                       style="background-color:#31708f;color: white;"><i
                                                class="fa fa-bookmark"></i> Propuestas</a>
                                    @endprensa
                                    @direccion
                                    @if(!is_null($area) && $area->name == 'Prensa' && !auth()->user()->isGod())

                                        <a href="{{route('proposeds.index')}}" class="btn btn-xs"
                                           style="background-color:#31708f;color: white;"><i
                                                    class="fa fa-bookmark"></i> Propuestas</a>
                                    @endif
                                    @enddireccion
                                </div>
                            </h2>
                            <h2 class=" hidden-md hidden-lg hidden-sm">
                                <div class="btn-group"
                                     style="margin: auto;display: flex;flex-direction: row;justify-content: center;">
                                    @direccion
                                    @if(!is_null($area) && $area->name == 'Prensa' && !auth()->user()->isGod())

                                        <a href="{{route('events.index')}}" class="btn btn-xs btn-info"><i
                                                    class="fa fa-bookmark"></i> Coberturas</a>
                                    @endif
                                    @enddireccion
                                    @prensa
                                    <a href="{{route('events.index')}}" class="btn btn-xs btn-info"><i
                                                class="fa fa-bookmark"></i> Coberturas</a>
                                    @endprensa
                                    <a href="{{route('tasks.maincreate',$area)}}" class="btn btn-xs btn-primary"><i
                                                class="fa fa-plus-circle"></i> @if(auth()->user()->hasPrensa() && auth()->user()->area->name !== 'Dirección' && (auth()->user()->isUser() || auth()->user()->isAdmin()) )
                                            Proponer
                                            tema @elseif( auth()->user()->area->name === 'Dirección' && $area->name === 'Prensa')
                                            Proponer tema   @else Crear tarea @endif</a>
                                    @if(auth()->user()->isAdmin() || auth()->user()->isGod())
                                        <a href="{{route('projects.createmain',$area)}}" class="btn btn-xs btn-default"><i
                                                    class="fa fa-list-alt"></i> Crear
                                            proyecto</a>
                                    @endif

                                </div>
                            </h2>
                        </div>
                        @if(!is_null($tasks))

                            <div class="form-group">
                                <form action="{{route('taskhome.index',$area)}}" method="GET">
                                    @if(auth()->user()->isGod() ||auth()->user()->isAdmin())
                                        <div class="row">
                                            <div class="col-md-6">
                                                {{--{!! Field::select('Buscar por area', $areas ,['id'=>'areas','name'=>'area_search','class'=>'js-states form-control','empty' => ('-- Seleccionar area --')]) !!}--}}
                                                <label for="areas">
                                                    Buscar por area
                                                </label>
                                                <select @if(!is_null($area)) disabled
                                                        @endif id='areas' name='area_search'
                                                        class="form-control">
                                                    <option hidden="" value="">--Seleccionar
                                                        área--
                                                    </option>
                                                    @foreach ($areas as $key => $value)
                                                        <option value="{{ $key }}"
                                                                @if(!is_null($area))
                                                                @if ($area->name == $value)
                                                                selected="selected"
                                                                @endif
                                                                @endif
                                                        >{{ $value }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">

                                                {!! Field::select('Buscar por usuario asignado', $users ,['id'=>'users','name'=>'user_search','class'=>'js-states form-control','empty' => ('-- Seleccionar usuario --')]) !!}

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="label-control">Estado de la
                                                    tarea</label>
                                                <div class="form-group">

                                                    <select class="form-control" name="status">
                                                        <option value="" selected="">--
                                                            Seleccionar
                                                            estado
                                                            --
                                                        </option>

                                                        <option value="Informado">Informado
                                                        </option>
                                                        <option value="Finalizado">Finalizado
                                                        </option>
                                                        <option value="En proceso">En proceso
                                                        </option>
                                                        <option value="Demorado">Demorado
                                                        </option>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">

                                                <label class="control-label">Buscar por nombre
                                                    de
                                                    tarea</label>
                                                <div class="form-group">

                                                    <input type="text" class="form-control"
                                                           name="searchTaskAll"
                                                           placeholder="Buscar por nombre de la tarea">

                                                </div><!-- /input-group -->
                                            </div>
                                        </div>
                                        <div class="row">


                                            <div class="col-md-12 text-center">
                                                <label class="label-control">Acción</label>
                                                <div class="form-group">

                                                    <div class="btn-group"
                                                         style="text-align: center !important;">
                                                        <button class="btn btn-primary"
                                                                type="submit"><i
                                                                    class="fa fa-filter"></i>
                                                            Buscar
                                                        </button>
                                                        <a href="{{route('home')}}"
                                                           class="btn btn-default "
                                                           type="submit"><i
                                                                    class="fa fa-refresh"></i>Refrescar</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @elseif(auth()->user()->isUser())
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="label-control">Estado de la
                                                    tarea</label>
                                                <div class="form-group">

                                                    <select class="form-control" name="status">
                                                        <option value="" selected="">--
                                                            Seleccionar
                                                            estado
                                                            --
                                                        </option>

                                                        <option value="Informado">Informado
                                                        </option>
                                                        <option value="Finalizado">Finalizado
                                                        </option>
                                                        <option value="En proceso">En proceso
                                                        </option>
                                                        <option value="Demorado">Demorado
                                                        </option>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">

                                                <label class="control-label">Buscar por nombre
                                                    de
                                                    tarea</label>
                                                <div class="form-group">

                                                    <input type="text" class="form-control"
                                                           name="searchTaskAll"
                                                           placeholder="Buscar por nombre de la tarea">

                                                </div><!-- /input-group -->
                                            </div>
                                            <div class="col-md-4">
                                                <label class="label-control">Acción</label>
                                                <div class="form-group">

                                                    <div class="btn-group"
                                                         style="display: flex;">
                                                        <button class="btn btn-primary"
                                                                type="submit"><i
                                                                    class="fa fa-filter"></i>
                                                            Aplicar
                                                        </button>
                                                        <a href="{{route('home')}}"
                                                           class="btn btn-default "
                                                           type="submit"><i
                                                                    class="fa fa-refresh"></i>Refrescar</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </form>

                            </div>
                            @if(!is_null($count))
                                @if(is_null($status))
                                    <div class="alert alert-warning text-center">Total de
                                        tareas <b>{{$count}}</b></div>
                                @elseif(!is_null($status) && $status == 'En proceso' )
                                    <div class="alert alert-warning text-center">Total de
                                        tareas <b>{{$count}}</b></div>
                                @elseif(!is_null($status) && $status == 'Informado' )
                                    <div class="alert alert-info text-center">Total de
                                        tareas <b>{{$count}}</b></div>
                                @elseif(!is_null($status) && $status == 'Demorado' )
                                    <div class="alert alert-danger text-center">Total de
                                        tareas <b>{{$count}}</b></div>
                                @elseif(!is_null($status) && $status == 'Finalizado' )
                                    <div class="alert alert-success text-center">Total de
                                        tareas <b>{{$count}}</b></div>
                                @endif
                            @endif
                            <table class="table table-hover table-bordered table-striped table-responsive">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th class="hidden-xs">Proyecto</th>
                                    <th class="hidden-xs">Creador</th>
                                    <th class="hidden-xs">Asignado</th>
                                    <th class="hidden-xs">Fecha de inicio
                                    </th>
                                    <th class="hidden-xs">Fecha de finalización
                                    </th>
                                    <th>Estado</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!is_null($tasks))
                                    @foreach($tasks as $task)

                                        <tr @if($task->isExpired())style="background-color: #f2dede; border-color: #ebccd1; border-style: solid;border-width: 3px;" @endif>
                                            <td>{{ $task->title }}</td>
                                            <td class="hidden-xs">@if(isset($task->project)){{$task->project->name}}@else
                                                    <label class="badge badge-warning"> Sin
                                                        asignar </label> @endif
                                            </td>
                                            <td class="hidden-xs">
                                                @if(isset($task->user))
                                                    <label class="badge badge-info">{{$task->user->name}}</label>
                                                @endif
                                            </td>
                                            <td class="hidden-xs">
                                                @if($task->assigneds->isEmpty())
                                                    <label class="badge badge-primary"> Sin
                                                        asignar </label>
                                                @else
                                                    @foreach($task->assigneds as $assigned)
                                                        <label class="badge badge-primary"> {{$assigned->name}} </label>
                                                    @endforeach
                                                @endif
                                            </td>

                                            <td class="hidden-xs">{{ $task->date_init->format('d/m/Y')}}</td>
                                            <td class="hidden-xs">{{$task->date_finish->format('d/m/Y')}}</td>
                                            <td>
                                                @if(!is_null($task->status))
                                                    <label class="label"
                                                           style="background-color: {{$task->status->color}}; color: white;">{{$task->status->name}}</label>
                                                @endif
                                                @if($task->isExpired())
                                                    <label class="label label-danger"
                                                           style="color: white;">Demorado</label>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td @if(auth()->user()->hasPrensa())  colspan="8"
                                                @else colspan="7"
                                                @endif  @if($task->isExpired())style="background-color: #f2dede; border-color: #ebccd1; border-style: solid;border-width: 3px;" @endif>
                                                <div class="btn-group"
                                                     style="margin: auto;display: flex;flex-direction: row;justify-content: center;">
                                                    @direccion

                                                    @if(!auth()->user()->isGod() && !is_null($task->urlnotice))
                                                        <a href="{{$task->urlnotice}}"
                                                           data-toggle="tooltip"
                                                           title="Ir a la url de la noticia "
                                                           class="btn btn-default">
                                                            <i class="fa fa-share"
                                                               aria-hidden="true"></i>
                                                        </a>
                                                    @endif
                                                    @enddireccion
                                                    @prensa
                                                    @if(!is_null($task->urlnotice))
                                                        <a href="{{$task->urlnotice}}"
                                                           data-toggle="tooltip"
                                                           title="Ir a la url de la noticia "
                                                           class="btn btn-default">
                                                            <i class="fa fa-share"
                                                               aria-hidden="true"></i>
                                                        </a>
                                                    @endif
                                                    @endprensa
                                                    @prensa
                                                    <a href="{{route('task.changeUrl', $task->id)}}"
                                                       data-toggle="tooltip"
                                                       title="Agregar url "
                                                       class="btn"
                                                       style="background-color: rgb(49, 112, 143);color: white;">
                                                        <i class="fa fa-link"
                                                           aria-hidden="true"></i>
                                                    </a>
                                                    @endprensa

                                                    @can('show', $task)
                                                        @if(!is_null($task->parent_id))

                                                            <a href="{{ route('task.home.show',['task'=> $task->id]) }}"
                                                               class="btn btn-info"
                                                               title="Ver tarea">
                                                                <i class="fa fa-eye"
                                                                   aria-hidden="true"></i>
                                                            </a>
                                                        @else
                                                            @if(!is_null($task->area) && !is_null($task->project))
                                                                <a href="{{ route('subtasks.show',['area'=>$task->area->name,'project'=>$task->project->id, 'task'=> $task->task_id,'subtask' => $task->id]) }}"
                                                                   class="btn btn-info"
                                                                   title="Ver tarea">
                                                                    <i class="fa fa-eye"
                                                                       aria-hidden="true"></i>
                                                                </a>
                                                            @endif
                                                        @endif
                                                    @endcan
                                                    @can('update', $task)
                                                        @if(!is_null($task->project) && !is_null($task->area))

                                                            <a href="{{ route('task.project.edit',['area' => $task->area->name, 'project'=>$task->project_id,'task'=> $task->id]) }}"
                                                               class="btn btn-warning"
                                                               title="Editar tarea">
                                                                <i class="fa fa-pencil-square-o"
                                                                   aria-hidden="true"></i>
                                                            </a>
                                                        @elseif(!is_null($task->area))
                                                            <a href="{{ route('task.area.edit',['area' => $task->area->name,'task'=> $task->id]) }}"
                                                               class="btn btn-warning"
                                                               title="Editar tarea">
                                                                <i class="fa fa-pencil-square-o"
                                                                   aria-hidden="true"></i>
                                                            </a>

                                                        @endif
                                                    @endcan
                                                    @can('destroy', $task)

                                                        <button data-id="{{ $task->id }}"
                                                                title="Eliminar tarea"
                                                                class="btn  btn-destroy btn-danger">
                                                            <i class="fa fa-trash-o"
                                                               aria-hidden="true"></i>
                                                        </button>
                                                    @endcan
                                                    <div class="btn-group show-on-hover">
                                                        <button type="button"
                                                                style="margin-top: -1px;height: 32px;"
                                                                class="btn btn-default dropdown-toggle"
                                                                data-toggle="dropdown"
                                                                aria-expanded="true">
                                                            <i class="fa fa-list"
                                                               aria-hidden="true"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right"
                                                            aria-labelledby="dropdownMenu1">

                                                            <li>
                                                                <a href="#"
                                                                   data-id="{{$task->id}}"
                                                                   class="init-task">
                                                                    <i class="fa fa-info"
                                                                       aria-hidden="true"></i>
                                                                    Informado
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#"
                                                                   data-id="{{$task->id}}"
                                                                   class="reply-task">
                                                                    <i class="fa fa-send"
                                                                       aria-hidden="true"></i>
                                                                    Respondido
                                                                </a>
                                                            </li>
                                                            @can('processtask', $task)
                                                                <li>
                                                                    <a href="#"
                                                                       data-id="{{$task->id}}"
                                                                       class="start-task">
                                                                        <i class="fa fa-tasks"
                                                                           aria-hidden="true"></i>
                                                                        En proceso
                                                                    </a>
                                                                </li>
                                                            @endcan
                                                            @can('finishtask', $task)
                                                                <li>
                                                                    <a href="#"
                                                                       class="finish-task"
                                                                       data-id="{{$task->id}}">
                                                                        <i class="fa fa-flag-checkered"
                                                                           aria-hidden="true"></i>
                                                                        Finalizar
                                                                    </a>
                                                                </li>
                                                            @endcan
                                                            @can('destroy', $task)
                                                                <li>
                                                                    <a href="#"
                                                                       class="pigeonhole"
                                                                       data-id="{{$task->id}}">
                                                                        <i class="fa fa-file-archive-o"
                                                                           aria-hidden="true"></i>
                                                                        Archivar
                                                                    </a>
                                                                </li>
                                                            @endcan
                                                            {{--@endif--}}

                                                        </ul>
                                                    </div>
                                                </div>


                                            </td>

                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            <div class="text-center">
                                @if(!is_null($tasks))
                                    {{ $tasks->links() }}
                                @endif
                            </div>

                    </div>

                </div>
                @endif
            </div>
            @novedad
            <div class="col-md-3 col-xs-12" style="margin-top: -20px;">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>
                            Novedades
                        </h5>
                    </div>
                    @if(!is_null($notices))
                        <div class="ibox-content">
                            <ul style="list-style-type: none;margin-left: -50px !important;">
                                @foreach($notices as $notice)
                                    <li class="well">
                                        <a style="color:#676a6c;" href="{{route('notices.show',$notice->id)}}">

                                            <small><i class="fa fa-calendar"></i> {{ $notice->getDateTime() }}
                                            </small>

                                            <h3><i class="fa fa-info-circle"></i><b> {{ $notice->title }}</b></h3>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                @endforeach
                                <li>
                                    <div class="text-center">
                                        <a class="btn btn-block btn-info" href="{{route('notices.index')}}">
                                            <i class="fa fa-newspaper-o"></i> <strong>Ver todas las
                                                novedades</strong>
                                        </a>
                                    </div>
                                </li>
                            </ul>

                        </div>
                    @endif
                </div>
            </div>
            @endnovedad
        </div>
    </div>



@endsection
@push('scripts')
@routes

<script src="{{ asset('plugins/select2/saaelect2.min.js') }}"></script>
<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>

<script>

    $(document).ready(function () {

        $('#users').select2();

        $(".collapse").on('click', function () {
            if ($(".collapse").hasClass('collapse in')) {
                $('.collapse').collapse('hide');

            }

        });
        $('.dropdown-toggle').dropdown();

        $('.btn-destroy').on('click', function () {
            var id = $(this).data('id');
            var url = route('tasks.destroy', {id: id});

            swal({
                title: "{{ "Confirmar" }}",
                text: "{{ "Una vez confirmada no es posible revertir esta acción" }}",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "{{ "Cancelar" }}",
                confirmButtonClass: "btn-warning",
                confirmButtonText: "{{ "Confirmar y borrar" }}",
                closeOnConfirm: !1
            }, function () {
                axios.delete(url, {data: {id: id}})
                    .then(function (res) {
                        console.log(res);
                        if (res.status === 200) {
                            swal("Tarea eliminada!", '', 'success');
                            window.location.reload();
                        } else {
                            swal("{{ __("An error has ocurred") }}", '', 'error');
                        }
                    });
            });
        });

        $('.finish-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.finishtask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.data.status == 200) {
                        swal({
                            title: "Tarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else if (res.data.status == 405) {
                        window.location.reload();

                    }
                    else {
                        swal({
                            title: "Tarea no modificada!",
                            text: 'Tiene subtareas pendientes a finalizar',
                            showConfirmButton: false,
                            type: 'error',
                            timer: 3000
                        });
                    }
                });
        });

        $('.init-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.inittask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {

                    if (res.status == 200) {
                        swal({
                            title: "Tarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });
        $('.start-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.processtask', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({
                            title: "Tarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });

        $('.reply-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.replytask', {id: id});
            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({
                            title: "Tarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });


        $('.pigeonhole').on('click', function () {
            var id = $(this).data('id');
            var url = route('task.pigeonhole', {id: id});

            axios.put(url, {data: {id: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({
                            title: "Tarea archivada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });

        $(".clickable-row").click(function () {
            window.location = $(this).data("href");
        });


    });
</script>
@endpush