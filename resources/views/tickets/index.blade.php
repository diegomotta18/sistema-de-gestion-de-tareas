@extends('layouts.app')

@section('content')
    <div class="alert alert-info" role="alert"><p><i class="fa fa-info-circle"></i>
            <strong>Importante</strong></p>
        <p>En esta sección se muestra la lista de errores de reportados al área de <b>Sistemas</b>. Para reportar un
            error de sistema hacer click en el botón <b>Crear ticket</b>. </p>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                Reporte de errores
            </h5>
            <div class="ibox-tools hidden-xs">
                <div class="btn-group">
                    <a href="{{route('tickets.create')}}" style="color: white;" class="btn btn-primary"><i
                                class="fa fa-plus"></i> Crear ticket</a>
                    <div class="btn-group">
                        <a class="btn btn-default"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           @if(auth()->user()->hasArea('Dirección')) href="{{route('direccion.index')}}"  @else href="{{route('home')}}" @endif><i class="fa fa-arrow-circle-left"></i>Volver</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="ibox-title  hidden-md hidden-sm hidden-lg">
            <div class="ibox-tools hidden-md hidden-sm hidden-lg pull-right">
                <div class="btn-group">
                    <a href="{{route('tickets.create')}}" style="color: white;" class="btn btn-xs btn-primary"><i
                                class="fa fa-plus"></i> Crear ticket</a>
                    <div class="btn-group">
                        <a class="btn btn-xs btn-default"
                           style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                           @if(auth()->user()->hasArea('Dirección')) href="{{route('direccion.index')}}"  @else href="{{route('home')}}" @endif><i class="fa fa-arrow-circle-left"></i>Volver</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="ibox-content">

            <table class="table table-hover table-bordered table-striped table-responsive">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th class="hidden-xs">Creador</th>
                    <th class="hidden-xs">Fecha de reporte
                    </th>
                    <th>Estado</th>
                </tr>
                </thead>
                <tbody>
                @if(!is_null($tickets))
                    @foreach($tickets as $ticket)

                        <tr>
                            <td>{{ $ticket->title }}</td>

                            <td class="hidden-xs">
                                @if(isset($ticket->user))
                                    <label class="badge badge-info">{{$ticket->user->name}}</label>
                                @endif
                            </td>
                            <td  class="hidden-xs">
                                {{$ticket->created_at->format('d/m/Y')}}
                            </td>
                            <td>
                                @if(!is_null($ticket->status))
                                    <label class="label"
                                           style="background-color: {{$ticket->status->color}}; color: white;">{{$ticket->status->name}}</label>
                                @endif
                            </td>

                        </tr>
                        <tr>
                            <td colspan="4">
                                <div class="btn-group"
                                     style="margin: auto;display: flex;flex-direction: row;justify-content: center;">

                                    @can('show', $ticket)

                                        <a href="{{ route('tickets.show', $ticket->id) }}"
                                           class="btn btn-info"
                                           title="Ver ticket">
                                            <i class="fa fa-eye"
                                               aria-hidden="true"></i>
                                        </a>

                                    @endcan
                                    @can('update', $ticket)
                                        <a href="{{ route('tickets.edit',$ticket->id) }}"
                                           class="btn btn-warning"
                                           title="Editar ticket">
                                            <i class="fa fa-pencil-square-o"
                                               aria-hidden="true"></i>
                                        </a>
                                    @endcan
                                    @if(auth()->user()->hasAreaUnique('Sistemas'))
                                        @can('destroy', $ticket)

                                            <button data-id="{{ $ticket->id }}"
                                                    title="Eliminar ticket"
                                                    class="btn  btn-destroy btn-danger">
                                                <i class="fa fa-trash-o"
                                                   aria-hidden="true"></i>
                                            </button>
                                        @endcan
                                        <div class="btn-group show-on-hover">
                                            <button type="button"
                                                    style="margin-top: -1px;height: 32px;"
                                                    class="btn btn-default dropdown-toggle"
                                                    data-toggle="dropdown"
                                                    aria-expanded="true">
                                                <i class="fa fa-list"
                                                   aria-hidden="true"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right"
                                                aria-labelledby="dropdownMenu1">

                                                <li>
                                                    <a href="#"
                                                       data-id="{{$ticket->id}}"
                                                       class="init-task">
                                                        <i class="fa fa-info"
                                                           aria-hidden="true"></i>
                                                        Informado
                                                    </a>
                                                </li>

                                                @can('processtask', $ticket)
                                                    <li>
                                                        <a href="#"
                                                           data-id="{{$ticket->id}}"
                                                           class="start-task">
                                                            <i class="fa fa-tasks"
                                                               aria-hidden="true"></i>
                                                            En proceso
                                                        </a>
                                                    </li>
                                                @endcan
                                                @can('denegatetask', $ticket)

                                                <li>
                                                    <a href="#" data-id="{{$ticket->id}}" class="denegado">
                                                        <i class="fa fa-close" aria-hidden="true"></i>
                                                        Denegado
                                                    </a>
                                                </li>
                                                @endcan
                                                @can('finishtask', $ticket)
                                                    <li>
                                                        <a href="#"
                                                           class="finish-task"
                                                           data-id="{{$ticket->id}}">
                                                            <i class="fa fa-flag-checkered"
                                                               aria-hidden="true"></i>
                                                            Finalizar
                                                        </a>
                                                    </li>
                                                @endcan
                                                @can('destroy', $ticket)
                                                    <li>
                                                        <a href="#"
                                                           class="pigeonhole"
                                                           data-id="{{$ticket->id}}">
                                                            <i class="fa fa-file-archive-o"
                                                               aria-hidden="true"></i>
                                                            Archivar
                                                        </a>
                                                    </li>
                                                @endcan
                                            </ul>
                                        </div>
                                    @endif
                                </div>

                            </td>

                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            <div class="text-center">
                @if(!is_null($tickets))
                    {{ $tickets->links() }}
                @endif
            </div>
        </div>
    </div>


@endsection
@push('scripts')
@routes

<script src="{{ asset('plugins/select2/saaelect2.min.js') }}"></script>
<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>

<script>

    $(document).ready(function () {

        $('#users').select2();

        $(".collapse").on('click', function () {
            if ($(".collapse").hasClass('collapse in')) {
                $('.collapse').collapse('hide');

            }

        });
        $('.dropdown-toggle').dropdown();

        $('.btn-destroy').on('click', function () {
            var id = $(this).data('id');
            var url = route('tickets.delete', {ticket: id});

            swal({
                title: 'Confirmar',
                text: "Una vez confirmada no es posible revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar y borrar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url, {data: {ticket: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Eliminado!',
                                    'La tarea ha sido eliminada',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });


        $('.finish-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('tickets.finish', {ticket: id});

            axios.put(url, {data: {ticket: id}})
                .then(function (res) {
                    if (res.data.status == 200) {
                        swal({
                            title: "Tarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else if (res.data.status == 405) {
                        window.location.reload();

                    }
                    else {
                        swal({
                            title: "Tarea no modificada!",
                            text: 'Tiene subtareas pendientes a finalizar',
                            showConfirmButton: false,
                            type: 'error',
                            timer: 3000
                        });
                    }
                });
        });

        $('.init-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('tickets.init', {ticket: id});

            axios.put(url, {data: {ticket: id}})
                .then(function (res) {

                    if (res.status == 200) {
                        swal({
                            title: "Tarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });
        $('.start-task').on('click', function () {
            var id = $(this).data('id');
            var url = route('tickets.inprocess', {ticket: id});

            axios.put(url, {data: {ticket: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({
                            title: "Tarea modificada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });


        $('.pigeonhole').on('click', function () {
            var id = $(this).data('id');
            var url = route('tickets.pigeonhole', {ticket: id});

            axios.put(url, {data: {ticket: id}})
                .then(function (res) {
                    if (res.status == 200) {
                        swal({
                            title: "Tarea archivada!",
                            showConfirmButton: false,
                            type: 'success',
                            timer: 3000
                        });
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });

        $(".clickable-row").click(function () {
            window.location = $(this).data("href");
        });


    });
</script>
@endpush