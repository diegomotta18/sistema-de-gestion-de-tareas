@extends('layouts.app')

@push('styles')
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">

@endpush
@section('content')

    <div class="ibox">
        <div class="ibox-title">
            <h5>
                Reportar un error
            </h5>
            <div class="ibox-tools hidden-xs">
                <div class="btn-group">
                    @if(!auth()->user()->hasAreaUnique('Sistemas'))
                        <a class="btn btn-primary hidden-xs"

                           href="{{route('tickets.index')}}"><i class="fa fa-list"></i> Mis Tickets</a>
                        <a class="btn btn-xs btn-primary hidden-md hidden-sm hidden-lg"
                           href="{{route('tickets.index')}}"><i class="fa fa-list"></i>Mis Tickets</a>
                    @endif
                    <a class="btn btn-default hidden-xs"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       @if(auth()->user()->hasArea('Dirección')) href="{{ route('direccion.index') }}"
                       @elseif( auth()->user()->hasArea('Sistemas')) href="{{route('tickets.index')}}"
                       @else  href="{{route('home')}}" @endif><i class="fa fa-arrow-circle-left"></i>Volver</a>

                </div>
            </div>
            <div class="ibox-tools hidden-md hidden-lg hidden-sm">
                <div class="btn-group">
                    @if(!auth()->user()->hasAreaUnique('Sistemas'))
                        <a class="btn btn-xs btn-primary hidden-xs"

                           href="{{route('tickets.index')}}"><i class="fa fa-list"></i> Mis Tickets</a>
                        <a class="btn btn-xs btn-primary hidden-md hidden-sm hidden-lg"
                           href="{{route('tickets.index')}}"><i class="fa fa-list"></i>Mis Tickets</a>
                    @endif
                    <a class="btn btn-xs btn-default hidden-xs"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       @if(auth()->user()->hasArea('Dirección')) href="{{ route('direccion.index') }}"
                       @elseif( auth()->user()->hasArea('Sistemas')) href="{{route('tickets.index')}}"
                       @else  href="{{route('home')}}" @endif><i class="fa fa-arrow-circle-left"></i>Volver</a>
                </div>
            </div>
        </div>

        <div class="ibox-content">

            <form method="POST" enctype="multipart/form-data" action="{{ route('tickets.store') }}">
                @csrf
                @if(!auth()->user()->hasAreaUnique('Sistemas'))
                    @include('tickets.partials.form')
                @else
                    @include('tickets.partials.formAdmin')

                @endif
            </form>
        </div>
    </div>
@endsection

@push('scripts')
@routes
<script src="{{ asset('plugins/select2/saaelect2.min.js') }}"></script>
<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('plugins/summernote/lang/summernote-es-ES.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('plugins/moment/es.js') }}"></script>


<script>
    $(document).ready(function () {


        $('.summernote').summernote({
            height: 200,
            lang: 'es-ES', // default: 'en-US'
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['link']]

            ]
        });
        $('.note-btn').removeAttr('title');
        $('#area').select2();
        $('#project').select2();
        $('#replacements').select2();
        $('#informeds').select2();

        $('#assigneds').select2();
        $('#users_show').select2();
        $('.dropdown-toggle').dropdown();
        $("#files").fileinput({
            dropZoneEnabled: false,
            theme: 'explorer-fa',
            language: "es",
            uploadUrl: "/file-upload-single",
            validateInitialCount: true,
            initialPreviewAsData: true,
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: false,
            maxFileCount: 5,
            showUpload: false,
//            allowedFileTypes: ["image", "video"],
            allowedFileExtensions: ["jpg", "png", "pdf", "jpeg", "xlsx", "gif", "docx"]

        });

    });


</script>
@endpush