<div class="col-md-12">
    {!! Field::text('title', isset($ticket) ? $ticket->title : '') !!}

</div>
<div class="col-md-12">

    <div class="form-group @if($errors->has('description')) has-error @endif">

        <label class="control-label form-group @if($errors->has('description')) text-danger @endif">Descripción</label>
        <div class="alert alert-warning" role="alert"><p><i class="fa fa-info-circle"></i>
                <strong style="font-size: 20px;">Importante</strong></p>
            <p style="font-size: 20px;"> Escriba una explicación detallada <b>paso a paso</b> del error que se está generando y <b>adjunte un captura de pantalla</b> del error, de esta manera podemos entender por donde empezar a solucionar el error.  </p>
        <p> <br><b>Ejemplo</b>: Al ingresar a "Proponer tema "/ "Crear tarea", en el formulario de carga de la tarea, cuando hago click en el botón "Guardar", me muestra este error: adjunto captura de pantalla.</p>
        </div>
        <textarea id="summernote" class="summernote form-control"
                  name="description">{{isset($ticket) ? $ticket->description : old('description')}}</textarea>
        {{--{!! Field::textarea('description', isset($task) ? $task->description : '',array('id' =>'summernote', 'class'=>'summernote form-control' )) !!}--}}
        @if($errors->has('description')) <p class="">{{ $errors->first('description') }}</p> @endif
    </div>
</div>
<div class="col-md-12">

    <div class="form-group">
        <label>Archivos</label>
        <input id="files" type="file" name="files[]" multiple/>
    </div>
</div>


<div class="form-group">

    <button type="submit" style="margin-left: 15px !important;"
            class="btn btn-primary waves-effect waves-classic btn-save">
        <i class="fa fa-save" aria-hidden="true"></i>
        Guardar
    </button>
</div>