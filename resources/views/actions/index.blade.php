@extends('layouts.app')
@push('styles')
<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">

@endpush
@section('content')
    <div class="alert alert-info" role="alert"><p><i class="fa fa-info-circle"></i>
            <strong>Importante</strong></p>
        <p style="text-align: justify;">Esta sección permite visualizar en el listado las acciones diarias que realiza el usuario en su actividad.
            El usuario se encarga de ingresar la hora de inicio, hora de finalización,
            la fecha del dia y una descripción de la actividad que realizó en la franja horaria ingresada en el formulario de carga que se accede desde <strong>Crear acción</strong>.</p>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                Acciones diarias
            </h5>

            <div class="ibox-tools">
                <div class="btn-group">
                    <a href="{{ route('actions.create') }}" class="btn btn-primary pull-right">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        Crear acción
                    </a>
                </div>
            </div>
        </div>
        <div class="ibox-title">

            <div class="ibox-tools">
                <form class="form-inline" method="GET" action="{{route('actions.index')}}">
                    <div class="input-group">
                        <div class="input-group-btn">
                            <label class="btn btn-default">
                                <b> Fecha desde</b>
                            </label>
                        </div>
                        <input type="text" id="date_init" class="form-control" name="date_init"
                               value="{{old('date_init')}}">
                        <div class="input-group-btn">
                            <label class="btn btn-default">
                                <b>Hasta</b>
                            </label>
                        </div>
                        <input type="text" id="date_finish" class="form-control" name="date_finish"
                               value="{{old('date_finish')}}">
                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>

                                            </button>
                                        </span>
                    </div>
                    @if(auth()->user()->isGod() || auth()->user()->isAdmin())
                        <div class="input-group mb-3">
                            <div class="input-group-btn">
                                <label class="btn btn-default">
                                    <b> Usuario</b>
                                </label>
                            </div>
                            <select name="user" id="" class="form-control">
                                <option value="" selected="selected">Seleccionar usuario
                                </option>@foreach($users as $key=> $user  )
                                    <option value="{{$key}}" {{ (old("user") == $key ? "selected":"") }}>{{$user}}</option>@endforeach
                            </select>
                            <span class="input-group-btn">
                            <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>

                            </button>
                          <a href="{{route('licenses.index')}}"
                             style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                             class="btn btn-default " type="submit"><i class="fa fa-refresh"></i></a>
                        </span>
                        </div>
                    @endif
                </form>

            </div>
        </div>

        <div class="ibox-content">
            <table id="table-holidays" class="table table-hover table-bordered table-striped table-responsive">
                <thead>
                <tr>
                    <th>Usuario</th>
                    <th>Hora de inicio</th>
                    <th>Hora de finalización</th>
                    <th>Descripción</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($actions as $action)
                    <tr>
                        <td>@if(!is_null($action->user)){{ $action->user->name }}@endif</td>
                        <td>@if(!is_null($action->time_init)){{ $action->time_init }} @endif</td>
                        <td>@if(!is_null($action->time_finish)){{ $action->time_finish }}@endif</td>
                        <td>{!! $action->description !!}</td>
                        <td>
                            <div class="btn-group" style="display: flex;text-align: center">

                                <a href="{{ route('actions.edit', $action->id) }}" class="btn btn-warning"
                                   title="Editar acción">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>

                                <button data-id="{{ $action->id }}" title="Eliminar acción"
                                        class="btn btn-destroy btn-danger">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
@routes
<script src="{{ asset('plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('plugins/moment/es.js') }}"></script>
<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();



        $('.btn-destroy').on('click', function () {
            var id = $(this).data('id');
            var url = route('actions.destroy', {action: id});

            swal({
                title: 'Confirmar',
                text: "Una vez confirmada no es posible revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar y borrar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url, {data: {action: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Eliminado!',
                                    'La acción ha sido eliminada',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });

    });
    $('#date_init').datetimepicker({
        format: "L",
        locale: 'es',

    });

    $('#date_finish').datetimepicker({
        format: "L",
        locale: 'es',
    });
</script>
@endpush