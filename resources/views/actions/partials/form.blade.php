<div class='col-md-4'>
    <div class="form-group">
        <label class="{{$errors->has('time_init')? 'text-danger' : '' }}"
               style="margin-left: 16px;">Hora de inicio</label>

        <input type="hidden" id="time_init" name="time_init"
               value="{{isset($action) ? $action->time_init:old('time_init') }}">
        @if($errors->has('time_init')) <p class="help-block"
                                     style="color: #a94442;">{{ $errors->first('time_init') }}</p> @endif
    </div>
</div>
<div class='col-md-4'>
    <div class="form-group">
        <label class="{{$errors->has('time_finish')? 'text-danger' : '' }}"
               style="margin-left: 16px;">Hora de finalización</label>

        <input type="hidden" id="time_finish" name="time_finish"
               value="{{isset($action) ? $action->time_finish:old('time_finish') }}">
        @if($errors->has('time_finish')) <p class="help-block"
                                          style="color: #a94442;">{{ $errors->first('time_finish') }}</p> @endif
    </div>
</div>
<div class="col-md-4">

    <div id="field_place" class="form-group">
        <label class="{{$errors->has('date')? 'text-danger' : '' }}" style="margin-left: 16px;">Fecha</label>

        <input type="hidden" id="date" name="date" value="{{isset($action) ? $action->date->format('d/m/Y'):old('date') }}">
        @if($errors->has('date')) <p class="help-block"
                                          style="color: #a94442;">{{ $errors->first('date') }}</p> @endif
    </div>


</div>

<div class="col-md-12">

    <div class="form-group @if($errors->has('description')) has-error @endif">

        <label class="control-label form-group @if($errors->has('description')) text-danger @endif">Descripción</label>
        <textarea id="summernote" class="summernote form-control"
                  name="description">{{isset($action) ? $action->description : old('description')}}</textarea>
        @if($errors->has('description')) <p class="">{{ $errors->first('description') }}</p> @endif
    </div>
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary waves-effect waves-classic" style="margin-left: 15px!important;">
        <i class="fa fa-save" aria-hidden="true"></i>
        Guardar
    </button>

</div>
