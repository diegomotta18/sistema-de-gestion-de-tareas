@extends('layouts.app')
@push('styles')
@endpush
@section('content')
<div class="wrapper wrapper-content animated fadeInRight">

    @foreach($areas->chunk(3) as $a)
        <div class="row">
            @foreach($a as $area)
                <div class="col-lg-4">
                    <div class="contact-box">
                        <a class="row" href="{{route('direccion.area',$area)}}">
                            <div class="col-4">
                                <div class="text-center">
                                    <i class="fa {{$area->icon}} fa-5x"></i>
                                    <div class="m-t-xs font-bold">{{$area->name}}</div>
                                </div>
                            </div>

                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    @endforeach

</div>

@endsection
@push('scripts')
@routes
<script>
    $('.dropdown-toggle').dropdown();

</script>
@endpush