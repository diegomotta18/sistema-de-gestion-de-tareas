@extends('layouts.app')
@section('content')

    <div class="col-xs-12">
        <div class="alert alert-info" role="alert"><p><i class="fa fa-info-circle"></i>
                <strong>Importante</strong></p>
            <p>
                En esta sección se visualiza las notificaciones de las novedades, las tareas asignadas, las tareas en
                proceso, las tareas finalizadas y las tareas informadas.
            </p>
        </div>
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Notificaciones
                </h5>
            </div>
            <div class="ibox-content">
                <div class="btn-group show-on-hover pull-right" style="margin-top: 10px;">
                    <button type="button" class="btn btn-default dropdown-toggle"
                            data-toggle="dropdown" aria-expanded="true">
                        <i class="fa fa-plus" aria-hidden="true"></i> Acciones
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">

                        <li>
                            <a class="btn" id="btn-all-read">
                                <i class="fa fa-check-circle" aria-hidden="true"></i>
                                Marcar todo como leido
                            </a>
                        </li>

                        <li>
                            <a class="btn" id="btn-all-destroy">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                                Eliminar todas las notificaciones
                            </a>
                        </li>
                    </ul>
                </div>
                <table class="table table-hover"
                       data-toggle="table"
                       data-show-columns="false"
                       data-pagination="true"
                       data-search="true">

                    <thead>

                    <tr>

                        <th data-visible="false">ID</th>
                        <th>Asunto</th>
                        <th>Fecha</th>
                        <th>Usuario</th>

                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!is_null($notifications))
                        @foreach($notifications as $notification)
                            <tr>
                                <td>{{$notification->id}}></td>
                                <td>{{$notification->data['message']}}</td>
                                <td>{{Illuminate\Support\Carbon::parse($notification->created_at)->format('d/m/Y h:m')}}</td>
                                <td>{{$notification->data['user']}}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{$notification->data['link']}}" title="ver detalle" class="btn btn-info">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>
                                        @if($notification->unread())
                                            <button data-id="{{ $notification->id }}" title="leer mensaje"
                                                    class="btn btn-read btn-primary">
                                                <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                            </button>
                                        @endif
                                        <button data-id="{{ $notification->id }}" title="Quitar mensaje"
                                                class="btn btn-destroy btn-danger">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </button>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
@routes

<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();

        $('.btn-destroy').on('click', function () {
            var id = $(this).data('id');
            var url = route('notifications.destroy', {notification: id});

            swal({
                title: "{{ "Confirmar" }}",
                text: "{{ "Una vez confirmada no es posible revertir esta acción" }}",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "{{ "Cancelar" }}",
                confirmButtonClass: "btn-warning",
                confirmButtonText: "{{ "Confirmar y borrar" }}",
                closeOnConfirm: !1
            }, function () {
                axios.delete(url)
                    .then(function (res) {
                        if (res.status == 200) {
                            swal("Mensaje eliminado!", '', 'success');
                            window.location.reload();
                        } else {
                            swal("{{ __("An error has ocurred") }}", '', 'error');
                        }
                    });
            });
        });

        $('#btn-all-read').on('click', function () {
            var url = route('notifications.allread');
            axios.put(url)
                .then(function (res) {
                    if (res.status == 200) {
                        swal("Todos los mensajes fueron leídos!", '', 'success');
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });


        $('#btn-all-destroy').on('click', function () {
            var url = route('notifications.destroyAll');
            axios.delete(url)
                .then(function (res) {
                    if (res.status == 200) {
                        swal("Todos los mensajes fueron eliminados!", '', 'success');
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });
        });

        $('.btn-read').on('click', function () {
            var id = $(this).data('id');
            var url = route('notifications.read', {notification: id});

            axios.put(url)
                .then(function (res) {
                    if (res.status == 200) {
                        swal("Mensaje fue leido!", '', 'success');
                        window.location.reload();
                    } else {
                        swal("{{ __("An error has ocurred") }}", '', 'error');
                    }
                });

        });

    });
</script>
@endpush