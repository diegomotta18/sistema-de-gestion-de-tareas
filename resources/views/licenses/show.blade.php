@extends('layouts.app')
@push('styles')

@endpush
@section('content')

    <div class="ibox float-e-margins">
        <div class="ibox-title" style="background-color:#1ab394;">
            <h5 style="color:white;">
                Detalle de la licencia de <b style="font-size: 20px;">{{$license->user->name}}</b>
            </h5>
            <div class="ibox-tools">
                <div class="btn-group">
                    <a href="{{route('licenses.index')}}"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       class="btn btn-default hidden-xs pull-right">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                    <a href="{{route('licenses.index')}}"
                       class="btn btn-xs btn-default hidden-md hidden-lg hidden-sm pull-right"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                </div>
            </div>

        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <h4>Fecha de inicio</h4>
                    <p>{{$license->date_init->format('d/m/Y')}}</p>
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <h4>Fecha de finalización</h4>
                    <p>{{$license->date_init->format('d/m/Y')}}</p>
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <h4>Fecha de reincoporación</h4>
                    <p>{{$license->date_reinstate->format('d/m/Y')}}</p>
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <h4>Tipo de licencia</h4>
                    <p>{{$license->typeLicense->name}}</p>
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <h4>Presento certificado</h4>
                    <p> @if($license->certifate) Si @else No @endif</p>
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <h4>Observaciones</h4>
                    @foreach($license->observations as $observation)
                        <div class="feed-element">
                            <div class="media-body">

                                <small style="font-size: 14px;"
                                       class="text-muted">{{$observation->created_at->format('d/m/Y')}}</small>
                                <div class="well" style="font-size: 14px;">{!! $observation->observations !!}</div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>

@endsection

@push('scripts')

<script>

    $(document).ready(function () {

        $('.dropdown-toggle').dropdown();

    });


</script>
@endpush