@extends('layouts.app')

@section('content')
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                Licencias finalizadas
            </h5>

            <div class="ibox-tools">
                <div class="btn-group">
                    <a href="{{route('licenses.index')}}"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;"
                       class="btn btn-default hidden-xs pull-right">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                    <a href="{{route('licenses.index')}}"
                       class="btn btn-xs btn-default hidden-md hidden-lg hidden-sm pull-right"
                       style="color: #333 !important;background-color: #fff !important;border-color: #ccc !important;">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                        Volver
                    </a>
                </div>
            </div>
        </div>

        <div class="ibox-content">
            <table id="table-licenses" class="table table-hover"
                   data-show-columns="true"
                   data-pagination="true"
                   data-search="true">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Inicio</th>
                    <th>Finalización</th>
                    <th>Reincorporación</th>
                    <th>Observaciones</th>
                    <th>Tipo de licencia</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($licenses as $license)
                    <tr>
                        <td>@if(!is_null($license->user)){{ $license->user->name }}@endif</td>
                        <td>@if(!is_null($license->date_init)){{ $license->date_init->format('d/m/Y') }} @endif</td>
                        <td>@if(!is_null($license->date_finish)){{ $license->date_finish->format('d/m/Y') }}@endif</td>
                        <td>@if(!is_null($license->date_reinstate)){{$license->date_reinstate->format('d/m/Y') }}@endif</td>
                        <th>@if(!is_null($license->observations)) {!! $license->observations !!}@else Sin observaciones @endif</th>
                        <td>@if(!is_null($license->typeLicense)){{ $license->typeLicense->name }}@endif</td>

                        <td>
                            <div class="btn-group" style="display: flex;text-align: center">
                                <a href="{{ route('licenses.show', $license->id) }}" class="btn btn-info"
                                   title="Ver licencia">
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                <a href="{{ route('licenses.edit', $license->id) }}" class="btn btn-warning"
                                   title="Editar licencia">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                                <button data-id="{{ $license->id }}" title="Iniciar licencia"
                                        class="btn btn-finish btn-primary">
                                    <i class="fa fa-power-off" aria-hidden="true"></i>
                                </button>
                                <button data-id="{{ $license->id }}" title="Eliminar licencia"
                                        class="btn btn-destroy btn-danger">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
@routes

<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();

        $('#table-licenses').on('click','.btn-destroy', function () {
            var id = $(this).data('id');
            var url = route('licenses.destroy', {license: id});

            swal({
                title: 'Confirmar',
                text: "Una vez confirmada no es posible revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar y borrar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url, {data: {license: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Eliminado!',
                                    'La licencia ha sido eliminad',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });

        $('#table-holidays').on('click','.btn-finish', function () {
            var id = $(this).data('id');
            var url = route('licenses.notfinish', {holiday: id});

            swal({
                title: 'Iniciar licencia',
                text: "",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Iniciar'
            }).then(function (result) {
                if (result.value) {
                    axios.put(url, {data: {holiday: id}})
                        .then(function (res) {
                            if (res.status === 200) {

                                swal(
                                    'Licencia iniciada!',
                                    'La licencia ha sido iniciada',
                                    'success'
                                );
                                window.location.reload();

                            } else {
                                swal("{{ __("An error has ocurred") }}", '', 'error');
                            }

                        });
                }
            });
        });
    });
</script>
@endpush