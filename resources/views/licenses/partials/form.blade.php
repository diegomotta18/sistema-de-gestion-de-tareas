<div class="col-md-6">


    {!! Field::select('user', $users, isset($license->user) ? $license->user->id : '' ,['name'=>'user','class'=>'js-states form-control','empty' => ('Seleccionar usuario')]) !!}

</div>
<div class="col-md-6">


    {!! Field::select('type_license', $typeLicenses, isset($license->typeLicense) ? $license->typeLicense->id : '' ,['name'=>'type','class'=>'js-states form-control','empty' => ('Seleccionar tipo de licencias')]) !!}

</div>

<div class="col-md-4">

    <div id="field_place" class="form-group">
        <label class="{{$errors->has('date_init')? 'text-danger' : '' }}" style="margin-left: 16px;">Fecha de
            inicio</label>

        <input type="hidden" id="date_init" name="date_init"
               value="{{isset($license) ? $license->date_init->format('d/m/Y'):old('date_init') }}">
        @if($errors->has('date_init')) <p class="help-block"
                                          style="color: #a94442;">{{ $errors->first('date_init') }}</p> @endif
    </div>


</div>
<div class="col-md-4">

    <div id="field_place" class="form-group">
        <label class="{{$errors->has('date_finish')? 'text-danger' : '' }}" style="margin-left: 16px;">Fecha de
            finalización</label>

        <input type="hidden" id="date_finish" name="date_finish"
               value="{{isset($license) ? $license->date_finish->format('d/m/Y'):old('date_finish')}}">
        @if($errors->has('date_finish')) <p class="help-block"
                                            style="color: #a94442;">{{ $errors->first('date_finish') }}</p> @endif
    </div>
</div>

<div class="col-md-4">

    <div id="field_place" class="form-group">
        <label class="{{$errors->has('date_reinstate')? 'text-danger' : old('date_reinstate') }}" style="margin-left: 16px;">Fecha de
            reincoporación</label>

        <input type="hidden" id="date_reinstate" name="date_reinstate"
               value="{{isset($license) ? $license->date_reinstate->format('d/m/Y'):''}}">
        @if($errors->has('date_reinstate')) <p class="help-block"
                                               style="color: #a94442;">{{ $errors->first('date_reinstate') }}</p> @endif
    </div>
</div>

<div class="col-md-12">
    @if(!isset($license))
    <div class="form-group @if($errors->has('observations')) has-error @endif">

        <label class="control-label form-group @if($errors->has('observations')) text-danger @endif">Observaciones</label>
        <textarea id="summernote" class="summernote form-control"
                  name="observations">{{old('observations')}}</textarea>
        @if($errors->has('observations')) <p class="">{{ $errors->first('observations') }}</p> @endif
    </div>
    @endif
    @if(isset($license->observations))
        <license-observation license="{{$license->id}}"></license-observation>
    @endif
</div>

@if(isset($license))
    <div class="col-md-12">
        {!! Field::checkbox('certificate', 'value', isset($license) ? $license->certificate :'' ) !!}
    </div>
    <div class="col-md-12">
        {!! Field::checkbox('finish_license', 'value', isset($license) ? $license->finish :'' ) !!}
    </div>
@endif

<div class="form-group">
    <button type="submit" class="btn btn-primary waves-effect waves-classic" style="margin-left: 15px!important;">
        <i class="fa fa-save" aria-hidden="true"></i>
        Guardar
    </button>

</div>
