<?php

/**
 * Created by PhpStorm.
 * User: diego
 * Date: 2/10/17
 * Time: 23:19
 */

namespace Tests;

use App\User;
use Spatie\Permission\Models\Role;

trait TestsHelper
{

    protected $defaultUser;

    public function defaultUser(array $attributes = [])
    {
        if ($this->defaultUser) {
            return $this->defaultUser;
        }

        $area_sistemas = \App\Area::whereName('SISTEMAS')->first();

        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions

        // create roles and assign existing permissions
        $role = Role::create(['name' => 'admin']);

        $user = User::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin#*')
        ]);

        $user->assignRole('admin');

        $user->areas()->sync($area_sistemas);

        return $this->defaultUser = $user;
    }

}