<?php

namespace Tests\Feature\Tasks;

use App\Mail\SendEmailTask;
use App\Mail\SendMailInformedTask;
use App\Project;
use App\Task;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class CreateTaskWithAreaProjectTest extends TestCase
{
    /** @test */
    function test_create_task_with_area_project()
    {
        Mail::fake();

        $admin = $this->createAdminUser();

        $role = Role::create(['name' => 'user']);

        $area = $admin->areas()->first();

        $user2 = $this->createUser([
            'name' => 'vegeta',
            'email' => 'vegeta@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $user3 = $this->createUser([
            'name' => 'goku',
            'email' => 'goku@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $user4 = $this->createUser([
            'name' => 'frizer',
            'email' => 'frizer@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $project = factory(Project::class)->create();

        $project->area()->associate($area);

        $project->save();

        $this->actingAs($admin);

        $response = $this->post(route('tasks.store', [$area->name, $project->id]), [
            'title' => 'title new',
            'description' => 'description new',
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
            'date_init' => Carbon::now()->format('d/m/Y'),
            'date_finish' => Carbon::now()->format('d/m/Y'),
            'informeds' => [$user2->id],
            'assigneds' => [$user3->id, $user4->id],
        ]);

        $this->assertDatabaseHas('tasks', [
            'title' => 'title new',
            'description' => 'description new',
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
        ]);

        Mail::assertQueued(SendEmailTask::class, function ($mail) use ($user3) {
            return $mail->hasTo($user3->email);
        });

        Mail::assertQueued(SendEmailTask::class, function ($mail) use ($user4) {
            return $mail->hasTo($user4->email);
        });

        Mail::assertQueued(SendMailInformedTask::class, function ($mail) use ($user2) {
            return $mail->hasTo($user2->email);
        });

        $task = Task::where('title', 'title new')->first();

        $this->assertDatabaseHas('task_informed', [
            'user_id' => $user2->id,
            'task_id' => $task->id,
        ]);

        $this->assertDatabaseHas('task_assigned', [
            'user_id' => $user3->id,
            'task_id' => $task->id,
        ]);

        $this->assertDatabaseHas('task_assigned', [
            'user_id' => $user4->id,
            'task_id' => $task->id,
        ]);
    }
}
