<?php

namespace Tests\Feature\Tasks;

use App\Mail\SendEmailTask;
use App\Mail\SendMailInformedTask;
use App\Mail\SendTaskFinishMail;
use App\Project;
use App\Status;
use App\Task;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class TestTaskFinishWithSubtaskFinishTest extends TestCase
{
    /** @test */
    function test_task_finish_with_subtask_finish()
    {
        Mail::fake();

        $admin = $this->createAdminUser();

        $role = Role::create(['name' => 'user']);

        $area = $admin->areas()->first();

        $status1 = factory(Status::class)->create(['name' => 'Informado']);

        $status2 = factory(Status::class)->create(['name' => 'Finalizado']);

        $user2 = $this->createUser([
            'name' => 'vegeta',
            'email' => 'vegeta@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $user3 = $this->createUser([
            'name' => 'goku',
            'email' => 'goku@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $user4 = $this->createUser([
            'name' => 'frizer',
            'email' => 'frizer@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $project = factory(Project::class)->create();

        $project->area()->associate($area);

        $project->save();

        $this->actingAs($admin);

        $response = $this->post(route('tasks.store', [$area->name, $project->id]), [
            'title' => 'title new task',
            'description' => 'description new',
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
            'assigneds' => [$user4->id],
            'date_init' => Carbon::now()->format('d/m/Y'),
            'date_finish' => Carbon::now()->format('d/m/Y'),
            'informeds' => [$user2->id, $user3->id]
        ]);

        Mail::assertQueued(SendEmailTask::class, function ($mail) use ($user4) {
            return $mail->hasTo($user4->email);
        });

        Mail::assertQueued(SendMailInformedTask::class, function ($mail) use ($user2) {
            return $mail->hasTo($user2->email);
        });

        Mail::assertQueued(SendMailInformedTask::class, function ($mail) use ($user3) {
            return $mail->hasTo($user3->email);
        });

        $task = Task::where('title', 'title new task')->where('description', 'description new')->first();

        $response = $this->post(route('subtasks.store', [$area->name, $project->id, $task->id]), [
            'title' => 'title subtask',
            'description' => 'description subtask',
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
            'assigneds' => [$user4->id],
            'date_init' => Carbon::now()->format('d/m/Y'),
            'date_finish' => Carbon::now()->format('d/m/Y'),
            'informeds' => [$user2->id, $user3->id]
        ]);

        $this->assertDatabaseHas('tasks', [
            'parent_id' => null,
            'title' => 'title subtask',
            'description' => 'description subtask',
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
            'status_id' => $status1->id,
            'task_id' => $task->id,
        ]);

        Mail::assertQueued(SendEmailTask::class, function ($mail) use ($user4) {
            return $mail->hasTo($user4->email);
        });

        Mail::assertQueued(SendMailInformedTask::class, function ($mail) use ($user2) {
            return $mail->hasTo($user2->email);
        });

        Mail::assertQueued(SendMailInformedTask::class, function ($mail) use ($user3) {
            return $mail->hasTo($user3->email);
        });

        $subtask = Task::where('title', 'title subtask')->where('description', 'description subtask')->first();

        $response = $this->put(route('subtask.finishtask', $subtask->id));

        $this->assertDatabaseHas('tasks', [
            'title' => 'title subtask',
            'description' => 'description subtask',
            'parent_id' => null,
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
            'status_id' => $status2->id,
            'task_id' => $task->id,
        ]);

        $response = $this->put(route('task.finishtask', $task->id));

        $response->assertJson(['status' => '200']);

        Mail::assertQueued(SendTaskFinishMail::class, function ($mail) use ($user4) {
            return $mail->hasTo($user4->email);
        });

        Mail::assertQueued(SendTaskFinishMail::class, function ($mail) use ($user2) {
            return $mail->hasTo($user2->email);
        });

        Mail::assertQueued(SendTaskFinishMail::class, function ($mail) use ($user3) {
            return $mail->hasTo($user3->email);
        });
    }
}
