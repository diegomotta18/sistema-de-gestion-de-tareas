<?php

namespace Tests\Feature\Tasks;

use App\Task;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class CreateTaskWithAreaTest extends TestCase
{
    /** @test */
    function test_create_task_with_area()
    {
        $admin = $this->createAdminUser();

        $area = $admin->areas()->first();

        $role = Role::create(['name' => 'user']);

        $user2 = $this->createUser([
            'name' => 'vegeta',
            'email' => 'vegeta@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $user3 = $this->createUser([
            'name' => 'goku',
            'email' => 'goku@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $user4 = $this->createUser([
            'name' => 'frizer',
            'email' => 'frizer@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $this->actingAs($admin);

        $response = $this->post(route('tasks.store', [$area->name, null]), [
            'title' => 'title new',
            'description' => 'description new',
            'area' => $area->id,
            'project' => null,
            'date_init' => Carbon::now()->format('d/m/Y'),
            'date_finish' => Carbon::now()->format('d/m/Y'),
            'informeds' => [$user2->id],
            'assigneds' => [$user3->id, $user4->id],

        ]);

        $this->assertDatabaseHas('tasks', [
            'title' => 'title new',
            'description' => 'description new',
            'area_id' => $area->id,
            'project_id' => null,
            'user_id' => $admin->id,

        ]);

        $task = Task::where('title', 'title new')->first();

        $this->assertDatabaseHas('task_informed', [
            'user_id' => $user2->id,
            'task_id' => $task->id,
        ]);

        $this->assertDatabaseHas('task_assigned', [
            'user_id' => $user3->id,
            'task_id' => $task->id,
        ]);

        $this->assertDatabaseHas('task_assigned', [
            'user_id' => $user4->id,
            'task_id' => $task->id,
        ]);
    }
}
