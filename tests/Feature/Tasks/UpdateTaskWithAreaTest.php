<?php

namespace Tests\Feature\Tasks;

use App\Mail\SendEmailTask;
use App\Task;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use Tests\TestCase;
use Illuminate\Support\Facades\Mail;

class UpdateTaskWithAreaTest extends TestCase
{
    /** @test */
    function test_update_task_with_area()
    {
        Mail::fake();

        $admin = $this->createAdminUser();

        $area = $admin->areas()->first();

        $role = Role::create(['name' => 'user']);

        $user2 = $this->createUser([
            'name' => 'vegeta',
            'email' => 'vegeta@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $user3 = $this->createUser([
            'name' => 'goku',
            'email' => 'goku@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $user4 = $this->createUser([
            'name' => 'frizer',
            'email' => 'frizer@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $this->actingAs($admin);

        $response = $this->post(route('tasks.store', [$area->name, null]), [
            'title' => 'title new',
            'description' => 'description new',
            'area_id' => $area->id,
            'project_id' => null,
            'date_init' => Carbon::now()->format('d/m/Y'),
            'date_finish' => Carbon::now()->format('d/m/Y'),
            'informeds' => [$user2->id],
            'assigneds' => [$user3->id, $user4->id],
        ]);

        $this->assertDatabaseHas('tasks', [
            'title' => 'title new',
            'description' => 'description new',
        ]);

        Mail::assertQueued(SendEmailTask::class, function ($mail) use ($user3) {
            return $mail->hasTo($user3->email);
        });

        Mail::assertQueued(SendEmailTask::class, function ($mail) use ($user4) {
            return $mail->hasTo($user4->email);
        });

        $task = Task::where('title', 'title new')->first();

        $response = $this->put(route('tasks.update', [$task->id, $area->name, null]), [
            'title' => 'title new',
            'description' => 'description new',
            'area_id' => $area->id,
            'project_id' => null,
            'date_init' => Carbon::now()->format('d/m/Y'),
            'date_finish' => Carbon::now()->format('d/m/Y'),
            'informeds' => [$user2->id],
            'assigneds' => [$user3->id, $user4->id],
        ]);

        $this->assertDatabaseHas('tasks', [
            'title' => 'title new',
            'description' => 'description new',
        ]);
    }
}
