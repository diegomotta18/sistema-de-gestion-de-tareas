<?php

namespace Tests\Feature\Tasks;

use App\Task;
use Tests\TestCase;

class DeleteTaskAreaTest extends TestCase
{
    /** @test */
    function test_task_delete_area()
    {
        $admin = $this->createAdminUser();

        $area = $admin->areas()->first();

        $this->actingAs($admin);

        $task = Task::create(
            [
                'title' => 'title new',
                'description' => 'description new',
                'area_id' => $area->id,
                'project_id' => null,
                'user_id' => $admin->id,
                'date_init' => "2018-02-20",
                'date_finish' => "2018-02-20",
            ]
        );

        $this->delete(route('tasks.destroy', $task->id));

        // Then
        $this->assertDatabaseMissing('tasks', [
            'title' => 'title new',
            'description' => 'description new',
            'area_id' => $area->id,
            'project_id' => null,
            'user_id' => $admin->id,
            'date_init' => "2018-02-20",
            'date_finish' => "2018-02-20",
            'deleted_at' => null
        ]);
    }
}
