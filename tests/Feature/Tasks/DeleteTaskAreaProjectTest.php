<?php

namespace Tests\Feature\Tasks;

use App\Project;
use App\Task;
use Tests\TestCase;

class DeleteTaskAreaProjectTest extends TestCase
{
    /** @test */
    function test_task_delete_area_project()
    {
        $admin = $this->createAdminUser();

        $area = $admin->areas()->first();

        $project = factory(Project::class)->create();

        $project->area()->associate($area);

        $project->save();

        $this->actingAs($admin);

        $task = Task::create(
            [
                'title' => 'title new',
                'description' => 'description new',
                'area_id' => $area->id,
                'project_id' => $project->id,
                'user_id' => $admin->id,
                'date_init' => "2018-02-20",
                'date_finish' => "2018-02-20",
            ]
        );

        Task::create(
            [
                'title' => '$subtask new',
                'description' => '$subtask new',
                'area_id' => $area->id,
                'project_id' => $project->id,
                'user_id' => $admin->id,
                'date_init' => "2018-02-20",
                'date_finish' => "2018-02-20",
                'task_id' => $task->id
            ]
        );

        $response = $this->delete(route('tasks.destroy', $task->id));

        $this->assertSame('ok', $response->content());

        $response->assertSuccessful();

        // Then
        $this->assertDatabaseMissing('tasks', [
            'title' => 'title new',
            'description' => 'description new',
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
            'date_init' => "2018-02-20",
            'date_finish' => "2018-02-20",
            'deleted_at' => null
        ]);
    }
}
