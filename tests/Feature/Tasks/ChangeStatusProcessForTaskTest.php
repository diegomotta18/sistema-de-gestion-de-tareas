<?php

namespace Tests\Feature\Tasks;

use App\Mail\SendEmailTask;
use App\Mail\SendMailInformedTask;
use App\Project;
use App\Status;
use App\Task;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class ChangeStatusProcessForTaskTest extends TestCase
{
    /** @test */
    public function test_change_in_process_task()
    {
        Mail::fake();

        $admin = $this->createAdminUser();

        $role = Role::create(['name' => 'user']);

        $area = $admin->areas()->first();

        $status1 = factory(Status::class)->create(['name' => 'Informado']);

        $status2 = factory(Status::class)->create(['name' => 'En Proceso']);

        $user1 = $this->createUser([
            'name' => 'maestro roshi',
            'email' => 'roshi@gmail.com',
            'password' => bcrypt('secret')
        ], $role, $area);

        $user2 = $this->createUser([
            'name' => 'vegeta',
            'email' => 'vegeta@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $user3 = $this->createUser([
            'name' => 'goku',
            'email' => 'goku@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $user4 = $this->createUser([
            'name' => 'frizer',
            'email' => 'frizer@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $project = factory(Project::class)->create();

        $project->area()->associate($area);

        $project->save();

        $this->actingAs($admin);

        $response = $this->post(route('tasks.store', [$area->name, $project->id]), [
            'title' => 'title new process',
            'description' => 'description new process',
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
            'assigneds' => [$user1->id, $user4->id],
            'date_init' => Carbon::now()->format('d/m/Y'),
            'date_finish' => Carbon::now()->format('d/m/Y'),
            'informeds' => [$user2->id, $user3->id]
        ]);

        $task = Task::where('title', 'title new process')->where('description', 'description new process')->first();

        $this->assertDatabaseHas('tasks', [
            'title' => 'title new process',
            'description' => 'description new process',
            'parent_id' => $task->id,
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
            'status_id' => $status1->id,
            'task_id' => null,
        ]);

        Mail::assertQueued(SendEmailTask::class, function ($mail) use ($user1) {
            return $mail->hasTo($user1->email);
        });

        Mail::assertQueued(SendEmailTask::class, function ($mail) use ($user4) {
            return $mail->hasTo($user4->email);
        });

        Mail::assertQueued(SendMailInformedTask::class, function ($mail) use ($user2) {
            return $mail->hasTo($user2->email);
        });

        Mail::assertQueued(SendMailInformedTask::class, function ($mail) use ($user3) {
            return $mail->hasTo($user3->email);
        });

        $this->assertDatabaseHas('task_assigned', [
            'user_id' => $user1->id,
            'task_id' => $task->id,
        ]);

        $this->assertDatabaseHas('task_assigned', [
            'user_id' => $user4->id,
            'task_id' => $task->id,
        ]);

        $this->assertDatabaseHas('task_informed', [
            'user_id' => $user2->id,
            'task_id' => $task->id,
        ]);

        $this->assertDatabaseHas('task_informed', [
            'user_id' => $user3->id,
            'task_id' => $task->id,
        ]);

        $response = $this->put(route('task.processtask', $task->id));

        $this->assertDatabaseHas('tasks', [
            'title' => 'title new process',
            'description' => 'description new process',
            'parent_id' => $task->id,
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
            'status_id' => $status2->id,
            'task_id' => null,
        ]);
    }
}
