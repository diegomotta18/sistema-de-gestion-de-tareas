<?php

namespace Tests\Feature\Tasks;

use App\Mail\SendEmailTask;
use App\Mail\SendMailInformedTask;
use App\Project;
use App\Task;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class UpdateTaskWithAreaProjectTest extends TestCase
{
    /** @test */
    function test_update_task_with_area_project()
    {
        Mail::fake();

        $admin = $this->createAdminUser();

        $role = Role::create(['name' => 'user']);

        $area = $admin->areas()->first();

        $user4 = $this->createUser([
            'name' => 'frizer',
            'email' => 'frizer@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $user5 = $this->createUser([
            'name' => 'tenchijan',
            'email' => 'tenchijan@gmail.com',
            'password' => bcrypt('secret')
        ], $role, $area);

        $project = factory(Project::class)->create();

        $project->area()->associate($area);

        $project->save();

        $this->actingAs($admin);

        $this->post(route('tasks.mainstore', [$area->name, null]), [
            'title' => 'title neww',
            'description' => 'description neww',
            'area' => $area->id,
            'project' => $project->id,
            'date_init' => Carbon::now()->format('d/m/Y'),
            'date_finish' => Carbon::now()->format('d/m/Y'),
            'informeds' => [$user5->id],
            'assigneds' => [$user4->id],

        ]);

        Mail::assertQueued(SendEmailTask::class, function ($mail) use ($user4) {
            return $mail->hasTo($user4->email);
        });

        Mail::assertQueued(SendMailInformedTask::class, function ($mail) use ($user5) {
            return $mail->hasTo($user5->email);
        });

        $task = Task::where('title', 'title neww')->where('description', 'description neww')->first();

        $this->put(route('tasks.update', [$task->id, $area->name, $project->id]), [
            'title' => 'title new2',
            'description' => 'description new2',
            'area' => $area->id,
            'project' => $project->id,
            'user_id' => $admin->id,
            'date_init' => Carbon::now()->format('d/m/Y'),
            'date_finish' => Carbon::now()->format('d/m/Y'),
            'assigneds' => [$user4->id],
            'informeds' => [$user5->id]

        ]);

        $this->assertDatabaseHas('tasks', [
            'id' => $task->id,
            'title' => 'title new2',
            'description' => 'description new2',
            'area_id' => $area->id,
            'project_id' => $project->id,

        ]);

        $this->assertDatabaseHas('task_assigned', [
            'user_id' => $user4->id,
            'task_id' => $task->id,
        ]);

        $this->assertDatabaseHas('task_informed', [
            'user_id' => $user5->id,
            'task_id' => $task->id,
        ]);
    }
}
