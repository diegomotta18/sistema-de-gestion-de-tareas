<?php

namespace Tests\Feature\Tasks;

use App\Project;
use App\Task;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class AssignedUserReplacementInTaskTest extends TestCase
{
    /** @test */
    function test_assigned_user_replacement_in_task()
    {
        $admin = $this->createAdminUser();

        $role = Role::create(['name' => 'user']);

        $area = $admin->areas()->first();

        $user4 = $this->createUser([
            'name' => 'frizer',
            'email' => 'frizer@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $user5 = $this->createUser([
            'name' => 'tenchijan',
            'email' => 'tenchijan@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $user6 = $this->createUser([
            'name' => 'goku',
            'email' => 'goku@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $project = factory(Project::class)->create();

        $project->area()->associate($area);

        $project->save();

        $this->actingAs($admin);

        $response = $this->post(route('tasks.store', [$area->name, $project->id]), [
            'title' => 'title new',
            'description' => 'description new',
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
            'date_init' => Carbon::now()->format('d/m/Y'),
            'date_finish' => Carbon::now()->format('d/m/Y'),
            'assigneds' => [$user5->id],
            'informeds' => [$user4->id],
            'replacements' => [$user6->id]

        ]);

        $this->assertDatabaseHas('tasks', [
            'title' => 'title new',
            'description' => 'description new',
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
        ]);

        $task = Task::where('title', 'title new')->first();

        $this->assertDatabaseHas('task_assigned', [
            'user_id' => $user5->id,
            'task_id' => $task->id,
        ]);

        $this->assertDatabaseHas('task_informed', [
            'user_id' => $user4->id,
            'task_id' => $task->id,
        ]);

        $this->assertDatabaseHas('task_replacement', [
            'user_id' => $user6->id,
            'task_id' => $task->id,
        ]);
    }
}
