<?php

namespace Tests\Feature;

use App\Mail\SendEmailTask;
use App\Mail\SendMailInformedTask;
use App\Mail\SendTaskFinishMail;
use App\Project;
use App\Status;
use App\Task;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class ChangeStatusFinishForTaskTest extends TestCase
{
    /** @test */
    function test_change_in_finish_task()
    {
//        $this->withoutMiddleware();
        $this->withoutExceptionHandling();
        Mail::fake();

        $admin = $this->createAdminUser();

        $this->createStatus();

        $role = Role::create(['name' => 'user']);

        $area = $admin->areas()->first();

        $user1 = $this->createUser([
            'name' => 'maestro roshi',
            'email' => 'roshi@gmail.com',
            'password' => bcrypt('secret')
        ], $role, $area);

        $user2 = $this->createUser([
            'name' => 'vegeta',
            'email' => 'vegeta@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $user3 = $this->createUser([
            'name' => 'goku',
            'email' => 'goku@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $user4 = $this->createUser([
            'name' => 'frizer',
            'email' => 'frizer@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $project = factory(Project::class)->create();

        $project->area()->associate($area);

        $project->save();

        $this->actingAs($admin);

        $response = $this->post(route('tasks.store', [$area->name, $project->id]), [
            'title' => 'title new21',
            'description' => 'description new21',
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
            'date_init' => Carbon::now()->format('d/m/Y'),
            'date_finish' => Carbon::now()->format('d/m/Y'),
            'informeds' => [$user4->id],
            'assigneds' => [$user2->id, $user3->id, $user1->id]
        ]);
//
        $task = Task::where('title', 'title new21')->first();

        $status = Status::where('name', 'Informado')->first();
//
        $this->assertDatabaseHas('tasks', [
            'title' => 'title new21',
            'parent_id' => $task->id,
            'description' => 'description new21',
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
            'status_id' => $status->id,
            'task_id' => null,
        ]);
//
        Mail::assertQueued(SendEmailTask::class, function ($mail) use ($user2) {
            return $mail->hasTo($user2->email);
        });

        Mail::assertQueued(SendEmailTask::class, function ($mail) use ($user3) {
            return $mail->hasTo($user3->email);
        });

        Mail::assertQueued(SendEmailTask::class, function ($mail) use ($user1) {
            return $mail->hasTo($user1->email);
        });

        Mail::assertQueued(SendMailInformedTask::class, function ($mail) use ($user4) {
            return $mail->hasTo($user4->email);
        });

        $response = $this->put(route('task.finishtask', $task->id));

        $status1 = Status::where('name', 'Finalizado')->first();

        $this->assertDatabaseHas('tasks', [
            'title' => 'title new21',
            'parent_id' => $task->id,
            'description' => 'description new21',
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
            'status_id' => $status1->id,
            'task_id' => null,

        ]);

        Mail::assertQueued(SendTaskFinishMail::class, function ($mail) use ($user2) {
            return $mail->hasTo($user2->email);
        });

        Mail::assertQueued(SendTaskFinishMail::class, function ($mail) use ($user3) {
            return $mail->hasTo($user3->email);
        });

        Mail::assertQueued(SendTaskFinishMail::class, function ($mail) use ($user1) {
            return $mail->hasTo($user1->email);
        });

        Mail::assertQueued(SendTaskFinishMail::class, function ($mail) use ($user4) {
            return $mail->hasTo($user4->email);
        });
    }
}
