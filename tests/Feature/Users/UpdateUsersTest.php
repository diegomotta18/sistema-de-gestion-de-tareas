<?php

namespace Tests\Feature\Users;

use App\Area;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class UpdateUsersTest extends TestCase
{
    /** @test */
    function admin_can_update_user()
    {
        $admin = $this->createAdminUser();

        $role = Role::create(['name' => 'user']);

        $area_com = factory(Area::class)->create(['name' => 'comprasmisiones']);

        $user1 = $this->createUser([
            'name' => 'maestro roshi',
            'email' => 'roshi@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area_com
        );

        $area_1 = factory(Area::class)->create();

        $area_2 = factory(Area::class)->create();

        $this->actingAs($admin);

        $this->put(route('users.update', $user1->id), [
            'name' => 'New name edited',
            'email' => 'another@mail.com',
            'role' => 'user',
            'area' => $area_1->id,
            'areas' => [
                $area_1->id,
                $area_2->id
            ],
        ]);

        $this->assertDatabaseHas('users', [
            'id' => $user1->id,
            'name' => 'New name edited',
            'email' => 'another@mail.com',
        ]);

        $this->assertDatabaseHas('area_user', [
            'area_id' => $area_1->id,
            'user_id' => $user1->id
        ]);

        $this->assertDatabaseHas('area_user', [
            'area_id' => $area_2->id,
            'user_id' => $user1->id
        ]);

        $this->assertSame('user', $user1->fresh()->roles()->first()->name);
    }
}
