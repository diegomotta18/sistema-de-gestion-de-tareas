<?php

namespace Tests\Feature\Users;

use App\Mail\UserCreated;
use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class CreateUsersTest extends TestCase
{
    /** @test */
    function admin_can_create_new_users()
    {
        Mail::fake();

        $admin = $this->createAdminUser();

        $role = Role::create(['name' => 'user']);

        $area = $admin->areas()->first();

        // When
        $this->actingAs($admin);

        $this->post(route('users.store'), [
            'name' => 'New User',
            'email' => 'new@user.com',
            'role' => 'user',
            'areas' => [
                $area->id,
            ],
            'chief' => null,
            'area' => $area->id
        ]);

        $user = User::whereEmail('new@user.com')->where('name', 'New User')->first();

        $this->assertDatabaseHas('users', [
            'name' => 'New User',
            'email' => 'new@user.com'
        ]);

        $this->assertDatabaseHas('model_has_roles', [
            'role_id' => $role->id,
            'model_type' => 'App\User',
            'model_id' => $user->id
        ]);

        $this->assertDatabaseHas('area_user', [
            'area_id' => $area->id,
            'user_id' => $user->id
        ]);

        // checks email are sended
        Mail::assertQueued(UserCreated::class, function ($mail) use ($user) {
            return $mail->hasTo($user->email);
        });
    }
}
