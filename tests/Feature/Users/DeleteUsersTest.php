<?php

namespace Tests\Feature\Users;

use App\Area;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class DeleteUsersTest extends TestCase
{
    /** @test */
    function admin_can_destroy_user()
    {
        $admin = $this->createAdminUser();

        $role = Role::create(['name' => 'user']);

        $area_com = factory(Area::class)->create(['name' => 'comprasmisiones']);

        $user1 = $this->createUser([
            'name' => 'maestro roshi',
            'email' => 'roshi@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area_com
        );

        $this->actingAs($admin);

        $this->delete(route('users.destroy', $user1->id));

        $this->assertDatabaseMissing('users', [
            'name' => 'New User',
            'email' => 'new@user.com',
            'deleted_at' => null
        ]);
    }
}
