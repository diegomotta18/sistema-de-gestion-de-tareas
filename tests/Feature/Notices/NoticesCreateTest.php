<?php

namespace Tests\Feature\Notices;

use App\Area;
use App\Mail\SendEmailNotice;
use App\Notice;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class NoticesCreateTest extends TestCase
{
    /** @test */
    function create_notice_and_send_all_user()
    {
        Mail::fake();

        $admin = $this->createAdminUser();

        $role = Role::create(['name' => 'user']);

        $area_com = factory(Area::class)->create(['name' => 'comprasmisiones']);

        $user2 = $this->createUser([
            'name' => 'vegeta',
            'email' => 'vegeta@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area_com
        );

        $user3 = $this->createUser([
            'name' => 'goku',
            'email' => 'goku@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area_com
        );

        $user4 = $this->createUser([
            'name' => 'frizer',
            'email' => 'frizer@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area_com
        );

        // When
        $this->actingAs($admin);

        $response = $this->post(route('notices.store'), [
            'title' => 'notice new',
            'description' => 'description',
            'user_all' => [$user2->id, $user3->id],
            'typeUser' => ['any_user']
        ]);

        $notice = Notice::where('title', 'notice new')
                        ->where('description', 'description')
                        ->first();

        $this->assertDatabaseHas('notices', [
            'title' => 'notice new',
            'description' => 'description'
        ]);

        $this->assertDatabaseHas('notice_user', [
            'notice_id' => $notice->id,
            'user_id' => $user2->id
        ]);

        $this->assertDatabaseHas('notice_user', [
            'notice_id' => $notice->id,
            'user_id' => $user3->id
        ]);

        Mail::assertQueued(SendEmailNotice::class, function ($mail) use ($user2) {
            return $mail->hasTo($user2->email);
        });

        Mail::assertQueued(SendEmailNotice::class, function ($mail) use ($user3) {
            return $mail->hasTo($user3->email);
        });
    }

    /** @test */
    function create_notice_and_send_area_user()
    {
        Mail::fake();

        $admin = $this->createAdminUser();

        $role = Role::create(['name' => 'user']);

        $area_com = factory(Area::class)->create(['name' => 'comprasmisiones']);

        $user2 = $this->createUser([
            'name' => 'vegeta',
            'email' => 'vegeta@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area_com
        );

        $user3 = $this->createUser([
            'name' => 'goku',
            'email' => 'goku@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area_com
        );

        // When
        $this->actingAs($admin);

        $response = $this->post(route('notices.store'), [
            'title' => 'notice new',
            'description' => 'description',
            'user_select' => [$user2->id, $user3->id],
            'typeUser' => ['area_user']
        ]);

        $notice = Notice::where('title', 'notice new')->where('description', 'description')->first();

        $response->assertSessionHas('alert-type', 'success')
                 ->assertSessionHas('message', 'Noticia creada!');

        $this->assertDatabaseHas('notices', [
            'title' => 'notice new',
            'description' => 'description'
        ]);

        $this->assertDatabaseHas('notice_user', [
            'notice_id' => $notice->id,
            'user_id' => $user2->id
        ]);

        $this->assertDatabaseHas('notice_user', [
            'notice_id' => $notice->id,
            'user_id' => $user3->id
        ]);

        Mail::assertQueued(SendEmailNotice::class, function ($mail) use ($user2) {
            return $mail->hasTo($user2->email);
        });

        Mail::assertQueued(SendEmailNotice::class, function ($mail) use ($user3) {
            return $mail->hasTo($user3->email);
        });
    }
}