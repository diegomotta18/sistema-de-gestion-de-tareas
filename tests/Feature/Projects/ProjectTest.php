<?php

namespace Tests\Feature\Projects;

use App\Area;
use App\Project;
use Tests\TestCase;

class ProjectTest extends TestCase
{
    /** @test */
    function it_can_create_project()
    {
        $admin = $this->createAdminUser();

        $area = $admin->areas()->first();

        // When
        $this->actingAs($admin);

        $response = $this->post(route('projects.store', $area->name), [
            'name' => 'name',
            'description' => 'description',
            'area_id' => $area->id
        ]);

        $this->assertDatabaseHas('projects', [
            'name' => 'name',
            'description' => 'description',
            'area_id' => $area->id
        ]);
    }

    /** @test */
    function test_update_project()
    {
        $admin = $this->createAdminUser();

        $area = $admin->areas()->first();

        $this->actingAs($admin);

        $project_new = Project::create([
            'name' => 'CRM',
            'description' => 'Este proyecto esta encargado de la gestion de contactos',
            'area_id' => $area->id,
            'user_id' => $admin->id,
        ]);

        $this->put(route('projects.update', [$area->name, $project_new->id]), [
            'name' => "Gestion de contactos",
            'description' => "Este proyecto esta encargado de la gestion de contactos",
        ]);

        $this->assertDatabaseHas('projects', [
            'id' => $project_new->id,
            'name' => "Gestion de contactos",
            'description' => "Este proyecto esta encargado de la gestion de contactos",
        ]);
    }

    /** @test */
    function test_destroy_project()
    {
        // Having
        $admin = $this->createAdminUser();

        $area = $admin->areas()->first();

        $project_new = Project::create([
            'name' => 'CRM',
            'description' => 'Este proyecto esta encargado de la gestion de contactos',
            'area_id' => $area->id,
            'user_id' => $admin->id,
        ]);

        // When
        $this->actingAs($admin);

        $this->delete(route('projects.destroy', $project_new->id));

        // Then
        $this->assertDatabaseMissing('projects', [
            'name' => 'CRM',
            'description' => 'Este proyecto esta encargado de la gestion de contactos',
            'deleted_at' => null
        ]);
    }

}
