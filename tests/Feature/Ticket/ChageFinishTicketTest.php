<?php

namespace Tests\Feature\Tasks;

use App\Area;
use App\Mail\SendEmailTask;
use App\Mail\SendMailInformedModifiedTask;
use App\Mail\SendMailInformedTask;
use App\Mail\SendTaskFinishMail;
use App\Mail\SendTaskProcessMail;
use App\Project;
use App\Status;
use App\Task;
use App\TypeTaskModel;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class ChangeFinishTicketTest extends TestCase
{
    /** @test */
    public function test_change_finish_ticket()
    {
        Mail::fake();
        $admin =$this->createAdminUser();
        $this->createStatus();

        $ticket = factory(TypeTaskModel::class)->create(['type' => 'Ticket']);
        $role =  Role::create(['name' => 'user']);

        $area_2 = factory(Area::class)->create(['name' => 'Prensa']);

        $user = $this->createUser(['name'=>'leo','email'=>'leo@leo.com'], $role,$area_2);
        $this->actingAs($user);

        $this->post(route('tickets.store'), [
            'title' => 'No carga un dato',
            'description' => 'description new',
            'area' => null,
            'project' => null,
            'date_init' => null,
            'date_finish' => null,

        ]);
        $status = Status::where('name', 'Informado')->first();

        $this->assertDatabaseHas('tasks', [
            'title' => 'No carga un dato',
            'description' => 'description new',
            'user_id' => $user->id,
            'type_task_id' => $ticket->id,
            'status_id' => $status->id,

        ]);


        $task = Task::where('title', 'No carga un dato')->first();

        $this->assertDatabaseHas('task_informed', [
            'user_id' => $admin->id,
            'task_id' => $task->id,
        ]);

        Mail::assertQueued(SendMailInformedModifiedTask::class, function ($mail) use ($admin) {
            return $mail->hasTo($admin->email);
        });

        $this->flushSession();

        $this->actingAs($admin);


//        $this->post(route('tickets.inprocess', $task->id));
//
//        $status1 = Status::where('name', 'En proceso')->first();
//
//        $this->assertDatabaseHas('tasks', [
//            'title' => 'No carga un dato',
//            'description' => 'description new',
//            'user_id' => $user->id,
//            'type_task_id' => $ticket->id,
//            'status_id' => $status1->id,
//        ]);


        $request = $this->put(route('tickets.finish', $task->id));
        $status2 = Status::where('name', 'Finalizado')->first();

        $this->assertDatabaseHas('tasks', [
            'title' => 'No carga un dato',
            'description' => 'description new',
            'user_id' => $user->id,
            'type_task_id' => $ticket->id,
            'status_id' => $status2->id,
        ]);

        Mail::assertQueued(SendTaskFinishMail::class, function ($mail) use ($user) {
            return $mail->hasTo($user->email);
        });
    }
}
