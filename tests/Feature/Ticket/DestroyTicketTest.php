<?php

namespace Tests\Feature\Ticket;

use App\Area;
use App\Mail\SendMailInformedModifiedTask;
use App\Task;
use App\TypeTaskModel;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class DestroyTicketTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_destroy_ticket()
    {
        Mail::fake();

        $admin =$this->createAdminUser();
        $this->createStatus();

        $ticket = factory(TypeTaskModel::class)->create(['type' => 'Ticket']);
        $role =  Role::create(['name' => 'user']);

        $area_2 = factory(Area::class)->create(['name' => 'Prensa']);

        $user = $this->createUser(['name'=>'leo','email'=>'leo@leo.com'], $role,$area_2);
        $this->actingAs($user);

        $this->post(route('tickets.store'), [
            'title' => 'No carga un dato',
            'description' => 'description new',
            'area' => null,
            'project' => null,
            'date_init' => null,
            'date_finish' => null,

        ]);


        $task = Task::where('title', 'No carga un dato')->first();



        Mail::assertQueued(SendMailInformedModifiedTask::class, function ($mail) use ($admin) {
            return $mail->hasTo($admin->email);
        });

        $request =$this->delete(route('tickets.delete',$task->id));
        $this->assertDatabaseMissing('tasks', [
            'title' => 'No carga un dato',
            'description' => 'description new',
            'area_id' => null,
            'project_id' => null,
            'user_id' => $user->id,
            'type_task_id' => $ticket->id,
            'deleted_at' => null
        ]);

    }
}
