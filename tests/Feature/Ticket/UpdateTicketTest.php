<?php

namespace Tests\Feature\Ticket;

use App\Area;
use App\Mail\SendMailInformedModifiedTask;
use App\Mail\SendMailInformedTask;
use App\Task;
use App\TypeTaskModel;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateTicketTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_update_ticket()
    {

        Mail::fake();
        $admin =$this->createAdminUser();
        $this->createStatus();

        $ticket = factory(TypeTaskModel::class)->create(['type' => 'Ticket']);
        $role =  Role::create(['name' => 'user']);

        $area_2 = factory(Area::class)->create(['name' => 'Prensa']);

        $user = $this->createUser(['name'=>'leo','email'=>'leo@leo.com'], $role,$area_2);
        $this->actingAs($user);

        $this->post(route('tickets.store'), [
            'title' => 'No carga un datoo',
            'description' => 'description new',
            'area' => null,
            'project' => null,
            'date_init' => null,
            'date_finish' => null,
        ]);


        $task = Task::where('title', 'No carga un datoo')->whereHas('typeTask', function($q){
            return $q->where('type','Ticket');
        })->first();


        $this->put(route('tickets.update', ['task'=>$task->id]), [
            'title' => 'No carga un dato de la pagina',
            'description' => 'description new',
            'area' => null,
            'project' => null,
            'date_init' => null,
            'date_finish' => null,

        ]);

        $this->assertDatabaseHas('tasks', [
            'id' => $task->id,
            'title' => 'No carga un dato de la pagina',
            'description' => 'description new',
            'user_id' => $user->id,
            'type_task_id' => $ticket->id
        ]);


        Mail::assertQueued(SendMailInformedModifiedTask::class, function ($mail) use ($admin) {
            return $mail->hasTo($admin->email);
        });
    }
}
