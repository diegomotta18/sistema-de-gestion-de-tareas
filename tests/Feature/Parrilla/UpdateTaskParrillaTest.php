<?php

namespace Tests\Feature\Tasks;

use App\Area;
use App\Mail\SendEmailTask;
use App\Mail\SendMailInformedTask;
use App\Project;
use App\Status;
use App\Task;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class UpdateTaskParrillaTest extends TestCase
{
    /** @test */
    public function test_update_task_parrilla()
    {
        $admin = $this->createAdminUser();

        $area = factory(Area::class)->create(['name' => 'Prensa']);

        $this->actingAs($admin);

        $response = $this->post(route('parrilla.store'), [
            'title' => 'title',
            'description' => 'description',
            'area' => $area->id,
            'project' => null,
            "date_init" => null,
            "date_finish" => null,


        ]);

        $this->assertDatabaseHas('tasks', [
            'title' => 'title',
            'description' => 'description',
            'parrilla' => true
        ]);

        $task = Task::where('title','title')->first();

        $response = $this->put(route('parrilla.update',$task->id), [
            'title' => 'titlen',
            'description' => 'descriptionn',
            'area' => $area->id,
            'project' => null,
            "date_init" => null,
            "date_finish" => null,
            'parrilla' => true

        ]);

        $this->assertDatabaseHas('tasks', [
            'title' => 'titlen',
            'description' => 'descriptionn',
            'parrilla' => true
        ]);

    }
}
