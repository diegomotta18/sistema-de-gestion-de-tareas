<?php

namespace Tests\Feature\Tasks;

use App\Area;
use Tests\TestCase;

class CreateTaskParrillaTest extends TestCase
{
    /** @test */
    public function test_create_task_parrilla()
    {
        $admin = $this->createAdminUser();

        $area = factory(Area::class)->create(['name' => 'Prensa']);

        $this->actingAs($admin);

        $response = $this->post(route('parrilla.store'), [
            'title' => 'title',
            'description' => 'description',
            'area' => $area->id,
            'project' => null,
            "date_init" => null,
            "date_finish" => null,


        ]);

        $this->assertDatabaseHas('tasks', [
            'title' => 'title',
            'description' => 'description',
            'parrilla' => true
        ]);
    }
}
