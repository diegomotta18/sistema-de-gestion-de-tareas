<?php

namespace Tests\Feature\Subtasks;

use App\Project;
use App\Task;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class CreateSubtaskWithProjectAreaTest extends TestCase
{
    /** @test */
    function test_create_subtask_with_project_area()
    {
        $admin = $this->createAdminUser();

        $role = Role::create(['name' => 'user']);

        $area = $admin->areas()->first();

        $user2 = $this->createUser([
            'name' => 'vegeta',
            'email' => 'vegeta@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $user3 = $this->createUser([
            'name' => 'goku',
            'email' => 'goku@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $area = $admin->areas()->first();

        $project = factory(Project::class)->create();

        $project->area()->associate($area);

        $project->save();

        $this->actingAs($admin);

        $response = $this->post(route('tasks.store', [$area->name, $project->id]), [
            'title' => 'title new task',
            'description' => 'description new',
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
            'assigneds' => [$user3->id],
            'date_init' => Carbon::now()->format('d/m/Y'),
            'date_finish' => Carbon::now()->format('d/m/Y'),
            'informeds' => [$user2->id]
        ]);

        $this->assertDatabaseHas('tasks', [
            'title' => 'title new task',
            'description' => 'description new',
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
        ]);

        $task = Task::where('title', 'title new task')->first();

        $this->assertDatabaseHas('task_informed', [
            'user_id' => $user2->id,
            'task_id' => $task->id,
        ]);

        $this->assertDatabaseHas('task_assigned', [
            'user_id' => $user3->id,
            'task_id' => $task->id,
        ]);


        $response = $this->post(route('subtasks.store', [$area->name, $project->id, $task->id]), [
            'title' => 'title subtask',
            'description' => 'description subtask',
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
            'assigneds' => [$user3->id],
            'date_init' => Carbon::now()->format('d/m/Y'),
            'date_finish' => Carbon::now()->format('d/m/Y'),
            'informeds' => [$user2->id]
        ]);

        $subtask = Task::where('parent_id', '=', null)
                       ->where('title', 'title subtask')
                       ->first();

        $this->assertDatabaseHas('tasks', [
            'title' => 'title subtask',
            'description' => 'description subtask',
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
            'task_id' => $task->id
        ]);

        $this->assertDatabaseHas('task_informed', [
            'user_id' => $user2->id,
            'task_id' => $subtask->id,
        ]);

        $this->assertDatabaseHas('task_assigned', [
            'user_id' => $user3->id,
            'task_id' => $subtask->id,
        ]);
    }
}
