<?php

namespace Tests\Feature\Subtasks;

use App\Project;
use App\Status;
use App\Task;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class ChangeStatusFinishForSubtaskTest extends TestCase
{
    /** @test */
    public function test_change_status_finish_for_subtask()
    {
        $admin = $this->createAdminUser();

        $role = Role::create(['name' => 'user']);

        $area = $admin->areas()->first();

        $status1 = factory(Status::class)->create(['name' => 'Informado']);

        $status2 = factory(Status::class)->create(['name' => 'Finalizado']);

        $user2 = $this->createUser([
            'name' => 'vegeta',
            'email' => 'vegeta@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $user3 = $this->createUser([
            'name' => 'goku',
            'email' => 'goku@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $user4 = $this->createUser([
            'name' => 'frizer',
            'email' => 'frizer@gmail.com',
            'password' => bcrypt('secret')
        ],
            $role, $area
        );

        $project = factory(Project::class)->create();

        $project->area()->associate($area);

        $project->save();

        $this->actingAs($admin);

        $response = $this->post(route('tasks.store', [$area->name, $project->id]), [
            'title' => 'title new',
            'description' => 'description new',
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
            'date_init' => Carbon::now()->format('d/m/Y'),
            'date_finish' => Carbon::now()->format('d/m/Y'),
            'informeds' => [$user2->id],
            'assigneds' => [$user3->id, $user4->id],
        ]);

        $this->assertDatabaseHas('tasks', [
            'title' => 'title new',
            'description' => 'description new',
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,

        ]);

        $task = Task::where('title', 'title new')->first();

        $response = $this->post(route('subtasks.store', [$area->name, $project->id, $task->id]), [
            'title' => 'title subtask',
            'description' => 'description subtask',
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
            'date_init' => Carbon::now()->format('d/m/Y'),
            'date_finish' => Carbon::now()->format('d/m/Y'),
            'informeds' => [$user2->id],
            'assigneds' => [$user3->id, $user4->id],
        ]);

        $this->assertDatabaseHas('tasks', [
            'parent_id' => null,
            'title' => 'title subtask',
            'description' => 'description subtask',
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
            'status_id' => $status1->id,
            'task_id' => $task->id,
        ]);

        $subtask = Task::where('title', 'title subtask')->where('description', 'description subtask')->first();

        $response = $this->put(route('subtask.finishtask', $subtask->id));

        $this->assertDatabaseHas('tasks', [
            'title' => 'title subtask',
            'description' => 'description subtask',
            'parent_id' => null,
            'area_id' => $area->id,
            'project_id' => $project->id,
            'user_id' => $admin->id,
            'status_id' => $status2->id,
            'task_id' => $task->id,
        ]);
    }
}
