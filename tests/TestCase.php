<?php

namespace Tests;

use App\Area;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Spatie\Permission\Models\Role;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase, DetectRepeatedQueries;

    //TODO: mover funciones helpers al trait
//    use TestsHelper;

    public function setUp()
    {
        parent::setUp();

        TestResponse::macro('viewData', function ($key) {
            $this->ensureResponseHasView();
            $this->assertViewHas($key);

            return $this->original->$key;
        });

        TestResponse::macro('assertViewCollection', function ($var) {
            return new TestCollectionData($this->viewData($var));
        });

        $this->enableQueryLog();
    }

    public function tearDown()
    {
        $this->flushQueryLog();

        parent::tearDown();
    }

    /**
     * @return \App\User
     */
    public function createAdminUser()
    {
        Role::create(['name' => 'god']);

        $admin_user = User::create([
            'name' => 'Admin User',
            'email' => 'admin@mail.com',
            'password' => bcrypt('secret'),
            'chief' => true,
        ]);

        $admin_user->assignRole('god');

        $area_1 = factory(Area::class)->create(['name' => 'Sistemas']);
        $admin_user->area_id = $area_1->id;

        $admin_user->save();
        $admin_user->areas()->save($area_1);

        return $admin_user;
    }

    /**
     * @return \App\User
     */
    public function createUser($attributes = [], Role $role, Area $area)
    {
        $user = factory(User::class)->create($attributes);

        $user->assignRole($role->name);
        $user->area_id = $area->id;
        $user->areas()->save($area);

        return $user;
    }

    /**
     * @return \App\User
     */
    public function createUserDefault(array $attributes = [])
    {
        $role = Role::create(['name' => 'user']);

        $area = factory(Area::class)->create(['name' => 'Sistemas']);

        $user = factory(User::class)->create($attributes);

        $user->assignRole('user');

        $user->areas()->sync($area);

        return $user;
    }

    public function createStatus()
    {
        factory(\App\Status::class)->create([
            'name' => 'Informado',
            'color' => '#1ab394'
        ]);

        factory(\App\Status::class)->create([
            'name' => 'En proceso',
            'color' => '#23c6c8'

        ]);

        factory(\App\Status::class)->create([
            'name' => 'Finalizado',
            'color' => '#ed5565'
        ]);
    }
}
