<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    //
    protected $table = 'boxes';
    protected $datas = [ 'created_at', 'updated_at'];

    protected $fillable = [
        'user_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function threads(){
        return $this->hasMany(BoxThread::class);
    }
}
