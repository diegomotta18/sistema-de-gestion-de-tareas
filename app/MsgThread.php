<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

class MsgThread extends Model
{
    //
    use SoftDeletes;

    protected $table = 'msg_threads';
    protected $datas = [ 'deleted_at','created_at', 'updated_at'];

    protected $fillable = [
        'user_id','message','thread_id'
    ];

    public function thread()
    {
        return $this->belongsTo(Thread::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getCreatedAtAttribute($value)
    {

        return Carbon::parse($value)->format('d/m/Y h:m ');

    }

    public function files()
    {
        return $this->hasMany(MsgFile::class);
    }

    public function tags(){
        return $this->belongsToMany(Tag::class);
    }

}
