<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    //
    protected $fillable = ['id','time_init','time_finish','description','date','user_id'];
    protected $dates = ['created_at','updated_at','deleted_at'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
