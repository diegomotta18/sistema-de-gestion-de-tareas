<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Observation extends Model
{
    //
    use SoftDeletes;

    protected $table = 'license_observations';
    protected $dates = ['created_at','updated_at','deleted_at'];
    protected $fillable = [
        'id', 'observations','license_id'
    ];

    public function license(){
        return $this->belongsTo(License::class);
    }
}
