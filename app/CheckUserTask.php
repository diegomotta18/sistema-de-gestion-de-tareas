<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckUserTask extends Model
{
    //
    protected $table = 'check_user_task';

    protected $datas = [ 'deleted_at','created_at', 'updated_at'];

    protected $fillable = [
        'task_id','user_id', 'status'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function task() {
        return $this->belongsTo(Task::class);
    }


}
