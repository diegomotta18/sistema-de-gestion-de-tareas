<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class DateToday implements Rule
{


    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        return Carbon::createFromFormat('d/m/Y', $value)->timestamp >= Carbon::now()->timestamp;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Fecha de inicio debe ser mayor o igual la fecha actual';
    }
}
