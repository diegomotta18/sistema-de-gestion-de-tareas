<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class BeforeToday implements Rule
{


    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    protected $customValue;

    public function __construct($customValue)
    {
        $this->customValue = $customValue;
    }


    public function passes($attribute, $value)
    {
        return  Carbon::createFromFormat('d/m/Y',$this->customValue)->timestamp <= Carbon::createFromFormat('d/m/Y', $value)->timestamp;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Fecha de finalización debe ser mayor o igual la fecha fecha de inicio';
    }
}
