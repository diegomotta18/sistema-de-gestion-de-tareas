<?php

namespace App\Providers;

use Laravel\Dusk\DuskServiceProvider;
use Illuminate\Support\ServiceProvider;
use Laravel\Horizon\Horizon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }
//
//        Horizon::routeMailNotificationsTo('diegomotta18@gmail.com');
//
//        Horizon::auth(function ($request) {
//            if (is_null($request->user())) {
//                return false;
//            }
//
//            return $request->user()->isGod();
//        });
    }
}
