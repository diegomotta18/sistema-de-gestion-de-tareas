<?php

namespace App\Providers;

use App\Area;
use App\Event;
use App\Exception;
use App\Freeday;
use App\Holiday;
use App\License;
use App\Notice;
use App\Policies\AreaPolicy;
use App\Policies\EventPolicy;
use App\Policies\ExceptionPolicy;
use App\Policies\FreedayPolicy;
use App\Policies\HolidayPolicy;
use App\Policies\LicensePolicy;
use App\Policies\NoticePolicy;
use App\Policies\ProjectPolicy;
use App\Policies\StatusPolicy;
use App\Policies\TaskPolicy;
use App\Policies\TypeLicensePolicy;
use App\Policies\UserPolicy;
use App\Project;
use App\Status;
use App\Task;
use App\TypeLicense;
use App\User;
use Blade;
use function foo\func;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Task::class => TaskPolicy::class,
        Notice::class => NoticePolicy::class,
        Project::class => ProjectPolicy::class,
        Area::class => AreaPolicy::class,
        User::class => UserPolicy::class,
        Status::class => StatusPolicy::class,
        Exception::class => ExceptionPolicy::class,
        Holiday::class => HolidayPolicy::class,
        TypeLicense::class => TypeLicensePolicy::class,
        License::class => LicensePolicy::class,
        Freeday::class => FreedayPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //

        Blade::if('direccion',function (){
            return !is_null(auth()->user()->area->first()) && !is_null(auth()->user()->area) && (auth()->user()->area->name === 'Dirección' || auth()->user()->isGod());
        });
        Blade::if('inicio',function (){

            return (auth()->user()->isUser()|| auth()->user()->isAdmin() || auth()->user()->isGod())  && ( !is_null( auth()->user()->area->first()) && auth()->user()->area->name !== 'Dirección') ;
        });

        Blade::if('prensa', function(){
            return ((auth()->user()->isUser() || auth()->user()->isAdmin()) &&  auth()->user()->hasArea('Prensa') ) ||  auth()->user()->isGod() ;
        });

        Blade::if('novedad', function(){
            return  auth()->user()->isUser();
        });

        Blade::if('admin',function (){
            return auth()->user()->isAdmin() || auth()->user()->isGod();

        });
    }
}
