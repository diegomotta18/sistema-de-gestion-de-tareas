<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MsgFile extends Model
{
    //
    protected $table = 'msg_file';
    protected $datas = [ 'deleted_at','created_at', 'updated_at'];

    protected $fillable = [
        'msg_thread_id','filename', 'title', 'extension'
    ];
}
