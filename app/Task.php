<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

class Task extends Model
{
    //
    protected $dates = ['date_init', 'date_finish', 'deleted_at', 'created_at', 'updated_at'];

    protected $fillable = [
        'id',
        'title',
        'description',
        'date_init',
        'date_finish',
        'user_id',
        'project_id',
        'area_id',
        'task_id',
        'url',
        'urlnotice',
        'parrilla',
        'type_task_id',
        'hora'
    ];
    use SoftDeletes;


    // Area a la que pertence una tarea

    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    /**
     * Acceso a todos los comentarios de una tarea
     */
    public function comments()
    {
        return $this->morphMany('App\Task', 'commentable');
    }

    // Estado actual de la tarea

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    // Tipo de  tarea

    public function typeTask()
    {
        return $this->belongsTo(TypeTaskModel::class);
    }

    // subtareas de la tarea

    public function subtasks()
    {
        return $this->hasMany(Task::class);
    }

    // Proyecto a la que pertence una tarea

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    //usuarios reemplazos de la tarea

    public function replacements()
    {
        return $this->belongsToMany(User::class, 'task_replacement');
    }

    //usuarios asignados de la tarea

    public function assigneds()
    {
        return $this->belongsToMany(User::class, 'task_assigned');
    }

    //usuarios informado de la tarea

    public function informeds()
    {
        return $this->belongsToMany(User::class, 'task_informed');
    }

    //usuario creador de la tarea
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function checks()
    {
        return $this->hasMany(CheckUserTask::class);
    }

    // No se esta usando mas esta función??? Verificar

    public function isShow()
    {
        $val = false;
        if (count($this->checks()->get()) > 0) {
            foreach ($this->checks()->get() as $checks) {
                if ($checks->user_id === auth()->user()->id) {
                    $val = $checks->status;
                }
            }

            return $val;
        } else {
            return true;
        }


    }

    // Trae las tareas por usuario
    // Si es usuario administrador puede filtrar por usuario
    // Si es usuario normal filtra por sus propias tareas
    public function scopeTasksToUser($query, $id, $status)
    {
        if (auth()->user()->hasRole('user')) {
            if (!is_null($status)) {
                if ($status == 'Demorado') {
                    return $query->with('status')
                                 ->whereHas('assigneds', function ($query) use ($id) {
                                     $query->where('user_id', '=', $id);
                                 })->whereHas('status', function ($query) {
                            $query->where('name', '!=', 'Finalizado');
                        })->whereDate('date_finish', '<', Carbon::today())
                                 ->orderBy('created_at', 'desc');
                } else {
                    return $query->with('status')
                                 ->whereHas('status', function ($query) use ($status) {
                                     $query->where('name', '=', $status);
                                 })->orderBy('created_at', 'desc');
                }
            }
        } else {
            if (!is_null($status)) {
                if ($status == 'Demorado') {
                    return $query->with('status')->whereHas('assigneds', function ($query) use ($id) {
                        $query->where('user_id', '=', $id);
                    })->whereHas('status', function ($query) {
                        $query->where('name', '!=', 'Finalizado');
                    })->whereDate('date_finish', '<', Carbon::today())->orderBy('created_at', 'desc');
                } else {
                    return $query->with('status')->whereHas('status', function ($query) use ($status) {
                        $query->where('name', '=', $status);
                    })->whereHas('assigneds', function ($query) use ($id) {
                        $query->where('user_id', '=', $id);
                    });
                }
            } else {
                return $query->with('status')->whereHas('assigneds', function ($query) use ($id) {
                    $query->where('user_id', '=', $id);
                });
            }
        }
    }

    // Filtar por estado de la tarea
    public function scopeForStatus($query, $status)
    {
        if ($status == 'Demorado') {
            return $query->with('status')->whereHas('status', function ($query) {
                $query->where('name', '!=', 'Finalizado');
            })->whereDate('date_finish', '<', Carbon::today())->orderBy('created_at', 'desc');
        } else {
            return $query->with('status')->whereHas('status', function ($query) use ($status) {
                $query->where('name', '=', $status);
            })->orderBy('created_at', 'desc');
        }
    }

    // Trae las tareas asignadas y creadas del usuario
    public function scopeMyTasksAssigned($query, Project $project = null)
    {
        if (!is_null($project)) {
            if (auth()->user()->hasAreaUnique('Sistemas')) {

                return $query->where('project_id', $project->id)
                             ->whereHas('assigneds', function ($q) {
                                 $q->where('user_id', '=', auth()->user()->id);

                             })->orderBy('created_at', 'desc');
            } else {
                return $query->where('project_id', $project->id)
                             ->whereHas('assigneds', function ($q) {
                                 $q->where('user_id', '=', auth()->user()->id);

                             })->whereNull('type_task_id')
                             ->orderBy('created_at', 'desc');

            }
        } else {
            if (auth()->user()->hasAreaUnique('Sistemas')) {

                return $query->whereHas('assigneds', function ($q) {
                    $q->where('user_id', '=', auth()->user()->id);
                })
                             ->orWhereHas('user', function ($q) {
                                 $q->where('id', auth()->user()->id);
                             })
                             ->orderBy('created_at', 'desc');
            } else {

                return $query
                    ->whereHas('assigneds', function ($q) {
                        $q->where('user_id', '=', auth()->user()->id);
                    })
                    ->orWhereHas('user', function ($q) {
                        $q->where('id', auth()->user()->id);
                    })->whereNull('type_task_id')
                    ->orderBy('created_at', 'desc');
            }

        }
    }


    // Filtra por nombre de la tarea
    public function scopeSearch($query, $search)
    {
        return $query->where('title', 'like', "%$search%")
                     ->orderBy('created_at', 'desc');
    }

    public function files()
    {
        return $this->hasMany(TaskFile::class);
    }

    // Verifica si la tarea esta vencida

    public function isExpired()
    {
        if (!is_null($this->status)) {

            if ((Carbon::today()->timestamp > Carbon::parse($this->date_finish)->timestamp) && $this->status->name != "Finalizado" && $this->status->name != "Denegado" && is_null($this->parrilla) && is_null($this->type_task_id)) {
                return true;
            }

            return false;
        }
    }


    public function scopeForArea($query, Area $area = null)
    {
        if (auth()->user()->hasRole('admin') || (auth()->user()->hasRole('god'))) {

            if (auth()->user()->hasAreaUnique('Sistemas')) {
                if (!is_null($area)) {
                    return $query->where('area_id', $area->id)->orderBy('id', 'desc');
                }
                $areas = auth()->user()->areas->pluck('id');

                return $query->whereIn('area_id', $areas->toArray())->orWhereHas('user', function ($q) {
                    $q->where('id', auth()->user()->id);
                })->orWhereHas('assigneds', function ($q) {
                    $q->where('user_id', auth()->user()->id);
                })->orderBy('id', 'desc');
            } else {

                if (!is_null($area)) {
                    return $query->where('area_id', $area->id)->whereNull('type_task_id')->orderBy('id', 'desc');
                }
                $areas = auth()->user()->areas->pluck('id');

                return $query->whereIn('area_id', $areas->toArray())->orWhereHas('user',
                    function ($q) {
                        $q->where('id', auth()->user()->id);
                    })->whereNull('type_task_id')->orWhereHas('assigneds', function ($q) {
                    $q->where('user_id', auth()->user()->id);
                })->whereNull('type_task_id')->orderBy('id', 'desc');
            }


        }
    }

    public function scopeTasksForArea($query, $area_search)
    {
        return $query->where('area_id', $area_search);
    }

    public function scopeTasksForAreaAndUser($query, $area_search, $user_search)
    {
        return $query->where('area_id', $area_search)->whereHas('assigneds', function ($q) use ($user_search) {
            $q->where('user_id', $user_search);
        });
    }

    public function scopeFilterForProposeds($q, $request)
    {

        $q->with('informeds', 'assigneds', 'status', 'project', 'area', 'user')
          ->whereNull('parrilla')
          ->whereNull('type_task_id')
          ->whereHas('area', function ($q) {
              $q->where('name', 'Prensa');
          })->whereDoesntHave('assigneds', function ($q) {
                $q->whereNotNull('user_id');
            });

        if (!is_null($request->input('fecha'))) {

            $q->whereDate('created_at', '=', date('Y-m-d', strtotime(str_replace('/', '-', $request->input('fecha')))));

        }

        return $q;
    }


    public function scopeFilterForEvents($q, $request)
    {
        $q->with('informeds', 'assigneds', 'status', 'project', 'area', 'user')
          ->whereNull('parrilla')
          ->whereNull('type_task_id')
          ->whereHas('area', function ($q) {
              $q->where('name', 'Prensa');

          })->whereHas('assigneds', function ($q) {
                $q->whereNotNull('user_id');
            });

        if (!is_null($request->input('fecha'))) {

            $q->whereDate('created_at', '=', date('Y-m-d', strtotime(str_replace('/', '-', $request->input('fecha')))));

        }

        return $q;
    }


    public function scopeFilterForProposedsMorning($q, $request)
    {
        $fecha = (string)Carbon::today()->format('Y-m-d');
        $horainicio = $fecha." 07:00:00";
        $horafin = $fecha." 14:00:00";
        $q->with('informeds', 'assigneds', 'status', 'project', 'area', 'user')
          ->whereNull('parrilla')
          ->whereNull('type_task_id')
          ->whereHas('area', function ($q) {
              $q->where('name', 'Prensa');
          })->whereDoesntHave('assigneds', function ($q) {
                $q->whereNotNull('user_id');
            });
        if (!is_null($request->input('fecha'))) {
            $fecha = (string)Carbon::createFromFormat('d/m/Y', $request->input('fecha'))->format('Y-m-d');
            $horainicio = $fecha." 07:00:00";
            $horafin = $fecha." 14:00:00";
            $q->whereBetween('created_at', [
                Carbon::createFromFormat('Y-m-d H:i:s', $horainicio),
                Carbon::createFromFormat('Y-m-d H:i:s', $horafin)
            ]);

        } else {
            $q->whereBetween('created_at', [
                Carbon::createFromFormat('Y-m-d H:i:s', $horainicio),
                Carbon::createFromFormat('Y-m-d H:i:s', $horafin)
            ]);
        }

        return $q;
    }

    public function scopeFilterForProposedsAfternoon($q, $request)
    {
        $fecha = (string)Carbon::today()->format('Y-m-d');
        $horainicio = $fecha." 14:01:00";
        $horafin = $fecha." 22:00:00";
        $q->with('informeds', 'assigneds', 'status', 'project', 'area', 'user')
          ->whereNull('parrilla')
          ->whereNull('type_task_id')
          ->whereHas('area', function ($q) {
              $q->where('name', 'Prensa');
          })->whereDoesntHave('assigneds', function ($q) {
                $q->whereNotNull('user_id');
            });

        if (!is_null($request->input('fecha'))) {
            $fecha = (string)Carbon::createFromFormat('d/m/Y', $request->input('fecha'))->format('Y-m-d');
            $horainicio = $fecha." 14:01:00";
            $horafin = $fecha." 22:00:00";
            $q->whereBetween('created_at', [
                Carbon::createFromFormat('Y-m-d H:i:s', $horainicio),
                Carbon::createFromFormat('Y-m-d H:i:s', $horafin)
            ]);

        } else {
            $q->whereBetween('created_at', [
                Carbon::createFromFormat('Y-m-d H:i:s', $horainicio),
                Carbon::createFromFormat('Y-m-d H:i:s', $horafin)
            ]);
        }

        return $q;
    }


    /*
     *
     * Busca las tareas por id de proyecto y estado , por fechas si es necesario
     *
     * */
    public function scopeByProjectAndStatus($query, $status, $proyect_id, $date_init = null, $date_finish = null)
    {

        if (!is_null($date_init) && !is_null($date_finish)) {
            $init = Carbon::createFromFormat('d/m/Y', $date_init);
            $finish = Carbon::createFromFormat('d/m/Y', $date_finish);

            return $query->where('project_id', $proyect_id)
                         ->whereHas('status', function ($q) use ($status) {
                             $q->where('name', $status);
                         })->whereBetween('date_init', [$init, $finish])
                         ->orderBy('updated_at', 'desc');
        }

        return $query->where('project_id', $proyect_id)->whereHas('status', function ($q) use ($status) {
            $q->where('name', $status);
        })->orderBy('updated_at', 'desc');

    }
}


