<?php

namespace App\Policies;

use App\Area;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AreaPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    public function show(User $user,Area $area)
    {
        if($user->hasRole('user') ){
            if ($user->areas->contains($area)){
                return true;
            }else{
                return false;
            }
        }
        else if ($user->hasRole('admin')){
            return true;
        }
        else if ($user->hasRole('god')){
            return true;
        }
    }


    public function store(User $user){
        return $user->hasRole('god');
    }

    public function destroy(User $user){
        return $user->hasRole('god');
    }

    public function update(User $user){
        return $user->hasRole('god');
    }

    public function index(User $user){
        return $user->hasRole('god');
    }
}
