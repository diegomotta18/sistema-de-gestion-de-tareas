<?php

namespace App\Policies;

use App\Event;
use App\Task;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EventPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(User $user){

        if ($user->hasRole('admin') ||$user->hasRole('user') ) {
            if ($user->hasArea('Prensa')) {
                return true;
            }
        } else {
            if ($user->hasRole('god')) {
                return true;
            }
        }
    }

    public function create(User $user){
        return $user->hasArea('Prensa');

    }

    public function store(User $user){
        return $user->hasArea('Prensa');

    }

    public function update(User $user, Event $event)
    {
        if ($user->hasRole('user')) {

            if ($user->hasArea('Prensa') && $user->id === $event->user_id) {
                return true;
            }

        } else {
            if ($user->hasRole('admin')) {
                if ($user->hasArea('Prensa')) {
                    return true;
                }
            } else {
                if ($user->hasRole('god')) {
                    return true;
                }
            }
        }
        return false;
    }

    public function show(User $user, Event $event)
    {
        if ($user->hasRole('god')) {
            return true;
        } else {
            if ($user->hasRole('admin')) {
                if ($user->hasArea('Prensa')) {
                    return true;
                }
            } else {
                if ($user->hasRole('user')) {
                    if ($user->hasArea('Prensa') && ( $user->id === $event->user_id || !$user->hasEventAssigned($event->id))) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function destroy(User $user, Event $event)
    {
        if ($user->hasRole('god')) {
            return true;
        } else {
            if ($user->hasRole('admin')) {
                if ($user->hasArea('Prensa')) {
                    return true;
                }
            }
        }
        return false;

    }

    public function replyevent(User $user, Event $event)
    {
        return true;
    }

    public function finishevent(User $user, Event $event)
    {
        if ($user->hasRole('user')) {
            return $user->id === $event->user_id;
        } else {
            if ($user->hasRole('admin')) {
                if ($user->id === $event->user_id) {
                    return true;
                }
            } else {
                if ($user->hasRole('god')) {
                    return true;
                }
            }
        }
    }

    public function processevent(User $user, Event $event)
    {
        if ($user->hasRole('user')) {
            return true;
        } else {
            if ($user->hasRole('admin')) {
                return true;
            } else {
                if ($user->hasRole('god')) {
                    return true;
                }
            }
        }
    }

    public function changestatus(User $user, Task $event)
    {
        if ($user->hasRole('user')) {
            return $user->id === $event->user_id;
        } else {
            if ($user->hasRole('admin')) {
                return true;
            } else {
                if ($user->hasRole('god')) {
                    return true;
                }
            }
        }
    }

}
