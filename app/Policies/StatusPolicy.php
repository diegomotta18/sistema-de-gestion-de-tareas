<?php

namespace App\Policies;

use App\Event;
use App\Status;
use App\Task;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class StatusPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(User $user)
    {
        return $user->hasRole('god');


    }

    public function store(User $user)
    {
        return $user->hasRole('god');

    }


    public function update(User $user)
    {
        return $user->hasRole('god');

    }


    public function destroy(User $user)
    {
        return $user->hasRole('god');
    }

}
