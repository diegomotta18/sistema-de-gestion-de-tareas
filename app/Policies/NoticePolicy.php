<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class NoticePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function create(User $user){
        return ($user->hasRole('admin')||$user->hasRole('god')) ? true: false;
    }

    public function update(User $user){
        return ($user->hasRole('admin')||$user->hasRole('god')) ? true: false;
    }

    public function destroy(User $user){
        return ($user->hasRole('admin')||$user->hasRole('god')) ? true: false;
    }

    public function before($user, $ability){
        if ($user->hasRole('admin')||$user->hasRole('god')){
            return true;
        }
        return false;
    }
}
