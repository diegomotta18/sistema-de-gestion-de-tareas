<?php

namespace App\Policies;

use App\Task;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function update(User $user, Task $task)
    {
        if ($user->hasRole('user')) {
            return $user->id === $task->user_id;
        } else {
            if ($user->hasRole('admin')) {
//                if ($task->area_id === $user->area_id || $user->id === $task->user_id) {
                    return true;
//                }
            } else {
                if ($user->hasRole('god')) {
                    return true;
                }
            }
        }
    }

    public function show(User $user, Task $task)
    {
        if ($user->hasRole('god')) {
            return true;
        } else {
            if ($user->hasRole('admin')) {
//                $areas = $user->areas->pluck('id');
//                if ($task->informeds->contains($user->id) || $task->assigneds->contains($user->id) || in_array($task->area_id,
//                        $areas->toArray()) || $user->id === $task->user_id
//                ) {
                    return true;
//                }
            } else {
                if ($user->hasRole('user')) {
                    if ($task->informeds->contains($user->id) || $task->assigneds->contains($user->id) || $user->id === $task->user_id || $user->hasArea('Prensa')) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function destroy(User $user, Task $task)
    {
        if ($user->hasRole('god')) {
            return true;
        } else {
            if ($user->hasRole('admin')) {
                if ($task->area_id === $user->area_id || $user->id === $task->user_id) {
                    return true;
                }
            } else {
                if ($user->hasRole('user')) {
                    return $user->id === $task->user_id;
                }
            }
        }

        return false;

    }

    public function replyTask(User $user, Task $task)
    {
        return true;
    }

    public function finishtask(User $user, Task $task)
    {
        if ($user->hasRole('user')) {
            return $user->id === $task->user_id;
        } else {
            if ($user->hasRole('admin')) {
                if ($task->area_id === $user->area_id || $user->id === $task->user_id) {
                    return true;
                }
            } else {
                if ($user->hasRole('god')) {
                    return true;
                }
            }
        }
    }


    public function pingeholetask(User $user, Task $task)
    {
        if ($user->hasRole('user')) {
            return $user->id === $task->user_id;
        } else {
            if ($user->hasRole('admin')) {
                if ($task->area_id === $user->area_id || $user->id === $task->user_id) {
                    return true;
                }
            } else {
                if ($user->hasRole('god')) {
                    return true;
                }
            }
        }
    }

    public function processtask(User $user, Task $task)
    {
        if ($user->hasRole('user')) {
            return true;
        } else {
            if ($user->hasRole('admin')) {
                return true;
            } else {
                if ($user->hasRole('god')) {
                    return true;
                }
            }
        }
    }

    public function denegatetask(User $user, Task $task)
    {
        if ($user->hasRole('user')) {
            return $user->id === $task->user_id;
        } else {
            if ($user->hasRole('admin')) {
                return true;
            } else {
                if ($user->hasRole('god')) {
                    return true;
                }
            }
        }
    }

    public function changestatus(User $user, Task $task)
    {
        if ($user->hasRole('user')) {
            return $user->id === $task->user_id;
        } else {
            if ($user->hasRole('admin')) {
                return true;
            } else {
                if ($user->hasRole('god')) {
                    return true;
                }
            }
        }
    }

    public function accesstasksproposed(User $user){
        if ($user->hasRole('user') && $user->hasArea('Prensa')) {
            return true;
        } else {
            if ($user->hasRole('admin') && $user->hasArea('Prensa') || $user->hasArea('Dirección')) {
                return true;
            } else {
                if ($user->hasRole('god')) {
                    return true;
                }
            }
        }
        return false;
    }

    public function accessTasksEvent(User $user){
        if ($user->hasRole('user') && $user->hasArea('Prensa')) {
            return true;
        } else {
            if ($user->hasRole('admin') && $user->hasArea('Prensa') || $user->hasArea('Dirección')) {
                return true;
            } else {
                if ($user->hasRole('god')) {
                    return true;
                }
            }
        }
        return false;

    }

}
