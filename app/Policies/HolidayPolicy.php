<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class HolidayPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function store(User $user)
    {
        return $user->hasRole('god') || ($user->hasAreaUnique('RRHH') && $user->isAdmin() );

    }

    public function show(User $user)
    {
        return $user->hasRole('god') || ($user->hasAreaUnique('RRHH') && $user->isAdmin() );

    }

    public function update(User $user)
    {
        return $user->hasRole('god') || ($user->hasAreaUnique('RRHH') && $user->isAdmin() );

    }

    public function index(User $user)
    {


        return $user->hasRole('god') || ($user->hasAreaUnique('RRHH') && $user->isAdmin() );
    }

    public function destroy(User $user)
    {
        return $user->hasRole('god') || ($user->hasAreaUnique('RRHH') && $user->isAdmin() );

    }

    public function finish(User $user)
    {
        return $user->hasRole('god') || ($user->hasAreaUnique('RRHH') && $user->isAdmin() );

    }
}
