<?php

namespace App\Policies;

use App\Area;
use App\Project;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function create(User $user)
    {
            return $user->hasRole('god') || ($user->hasRole('admin') && $user->hasAreaUnique('RRHH'));

    }

    public function show(User $user)
    {
        return $user->hasRole('god') || ($user->hasRole('admin') && $user->hasAreaUnique('RRHH'));

    }

    public function update(User $user)
    {
        return $user->hasRole('god') || ($user->hasRole('admin') && $user->hasAreaUnique('RRHH'));

    }

    public function index(User $user)
    {
        return $user->hasRole('god') || ($user->hasRole('admin') && $user->hasAreaUnique('RRHH'));
    }

    public function destroy(User $user )
    {
        return $user->hasRole('god') || ($user->hasRole('admin') && $user->hasAreaUnique('RRHH'));

    }

}
