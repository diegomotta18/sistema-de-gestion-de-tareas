<?php

namespace App\Policies;

use App\Area;
use App\Project;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExceptionPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function show(User $user)
    {
        return $user->hasRole('god');

    }


    public function index(User $user)
    {
        return $user->hasRole('god');
    }

    public function destroy(User $user )
    {
        return $user->hasRole('god');

    }

}
