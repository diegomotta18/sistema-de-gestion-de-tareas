<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

class Project extends Model
{
    //
    use SoftDeletes;
    protected $datas = [ 'deleted_at','created_at', 'updated_at'];

    protected $fillable = [
        'name',
        'description','area_id','user_id'
    ];


    public function area(){

        return $this->belongsTo(Area::class);
    }

    public function user(){

        return $this->belongsTo(User::class);
    }

    public function tasks(){

        return $this->hasMany(Task::class);
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y h:m');
    }

    public function scopeMyProjects($query)
    {

        if (auth()->user()->hasRole('user')){
            return $query->whereHas('tasks', function ($q) {

                $q->whereNull('parrilla')->whereHas('assigneds', function ($k) {

                    $k->where('user_id', '=', auth()->user()->id);

                })->orWhereHas('user',function ($q){

                    $q->where('id', auth()->user()->id);

                })->orderBy('created_at', 'desc');

            });
        }else if (auth()->user()->hasRole('admin') || auth()->user()->hasRole('god') ){
            $areas = auth()->user()->areas->pluck('id');
            $query->whereIn('area_id', $areas->toArray())->orWhereHas('tasks', function ($q) {
                $q->whereNull('parrilla')->whereHas('assigneds', function ($q) {
                    $q->where('user_id', auth()->user()->id);
                });
            })->orderBy('created_at',   'desc');
        }

    }

    public function scopeForArea($query,Area $area= null){
        if(auth()->user()->hasRole('admin') ||auth()->user()->hasRole('god')  ){
            if(!is_null($area)){
                return $query->where('area_id',$area->id)->orderBy('id', 'desc');
            }

            $user = User::findOrFail(auth()->user()->id);
            $areas = $user->areas->pluck('id')->toArray();
            return $query->whereIn('area_id',$areas)->orderBy('id', 'desc');

        }elseif (auth()->user()->hasRole('god')){
            return $query->orderBy('created_at', 'desc');
        }
    }
}
