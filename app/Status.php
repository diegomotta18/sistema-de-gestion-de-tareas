<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Status extends Model
{
    //
    use SoftDeletes;

    protected $table = 'statuses';

    protected $datas = [ 'deleted_at','created_at', 'updated_at'];

    protected $fillable = [
        'name','color'
    ];

    public function task(){
        return $this->hasOne(Task::class);
    }

    public function event(){
        return $this->hasOne(Event::class);
    }




}
