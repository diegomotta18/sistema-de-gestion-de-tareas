<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notice extends Model
{
    //
    use SoftDeletes;

    protected $fillable = [
        'title',
        'description','user_id'
    ];


    public function user(){

        return $this->belongsTo(User::class);
    }

    public function users(){

        return $this->belongsToMany(User::class);
    }

    public function scopeMyNotices($query){
        return $query->with('users')->whereHas('users', function ($query) {

            $query->where('user_id', '=', auth()->user()->id);

        });
    }

    public function getDateTime(){
        return  $this->created_at->format('d/m/Y h:m') . ' hs ';
    }

}
