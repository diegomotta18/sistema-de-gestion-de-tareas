<?php

namespace App\Console;

use App\Console\Commands\CheckEventServed;
use App\Console\Commands\CheckTasksWeek;
use App\Console\Commands\CkeckTaskExpired;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //

        CkeckTaskExpired::class,
        CheckTasksWeek::class

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('check:taskexpired')->dailyAt('07:45');
        $schedule->command('check:taskperweek')->weekly()->mondays()->at('07:45');

        // Horizon snapshots
        $schedule->command('horizon:snapshot')->everyFiveMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
//        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
