<?php

namespace App\Console\Commands;

use App\Mail\NotifyTaskExpired;
use App\Notifications\ExpiredNotification;
use App\Task;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class CkeckTaskExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:taskexpired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command look for expired task';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $tasks = Task::all();

        foreach ($tasks->chunk(10 ) as $t) {
            foreach ($t as $task) {

                if ($task->isExpired()) {
//                    // Notificar al informados que se venció la tarea
//
//                    foreach ($task->informeds()->get()->chunk(3) as $c) {
//                        foreach ($c as $u) {
//                            $u->notify(new ExpiredNotification($task));
//
//                            Mail::to($u->email)->queue(new NotifyTaskExpired($task));
//                        }
//                    }
                    // Notificar al asignado que se venció la tarea

                    foreach ($task->assigneds()->get()->chunk(3) as $c) {
                        foreach ($c as $u) {
                            $u->notify(new ExpiredNotification($task));
                            Mail::to($u->email)->queue(new NotifyTaskExpired($task));
                        }
                    }
                }
            }

        }

        $this->info('Finish check expired task');

    }
}
