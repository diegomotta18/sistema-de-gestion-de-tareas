<?php

namespace App\Console\Commands;

use App\Mail\SendExpiredTasks;
use App\Notifications\ExpiredTasksNotification;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class CheckTasksWeek extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:taskperweek';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command look for expired task per week';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $user = User::all();
        $tasks = [];
        $array = null;
        foreach ($user->chunk(10 ) as $u) {
            foreach ($u as $user) {

                foreach ($user->tasksAssigneds()->get() as $key => $value ){

                    if ($value->isExpired()) {
                        $tasks[] =  [$value];
                    }

                }


                if (!is_null($tasks) && !empty($tasks)){
                    Mail::to($user->email)->queue(new SendExpiredTasks($user,$tasks));
                    $tasks = [];
                }
            }
        }
        $this->info('Finish check expired task');
    }
}
