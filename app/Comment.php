<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Comment extends Model
{
    //
    protected $datas = [ 'deleted_at','created_at', 'updated_at'];

    protected $fillable = [
        'id','comment', 'task_id','user_id','commentable_type'
    ];


    /**
     * Acceso a todos los comentarios del modelo relacionado
     */
    public function commentable()
    {
        return $this->morphTo();
    }


    public function task()
    {
        return $this->belongsTo(Task::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function subcomments(){
        return $this->hasMany(Comment::class);
    }

    public function getCreatedAtAttribute($value)
    {

        return Carbon::parse($value)->format('d/m/Y h:m');

    }


    public function files()
    {
        return $this->hasMany(CommentFile::class);
    }


}
