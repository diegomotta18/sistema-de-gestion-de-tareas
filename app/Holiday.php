<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Holiday extends Model
{
    //
    use SoftDeletes;

    protected $table = 'holidays';
    protected $dates = ['date_init','date_finish','created_at','updated_at','deleted_at','date_reinstate'];
    protected $fillable = [
        'id', 'observations','finish_holiday','user_id','date_finish','date_reinstate','date_init','url'
    ];

    //usuario seleccionado que se va ir de vacaciones


    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }


    //usuarios que reemplazan a la persona que se va de vacaciones

    public function replacements()
    {
        return $this->belongsToMany(User::class, 'holiday_replacements');
    }
}
