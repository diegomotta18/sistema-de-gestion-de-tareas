<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendExpiredTasks extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $tasks;
    public $user;

    public function __construct($user, $tasks)
    {
        //

        $this->tasks = $tasks;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Tareas vencidas de la semana')
                    ->view('emails.task.sendExpiredTasks')->with([
                'tasks' => $this->tasks,
            ]);
    }
}
