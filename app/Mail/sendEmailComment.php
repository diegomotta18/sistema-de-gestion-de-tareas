<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendEmailComment extends Mailable
{
    use Queueable, SerializesModels;


    /**
     * Build the message.
     *
     * @return $this
     */


    public $comment;
    public $task_title;
    public $area;
    public $task_description;
    public  $url;
    public $user;
    public $project;
    public function __construct($user,$comment,$task_title,$url,$area,$project)
    {
        $this->comment = $comment;
        $this->task_title = $task_title;
        $this->url = $url;
        $this->area = $area;
        $this->user = $user;
        $this->project = $project;

    }



    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Nuevo comentario en la tarea: '.$this->task_title)
            ->view('emails.comments.created');


    }
}
