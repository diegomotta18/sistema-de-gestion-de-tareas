<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class HolidayCoverageMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $holiday;
    public $auth;
    public function __construct($holiday,$auth)
    {
        //
        $this->holiday =$holiday;
        $this->auth = $auth;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'user'           => $this->holiday->user->name,
            'date_init'      => $this->holiday->date_init->format('d/m/Y'),
            'date_finish'    => $this->holiday->date_finish->format('d/m/Y'),
            'date_reinstate' => $this->holiday->date_reinstate->format('d/m/Y'),
            'observations'   => $this->holiday->observations,
            'replacements'   => $this->holiday->replacements
        ];

        return $this->subject('Cobertura durante fechas de vacaciones')
                    ->view('emails.holiday.coverage', compact('data'));    }
}
