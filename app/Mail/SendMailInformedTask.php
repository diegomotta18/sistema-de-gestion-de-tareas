<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailInformedTask extends Mailable
{
    use Queueable, SerializesModels;

    public $task;
    public $auth;
    public function __construct($task,$auth)
    {
        $this->task = $task;
        $this->auth = $auth;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject('Nueva tarea informada: '.$this->task->title)
            ->view('emails.task.sendInformedTask');
    }
}
