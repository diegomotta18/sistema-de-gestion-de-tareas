<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMsgThread extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $description;
    public $user;
    public $subject;
    public $url;

    public function __construct($user,$subject,$description,$url)
    {
        $this->subject = $subject;
        $this->description = $description;
        $this->user = $user;
        $this->url = $url;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $data = [
            'subject' => $this->subject,
            'message' => $this->description,
            'user' => $this->user->name,
            'url' => $this->url
        ];

        return $this->subject('Nuevo mensaje: '.$this->subject)
            ->view('emails.threads.created',compact('data'));

    }
}
