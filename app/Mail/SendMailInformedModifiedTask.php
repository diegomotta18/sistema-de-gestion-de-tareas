<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailInformedModifiedTask extends Mailable
{
    use Queueable, SerializesModels;

    public $task;
    public $auth;
    public function __construct($task,$auth)
    {
        $this->task = $task;
        $this->auth = $auth;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject('La tarea informada a sido modificada: '.$this->task->title)
                    ->view('emails.task.sendInformedModifiedTask');
    }
}
