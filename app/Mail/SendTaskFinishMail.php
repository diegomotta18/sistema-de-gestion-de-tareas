<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendTaskFinishMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $task;

    public function __construct($task)
    {
        //
        $this->task = $task;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'title' => $this->task->title,
            'description' => $this->task->description,
            'status' => $this->task->user->name,
            'url' => $this->task->url
        ];

        return $this->subject('A cambiado el estado de la tarea '. $this->task->title)
            ->view('emails.task.taskfinish',compact('data'));
    }
}
