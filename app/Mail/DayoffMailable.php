<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DayoffMailable extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $dayoff;
    public $auth;
    public function __construct($dayoff,$auth)
    {
        //
        $this->dayoff = $dayoff;
        $this->auth = $auth;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'user'           => $this->dayoff->user->name,
            'date_taken'      => $this->dayoff->date_taken->format('d/m/Y'),
            'observations'   => $this->dayoff->observations,
        ];

        return $this->subject('Dia franco '.$this->dayoff->date_taken->format('d/m/Y')  )
                    ->view('emails.dayoff.dayoff', compact('data'));    }
}
