<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendEmailMessage extends Mailable
{
    use Queueable, SerializesModels;


    /**
     * Build the message.
     *
     * @return $this
     */


    public $description;
    public $user;
    public $subject;

    public function __construct($user,$subject,$description)
    {
        $this->subject = $subject;
        $this->description = $description;
        $this->user = $user;


    }



    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'subject' => $this->subject,
            'message' => $this->description,
            'user' => $this->user->name,
        ];

        return $this->subject('Nuevo mensaje: '.$this->subject)
            ->view('emails.messages.created',compact('data'));

    }
}
