<?php

namespace App\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEmailNotice extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     *
     * @var int
     */


    public $notice;

    public function __construct($notice)
    {
        $this->notice = $notice;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'title' => $this->notice->title,
            'description' => $this->notice->description,
        ];


        return $this->subject('Nueva novedad: ' . $this->notice->title)
            ->view('emails.notices.created', compact('data'));
    }
}
