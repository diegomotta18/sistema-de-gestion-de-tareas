<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LicenseMailable extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $license;
    public $auth;

    public function __construct($license, $auth)
    {
        //
        $this->license =$license;
        $this->auth = $auth;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'user'           => $this->license->user->name,
            'date_init'      => $this->license->date_init->format('d/m/Y'),
            'date_finish'    => $this->license->date_finish->format('d/m/Y'),
            'date_reinstate' => $this->license->date_reinstate->format('d/m/Y'),
            'observations'   => $this->license->observations,
            'type_license'   => $this->license->typeLicense->name,

        ];

        return $this->subject('Licencia por '.$this->license->typeLicense->name )
                    ->view('emails.license.license', compact('data'));
    }
}
