<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserCreated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var \App\User
     */
    public $user;

    /**
     * @var
     */
    public $password;
    public $message;

    /**
     * Create a new message instance.
     *
     * @param \App\User $user
     */
    public function __construct($message, $user, $password)
    {
        $this->user = $user;
        $this->password = $password;
        $this->message = $message;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject($this->message .config('app.name'))
            ->view('emails.users.created');


    }
}
