<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class License extends Model
{
    //
    use SoftDeletes;

    protected $table = 'licenses';
    protected $dates = ['date_init','date_finish','created_at','updated_at','deleted_at','date_reinstate'];
    protected $fillable = [
        'id','certificate','user_id','date_finish','date_reinstate','date_init','type_license_id'
    ];

    public function typeLicense(){
        return $this->belongsTo(TypeLicense::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function observations(){
        return $this->hasMany(Observation::class);
    }
}
