<?php

namespace App\Jobs;

use App\Mail\SendEmailTask;
use App\Mail\SendTaskProcessMail;
use App\Notifications\TaskNotification;
use App\Notifications\TaskProcessNotifiation;
use App\Notifications\TaskProcessNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class TaskProcessJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 5;

    public $task;
    public $auth;
    public $users;

    public function __construct($users, $task,$auth)
    {
        $this->task = $task;
        $this->auth = $auth;
        $this->users = $users;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->users as $u) {
            if ($u->id != $this->auth->id) {
                $u->notify(new TaskProcessNotification($this->task));
                Mail::to($u->email)->queue(new SendTaskProcessMail($this->task));
            }
        }

    }
}
