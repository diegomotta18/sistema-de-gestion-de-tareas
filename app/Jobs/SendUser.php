<?php

namespace App\Jobs;

use App\Mail\UserCreated;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * @var \App\User
     */
    public $user;

    /**
     * @var
     */
    public $password;
    public $message;

    public $tries = 5;

    /**
     * Create a new message instance.
     *
     * @param \App\User $user
     */
    public function __construct($user, $password, $message)
    {
        $this->user = $user;
        $this->password = $password;
        $this->message = $message;

    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
            Mail::to($this->user->email)
                ->queue(new UserCreated($this->message, $this->user, $this->password));


    }
}
