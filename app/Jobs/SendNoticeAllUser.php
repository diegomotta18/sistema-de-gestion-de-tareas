<?php

namespace App\Jobs;

use App\Mail\SendEmailNotice;
use App\Notifications\NovedadNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;


class SendNoticeAllUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 5;

    public $users;
    public $notice;
    public $auth;

    public function __construct($users, $notice, $auth)
    {
        //
        $this->users = $users;
        $this->notice = $notice;
        $this->auth = $auth;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $this->notice->users()->attach($this->users);
        $this->notice->save();
        foreach ($this->users->chunk(3) as $c) {
            foreach ($c as $u) {
                if ($u->id != $this->auth->id) {
                    $u->notify(new NovedadNotification($this->notice, $this->auth));
                    Mail::to($u->email)
                        ->queue(new SendEmailNotice($this->notice));
                }
            }
        }

    }
}
