<?php

namespace App\Jobs;

use App\Mail\SendCommentInEventEmail;
use App\Mail\sendEmailComment;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendCommentInEventJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 5;

    public $auth;
    public $assigneds;
    public $comment;
    public $event;

    public function __construct( $auth,$assigneds, $comment,$event)
    {
        //
        $this->auth = $auth;
        $this->assigneds = $assigneds;
        $this->comment = $comment;
        $this->event = $event;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //

        foreach ($this->assigneds as $u) {
            if ($u->id != $this->auth->id) {
                Mail::to($u->email)
                    ->queue(new SendCommentInEventEmail($this->auth,$this->comment, $this->event->name,$this->event->url));
            }
        }



    }
}
