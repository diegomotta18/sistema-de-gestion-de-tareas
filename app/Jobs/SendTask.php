<?php

namespace App\Jobs;

use App\Mail\SendEmailTask;
use App\Notifications\TaskNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendTask implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 5;

    public $users;
    public $task;
    public $auth;

    public function __construct($users, $task, $auth)
    {
        $this->users = $users;
        $this->task = $task;
        $this->auth = $auth;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->users as $u) {
            if ($u->id != $this->auth->id) {
                $u->notify(new TaskNotification($this->task, $this->auth));
                Mail::to($u->email)
                    ->queue(new SendEmailTask($this->task,$this->auth));
            }
        }

    }
}
