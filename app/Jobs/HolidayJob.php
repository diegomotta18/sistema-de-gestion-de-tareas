<?php

namespace App\Jobs;

use App\Mail\HolidayCoverageMail;
use App\Mail\HolidayCreateMail;
use App\Notifications\HolidayCreatedNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class HolidayJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected  $tries=5;
    protected  $auth;
    protected  $holiday;


    public function __construct($holiday,$auth)
    {
        //
        $this->holiday = $holiday;
        $this->auth =$auth;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $this->holiday->user->notify(new HolidayCreatedNotification($this->holiday,$this->auth));
        Mail::to($this->holiday->user->email)->queue(new HolidayCreateMail($this->holiday,$this->auth));
        foreach ($this->holiday->replacements as $u) {
            Mail::to($u)->queue(new HolidayCoverageMail($this->holiday,$this->auth));
        }
    }
}
