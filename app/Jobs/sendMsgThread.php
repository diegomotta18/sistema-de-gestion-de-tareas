<?php

namespace App\Jobs;

use App\Mail\sendEmailMessage;
use App\Mail\SendEmailNotice;
use App\Notifications\MessageNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendMsgThread implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 5;

    public $users;
    public $user;
    public $url;
    public $message;
    public $subject;

    public function __construct($users,$user , $subject,$message,$url)
    {
        //
        $this->user = $user;
        $this->url = $url;
        $this->users = $users;
        $this->message = $message;
        $this->subject = $subject;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        foreach ($this->users as $u) {
            if($this->user->id != $u->id){
                Mail::to($u->email)
                    ->queue(new \App\Mail\SendMsgThread($this->user,$this->subject, $this->message,$this->url));
            }

        }
    }
}