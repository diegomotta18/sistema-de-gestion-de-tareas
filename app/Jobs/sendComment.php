<?php

namespace App\Jobs;

use App\Mail\sendEmailComment;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class sendComment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 5;

    public $auth;
    public $users;

    public $comment;
    public $task;
    public $area;
    public $project;
    public function __construct( $auth,$users,  $comment,$task, $area, $project)
    {
        //
        $this->auth = $auth;
        $this->users = $users;
        $this->comment = $comment;
        $this->task = $task;
        $this->area = $area;
        $this->project = $project;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        foreach ($this->users as $u) {
            if ($u->id != $this->auth->id) {
                Mail::to($u->email)
                    ->queue(new SendEmailComment($this->auth, $this->comment, $this->task->title, $this->task->url,
                        $this->area,$this->project));
            }
        }

    }
}
