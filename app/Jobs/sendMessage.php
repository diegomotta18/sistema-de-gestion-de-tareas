<?php

namespace App\Jobs;

use App\Mail\sendEmailMessage;
use App\Mail\SendEmailNotice;
use App\Notifications\MessageNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 5;

    public $users;
    public $sender;
    public $message;

    public function __construct($users, $sender, $message)
    {
        //
        $this->users = $users;
        $this->sender = $sender;
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        foreach ($this->users as $u) {
            $u->notify(new MessageNotification($this->message));
            Mail::to($u->email)
                ->queue(new SendEmailMessage($this->sender, $this->message->subject, $this->message->description));
        }
    }
}
