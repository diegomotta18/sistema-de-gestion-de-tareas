<?php

namespace App\Jobs;

use App\Mail\SendEmailNotice;
use App\Notifications\NovedadNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendNotice implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 5;

    public $notice;
    public $auth;
    public $users;

    public function __construct($users, $notice, $auth)
    {
        //
        $this->notice = $notice;
        $this->auth = $auth;
        $this->users = $users;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->users as $u) {
            if ($u->id != $this->auth->id) {
                $u->notify(new NovedadNotification($this->notice, $this->auth));
                Mail::to($u->email)
                    ->queue(new SendEmailNotice($this->notice));
            }
        }

    }
}
