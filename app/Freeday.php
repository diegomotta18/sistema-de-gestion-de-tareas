<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Freeday extends Model
{
    //
    //
    use SoftDeletes;

    protected $table = 'freeday';
    protected $dates = ['created_at','updated_at','deleted_at','date_taken'];
    protected $fillable = [
        'id', 'date_taken','observations','user_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
