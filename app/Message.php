<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

class Message extends Model
{
    //
    use SoftDeletes;
    protected $datas = [ 'deleted_at','created_at', 'updated_at'];

    protected $fillable = [
        'subject',
        'description','user_id'
    ];

    public function user(){

        return $this->belongsTo(User::class,'user_id');
    }
    public function users(){

        return $this->belongsToMany(User::class);
    }

    public function getCreatedAtAttribute($value)
    {

        return Carbon::parse($value)->format('d/m/Y h:m');

    }
}
