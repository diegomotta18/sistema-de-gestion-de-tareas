<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exception extends Model
{
    //

    protected $table = 'exceptions';

    protected $fillable = [
        'id', 'content','created_at','message','line','code','trace','file'
    ];


}
