<?php

namespace App\Exceptions;

use App\Mail\ExceptionOccured;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        $this->saveExpection($exception); // sends an email
        parent::report($exception);

    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof AuthorizationException) {
//            $notification = [
//                'alert-type' => 'error',
//                'message' => 'No tiene los privelegios para realizar esta acción!'
//            ];
//            return redirect()->back()->with($notification);
            if (auth()->guest()) {
                return redirect()->route('login');
            }

            return redirect()->route('home');
        }

        if ($this->isHttpException($exception)) {
//            dd($exception->getStatusCode());
            switch (intval($exception->getStatusCode())) {
                // not found
                case 405:
//                    $notification = [
//                        'status' => '405',
//                        'message' => 'No tiene los privelegios para realizar esta acción!'
//                    ];
//                    return response()->json($notification);
//                    break;
                    return redirect()->back();
                    break;
                case 404:
                    return redirect()->back();
                    break;
                // internal error
                case 500:
                    return redirect()->back();

                    break;

                default:
                    return $this->renderHttpException($exception);
                    break;
            }
        } else {
            return parent::render($request, $exception);
        }


    }

    /**
     * Sends an email to the developer about the exception.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function saveExpection(Exception $exception)
    {
        try {
            $e = FlattenException::create($exception);

            $handler = new SymfonyExceptionHandler();

            $html = $handler->getHtml($e);

            \App\Exception::create([
                'content' => $html,
                'message' => $exception->getMessage(),
                'code'    => $exception->getCode(),
                'file'    => $exception->getFile(),
                'line'    => $exception->getLine(),
                'trace'   => $exception->getTraceAsString()
            ]);

        } catch (Exception $ex) {
            Log::error($ex);
        }
    }

}
