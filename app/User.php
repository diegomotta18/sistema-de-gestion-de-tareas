<?php

namespace App;

use App\Notifications\MailResetPasswordToken;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'chief',
        'id',
        'area_id',
        'last_seen'
    ];

    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Send a password reset email to the user
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordToken($token));
    }


    public function area()
    {
        return $this->belongsTo(Area::class, 'area_id');
    }

    public function areas()
    {
        return $this->belongsToMany(Area::class);
    }

    public function box()
    {
        return $this->hasOne(Box::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function freedays()
    {
        return $this->hasMany(Freeday::class);
    }

    // vacaciones del usuario
    public function holidays()
    {
        return $this->hasMany(Holiday::class);
    }

    public function messages()
    {
        return $this->belongsToMany(Message::class);
    }

    public function notices()
    {
        return $this->belongsToMany(Notice::class);
    }

    // tareas creadas por el usuario
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function tasksAssigneds()
    {
        return $this->belongsToMany(Task::class, 'task_assigned');
    }

    public function tasksInformeds()
    {
        return $this->belongsToMany(Task::class, 'task_informed');
    }

    public function tasksReplacements()
    {
        return $this->belongsToMany(Task::class, 'task_replacement');
    }


    public function getHtmlIsActiveAttribute()
    {
        if (!is_null($this->deleted_at)) {
            return "<span class='label label-danger'> Desactivado </span>";
        }

        return '';
    }


    public function hasArea($value)
    {
        $area = Area::where('name', $value)->first();

        if (!$this->areas->contains($area->id)) {
            return false;
        }

        return true;
    }

    public function hasAreaUnique($value)
    {
        if (!is_null($this->area) && $this->area->name === $value) {
            return true;
        }

        return false;
    }

    public function hasEventAssigned($value)
    {
        $events = Event::with('users')->where('id', $value)->whereHas('assigneds', function ($q) {
            return $q->where('user_id', auth()->user()->id);
        })->get();

        return $events->isEmpty();
    }

    public function hasPrensa()
    {
        return $this->hasArea('Prensa');
    }


    public function isAdmin()
    {
        return $this->hasRole('admin');
    }

    public function isGod()
    {
        return $this->hasRole('god');
    }

    public function isUser()
    {
        return $this->hasRole('user');
    }

    
    public function scopeEmailChiefToArea($query, $area_id)
    {
        return $query->whereHas('area', function ($q) use ($area_id) {
            $q->where('id', $area_id);
        })->where('chief', true)->get();
    }

    public function scopeEmailChiefToAreaName($query, $name_area)
    {
        return $query->whereHas('area', function ($q) use ($name_area) {
            return $q->where('name', $name_area);
        })->where('chief', true)->get();
    }

    public function scopeUserForArea($query)
    {
        $areas = auth()->user()->areas->pluck('id')->toArray();

        return $query->whereIn('area_id', $areas)
        ->orWhere('chief', true)->orwhereHas('areas', function ($q) {
            return $q->where('area_id', auth()->user()->area_id);
        });
    }

    public function scopeUserToAreaCompras($q)
    {
        $compra = Area::where('name', 'Compras')->first();

        return $q->with('areas')->whereHas('areas', function ($q) use ($compra) {
            return $q->where('area_id', $compra->id);
        })->orWhereHas('area', function ($q) {
            return $q->where('area_id', auth()->user()->area_id);
        })->orWhere('chief', true);
    }
}
