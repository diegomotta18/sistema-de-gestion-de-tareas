<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $datas = [ 'deleted_at','created_at', 'updated_at'];

    protected $fillable = [
        'name','icon'
    ];
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function user(){
        return $this->hasOne(User::class,'user_id');
    }

    public function projects(){
        return $this->hasMany(Project::class);
    }

    public function getRouteKeyName()
    {
        return 'name';
    }

    public function tasks(){

        return $this->hasMany(Task::class);
    }
}
