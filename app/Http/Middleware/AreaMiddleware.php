<?php

namespace App\Http\Middleware;

use Closure;

class AreaMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();
        if (($user->hasRole('admin')  || $user->hasRole('god') )&& $user->hasArea('Dirección'))
        {
            return $next($request);
        }
        return redirect()->route('home');
    }
}
