<?php

namespace App\Http\Controllers;

use App\User;
use function GuzzleHttp\is_host_in_noproxy;
use Illuminate\Http\Request;

class WebController extends Controller
{
    //

    public function getCheckVisit($email)
    {

        if (!is_null($email) &&  filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', $email)) {
            $user = User::where('email', $email)->first();
            if (!is_null($user)) {
                return $user->last_seen;
            }
        }
        return "No es un dato válido";
    }
}
