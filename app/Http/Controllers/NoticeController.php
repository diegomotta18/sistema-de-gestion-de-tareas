<?php

namespace App\Http\Controllers;

use App\Http\Requests\NoticeRequest;
use App\Jobs\SendNotice;
use App\Jobs\SendNoticeAllUser;
use App\Mail\SendEmailNotice;
use App\Notice;
use App\Notifications\NovedadNotification;
use App\User;
use Illuminate\Http\Request;
use Mockery\Matcher\Not;
use Illuminate\Support\Facades\Mail;

class NoticeController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $notices = null;
        if (auth()->user()->hasRole('admin') || auth()->user()->hasRole('god')) {
            $notices = Notice::orderBy('created_at','desc')->get();
        } else {
            $notices = Notice::myNotices()->orderBy('created_at','desc')->get();
        }
        return view('notices.index', compact('notices'));

    }

    public function create()
    {
        $this->authorize('create', Notice::class);

        return view('notices.create');
    }

    public function edit(Notice $notice)
    {

    }


    public function show(Notice $notice)
    {

        return view('notices.show', compact('notice'));
    }

    public function store(NoticeRequest $request)
    {
        $this->authorize('create', Notice::class);
        $notice = Notice::create([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'user_id' => auth()->user()->id
        ]);
        if ($request->input('typeUser')[0] === 'area_user') {
            if (!is_null($request->input('user_select'))) {

                $notice->users()->attach($request->input('user_select'));
                $notice->save();
                $job = new SendNotice($notice->users()->get(), $notice, auth()->user());
                dispatch($job);
            }
        } else if ($request->input('typeUser')[0] === 'any_user') {
            if (!is_null($request->input('user_all'))) {
                $notice->users()->attach($request->input('user_all'));
                $notice->save();
                $job = new SendNotice($notice->users()->get(), $notice, auth()->user());
                dispatch($job);
            }
        } else if ($request->input('typeUser')[0] === 'all') {
            $users = User::all();
            $job = new SendNoticeAllUser($users, $notice, auth()->user());
            dispatch($job);
        }

        $notification = [
            'alert-type' => 'success',
            'message' => 'Noticia creada!'
        ];

        return redirect()
            ->route('notices.create')
            ->with($notification);
    }

    public function update(Notice $notice, NoticeRequest $request)
    {
        $this->authorize('update', Notice::class);

        $notice = Notice::find($notice->id);
        $notice->title = $request->input('title');
        $notice->description = $request->input('description');
        $notice->save();

        $notification = [
            'alert-type' => 'success',
            'message' => 'Noticia modificada!'
        ];

        return redirect()
            ->route('notices.index')
            ->with($notification);
    }

    public function destroy($id)
    {
        $notice = Notice::find($id);
        $this->authorize('destroy', Notice::class);

        $notice->delete();
        return 'ok';
    }
}
