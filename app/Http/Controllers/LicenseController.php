<?php

namespace App\Http\Controllers;

use App\Http\Requests\LicenseRequest;
use App\License;
use App\Mail\LicenseMailable;
use App\Observation;
use App\TypeLicense;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class LicenseController extends Controller
{
    //lista todas las licencias iniciadas
    public function index(Request $request)
    {
        $this->authorize('index', License::class);
        $users = $this->getUsers();

        $licenses = License::query();
        $licenses->with('user', 'typeLicense', 'observations')
                 ->where('finish', false);

        //filtar por usuario

        if (!is_null($request->get('user'))) {
            $request->flash(['user']);

            $licenses->where('user_id', $request->get('user'));
        }
        //filtar por fecha de inicio y fecha de finalizacion
        else if (!is_null($request->get('date_init')) && !is_null($request->get('date_finish'))) {
            $request->flash(['date_init', 'date_finish']);

            $finish = Carbon::createFromFormat('d/m/Y', $request->get('date_finish'));
            $init = Carbon::createFromFormat('d/m/Y', $request->get('date_init'));
            $licenses->where('date_init', $init->format('Y-m-d'));
            $licenses->where('date_finish',$finish->format('Y-m-d'));

        }

        $licenses = $licenses
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return view('licenses.index', compact('licenses', 'users'))->with(Input::all());
    }

    //lista todas las licencias finalizadas
    public function finished()
    {
        $this->authorize('index', License::class);

        $licenses = License::with(['user', 'typeLicense'])
                           ->where('finish', true)
                           ->orderBy('created_at', 'desc')
                           ->paginate(10);

        return view('licenses.finished', compact('licenses'));
    }

    //crea una nueva licencia asociando el tipo de licencia y el usuario
    public function store(LicenseRequest $request)
    {
        $this->authorize('store', License::class);

        $date_init = Carbon::createFromFormat('d/m/Y', $request->input('date_init'));
        $date_finish = Carbon::createFromFormat('d/m/Y', $request->input('date_finish'));
        $date_reinstate = Carbon::createFromFormat('d/m/Y', $request->input('date_reinstate'));
        $license = License::create([
            "user_id"         => $request->input('user'),
            "type_license_id" => $request->input('type'),
            'date_init'       => $date_init,
            'date_finish'     => $date_finish,
            'date_reinstate'  => $date_reinstate,
        ]);

        if (!is_null($request->input('observations'))) {
            $observation = Observation::create([
                'observations' => $request->input('observations'),
                'license_id'   => $license->id
            ]);
            $license->observations()->save($observation);
        }
        $user = User::where('id',$license->user_id)->first();
        Mail::to($user->email)->queue(new LicenseMailable($license,auth()->user()));
        $notification = ['alert-type' => 'success', 'message' => 'Licencia creada!'];

        return redirect()->action('LicenseController@index')->with($notification);
    }

    //modifica los datos de la licencia
    public function update(LicenseRequest $request, License $license)
    {
        $this->authorize('update', License::class);

        $date_init = Carbon::createFromFormat('d/m/Y', $request->input('date_init'));
        $date_finish = Carbon::createFromFormat('d/m/Y', $request->input('date_finish'));
        $date_reinstate = Carbon::createFromFormat('d/m/Y', $request->input('date_reinstate'));
        $license->user_id = $request->input('user');
        $license->type_license_id = $request->input('type');
        $license->date_init = $date_init;
        $license->date_finish = $date_finish;
        $license->date_reinstate = $date_reinstate;
        $license->certificate = $request->has('certificate');
        $license->finish = $request->has('finish_license');

        $license->save();
        Mail::to($license->user->email)->queue(new LicenseMailable($license,auth()->user()));

        $notification = ['alert-type' => 'success', 'message' => 'Licencia creada!'];

        return redirect()->action('LicenseController@index')->with($notification);
    }

    // obtiene la vista para crear la licencia
    public function create()
    {
        $this->authorize('store', License::class);

        $typeLicenses = $this->getTypeLicenses();
        $users = $this->getUsers();

        return view('licenses.create', compact('typeLicenses', 'users'));
    }

    // obtiene la vista para crear la licencia
    public function edit(License $license)
    {
        $this->authorize('update', License::class);

        $typeLicenses = $this->getTypeLicenses();
        $users = $this->getUsers();
        return view('licenses.edit', compact('typeLicenses', 'users','license'));
    }

    //obtiene todas las observaciones de la licencia
    public function getObservations($id){
        $this->authorize('index', License::class);
        $license = License::with('observations')->where('id',$id)->first();
        return response()->json( $license->observations);
    }

    //guarda las observaciones de la licencia
    public function storeObservation(Request $request){

        $this->authorize('store', License::class);
        $license = License::where('id',$request->get('id'))->first();
        $observation = Observation::create([
            'observations' => $request->get('observation'),
            'license_id'   => $license->id
        ]);
        $license->observations()->save($observation);
        return $observation;
    }

    //edita las observaciones de la licencia
    public function updateObservation(Request $request, $id){
        $this->authorize('update', License::class);
        $observation =  Observation::where('id',$id)->first();
        $observation->observations = $request->get('observation');
        $observation->save();
        return $observation;
    }

    //elimina la licencia
    public function destroy(License $license)
    {
        $this->authorize('destroy', License::class);

        $license->delete();

        return 'ok';
    }

    //Visualiza los detalles de la licencia
    public function show(License $license)
    {
        return view('licenses.show', compact('license'));
    }

    //finaliza la licencia
    public function finish(License $license)
    {
        $this->authorize('finish', License::class);

        $license->finish = true;
        $license->save();

        return 'ok';
    }

    //inicia la licencia nuevamente si esta como finalizadas

    public function notfinish(License $license)
    {
        $this->authorize('finish', License::class);

        $license->finish = false;
        $license->save();

        return 'ok';
    }


    //obtiene los usuarios para mostrar en el select
    protected function getUsers()
    {
        $users = [];
        $tmp = User::all();

        foreach ($tmp as $k => $v) {
            $users[$v->id] = $v->name;
        }

        return $users;
    }

    //obtiene los tipos de licencias para mostrar en el select
    protected function getTypeLicenses()
    {
        $typeLicences = [];
        $tmp = TypeLicense::all();

        foreach ($tmp as $k => $v) {
            $typeLicences[$v->id] = $v->name;
        }

        return $typeLicences;
    }
}
