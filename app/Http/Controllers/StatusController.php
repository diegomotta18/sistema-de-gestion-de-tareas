<?php

namespace App\Http\Controllers;

use App\Status;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(){
        $this->authorize('index', Status::class);

        $statuses = Status::all();
        return view('statuses.index',compact('statuses'));
    }

    public function create(){
        $this->authorize('store', Status::class);

        return view('statuses.create');
    }

    public function store(Request $request){

        $this->authorize('store', Status::class);

        $request->validate([
            'name'  => 'required|unique:statuses',
            'color' => 'required'
        ]);

        Status::create([
            'name'     => $request->get('name'),
            'color'    => $request->get('color'),

        ]);

        $notification = ['alert-type' => 'success', 'message' => 'Estado creada!'];

        return redirect()->action('StatusController@index')->with($notification);
    }

    public function update (Request $request, Status $status){
        $this->authorize('update', Status::class);

        $request->validate([
            'name'  => 'required',
            'color' => 'required'
        ]);
        $status->name = $request->input('name');
        $status->color = $request->input('color');
        $status->save();

        $notification = ['alert-type' => 'success', 'message' => 'Estado actualizado!'];

        return redirect()->action('StatusController@index')->with($notification);
    }

    public function  edit(Status $status){
        $this->authorize('update', Status::class);
        return view('statuses.edit',compact('status'));
    }


    public function destroy($id){
        $this->authorize('destroy', Status::class);
        $status = Status::where('id',$id);
        $status->delete();
        return 'ok';
    }


}
