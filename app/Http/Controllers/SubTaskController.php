<?php

namespace App\Http\Controllers;

use App\Area;
use App\Http\Requests\TaskRequest;
use App\Jobs\RespondedTaskJob;
use App\Jobs\SendInformedTask;
use App\Jobs\SendTask;
use App\Jobs\TaskFinishJob;
use App\Jobs\TaskProcessJob;
use App\Project;
use App\Status;
use App\Task;
use App\TaskFile;
use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class SubTaskController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(Area $area = null, Project $project = null, Task $task)
    {
        $replacements = $this->getUsers();
        $assigneds = $this->getUsersAssigneds();
        $url = url()->previous();
        return view('subtasks.create', compact('area', 'project', 'replacements', 'task', 'assigneds', 'url'));
    }

    protected function getUsers()
    {
        $users = [];

        $tmp = User::with('areas')->where('id', '!=', auth()->user()->id)->get();
        if (auth()->user()->hasRole('user') || auth()->user()->hasRole('admin')) {
            $tmp = User::with('areas')->where('id', '!=', auth()->user()->id)->userForArea()->get();
        }
        foreach ($tmp as $k => $v) {
            $value = null;
            if ($v->chief === 1) {
                $value = " (Jefe de ".$v->areas->first()->name." )";
            }
            $users[$v->id] = $v->name.$value;
        }

        return $users;
    }

    protected function getUsersAssigneds()
    {
        $assigneds = [];

        $tmp = User::with('areas')->get();

        if (auth()->user()->hasRole('user') || auth()->user()->hasRole('admin')) {
            $tmp = User::with('areas')->userForArea()->get();
        }

        foreach ($tmp as $k => $v) {
            $value = null;
            if ($v->chief === 1) {
                $value = " (Jefe de ".$v->areas->first()->name." )";
            }
            $assigneds[$v->id] = $v->name.$value;
        }

        return $assigneds;
    }


    public function editwithareaproject(Area $area, Project $project, Task $task, Task $subtask)
    {
        $this->authorize('update', $task);
        $replacements = $this->getUsers();
        $assigneds = $this->getUsersAssigneds();
        $url = url()->previous();
        return view('subtasks.edit', compact('project', 'area', 'task', 'replacements', 'subtask', 'assigneds','url'));
    }

    public function store(Area $area = null, Project $project = null, Task $task, TaskRequest $request)
    {
        $date_init = Carbon::createFromFormat('d/m/Y', $request->input('date_init'));
        $date_finish = Carbon::createFromFormat('d/m/Y', $request->input('date_finish'));
        $subtask = Task::create([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'date_init' => $date_init,
            'date_finish' => $date_finish,
            'user_id' => Auth::user()->id,
        ]);

        if (!is_null($request->input('replacements'))) {
            $subtask->replacements()->attach($request->input('replacements'));
        }

        if (!is_null($request->input('informeds'))) {
            $subtask->informeds()->attach($request->input('informeds'));
            $subtask->informeds()->attach(auth()->user()->id);

        } else if (is_null($request->input('informeds'))) {
            $subtask->informeds()->attach(auth()->user()->id);
        }

        if ($request->hasFile('files')) {
            foreach ($request->file('files') as $photo) {
                $filename = $photo->store('public');
                $title = $photo->getClientOriginalName();
                $extension = $photo->getMimeType();
                TaskFile::create([
                    'task_id' => $subtask->id,
                    'filename' => $filename,
                    'title' => $title,
                    'extension' => $extension
                ]);
            }
        }

        if (!is_null($request->input('assigneds'))) {
            $subtask->assigneds()->attach($request->input('assigneds'));
        }

        $estado = Status::where('name', 'Informado')->first();
        $subtask->status()->associate($estado);
        $subtask->save();

        $task->subtasks()->save($subtask);

        $notification = [
            'alert-type' => 'success',
            'message' => 'Tarea creada!'
        ];

        if ($area != null && $project != null) {
            $subtask->area()->associate($area);
            $subtask->project()->associate($project);
            $subtask->url = route('subtasks.show', [$area->name, $project->id, $task->id, $subtask->id]);

            $subtask->save();

            //Envia un email a todos los usuarios asignados a la tarea

            $job = new SendTask($subtask->assigneds()->get(), $subtask, auth()->user());
            dispatch($job);

            //Envia un email a todos los usuarios informados  de la tarea

            $job = new SendInformedTask($subtask->informeds()->get(), $subtask, auth()->user());
            dispatch($job);

            return redirect()
                ->route('task.project.show', [$area->name, $project->id, $task->id])
                ->with($notification);

        }
        else if ($area != null && $project == null) {
            $task->area()->associate($area->id);
            $task->save();

            return redirect()
                ->back()
                ->with($notification);

        } else {

            return redirect()->back()
                ->with($notification);
        }
    }

    public function update(Task $subtask, Task $task, Area $area = null, Project $project = null, TaskRequest $request)
    {
        $this->authorize('update', $task);
        $date_init = Carbon::createFromFormat('d/m/Y', $request->input('date_init'));
        $date_finish = Carbon::createFromFormat('d/m/Y', $request->input('date_finish'));
        $subtask->date_init = $date_init;
        $subtask->date_finish = $date_finish;
        $subtask->title = $request->input('title');
        $subtask->description = $request->input('description');
        $subtask->user_id = \auth()->user()->id;

        if (!is_null($request->input('assigneds'))) {
            $subtask->assigneds()->detach();
            $subtask->save();
            $subtask->assigneds()->attach($request->input('assigneds'));
        }

        if (!is_null($request->input('replacements'))) {
            $subtask->replacements()->detach();
            $subtask->save();
            $subtask->replacements()->attach($request->input('replacements'));
        }

        if (!is_null($request->input('informeds'))) {
            $subtask->informeds()->detach();
            $subtask->save();
            $subtask->informeds()->attach($request->input('informeds'));
            $subtask->informeds()->attach(auth()->user()->id);
        } else if (is_null($request->input('informeds'))) {
            $subtask->informeds()->detach();
            $subtask->save();
            $subtask->informeds()->attach(auth()->user()->id);
        }

        if (!is_null($area)) {
            $subtask->area_id = $area->id;
        } else {
            $subtask->area_id = null;

        }
        if (!is_null($project)) {
            $subtask->project_id = $project->id;
        } else {
            $subtask->project_id = null;
        }

        $subtask->save();

        $notification = [
            'alert-type' => 'success',
            'message' => 'Tarea actualizada!'
        ];


        if ($area != null && $project != null) {
            $subtask->area()->associate($area);
            $subtask->project()->associate($project);
            $subtask->save();

            return redirect()
                ->route('task.project.show', [$area->name, $project->id, $task->id])
                ->with($notification);

        }
        else if ($area != null && $project == null) {

            return redirect()
                ->back()
                ->with($notification);

        } else {

            return redirect()->back()
                ->with($notification);
        }
    }

    public function show(Area $area, Project $project, Task $task, Task $subtask)
    {
        $this->authorize('show', $task);

        $url = url()->previous();
        return view('subtasks.show', compact('subtask', 'area', 'project', 'task','url'));
    }


    public function destroy($id)
    {
        $task = Task::find($id);
        $this->authorize('destroy', $task);
        $task->delete();
        return 'ok';
    }

    public function finishTask($id)
    {
        $task = Task::find($id);
        $this->authorize('finishtask', $task);
        $job = new TaskFinishJob($task->informeds()->get(), $task, auth()->user());
        dispatch($job);
        $job = new TaskFinishJob($task->assigneds()->get(), $task, auth()->user());
        dispatch($job);
        $status = Status::where('name', 'Finalizado')->first();
        $task->status()->dissociate();
        $task->status()->associate($status);
        $task->save();

        return response()->json(['status' => '200','estado' =>  $task->status()->first()->toJson()]);
    }

    public function replyTask($id){
        $task = Task::find($id);
        $status = Status::where('name', 'Respondido')->first();
        $task->status()->dissociate();
        $task->status()->associate($status);
        $task->save();
        $job = new RespondedTaskJob($task->assigneds()->get(), $task, auth()->user());
        dispatch($job);
        $job = new RespondedTaskJob($task->informeds()->get(), $task, auth()->user());
        dispatch($job);
        return response()->json(['status' => '200','estado' =>  $task->status()->first()->toJson()]);
    }

    public function processTask($id)
    {
        $task = Task::find($id);
        $this->authorize('processtask', $task);

        $status = Status::where('name', 'En proceso')->first();
        $task->status()->dissociate();
        $task->status()->associate($status);
        $task->save();
        $job = new TaskProcessJob($task->assigneds()->get(), $task, auth()->user());
        dispatch($job);
        $job = new TaskProcessJob($task->informeds()->get(), $task, auth()->user());
        dispatch($job);
        return response()->json(['status' => '200','estado' =>  $task->status()->first()->toJson()]);
    }


    public function initTask($id)
    {
        $task = Task::find($id);
        $status = Status::where('name', 'Informado')->first();
        $task->status()->dissociate();
        $task->status()->associate($status);
        $task->save();
        return response()->json(['status' => '200','estado' =>  $task->status()->first()->toJson()]);
    }

    public function denegateTask($id)
    {
        $task = Task::find($id);
        $this->authorize('denegatetask', $task);

        $status = Status::where('name', 'Denegado')->first();
        $task->status()->dissociate();
        $task->status()->associate($status);
        $task->save();

        return response()->json(['status' => '200', 'estado' => $task->status()->first()->toJson()]);
    }
}
