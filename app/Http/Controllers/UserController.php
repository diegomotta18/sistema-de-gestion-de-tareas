<?php

namespace App\Http\Controllers;

use App\Area;
use App\Box;
use App\Jobs\SendUser;
use App\LoginModel;
use App\Mail\UserCreated;
use App\User;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->authorize('index', User::class);

        $users = User::with('roles')->with('area')->with('areas')->get();

        return view('users.index', compact('users'));
    }

    public function loginsUser(){
        $this->authorize('show', User::class);
        $users =  LoginModel::with('user')->orderBy('login.start_date', 'desc')->paginate(10);
       return view ('users.logins.index',compact('users'));
    }

    public function create()
    {
        $this->authorize('create', User::class);

        $roles = $this->getRoles();

        $areas = $this->getAreas();

        return view('users.create', compact('roles', 'areas'));
    }

    protected function getRoles()
    {
        $roles = [];

        $tmp = Role::orderBy('name', 'ASC')->get()->pluck('name');

        foreach ($tmp as $k => $v) {
            $roles[$v] = $v;
        }

        return $roles;
    }

    protected function getAreas()
    {
        $areas = [];

        $tmp = Area::all();

        foreach ($tmp as $k => $v) {
            $areas[$v->id] = $v->name;
        }

        return $areas;
    }

    public function store(Request $request)
    {
        $this->authorize('create', User::class);

        $request->validate([
            'name'  => 'required',
            'email' => 'required|unique:users,email',
            'role'  => 'required|exists:roles,name',
            'areas' => 'required|array',
            'area' => 'required',
            'chief'
        ]);

        $tmp_pass = str_random(8);

        $user = User::create([
            'name'     => $request->get('name'),
            'email'    => $request->get('email'),
            'password' => bcrypt($tmp_pass),
            'chief' => $request->has('chief'),
            'area_id' => $request->input('area')
        ]);

        Box::create([
            'user_id' => $user->id,
        ]);

        $user->assignRole($request->get('role'));

        $user->areas()->sync($request->get('areas'));

        $job = new SendUser($user, $tmp_pass, 'Ha sido dado de alta a ');
        dispatch($job);

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Usuario creado!'
        ];

        return redirect()
            ->route('users.index')
            ->with($notification);
    }

    public function edit(User $user)
    {
        $this->authorize('update', User::class);

        $roles = $this->getRoles();

        $areas = $this->getAreas();

        return view('users.edit', compact('user', 'roles', 'areas'));
    }

    public function update(User $user, Request $request)
    {
        $this->authorize('update', User::class);

        $request->validate([
            'name'  => 'required',
            'email' => 'required|unique:users,email,'.$user->id,
            'role'  => 'required|exists:roles,name',
            'areas' => 'required|array',
            'area' => 'required'
        ]);

        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->area_id =  $request->get('area');
        $user->chief = $request->has('chief');
        $user->save();


        $user->syncRoles($request->get('role'));

        $user->areas()->sync($request->get('areas'));

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Usuario actualizado!'
        ];

        $job = new SendUser($user, null,'Ha sido modificado tu usuario de acceso a ');
        dispatch($job);

        return redirect()
            ->route('users.index')
            ->with($notification);
    }

    public function destroy($id)
    {
        $user = User::where('id',$id);
        $this->authorize('destroy', User::class);

        $user->delete();
        
        return 'ok';
    }

    public function allUser(){
        $users = User::with('areas')->where('id','!=', auth()->user()->id)->get();
        return $users->toJson();
    }
}
