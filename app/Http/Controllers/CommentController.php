<?php

namespace App\Http\Controllers;

use App\Comment;
use App\CommentFile;
use App\Event;
use App\Jobs\sendComment;
use App\Jobs\SendCommentInEventJob;
use App\Notifications\CommentNotification;
use App\Task;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CommentController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
     *  Gestion de comentarios en las tareas
     *
     * */

    public function commentsTask($id)
    {
        $task = Task::find($id);
        $comments = Comment::with(['user','files'])->where('commentable_type', 'App\Task')->where('commentable_id',
            $task->id)->get();

        return $comments->toJson();
    }

    public function commentInTaskStore(Request $request, $id)
    {
        $task = Task::find($id);
        $comment = new Comment([
            'comment' => $request->input('comment'),
            'user_id' => auth()->user()->id,
        ]);




        $task->comments()->save($comment);

        $comment = Comment::all()->last();
        if ($request->hasFile('attachments')) {
            foreach ($request->file('attachments') as $photo) {
                $filename = $photo->store('public');
                $title = $photo->getClientOriginalName();
                $extension = $photo->getMimeType();
                 CommentFile::create([
                    'filename'  => $filename,
                    'title'     => $title,
                    'extension' => $extension,
                    'url' => Storage::url($filename),
                    'comment_id' =>$comment->id
                ]);
            }
        }

        $project = null;

        if (!is_null($task->project)){
            $project =  $task->project()->first();
        }
        $job1 = new SendComment(auth()->user(), $task->assigneds()->get(), $comment->comment, $task, $task->area->name, $project);
        dispatch($job1);


        $job2 = new SendComment(auth()->user() , $task->informeds()->get(), $comment->comment, $task, $task->area->name, $project);
        dispatch($job2);

        return $comment->toJson();
    }

    public function commentInTaskUpdate(Request $request, $comment_id)
    {
        $comment = Comment::find($comment_id);
        $comment->comment = $request->input('comment');
        $comment->save();

        return $comment;
    }

    /*
     *  Gestion de comentarios en los eventos
     *
     * */


    public function commentsEvent($id)
    {
        $event = Event::find($id);
        $comments = Comment::with('user')->where('commentable_type', 'App\Event')->where('commentable_id',
            $event->id)->get();

        return $comments->toJson();
    }

    public function commentInEventStore(Request $request, $id)
    {
        $event = Event::find($id);
        $comment = new Comment([
            'comment' => $request->input('comment'),
            'user_id' => auth()->user()->id,
        ]);
        $event->comments()->save($comment);

        $job1 = new SendCommentInEventJob(auth()->user(), $event->assigneds()->get(), $comment->comment, $event);
        dispatch($job1);

        return $comment->toJson();
    }

    public function commentInEventUpdate(Request $request, $comment_id)
    {
        $comment = Comment::find($comment_id);
        $comment->comment = $request->input('comment');
        $comment->save();

        return $comment;
    }

    public function commentDestroy($comment_id)
    {
        $comment = Comment::find($comment_id);
        $comment->delete();

        return 'ok';
    }

}

