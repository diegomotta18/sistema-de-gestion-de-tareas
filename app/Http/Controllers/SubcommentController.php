<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Jobs\sendComment;
use App\Task;
use App\User;
use Illuminate\Http\Request;

class SubcommentController extends Controller
{
    //
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request){

        $subcomment = Comment::create([
            'comment' => $request->input('subcomment'),
            'task_id' => $request->input('id_task'),
            'user_id' => auth()->user()->id,

        ]);
        $subcomment->save();
        $comment = Comment::find($request->input('comment_id'));
        $comment->subcomments()->save($subcomment);
        $task = Task::find($subcomment->task_id);
        //recorrer para enviar mail a todos los usuarios asignados en la tarea.
        foreach ($task->assigneds()->get()->chunk(3) as $c) {
            foreach ($c as  $u) {
                if ($u->id != auth()->user()->id){
                    $job = new SendComment( auth()->user(),$u, $subcomment->comment,$task,$task->area->name);
                    dispatch($job);
                }
            }
        }
        foreach ($task->informeds()->get()->chunk(3) as $c) {
            foreach ($c as  $u) {
                if ($u->id != auth()->user()->id){
                    $job = new SendComment( auth()->user(),$u, $subcomment->comment,$task,$task->area->name);
                    dispatch($job);
                }
            }
        }
        return $subcomment->toJson();

    }

    public function  update(Request $request, Comment $subcomment){
        $subcomment->comment = $request->input('subcomment');
        $subcomment->save();
        return $subcomment->toJson();
    }
}
