<?php

namespace App\Http\Controllers;

use App\Area;
use App\Event;
use App\Notice;
use App\Project;
use App\Task;
use App\User;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request, Area $area = null)
    {
        if (auth()->user()->isAdmin() || auth()->user()->isGod()) {
            return $this->indexGodAdmin($request, $area);
        } else {
            return $this->indexUser($request, $area);
        }
    }

    public function indexGodAdmin(Request $request, Area $area = null)
    {
        $count = null;
        $areas = $this->getAreas();
        $users = $this->getUsers();

        $tasks = Task::with('informeds', 'assigneds', 'status', 'project', 'area', 'user',
            'typeTask')->whereNull('parrilla')->forArea($area)->paginate(10);
        $search = \Request::get('searchTaskAll'); //<-- we use global request to get the param of URI
        $user_search = \Request::get('user_search');
        $area_search = \Request::get('area_search');
        $status = \Request::get('status');

        /*
         * Busca por area
         * */

        if (!is_null($area_search) && is_null($user_search) && is_null($status)) {
            $tasks = Task::with('informeds', 'assigneds', 'status', 'project', 'area', 'user', 'typeTask')
                         ->whereNull('parrilla')
                         ->forArea()
                         ->tasksForArea($area_search)
                         ->paginate(20);
            $tasks->appends($request->query());

            /*
             *
             * Busca por area y por usuario
             *
             * */

        } elseif (!is_null($area_search) && !is_null($user_search) && is_null($status)) {
            $tasks = Task::with('informeds', 'assigneds', 'status', 'project', 'area', 'user',
                'typeTask')->whereNull('parrilla')
                         ->forArea()
                         ->tasksForAreaAndUser($area_search, $user_search)
                         ->paginate(20);
            $tasks->appends($request->query());

            /*
            *
            * Busca por area y estado de la tarea
            *
            * */


        } elseif (!is_null($area_search) && is_null($user_search) && !is_null($status) && is_null($search)) {
            $tasks = Task::with('informeds', 'assigneds', 'status', 'project', 'area', 'user', 'typeTask')
                         ->whereNull('parrilla')
                         ->forArea()
                         ->tasksForArea($area_search)
                         ->forStatus($status)
                         ->paginate(20);

        }

        /*
        *
        * Busca por area,  usuario y estado de la tarea
        *
        * */

        elseif (!is_null($area_search) && !is_null($user_search) && !is_null($status) && is_null($search)){
            $tasks = Task::with('informeds', 'assigneds', 'status', 'project', 'area', 'user',
                'typeTask')->whereNull('parrilla')
                         ->forArea()
                         ->tasksForAreaAndUser($area_search, $user_search)
                         ->forStatus($status)
                         ->paginate(20);
            $tasks->appends($request->query());

        }


        /*
        *
        * Busca por nombre de la tarea
        *
        * */


        if (!is_null($search) && is_null($area_search) && is_null($user_search) && is_null($status)) {
            $tasks = Task::with('informeds', 'assigneds', 'status', 'project', 'area', 'user', 'typeTask')
                         ->whereNull('parrilla')
                         ->forArea($area)
                         ->search($search)
                         ->paginate(20);
            $tasks->appends($request->query());

        /*
        *
        * Busca por usuario
        *
        * */


        } elseif (!is_null($user_search) && is_null($search) && is_null($area_search) && is_null($status)) {
            $tasks = Task::with('informeds', 'assigneds', 'status', 'project', 'area', 'user', 'typeTask')
                         ->whereNull('parrilla')
                         ->forArea($area)
                         ->tasksToUser($user_search, $status)
                         ->paginate(20);
            $count = Task::with('informeds', 'assigneds', 'status', 'project', 'area', 'user',
                'typeTask')->whereNull('parrilla')->forArea($area)->tasksToUser($user_search, $status)->count();
            $tasks->appends($request->query());

            /*
             *
             * Busca por usuario y estado de la tarea del usuario
             *
             * */

        } elseif (!is_null($user_search) && is_null($search) && is_null($area_search) && !is_null($status)) {
            $tasks = Task::with('informeds', 'assigneds', 'status', 'project', 'area', 'user',
                'typeTask')->whereNull('parrilla')->forArea($area)->tasksToUser($user_search, $status)->paginate(20);
            $count = Task::with('informeds', 'assigneds', 'status', 'project', 'area', 'user',
                'typeTask')->whereNull('parrilla')->forArea($area)->tasksToUser($user_search, $status)->count();
            $tasks->appends($request->query());
        }

        if (auth()->user()->isGod()) {
            return view('homes.god',
                compact('tasks', 'users', 'count', 'status', 'areas', 'area'));
        } elseif (auth()->user()->isAdmin()) {
            return view('homes.admin',
                compact('tasks', 'users', 'count', 'status', 'areas', 'area'));
        }

    }

    public function indexUser(Request $request, Area $area = null)
    {
        $count = null;
        $areas = $this->getAreas();
        $users = $this->getUsers();
        $tasks = Task::with('informeds', 'assigneds', 'status', 'project', 'area', 'user',
            'typeTask')->whereNull('parrilla')->myTasksAssigned()->paginate(10);
        $search = \Request::get('searchTaskAll');
        $status = \Request::get('status');
        if (!is_null($search) && strlen($search) > 3) {
            $tasks = Task::with('informeds', 'assigneds', 'status', 'project', 'area', 'user',
                'typeTask')->whereNull('parrilla')->myTasksAssigned()->search($search)->paginate(20);
            $tasks->appends($request->query());
        }
        if (!is_null($status) && strlen($status) > 3) {
            $tasks = Task::with('informeds', 'assigneds', 'status', 'project', 'area', 'user',
                'typeTask')->whereNull('parrilla')->myTasksAssigned()->tasksToUser(auth()->user()->id,
                $status)->paginate(20);
            $count = Task::with('informeds', 'assigneds', 'status', 'project', 'area', 'user',
                'typeTask')->whereNull('parrilla')->myTasksAssigned()->tasksToUser(auth()->user()->id,
                $status)->count();
            $tasks->appends($request->query());
        }
        $notices = Notice::myNotices()->limit(5)->orderBy('created_at', 'desc')->get();

        return view('homes.user',
            compact('tasks', 'notices', 'users', 'count', 'status', 'areas', 'area'));

    }

    protected function getUsers()
    {
        $users = [];

        $tmp = User::with('area')->get();

        if (auth()->user()->hasRole('user') || auth()->user()->hasRole('admin') && !auth()->user()->hasArea('Dirección')) {
            $tmp = User::with('area')->with('areas')->userForArea()->get();
        }
        foreach ($tmp as $k => $v) {
            $value = null;

            $users[$v->id] = $v->name;
        }

        return $users;
    }


    protected function getAreas()
    {
        $areas = [];

        $tmp = Area::with('users')->whereHas('users', function ($query) {
            $query->where('user_id', auth()->user()->id);
        })->get();

        foreach ($tmp as $k => $v) {
            $value = null;
            if ($v->chief === 1) {
                $value = " (Jefe de ".$v->area->name." )";
            }
            $areas[$v->id] = $v->name.$value;
        }

        return $areas;
    }


    public function getProjects()
    {
        $projects = Project::myProjects()->get();

        return $projects->toJson();
    }


}
