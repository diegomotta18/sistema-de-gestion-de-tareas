<?php

namespace App\Http\Controllers;

use App\Action;
use App\Http\Requests\ActionRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Input;

class ActionController extends Controller
{
    //visualiza en un listado todos los registros de accion de los usuarios depenediendo el rol
    //del usuario
    public function index(Request $request)
    {
        $users = $this->getUsers();

        $actions = Action::query()->with('user');
        //filtar por fecha de desde / hasta tomando la fecha de creación del registro

        if (!is_null($request->get('date_init')) && !is_null($request->get('date_finish'))) {
            $request->flash(['date_init', 'date_finish']);
            $finish = Carbon::createFromFormat('d/m/Y', $request->get('date_finish'));
            $init = Carbon::createFromFormat('d/m/Y', $request->get('date_init'));
            $actions->whereBetween('created_at', [$init->format('Y-m-d'), $finish->format('Y-m-d')]);
        }
        //obtiene todas las acciones de todos los usuarios del sistema
        if (auth()->user()->isGod() || auth()->user()->isAdmin()) {
            //filtar por usuario
            if (!is_null($request->get('user'))) {
                $request->flash(['user']);
                $actions->where('user_id', $request->get('user'));
            }
            $actions = $actions->orderBy('created_at', 'desc')->paginate(10);
        } else {
            //obtiene todas las acciones del usuario que no es admin o god

            $actions = $actions->where('user_id', auth()->user()->id)->orderBy('created_at', 'desc')->paginate(10);

        }

        return view('actions.index', compact('actions', 'users'))->with(Input::all());
    }

    //almacena las acciones del usuario
    public function store(ActionRequest $request)
    {

        $date = Carbon::createFromFormat('d/m/Y', $request->get('date'));
        $time_init = Carbon::createFromFormat('H:i A', $request->input('time_init'));
        $time_finish = Carbon::createFromFormat('H:i A', $request->input('time_finish'));
        Action::create([
            'date'        => $date,
            'time_init'   => $time_init,
            'time_finish' => $time_finish,
            'description' => $request->input('description'),
            'user_id'     => auth()->user()->id
        ]);
        $notification = [
            'alert-type' => 'success',
            'message'    => 'Accion creada!'
        ];

        return redirect()->route('actions.index')->with($notification);
    }


    //obtiene el formulario para crear la accion
    public function create()
    {
        return view('actions.create');
    }

    //obtiene el formulario para editar la accion
    public function edit(Action $action)
    {
        return view('actions.edit', compact('action'));
    }

    //actualiza los datos de la accion
    public function update(ActionRequest $request, Action $action)
    {
        $date = Carbon::createFromFormat('d/m/Y', $request->get('date'));
        $time_init = Carbon::createFromFormat('H:i A', $request->input('time_init'));
        $time_finish = Carbon::createFromFormat('H:i A', $request->input('time_finish'));
        $action->date = $date;
        $action->time_init = $time_init;
        $action->time_finish = $time_finish;
        $action->description = $request->input('description');
        $action->save();

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Accion actualizada!'
        ];

        return redirect()->route('freedays.index')->with($notification);

    }

    //elimina la accion
    public function destroy(Action $action)
    {
        $action->delete();

        return 'ok';
    }

    //obtiene todos los usuarios para mostrar en el combo para filtrar usuarios desde la vista index
    protected function getUsers()
    {
        $users = [];
        $tmp = User::all();

        foreach ($tmp as $k => $v) {
            $users[$v->id] = $v->name;
        }

        return $users;
    }
}
