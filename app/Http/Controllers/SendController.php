<?php

namespace App\Http\Controllers;

use App\Message;
use App\MsgThread;
use Illuminate\Http\Request;

class SendController extends Controller
{
    //
    public function index(){
        $messages = MsgThread::with('thread')->with('thread.members')->where('user_id', auth()->user()->id)->orderby('created_at','desc')->get();
        return view('box.sends.index',compact('messages'));
    }
}
