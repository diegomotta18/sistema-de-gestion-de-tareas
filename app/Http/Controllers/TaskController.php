<?php

namespace App\Http\Controllers;

use App\Area;
use App\Http\Requests\TaskRequest;
use App\Http\Requests\TaskRequestMain;
use App\Jobs\RespondedTaskJob;
use App\Jobs\SendInformedTask;
use App\Jobs\SendTask;
use App\Jobs\TaskFinishJob;
use App\Jobs\TaskProcessJob;
use App\Mail\SendEmailTask;
use App\Notifications\TaskNotification;
use App\Status;
use App\Project;
use App\Task;
use App\TaskFile;
use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use URL;

class TaskController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
     *
     * Muestra el formulario de carga de tarea sin la opcion de elegir area y proyecto
     *
     * */

    public function create(Area $area = null, Project $project = null)
    {
        $replacements = $this->getUsers();
        $assigneds = $this->getUsersAssigneds();
        session()->put('url-previous', url(URL::previous()));

        return view('tasks.create', compact('area', 'project', 'replacements', 'assigneds'));
    }

    /*
     *
     * Devuelve los usuarios para reemplazos de tareas
     *
     * */

    protected function getUsers()
    {
        $users = [];
        $tmp = User::with('roles')->with('area')->where('id', '!=', auth()->user()->id)->userForArea()->get();

        if (!auth()->user()->hasArea('Sistemas')) {
            foreach ($tmp as $k => $v) {
                if ($v->isGod()) {
                    $tmp->forget($k);
                }
            }
        }

        foreach ($tmp as $k => $v) {
            $value = null;
            if ($v->chief === 1) {
                $value = " (Jefe de ".$v->area->name." )";
            }
            $users[$v->id] = $v->name.$value;
        }

        return $users;
    }

    /*
     *
     *  Devuelo los usuarios asignados por area
     *
     * */

    protected function getUsersAssigneds()
    {
        $assigneds = [];

        $tmp = User::with('roles')->with('area')->userForArea()->get();
        if (!auth()->user()->hasArea('Sistemas')) {
            foreach ($tmp as $k => $v) {
                if ($v->isGod()) {
                    $tmp->forget($k);
                }
            }
        }

        foreach ($tmp as $k => $v) {
            $value = null;
            if ($v->chief === 1) {
                $value = " (Jefe de ".$v->area->name." )";
            }
            $assigneds[$v->id] = $v->name.$value;
        }

        return $assigneds;
    }

    /*
     *
     *  Muestra el formulario de carga de un tarea, permitiendo elegir un área y proyecto
     *
     * */

    public function createTaskMain(Area $area = null)
    {
        $replacements = $this->getUsers();
        $assigneds = $this->getUsersAssigneds();
        session()->put('url-previous', url(URL::previous()));
        if (auth()->user()->hasRole('user') && auth()->user()->hasAreaUnique('Prensa')) {
            return view('tasks.createTaskPrensa', compact('replacements', 'area'));
        }

        return view('tasks.createTaskMain', compact('replacements', 'assigneds', 'area'));
    }


    /*
     *  Esta accion se produce cuando el usuario quiere crear una tarea desde la home accediendo desde el boton "crear tarea"
     *  Se crea una tarea junto con el area seleccionada y/o proyecto seleccionado
     * */

    public function saveTaskMain(TaskRequestMain $request, Area $area = null)
    {
        $date_init = Carbon::createFromFormat('d/m/Y', $request->input('date_init'));
        $date_finish = Carbon::createFromFormat('d/m/Y', $request->input('date_finish'));
        $hora = null;
        if (!is_null($request->input('hora'))) {
            $hora = Carbon::createFromFormat('H:i A', $request->input('hora'));
        }

        $task = Task::create([
            'title'       => $request->input('title'),
            'description' => $request->input('description'),
            'date_init'   => $date_init,
            'date_finish' => $date_finish,
            'user_id'     => Auth::user()->id,
            'hora'        => $hora
        ]);
        $task->parent_id = $task->id;

        $task->area_id = $request->input('area');
        $task->project_id = $request->input('project');

        $estado = Status::where('name', 'Informado')->first();
        $task->status()->associate($estado);


        if (!is_null($request->input('replacements'))) {
            $task->replacements()->attach($request->input('replacements'));
        }

        if (!is_null($request->input('informeds'))) {
            $task->informeds()->attach($request->input('informeds'));
            $task->informeds()->attach(auth()->user()->id);

        } else {
            if (is_null($request->input('informeds'))) {
                $task->informeds()->attach(auth()->user()->id);
            }
        }
        if (!is_null($request->input('assigneds'))) {
            $task->assigneds()->attach($request->input('assigneds'));
        }


        if (!is_null($task->area()) && is_null($task->project())) {
            $task->url = route('task.project.show', [$task->area->name, $task->project->id, $task->id]);
        } else {
            if (!is_null($task->area()) && !is_null($task->project())) {
                $task->url = route('task.area.show', [$task->area->name, $task->id]);
            }
        }
        $task->save();

        if ($request->hasFile('files')) {
            foreach ($request->file('files') as $photo) {
                $filename = $photo->store('public');
                $title = $photo->getClientOriginalName();
                $extension = $photo->getMimeType();
                TaskFile::create([
                    'task_id'   => $task->id,
                    'filename'  => $filename,
                    'title'     => $title,
                    'extension' => $extension
                ]);
            }
        }

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Tarea creada!'
        ];
        if (!is_null($request->input('assigneds'))) {

            $job = new SendTask($task->assigneds()->get(), $task, auth()->user());
            dispatch($job);
        } else {
            /*
             * Cuando no hay un usuario asignado se envia un email al  jefe del area seleccionada
             * */
            $user = User::emailChiefToArea($request->input('area'))->first();
            $user->notify(new TaskNotification($task, auth()->user()));

            Mail::to($user->email)
                ->queue(new SendEmailTask($task, auth()->user()));
        }
        if (!is_null($request->input('informeds'))) {

            $job = new SendInformedTask($task->informeds()->get(), $task, auth()->user());
            dispatch($job);

        }

        return redirect(session()->get('url-previous'))->with($notification);
    }

    public function store(Area $area = null, Project $project = null, TaskRequest $request)
    {
        $date_init = Carbon::createFromFormat('d/m/Y', $request->input('date_init'));
        $date_finish = Carbon::createFromFormat('d/m/Y', $request->input('date_finish'));
        $task = Task::create([
            'title'       => $request->input('title'),
            'description' => $request->input('description'),
            'date_init'   => $date_init,
            'date_finish' => $date_finish,
            'user_id'     => Auth::user()->id,
        ]);
        $task->parent_id = $task->id;

        $estado = Status::where('name', 'Informado')->first();
        $task->status()->associate($estado);


        if (!is_null($request->input('replacements'))) {
            $task->replacements()->attach($request->input('replacements'));
        }

        if (!is_null($request->input('informeds'))) {
            $task->informeds()->attach($request->input('informeds'));
            $task->informeds()->attach(auth()->user()->id);

        } else {
            if (is_null($request->input('informeds'))) {
                $task->informeds()->attach(auth()->user()->id);
            }
        }


        if ($request->hasFile('files')) {
            foreach ($request->file('files') as $photo) {
                $filename = $photo->store('public');
                $title = $photo->getClientOriginalName();
                $extension = $photo->getMimeType();
                TaskFile::create([
                    'task_id'   => $task->id,
                    'filename'  => $filename,
                    'title'     => $title,
                    'extension' => $extension
                ]);
            }
        }

        if (!is_null($request->input('assigneds'))) {
            $task->assigneds()->attach($request->input('assigneds'));
        }

        $task->save();

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Tarea creada!'
        ];

        /*
         * Envia a la tarea si tiene un area y proyecto
         * */

        if ($area != null && $project != null) {
            $task->area()->associate($area);
            $task->project()->associate($project);
            $task->url = route('task.project.show', [$area->name, $project->id, $task->id]);
            $task->save();

            //Envia un email a todos los usuarios asignados a la tarea
            if (!is_null($request->input('assigneds'))) {

                $job = new SendTask($task->assigneds()->get(), $task, auth()->user());
                dispatch($job);
            }

            //Envia un email a todos los usuarios informados  de la tarea
            if (!is_null($request->input('informeds'))) {

                $job = new SendInformedTask($task->informeds()->get(), $task, auth()->user());
                dispatch($job);
            }

        } else {
            if ($area != null && $project == null) {

                $task->area()->associate($area->id);

                $task->url = route('task.area.show', [$area->name, $task->id]);

                $task->save();

                //Envia un email a todos los usuarios asignados a la tarea
                if (!is_null($request->input('assigneds'))) {

                    $job = new SendTask($task->assigneds()->get(), $task, auth()->user());
                    dispatch($job);
                }

                if (!is_null($request->input('informeds'))) {

                    $job = new SendInformedTask($task->informeds()->get(), $task, auth()->user());
                    dispatch($job);
                }

            } else {

                //Envia un email a todos los usuarios asignados a la tarea
                if (!is_null($request->input('assigneds'))) {

                    $job = new SendTask($task->assigneds()->get(), $task, auth()->user());
                    dispatch($job);
                }

                if (!is_null($request->input('informeds'))) {

                    $job = new SendInformedTask($task->informeds()->get(), $task, auth()->user());
                    dispatch($job);
                }


            }
        }
        if(!is_null(session()->get('url-previous'))){
            return redirect(session()->get('url-previous'))->with($notification);
        }else{
            redirect()->back()->with($notification);
        }

    }

    public function edit(Task $task)
    {
        $this->authorize('update', $task);

        $project = null;
        $area = null;
        $replacements = $this->getUsers();
        $assigneds = $this->getUsersAssigneds();
        session()->put('url-previous', url(URL::previous()));

        return view('tasks.edit', compact('project', 'area', 'task', 'replacements', 'assigneds'));
    }

    public function editwitharea(Area $area, Task $task)
    {
        $this->authorize('update', $task);
        $project = null;
        $replacements = $this->getUsers();
        $assigneds = $this->getUsersAssigneds();
        session()->put('url-previous', url(URL::previous()));

        return view('tasks.edit', compact('project', 'area', 'task', 'replacements', 'assigneds'));
    }


    public function editwithoutarea(Task $task)
    {
        $this->authorize('update', $task);
        $replacements = $this->getUsers();
        $project = null;
        $area = null;
        $assigneds = $this->getUsersAssigneds();
        session()->put('url-previous', url(URL::previous()));

        return view('tasks.edit', compact('task', 'replacements', 'project', 'area', 'assigneds'));
    }

    public function editwithareaproject(Area $area, Project $project, Task $task)
    {
        $this->authorize('update', $task);
        $replacements = $this->getUsers();
        $assigneds = $this->getUsersAssigneds();
        session()->put('url-previous', url(URL::previous()));

        return view('tasks.edit', compact('project', 'area', 'task', 'replacements', 'assigneds'));
    }


    public function update(Task $task, Area $area = null, Project $project = null, TaskRequest $request)
    {
        $this->authorize('update', $task);
        $date_init = Carbon::createFromFormat('d/m/Y', $request->input('date_init'));
        $date_finish = Carbon::createFromFormat('d/m/Y', $request->input('date_finish'));
        $hora = null;
        if (!is_null($request->input('hora'))) {
            $hora = Carbon::createFromFormat('H:i A', $request->input('hora'));
        }
        $task->hora = $hora;
        $task->date_init = $date_init;
        $task->date_finish = $date_finish;
        $task->title = $request->input('title');
        $task->description = $request->input('description');
        $task->area_id = $request->input('area');
        $task->project_id = $request->input('project');

        if (!is_null($request->input('assigneds'))) {
            $task->assigneds()->detach();
            $task->save();
            $task->assigneds()->attach($request->input('assigneds'));
        }

        if (!is_null($request->input('replacements'))) {
            $task->replacements()->detach();
            $task->save();
            $task->replacements()->attach($request->input('replacements'));
        }

        if (!is_null($request->input('informeds'))) {
            $task->informeds()->detach();
            $task->save();
            $task->informeds()->attach($request->input('informeds'));
            $task->informeds()->attach(auth()->user()->id);
        } else {
            if (is_null($request->input('informeds'))) {
                $task->informeds()->detach();
                $task->save();
                $task->informeds()->attach(auth()->user()->id);
            }
        }


        if ($request->hasFile('files')) {
            foreach ($request->file('files') as $photo) {

                $filename = $photo->store('public');
                $title = $photo->getClientOriginalName();
                $extension = $photo->getMimeType();
                TaskFile::create([
                    'task_id'   => $task->id,
                    'filename'  => $filename,
                    'title'     => $title,
                    'extension' => $extension
                ]);
            }
        }

        $task->save();

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Tarea actualizada!'
        ];

        if ($area != null && $project != null) {

            if (!is_null($request->input('assigneds'))) {

                $job = new SendTask($task->assigneds()->get(), $task, auth()->user());
                dispatch($job);

            }
            if (!is_null($request->input('informeds'))) {

                $job = new SendInformedTask($task->informeds()->get(), $task, auth()->user());
                dispatch($job);
            }

        } else {
            if ($area != null && $project == null) {

                if (!is_null($request->input('assigneds'))) {

                    $job = new SendTask($task->assigneds()->get(), $task, auth()->user());
                    dispatch($job);

                }
                if (!is_null($request->input('informeds'))) {

                    $job = new SendInformedTask($task->informeds()->get(), $task, auth()->user());
                    dispatch($job);
                }

            }
        }

        return redirect(session()->get('url-previous'))->with($notification);

    }

    public function pigeonhole($id)
    {
        $task = Task::find($id);
        $this->authorize('destroy', $task);
        $task->delete();

        return 'ok';
    }

    public function destroy($id)
    {
        $task = Task::find($id);
        $this->authorize('destroy', $task);
        if (!is_null($task->subtasks()->get())) {
            foreach ($task->subtasks()->get() as $subtask) {
                $t = Task::find($subtask->id);
                $t->assigneds()->detach();
                $t->informeds()->detach();
                $t->project_id = null;
                $t->user_id = null;
                $t->area_id = null;
                $t->task_id = null;
                $t->save();
                $t->forceDelete();
            }
        }
        if (!is_null($task->files()->get())) {
            foreach ($task->files()->get() as $f) {
                $file = TaskFile::where('id', $f->id)->first();
                Storage::delete($file->filename);
                $file->delete();
            }
        }
        $task->assigneds()->detach();
        $task->informeds()->detach();
        $task->project_id = null;
        $task->user_id = null;
        $task->area_id = null;
        $task->save();
        $task->forceDelete();

        return 'ok';
    }

    public function show_with_area_project(Area $area = null, Project $project = null, Task $task)
    {
        $project = null;
        $area = null;
        if (!is_null($task->project_id)) {
            $project = Project::where('id', $task->project_id)->first();
        }
        if (!is_null($task->area_id)) {
            $area = Area::where('id', $task->area_id)->first();
        }
        if (!auth()->check()) {
            $url = route('task.project.show', [$area->name, $project->id, $task->id]);
            session(['url' => $url]);
        }
        $this->authorize('show', $task);

        return view('tasks.show', compact('area', 'project', 'task'));
    }

    public function show_with_area(Area $area, Task $task)
    {
        $area = null;
        if (!is_null($task->area_id)) {
            $area = Area::where('id', $task->area_id)->first();
        }
        $project = null;
        if (!auth()->check()) {
            $url = route('task.area.show', [$area->name, $task->id]);;
            session(['url' => $url]);
        }
        $this->authorize('show', $task);

        return view('tasks.show', compact('area', 'project', 'task'));
    }

    public function show(Task $task)
    {
        if (!auth()->check()) {
            $url = route('task.home.show', $task->id);;
            session(['url' => $url]);
        }
        $this->authorize('show', $task);
        $project = null;
        $area = null;
        if (!is_null($task->project_id)) {
            $project = Project::where('id', $task->project_id)->first();
        }
        if (!is_null($task->area_id)) {
            $area = Area::where('id', $task->area_id)->first();
        }

        return view('tasks.show', compact('area', 'project', 'task'));

    }

    public function finishTask($id)
    {

        $task = Task::find($id);
        $this->authorize('finishtask', $task);
        $job = new TaskFinishJob($task->assigneds()->get(), $task, auth()->user());
        dispatch($job);
        $job = new TaskFinishJob($task->informeds()->get(), $task, auth()->user());
        dispatch($job);


        $tasks = Task::where('task_id', $task->id)->get();
        if (!is_null($tasks)) {
            if ($this->checkSubtasks($task)) {
                $status = Status::where('name', 'Finalizado')->first();
                $task->status()->dissociate();
                $task->status()->associate($status);
                $task->save();

                return response()
                    ->json(['status' => '200', 'estado' => $task->status()->first()->toJson()]);
            }

            return response()
                ->json(['status' => '404']);
        }
        $status = Status::where('name', 'Finalizado')->first();
        $task->status()->dissociate();
        $task->status()->associate($status);
        $task->save();

        return response()->json(['status' => '200', 'estado' => $task->status()->first()->toJson()]);

    }

    public function checkSubtasks(Task $task)
    {
        $check = true;
        $tasks = Task::where('task_id', $task->id)->get();
        foreach ($tasks as $task) {
            if ($task->status->name != 'Finalizado') {
                $check = false;
                break;
            }
        }

        return $check;
    }

    public function processTask($id)
    {

        $task = Task::find($id);
        $this->authorize('processtask', $task);

        $status = Status::where('name', 'En proceso')->first();
        $task->status()->dissociate();
        $task->status()->associate($status);
        $task->save();
        $job = new TaskProcessJob($task->assigneds()->get(), $task, auth()->user());
        dispatch($job);
        $job = new TaskProcessJob($task->informeds()->get(), $task, auth()->user());
        dispatch($job);

        return response()->json(['status' => '200', 'estado' => $task->status()->first()->toJson()]);
    }

    public function replyTask($id)
    {
        $task = Task::find($id);
        $status = Status::where('name', 'Respondido')->first();
        $task->status()->dissociate();
        $task->status()->associate($status);
        $task->save();
        $job = new RespondedTaskJob($task->assigneds()->get(), $task, auth()->user());
        dispatch($job);
        $job = new RespondedTaskJob($task->informeds()->get(), $task, auth()->user());
        dispatch($job);

        return response()->json(['status' => '200', 'estado' => $task->status()->first()->toJson()]);
    }


    public function initTask($id)
    {
        $task = Task::find($id);
        $status = Status::where('name', 'Informado')->first();
        $task->status()->dissociate();
        $task->status()->associate($status);
        $task->save();

        return response()->json(['status' => '200', 'estado' => $task->status()->first()->toJson()]);
    }


    public function denegateTask($id)
    {
        $task = Task::find($id);
        $this->authorize('denegatetask', $task);

        $status = Status::where('name', 'Denegado')->first();
        $task->status()->dissociate();
        $task->status()->associate($status);
        $task->save();

        return response()->json(['status' => '200', 'estado' => $task->status()->first()->toJson()]);
    }

    public function download($file)
    {
        $file = TaskFile::where('id', $file)->get();

        return response()->download(storage_path('app/'.$file->first()->filename), $file->first()->title);

    }

    public function deleteFile(Area $area, Project $project = null, $key)
    {
        $file = TaskFile::where('id', $key)->first();
        Storage::delete($file->filename);
        $file->delete();

        return response()->json($key);
    }

    public function getTask($id)
    {
        $task = Task::find($id);

        return $task->toJson();
    }

    /*
     * Visualizar vista para cargar url
     * */


    public function changeUrl(Task $task)
    {
        session()->put('url-previous', url(URL::previous()));

        return view('tasks.changeUrl', compact('task'));
    }

    /*
     * Agregar la url de la noticia a la tarea
     * */

    public function addUrl(Request $request, Task $task)
    {

        $request->validate([
            'urlnotice' => 'required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
        ]);

        $task->urlnotice = $request->input('urlnotice');
        $task->save();

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Url de la noticia agregada!'
        ];

        return redirect(session()->get('url-previous'))->with($notification);

    }

    public function allTaskEvent(Area $area = null, Request $request)
    {
        $this->authorize('accessTasksEvent', Task::class);
        $area = Area::where('name', 'Prensa')->first();
        $tasks = Task::filterForEvents($request)->orderBy('created_at', 'desc')->paginate(20);

        return view('events.index', compact('tasks', 'area'));

    }

    public function allTaskEventMorning(Area $area = null, Request $request)
    {
        $this->authorize('accessTasksEvent', Task::class);
        $area = Area::where('name', 'Prensa')->first();
        $tasks = Task::filterForEvents($request)->whereBetween('hora', ["07:00 AM", "14:00 PM"])->orderBy('created_at',
            'desc')->paginate(20);

        return view('events.events_morning', compact('tasks', 'area'));

    }

    public function allTaskEventAfternoon(Area $area = null, Request $request)
    {
        $this->authorize('accessTasksEvent', Task::class);
        $area = Area::where('name', 'Prensa')->first();
        $tasks = Task::filterForEvents($request)->whereBetween('hora', ["14:01 AM", "22:00 PM"])->orderBy('created_at',
            'desc')->paginate(20);

        return view('events.events_afternoon', compact('tasks', 'area'));

    }


    public function allTaskProposeds(Area $area = null, Request $request)
    {
        $this->authorize('accesstasksproposed', Task::class);
        $area = Area::where('name', 'Prensa')->first();
        $tasks = Task::filterForProposeds($request)->orderBy('created_at', 'desc')->paginate(20);

        return view('proposeds.index', compact('tasks', 'area'));
    }

    public function allTaskProposedsMorning(Area $area = null, Request $request)
    {
        $this->authorize('accesstasksproposed', Task::class);
        $area = Area::where('name', 'Prensa')->first();
        $tasks = Task::filterForProposedsMorning($request)
            ->orderBy('created_at', 'desc')->paginate(20);
        return view('proposeds.proposeds_morning', compact('tasks', 'area'));
    }


    public function allTaskProposedsAfternoon(Area $area = null, Request $request)
    {
        $this->authorize('accesstasksproposed', Task::class);
        $area = Area::where('name', 'Prensa')->first();
        $tasks = Task::filterForProposedsAfternoon($request)
                     ->orderBy('created_at', 'desc')->paginate(20);
        return view('proposeds.proposeds_afternoon', compact('tasks', 'area'));
    }
}
