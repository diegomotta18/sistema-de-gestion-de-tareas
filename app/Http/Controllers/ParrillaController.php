<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskParrilla;
use App\Jobs\SendInformedTask;
use App\Jobs\SendTask;
use App\Mail\SendEmailTask;
use App\Notifications\TaskNotification;
use App\Status;
use App\Task;
use App\TaskFile;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class ParrillaController extends Controller
{
    //
    public function index()
    {
        $tasks = Task::with(['project','user','assigneds','informeds','status'])->whereNotNull('parrilla')->orderBy('created_at',
            'desc')->paginate(10);

        return view('parrilla.index', compact('tasks'));
    }

    public function store(TaskParrilla $request)
    {

        $date_init = is_null($request->input('date_init')) ? null : Carbon::createFromFormat('d/m/Y',
            $request->input('date_init'));
        $date_finish = is_null($request->input('date_finish')) ? null : Carbon::createFromFormat('d/m/Y',
            $request->input('date_finish'));
        $task = Task::create([
            'title'       => $request->input('title'),
            'description' => $request->input('description'),
            'date_init'   => $date_init,
            'date_finish' => $date_finish,
            'user_id'     => Auth::user()->id,
        ]);
        $task->parent_id = $task->id;

        $task->area_id = $request->input('area');
        $task->project_id = $request->input('project');

        $estado = Status::where('name', 'Informado')->first();
        $task->status()->associate($estado);


        if (!is_null($request->input('replacements'))) {
            $task->replacements()->attach($request->input('replacements'));
        }

        if (!is_null($request->input('informeds'))) {
            $task->informeds()->attach($request->input('informeds'));
            $task->informeds()->attach(auth()->user()->id);

        } else {
            if (is_null($request->input('informeds'))) {
                $task->informeds()->attach(auth()->user()->id);
            }
        }
        if (!is_null($request->input('assigneds'))) {
            $task->assigneds()->attach($request->input('assigneds'));
        }


        $task->url = route('parrilla.show',  $task->id);

        $task->parrilla = true;
        $task->save();

        if ($request->hasFile('files')) {
            foreach ($request->file('files') as $photo) {
                $filename = $photo->store('public');
                $title = $photo->getClientOriginalName();
                $extension = $photo->getMimeType();
                TaskFile::create([
                    'task_id'   => $task->id,
                    'filename'  => $filename,
                    'title'     => $title,
                    'extension' => $extension
                ]);
            }
        }

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Tarea creada!'
        ];
        if (!is_null($request->input('assigneds'))) {

            $job = new SendTask($task->assigneds()->get(), $task, auth()->user());
            dispatch($job);
        } else {
            /*
             * Cuando no hay un usuario asignado se envia un email al  jefe del area seleccionada
             * */
            $user = User::emailChiefToArea($request->input('area'))->first();
            $user->notify(new TaskNotification($task, auth()->user()));

            Mail::to($user->email)
                ->queue(new SendEmailTask($task, auth()->user()));
        }

        if (!is_null($request->input('informeds'))) {
            $job = new SendInformedTask($task->informeds()->get(), $task, auth()->user());
            dispatch($job);
        }

        return redirect()->route('parrilla.index')->with($notification);
    }

    public function update($id, TaskParrilla $request)
    {
        $task = Task::where('id', $id)->first();
        $this->authorize('update', $task);
        $date_init = is_null($request->input('date_init')) ? null : Carbon::createFromFormat('d/m/Y',
            $request->input('date_init'));
        $date_finish = is_null($request->input('date_finish')) ? null : Carbon::createFromFormat('d/m/Y',
            $request->input('date_finish'));
        $task->date_init = $date_init;
        $task->date_finish = $date_finish;
        $task->title = $request->input('title');
        $task->description = $request->input('description');
        $task->area_id = $request->input('area');
        $task->project_id = $request->input('project');

        if (!is_null($request->input('assigneds'))) {
            $task->assigneds()->detach();
            $task->save();
            $task->assigneds()->attach($request->input('assigneds'));
        }

        if (!is_null($request->input('replacements'))) {
            $task->replacements()->detach();
            $task->save();
            $task->replacements()->attach($request->input('replacements'));
        }

        if (!is_null($request->input('informeds'))) {
            $task->informeds()->detach();
            $task->save();
            $task->informeds()->attach($request->input('informeds'));
            $task->informeds()->attach(auth()->user()->id);
        } else {
            if (is_null($request->input('informeds'))) {
                $task->informeds()->detach();
                $task->save();
                $task->informeds()->attach(auth()->user()->id);
            }
        }


        if ($request->hasFile('files')) {
            foreach ($request->file('files') as $photo) {

                $filename = $photo->store('public');
                $title = $photo->getClientOriginalName();
                $extension = $photo->getMimeType();
                TaskFile::create([
                    'task_id'   => $task->id,
                    'filename'  => $filename,
                    'title'     => $title,
                    'extension' => $extension
                ]);
            }
        }
        $task->parrilla = $request->input('parrilla');
        $task->save();

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Tarea actualizada!'
        ];


        if (!is_null($request->input('assigneds'))) {


            $job = new SendTask($task->assigneds()->get(), $task, auth()->user());
            dispatch($job);


        }
        if (!is_null($request->input('informeds'))) {

            $job = new SendInformedTask($task->informeds()->get(), $task, auth()->user());
            dispatch($job);
        }

        return redirect()->route('parrilla.index')
                         ->with($notification);


    }

    public function edit(Task $task)
    {
        $this->authorize('update', $task);
        $replacements = $this->getUsers();
        $assigneds = $this->getUsersAssigneds();

        return view('parrilla.edit', compact('task', 'replacements', 'assigneds'));
    }

    protected function getUsers()
    {
        $users = [];
        $tmp = User::with('area')->where('id', '!=', auth()->user()->id)->userForArea()->get();
        if (auth()->user()->hasArea('Compras')) {
            $tmp = User::with('area')->where('id', '!=', auth()->user()->id)->userToAreaCompras()->get();
        }


        foreach ($tmp as $k => $v) {
            $value = null;
            if ($v->chief === 1) {
                $value = " (Jefe de ".$v->area->name." )";
            }
            $users[$v->id] = $v->name.$value;
        }

        return $users;
    }

    /*
     *
     *  Devuelo los usuarios asignados por area
     *
     * */

    protected function getUsersAssigneds()
    {
        $assigneds = [];

        $tmp = User::with('area')->userForArea()->get();

        if (auth()->user()->hasArea('Compras')) {
            $tmp = User::with('area')->userToAreaCompras()->get();
        }

        foreach ($tmp as $k => $v) {
            $value = null;
            if ($v->chief === 1) {
                $value = " (Jefe de ".$v->area->name." )";
            }
            $assigneds[$v->id] = $v->name.$value;
        }

        return $assigneds;
    }

    /*
  *
  *  Muestra el formulario de carga de un tarea tipo parrilla, permitiendo elegir un área y proyecto
  *
  * */


    public function create()
    {
        $replacements = $this->getUsers();
        $assigneds = $this->getUsersAssigneds();

        return view('parrilla.create', compact('replacements', 'assigneds'));
    }

    public function show(Task $task)
    {

        $this->authorize('show', $task);

        return view('parrilla.show', compact('area', 'project', 'task'));
    }

    public function denegateTask($id)
    {
        $task = Task::find($id);
        $status = Status::where('name', 'Denegado')->first();
        $task->status()->dissociate();
        $task->status()->associate($status);
        $task->save();

        return response()->json(['status' => '200', 'estado' => $task->status()->first()->toJson()]);
    }

}
