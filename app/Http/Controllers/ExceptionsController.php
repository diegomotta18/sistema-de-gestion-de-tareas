<?php

namespace App\Http\Controllers;

use App\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExceptionsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('index', Exception::class);

        $exceptions = Exception::orderBy('created_at','desc')->paginate(10);
        return view('exceptions.index',compact('exceptions'));
    }


    public function show($id){
        $this->authorize('show', Exception::class);
        $exception = Exception::where('id',$id)->first();
        return view('exceptions.show',compact('exception'));
    }

    public function destroy($id){
        $this->authorize('destroy', Exception::class);
        $exception = Exception::where('id',$id)->first();
        $exception->delete();
        return 'ok';
    }

    public function destroyall(){
        $this->authorize('destroy', Exception::class);
        DB::table('exceptions')->truncate();
        return 'ok';

    }

}
