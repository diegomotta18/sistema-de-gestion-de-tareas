<?php

namespace App\Http\Controllers;

use App\Box;
use App\BoxThread;
use App\MsgFile;
use App\MsgThread;
use App\Notifications\MsgThreadNotification;
use App\Thread;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Storage;

class BoxThreadController extends Controller
{
    //
    public function index()
    {
        $box = Box::with('threads')->where('user_id', auth()->user()->id)->first();
        $threads = BoxThread::with('notifications')
                            ->with('unreadNotifications')
                            ->with('messages')
                            ->with('thread')
                            ->with('thread.members')
                            ->with('messages.files')
                            ->where('box_id', $box->id)
                            ->orderby('created_at','desc')->get();
        return view('box.thread.index', compact('threads'));
    }

    public function show(BoxThread $boxthread)
    {
        $messages = MsgThread::with('user')->with('files')->where('thread_id', $boxthread->thread->id)->paginate(10);
        if (isset($boxthread->unreadNotifications)) {
            foreach ($boxthread->unreadNotifications as $notification) {
                $notif = DatabaseNotification::where('id', $notification->id)->get();
                $msg_id = $notification->data['msg_id'];
                $notif->markAsRead();
                foreach (auth()->user()->unreadNotifications()->where('type',MsgThreadNotification::class)->get()->chunk(3) as $notificationsu) {
                    foreach ($notificationsu as $notificationu) {
                        if ($notificationu->data['msg_id'] == $msg_id) {
                            $notificationu->markAsRead();
                        }
                    }
                }
            }
        }

        $thread = $boxthread->thread;

        return view('box.thread.show', compact('messages', 'thread'));
    }

    public function download($file)
    {
        $file = MsgFile::where('id', $file)->get();
        return response()->download(storage_path('app/' . $file->first()->filename), $file->first()->title);
    }

    public function destroy($id){
        $thread =BoxThread::find($id);
        $t= Thread::find($thread->thread_id);
        $t->members()->detach(auth()->user()->id);
        $t->save();
        $thread->delete();
        return response()->json($thread);
    }
}
