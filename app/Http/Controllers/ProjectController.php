<?php

namespace App\Http\Controllers;

use App\Area;
use App\Http\Requests\ProjectRequestMain;
use App\Http\Requests\RequestProject;
use App\Project;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use URL;

class ProjectController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
     *
     * Carga la vista de las tareas de un proyecto seleccionado
     *
     * */

    protected function index(Area $area = null)
    {
        $projects = null;
        if (auth()->user()->isUser()) {

            $projects = Project::with('area')->with('user')->with('tasks')->myProjects()->paginate(10);
        } else {
            if (auth()->user()->isAdmin() || auth()->user()->isGod()) {
                $projects = Project::with('area')->with('user')->with('tasks')->forArea($area)->paginate();

            }
        }

        return view('projects.index', compact('projects', 'area'));
    }

    protected function getAreas()
    {
        $areas = [];

        $tmp = Area::all();

        foreach ($tmp as $k => $v) {
            $areas[$v->id] = $v->name;
        }

        return $areas;
    }


    public function createmain(Area $area = null)
    {
        $this->authorize('create', Project::class);
        $areas = $this->getAreas();
        session()->put('url-previous', url(URL::previous()));

        return view('projects.createmain', compact('areas', 'area'));
    }

    /*
    *
    * Permite el alta de un nuevo proyecto desde la home
    *
    * */

    public function storemain(ProjectRequestMain $request, Area $area = null)
    {
        Project::create([
            'name'        => $request->input('name'),
            'description' => $request->input('description'),
            'area_id'     => $request->input('areas'),
            'user_id'     => auth()->user()->id
        ]);

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Proyecto creado!'
        ];

        return redirect(session()->get('url-previous'))->with($notification);

    }

    public function create(Area $area)
    {
        $this->authorize('create', Project::class);
        session()->put('url-previous', url(URL::previous()));

        return view('projects.create', compact('area'));
    }

    /*
    *
    * Permite el alta de un nuevo proyecto desde la vista de todos los proyectos
    *
    * */

    public function store(Area $area, RequestProject $request)
    {
        $project = Project::create([
            'name'        => $request->input('name'),
            'description' => $request->input('description'),
            'user_id'     => auth()->user()->id
        ]);

        $project->area()->associate($area);
        $project->save();

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Proyecto creado!'
        ];

        return redirect()
            ->route('areas.show', $area->name)
            ->with($notification);
    }


    /*
    *
    * Permite  actualizar los datos de un proyecto desde la vista de todos los proyectos
    *
    * */


    public function update(Area $area, Project $project, RequestProject $request)
    {

        $this->authorize('update', $project);
        $project->fill($request->all());
        $project->save();

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Proyecto actualizado!'
        ];

        return redirect()
            ->route('areas.show', $area->name)
            ->with($notification);
    }


    /*
    *
    * Permite  eliminar los datos de un proyecto desde la vista de todos los proyectos
    *
    * */

    public function destroy($id)
    {
        $project = Project::find($id);
        $this->authorize('destroy', $project);
        $project->delete();

        return 'ok';
    }

    public function edit(Area $area, Project $project)
    {
        $this->authorize('update', $project);

        return view('projects.edit', compact('project', 'area'));
    }

    /*
    *
    * Visualiza todas las tareas dentro de un proyecto
    *
    * */

    public function show(Area $area, Project $project)
    {
        $tasks = null;
        if (auth()->user()->hasRole('god') || auth()->user()->hasRole('admin')) {
            $tasks = Task::with('status')->with('subtasks')->with('subtasks.status')->with('assigneds')->with('informeds')->with('area')->with('project')->where('area_id',
                $area->id)
                         ->where('project_id', $project->id)
                         ->where('parent_id', '!=', null)->orderBy('created_at', 'desc')
                         ->whereNull('parrilla')
                         ->paginate(20);
        } elseif (auth()->user()->hasRole('user')) {
            $tasks = Task::with('status')->with('assigneds')->with('subtasks.status')->with('informeds')->with('area')->with('project')
                         ->myTasksAssigned($project)
                         ->orderBy('created_at', 'desc')
                         ->whereNull('parrilla')
                         ->paginate(20);

        }

        return view('projects.show', compact('area', 'project', 'tasks'));

    }


    /*
     *
     * Visualiza las tareas en columnas las tareas informadas, en proceso y finalizadas
     *
     * */

    public function agileboard(Project $project, Request $request)
    {
        $area = Area::where('id', $project->area_id)->first();

        /*
         *
         * busca por titulo de la tarea
         *
         * */
        if(!is_null($request->input('title')) ){
            $request->flash(['title']);

            $informados =  Task::search($request->input('title'))->byProjectAndStatus('Informado',$project->id)->get();
            $procesos =  Task::search($request->input('title'))->byProjectAndStatus('En proceso',$project->id)->get();
            $finalizados =  Task::search($request->input('title'))->byProjectAndStatus('Finalizado',$project->id)->get();
            return view('projects.agileboard', compact('project', 'area', 'informados', 'procesos', 'finalizados'))->withInput(Input::all());
        }
        /*
         *
         * busca por fecha de inicio de las tareas en un rango de fechas
         *
         * */
        if (!is_null($request->input('date_init')) && !is_null($request->input('date_finish') && is_null($request->input('title')))) {
            $request->flash(['date_init', 'date_finish']);

            $informados = Task::byProjectAndStatus('Informado',$project->id,$request->input('date_init'),$request->input('date_finish'))->get();
            $procesos = Task::byProjectAndStatus('En proceso',$project->id,$request->input('date_init'),$request->input('date_finish'))->get();
            $finalizados = Task::byProjectAndStatus('Finalizado',$project->id,$request->input('date_init'),$request->input('date_finish'))->get();

            return view('projects.agileboard',
                compact('project', 'area', 'informados', 'procesos', 'finalizados'))->withInput(Input::all());
        }
        /*
         *
         * Busca por id de proyecto y por estado
         *
         * */
        $informados = Task::byProjectAndStatus('Informado',$project->id)->get();
        $procesos = Task::byProjectAndStatus('En proceso',$project->id)->get();
        $finalizados = Task::byProjectAndStatus('Finalizado',$project->id)->get();

        return view('projects.agileboard', compact('project', 'area', 'informados', 'procesos', 'finalizados'));

    }



}
