<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    //
    public function index(){
        return view ('tags.index');
    }

    public function allTags(){
        $tags = Tag::all();
        return $tags->toJson();
    }


    public function create(){

    }

    public function store(Request $request){
        $tag = Tag::create([
            'name' => $request->input('name'),
            'color' => $request->input('color'),
        ]);
        return $tag->toJson();
    }

    public function update($id, Request $request){
        $tag = Tag::find($id);
        if(!is_null($request->input('color'))){
            $tag->color = $request->input('color');
        }
        $tag->name = $request->input('name');
        $tag->save();
        return $tag->toJson();
    }

    public function destroy($id){}
}
