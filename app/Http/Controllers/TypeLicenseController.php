<?php

namespace App\Http\Controllers;

use App\Http\Requests\TypeLicenseRequest;
use App\TypeLicense;
use Illuminate\Http\Request;

class TypeLicenseController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(){
        $this->authorize('index', TypeLicense::class);
        $typelicenses = TypeLicense::all();
        return view('typelicense.index',compact('typelicenses'));
    }

    public function create(){
        $this->authorize('store', TypeLicense::class);
        return view('typelicense.create');
    }

    public function store(TypeLicenseRequest $request){

        $this->authorize('store', TypeLicense::class);

        TypeLicense::create([
            'name'     => $request->input('name'),
        ]);

        $notification = ['alert-type' => 'success', 'message' => 'Tipo de licencia creada!'];

        return redirect()->action('TypeLicenseController@index')->with($notification);
    }

    public function update (TypeLicenseRequest $request, TypeLicense $typelicense){
        $this->authorize('update', TypeLicense::class);
        $typelicense->nameg = $request->input('name');
        $typelicense->save();
        $notification = ['alert-type' => 'success', 'message' => 'Tipo de licencia actualizado!'];

        return redirect()->action('TypeLicenseController@index')->with($notification);
    }

    public function  edit(TypeLicense $typelicense){
        $this->authorize('update', TypeLicense::class);
        return view('typelicense.edit',compact('typelicense'));
    }

    public function destroy($id){
        $this->authorize('destroy', TypeLicense::class);
        $typelicense = TypeLicense::where('id',$id);
        $typelicense->delete();
        return 'ok';
    }
}
