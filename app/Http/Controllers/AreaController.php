<?php

namespace App\Http\Controllers;

use App\Area;
use App\Http\Requests\AreaCreatedRequest;
use App\Http\Requests\AreaUpdateRequest;
use App\Project;
use App\Task;
use App\User;
use Psy\Util\Json;

class AreaController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->authorize('index', Area::class);

        $areas = Area::all();
        return view('areas.index', compact('areas'));

    }

    public function create()
    {
        $this->authorize('store', Area::class);

        return view('areas.create');
    }

    public function store(AreaCreatedRequest $request)
    {
        $this->authorize('store', Area::class);

        Area::create([
            'name' => $request->input('name'),
            'icon' => $request->input('icon')
        ]);
        $notification = ['alert-type' => 'success', 'message' => 'Área creada!'];

        return redirect()->action('AreaController@index')->with($notification);
    }

    public function update(AreaUpdateRequest $request,$id)
    {
        $this->authorize('update', Area::class);
        $area = Area::where('id',$id)->first();
        $area->name = $request->input('name');
        $area->icon = $request->input('icon');

        $area->save();
        $notification = ['alert-type' => 'success', 'message' => 'Área actualizada!'];

        return redirect()->action('AreaController@index')->with($notification);
    }

    public function destroy($id)
    {
        $this->authorize('destroy', Area::class);

        $area = Area::findOrFail($id);
        $area->delete();
        return 'ok';
    }


    public function edit(Area $area)
    {
        $this->authorize('update', Area::class);

        return view('areas.edit',compact('area'));
    }

    public function show(Area $area)
    {
        $this->authorize('show', $area);
        if(auth()->user()->hasRole('user')){
            $projects = Project::with('user')->where('area_id', $area->id)->myProjects()->orderBy('created_at', 'desc')->get();

        }else{
            $projects = Project::with('user')->where('area_id', $area->id)->orderBy('created_at', 'desc')->get();

        }

        return view('areas.show', compact('area', 'projects'));
    }

    public function tasks(Area $area)
    {
        $tasks = Task::with(['status','informeds','assigneds','project','area','user'])->where('area_id', $area->id)->where('project_id', null)->orderBy('created_at', 'desc')->get();
        return view('areas.taskslist', compact('tasks', 'area'));
    }

    public function allAreas()
    {
        $areas = Area::all();
        return $areas->toJson();
    }

    public function usersForArea(Area $area)
    {
        $users = User::with('area')->whereHas('areas',function ($q) use ($area){
            $q->where('area_id',$area->id);
        })->get();
        return $users->toJson();
    }

    public function areaForUser()
    {
//        $areas = auth()->user()->areas()->get();
        $areas = Area::all();
        return $areas->toJson();
    }

    public function projectForArea($id)
    {
        $projects = Project::with('area')->where('area_id', $id)->get();
        return $projects->toJson();
    }

}
