<?php

namespace App\Http\Controllers;

use App\Freeday;
use App\Http\Requests\FreedayRequest;
use App\Mail\DayoffMailable;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class FreedayController extends Controller
{
    //Muestra en un listado todos los francos o devuelve los franco
    //por filtro de fecha de franco o por usuario
    public function index(Request $request)
    {
        $this->authorize('index', Freeday::class);
        $users = $this->getUsers();

        $daysoff = Freeday::query()->with('user');

        //filtar por usuario
        if (!is_null($request->get('user'))) {
            $request->flash(['user']);
            $daysoff->where('user_id', $request->get('user'));
        } //filtar por fecha de inicio y fecha de finalizacion
        else {
            if (!is_null($request->get('date_init')) && !is_null($request->get('date_finish'))) {
                $request->flash(['date_init', 'date_finish']);
                $finish = Carbon::createFromFormat('d/m/Y', $request->get('date_finish'));
                $init = Carbon::createFromFormat('d/m/Y', $request->get('date_init'));
                $daysoff->whereBetween('date_taken', [$init->format('Y-m-d'), $finish->format('Y-m-d')]);

            }
        }

        $daysoff = $daysoff->orderBy('created_at', 'desc')->paginate(10);

        return view('daysoff.index', compact('daysoff', 'users'))->with(Input::all());
    }

    //Guarda los datos del franco
    public function store(FreedayRequest $request)
    {
        $this->authorize('store', Freeday::class);
        $date_taken = Carbon::createFromFormat('d/m/Y', $request->get('date_taken'));

        $dayoff = Freeday::create([
            'date_taken'   => $date_taken,
            'user_id'      => $request->input('user'),
            'observations' => $request->input('observations')
        ]);
        Mail::to($dayoff->user->email)->queue(new DayoffMailable($dayoff,auth()->user()));

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Franco creado!'
        ];

        return redirect()->route('freedays.index')->with($notification);
    }

    //Actualiza los datos del franco
    public function update(FreedayRequest $request, Freeday $dayoff)
    {
        $this->authorize('update', Freeday::class);
        $date_taken = Carbon::createFromFormat('d/m/Y', $request->get('date_taken'));
        $dayoff->date_taken = $date_taken;
        $dayoff->user_id = $request->input('user');
        $dayoff->observations = $request->input('observations');
        $dayoff->save();
        Mail::to($dayoff->user->email)->queue(new DayoffMailable($dayoff,auth()->user()));

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Franco actualizado!'
        ];

        return redirect()->route('freedays.index')->with($notification);
    }

    //obtiene el formulario para editar el franco
    public function edit(Freeday $dayoff)
    {
        $this->authorize('update', Freeday::class);

        $users = $this->getUsers();

        return view('daysoff.edit', compact('users', 'dayoff'));
    }

    //obtiene el formulario para crear el franco
    public function create()
    {
        $this->authorize('store', Freeday::class);

        $users = $this->getUsers();

        return view('daysoff.create', compact('users'));
    }

    //elimina el franco
    public function destroy(Freeday $daysoff)
    {
        $this->authorize('destroy', Freeday::class);
        $daysoff->delete();

        return 'ok';
    }


    // obtiene todos usuarios
    protected function getUsers()
    {
        $users = [];
        $tmp = User::all();

        foreach ($tmp as $k => $v) {
            $users[$v->id] = $v->name;
        }

        return $users;
    }
}
