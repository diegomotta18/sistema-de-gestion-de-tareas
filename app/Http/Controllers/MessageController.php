<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageRequest;
use App\Jobs\SendMessage;
use App\Message;
use App\Notifications\MessageNotification;
use App\User;
use Illuminate\Notifications\DatabaseNotification;

class MessageController extends Controller
{
    //

    protected function getUsers()
    {
        $users = [];

        $tmp = User::where('id','!=', auth()->user()->id)->get();

        foreach ($tmp as $k => $v) {
            $users[$v->id] = $v->name;
        }

        return $users;
    }
    public  function create(){
        $users = $this->getUsers();
        return view('messages.create',compact('users'));
    }

    public function store(MessageRequest $request){
        $message = Message::create([
            'subject' => $request->input('subject'),
            'description' => $request->input('description'),
            'user_id' => auth()->user()->id
        ]);

        $message->users()->attach($request->input('users'));
        $message->save();
        $job = new SendMessage($message->users()->get(), auth()->user(),$message);
        dispatch($job);

        $notification = [
            'alert-type' => 'success',
            'message' => 'Mensaje enviado!'
        ];

        return back()->with($notification);
    }


    public function show(Message $message){
        return view ('messages.show',compact('message'));
    }

    public function index(){

        return view('messages.index',[
            'notifications' => auth()->user()->notifications->where('type',MessageNotification::class),
            'unReadNotifications' => auth()->user()->unreadNotifications->where('type',MessageNotification::class),
            'readNotifications' => auth()->user()->readNotifications->where('type',MessageNotification::class),
        ]);
    }

    public function read($id){
        DatabaseNotification::find($id)->markAsRead();

        return 'ok';
    }

    public function destroy($id){
        DatabaseNotification::find($id)->delete();

        return 'ok';
    }


}
