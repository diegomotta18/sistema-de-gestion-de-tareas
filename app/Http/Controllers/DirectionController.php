<?php

namespace App\Http\Controllers;

use App\Area;
use App\Http\Requests\EventRequest;
use App\Jobs\EventFinishJob;
use App\Jobs\EventProcessJob;
use App\Jobs\SendEventJob;
use App\Status;
use App\User;
use Illuminate\Support\Carbon;

class DirectionController extends Controller
{
    //
    public  function  index(){

        $areas = Area::all();
        return view('direccion.dashboard',compact('areas'));
    }


    public function getArea(Area $area){

        return redirect()->action('HomeController@index', $area);
    }

}
