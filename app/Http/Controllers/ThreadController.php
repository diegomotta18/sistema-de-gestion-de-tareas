<?php

namespace App\Http\Controllers;

use App\Box;
use App\BoxThread;
use App\Http\Requests\MsgThreadRequest;
use App\Http\Requests\ReplyRequest;
use App\Jobs\SendMsgThread;
use App\MsgFile;
use App\MsgThread;
use App\Notifications\MsgThreadNotification;
use App\Thread;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;

class ThreadController extends Controller
{
    //
    protected function getUsers()
    {
        $users = [];

        $tmp = User::where('id', '!=', auth()->user()->id)->get();

        foreach ($tmp as $k => $v) {
            $users[$v->id] = $v->name;
        }

        return $users;
    }

    public function create()
    {
        $users = $this->getUsers();

        return view('box.thread.create', compact('users'));
    }


    public function store(MsgThreadRequest $request)
    {
        $thread = Thread::create([
            'title' => $request->input('subject')
        ]);

        $message = MsgThread::create([
            'user_id' => auth()->user()->id,
            'message' => $request->input('description'),
            'thread_id' => $thread->id
        ]);

        if ($request->hasFile('files')) {
            foreach ($request->file('files') as $photo) {
                $filename = $photo->store('public');
                $title = $photo->getClientOriginalName();
                $extension = $photo->getMimeType();
                MsgFile::create([
                    'msg_thread_id' => $message->id,
                    'filename' => $filename,
                    'title' => $title,
                    'extension' => $extension
                ]);
            }
        }

        $thread->members()->attach($request->input('users'));
        $thread->members()->attach(auth()->user()->id);
        $thread->save();

        foreach ($thread->members()->get() as $member) {
            $boxthread = new BoxThread();
            $boxthread->thread_id = $thread->id;
            $boxthread->box_id = $member->box->id;
            $boxthread->save();
            $boxthread->messages()->attach($message);
            $boxthread->save();
            $boxthread->notify(new MsgThreadNotification($message, $boxthread->id));

            if ($member->id != auth()->user()->id) {

                $member->notify(new MsgThreadNotification($message, $boxthread->id));
            }
            if ($member->id == auth()->user()->id) {
                if (!is_null($boxthread->unreadNotifications()->get())) {
                    foreach ($boxthread->unreadNotifications()->get()->chunk(3) as $notifications) {
                        foreach ($notifications as $notification){
                            if ($notification->data['msg_id'] == $message->id) {
                                $notification->markAsRead();
                            }
                        }
                    }
                }
            }
        }

        //Envia un email a todos los usuarios que se encuentran en el msg

        $job = new SendMsgThread($thread->members()->get(), auth()->user(),$thread->title, $message->message , route('threads.show',$boxthread->id));
        dispatch($job);


        $notification = ['alert-type' => 'success',
            'message' => 'Mensaje creado!'];

        return redirect()->route('threads.index')
            ->with($notification);
    }

    public function reply(Thread $thread, ReplyRequest $request)
    {
        $message = MsgThread::create([
            'user_id' => auth()->user()->id,
            'message' => $request->input('description'),
            'thread_id' => $thread->id
        ]);

        if ($request->hasFile('files')) {
            foreach ($request->file('files') as $photo) {
                $filename = $photo->store('public');
                $title = $photo->getClientOriginalName();
                $extension = $photo->getMimeType();
                MsgFile::create([
                    'msg_thread_id' => $message->id,
                    'filename' => $filename,
                    'title' => $title,
                    'extension' => $extension
                ]);
            }
        }

        foreach ($thread->members()->get() as $member) {
            $boxthread = BoxThread::where('box_id', $member->box->id)->where('thread_id', $thread->id)->first();
            $boxthread->messages()->attach($message);
            $boxthread->save();
            $boxthread->notify(new MsgThreadNotification($message, $boxthread->id));

            if ($member->id != auth()->user()->id) {

                $member->notify(new MsgThreadNotification($message, $boxthread->id));
            }
            if ($member->id == auth()->user()->id) {
                if (!is_null($boxthread->unreadNotifications()->get())) {
                    foreach ($boxthread->unreadNotifications()->get()->chunk(3) as $notifications) {
                        foreach ($notifications as $notification){
                            if ($notification->data['msg_id'] == $message->id) {
                                $notification->markAsRead();
                            }
                        }
                    }
                }
            }
        }


        $job = new SendMsgThread($thread->members()->get(), auth()->user(),$thread->title, $message->message , route('threads.show',$boxthread->id));
        dispatch($job);

        $notification = ['alert-type' => 'success',
            'message' => 'Mensaje enviado!'];

        return redirect()->route('threads.index')
            ->with($notification);
    }

}
