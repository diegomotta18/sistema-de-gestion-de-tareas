<?php

namespace App\Http\Controllers;

use App\Area;
use App\Http\Requests\TicketRequest;
use App\Jobs\SendInformedModifiedTask;
use App\Jobs\SendInformedTask;
use App\Jobs\SendTask;
use App\Jobs\TaskFinishJob;
use App\Jobs\TaskProcessJob;

use App\Status;
use App\Task;
use App\TaskFile;
use App\TypeTaskModel;
use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class TicketController extends Controller
{
    //


    public function create()
    {
        $replacements = $this->getUsers();
        $assigneds = $this->getUsersAssigneds();

        return view('tickets.create', compact('replacements', 'assigneds'));
    }

    public function index()
    {

        if (!auth()->user()->hasAreaUnique('Sistemas')) {
            $tickets = Task::query()
                           ->with('user')
                           ->with('status')
                           ->with('informeds')
                           ->with('assigneds')
                           ->where('user_id', auth()->user()->id)
                           ->whereHas('typeTask', function ($q) {
                               return $q->where('type', 'Ticket');
                           })->orderBy('created_at','desc')
                           ->paginate(10);

            return view('tickets.index', compact('tickets'));

        } else {
            $tickets = Task::query()
                           ->with('user')
                           ->with('status')
                           ->with('informeds')
                           ->with('assigneds')
                           ->whereHas('typeTask', function ($q) {
                               return $q->where('type', 'Ticket');
                           })->orderBy('created_at','desc')
                           ->paginate(10);

            return view('tickets.index', compact('tickets'));

        }


    }

    public function store(Request $request)
    {
        $request->validate([
            'title'  => 'required|max:255',
            'description'  => 'required|max:15000',
            'files'
        ]);

        $type_tast = TypeTaskModel::where('type', 'Ticket')->first();
        $ticket = Task::create([
            'title'       => $request->input('title'),
            'description' => $request->input('description'),
            'date_init'   => null,
            'date_finish' => null,
            'user_id'     => Auth::user()->id,
        ]);

        $ticket->parent_id = $ticket->id;

        $ticket->type_task_id = $type_tast->id;
        $sistemas = Area::where('name','Sistemas')->first();
        $ticket->area_id = $sistemas->id;
        $ticket->project_id = null;

        $estado = Status::where('name', 'Informado')->first();
        $ticket->status()->associate($estado);


        $user = User::emailChiefToAreaName('Sistemas')->first();

        $ticket->informeds()->attach([auth()->user()->id, $user->id]);

        $ticket->url = route('tickets.show', $ticket->id);

        if ($request->hasFile('files')) {
            foreach ($request->file('files') as $photo) {
                $filename = $photo->store('public');
                $title = $photo->getClientOriginalName();
                $extension = $photo->getMimeType();
                TaskFile::create([
                    'task_id'   => $ticket->id,
                    'filename'  => $filename,
                    'title'     => $title,
                    'extension' => $extension
                ]);
            }
        }

        $ticket->save();
        $job = new SendInformedModifiedTask($ticket->informeds()->get(), $ticket, auth()->user());
        dispatch($job);

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Ticket creado!'
        ];

        return redirect()->route('tickets.index')->with($notification);

    }

    public function update(Task $ticket, Request $request)
    {
        $this->authorize('update', $ticket);
        $request->validate([
            'title'  => 'required|max:255',
            'description'  => 'required|max:15000',
            'files'
        ]);
        $ticket->title = $request->input('title');
        $ticket->description = $request->input('description');
        $ticket->save();
        $job = new SendInformedModifiedTask($ticket->informeds()->get(), $ticket, auth()->user());
        dispatch($job);

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Ticket modificado!'
        ];


        return redirect()->route('ticket.index')
                         ->with($notification);
    }

    public function updateAdmin(Task $ticket, TicketRequest $request)
    {
        $this->authorize('update', $ticket);
        $date_init = Carbon::createFromFormat('d/m/Y', $request->input('date_init'));
        $date_finish = Carbon::createFromFormat('d/m/Y', $request->input('date_finish'));
        $ticket->date_init = $date_init;
        $ticket->date_finish = $date_finish;
        $ticket->title = $request->input('title');
        $ticket->description = $request->input('description');

        if (!is_null($request->input('assigneds'))) {
            $ticket->assigneds()->detach();
            $ticket->save();
            $ticket->assigneds()->attach($request->input('assigneds'));
        }

        if (!is_null($request->input('replacements'))) {
            $ticket->replacements()->detach();
            $ticket->save();
            $ticket->replacements()->attach($request->input('replacements'));
        }

        if (!is_null($request->input('informeds'))) {
            $ticket->informeds()->detach();
            $ticket->save();
            $ticket->informeds()->attach($request->input('informeds'));
            $ticket->informeds()->attach(auth()->user()->id);
        } else {
            if (is_null($request->input('informeds'))) {
                $ticket->informeds()->detach();
                $ticket->save();
                $ticket->informeds()->attach(auth()->user()->id);
            }
        }


        if ($request->hasFile('files')) {
            foreach ($request->file('files') as $photo) {

                $filename = $photo->store('public');
                $title = $photo->getClientOriginalName();
                $extension = $photo->getMimeType();
                TaskFile::create([
                    'task_id'   => $ticket->id,
                    'filename'  => $filename,
                    'title'     => $title,
                    'extension' => $extension
                ]);
            }
        }

        $ticket->save();

        $notification = [
            'alert-type' => 'success',
            'message'    => 'Tarea actualizada!'
        ];


        if (!is_null($request->input('assigneds'))) {

            $job = new SendTask($ticket->assigneds()->get(), $ticket, auth()->user());
            dispatch($job);

        }
        if (!is_null($request->input('informeds'))) {

            $job = new SendInformedTask($ticket->informeds()->get(), $ticket, auth()->user());
            dispatch($job);
        }

        return redirect()->route('tickets.index')
                         ->with($notification);


    }


    public function show(Task $ticket)
    {
        return view('tickets.show', compact('ticket'));
    }


    public function edit(Task $ticket)
    {
        $replacements = $this->getUsers();
        $assigneds = $this->getUsersAssigneds();

        return view('tickets.edit', compact('ticket', 'replacements', 'assigneds'));
    }

    public function delete(Task $ticket)
    {
        $task = Task::find($ticket->id);
        $this->authorize('destroy', $task);
        if (!is_null($task->files()->get())) {
            foreach ($task->files()->get() as $f) {
                $file = TaskFile::where('id', $f->id)->first();
                Storage::delete($file->filename);
                $file->delete();
            }
        }
        $task->assigneds()->detach();
        $task->informeds()->detach();
        $task->project_id = null;
        $task->user_id = null;
        $task->area_id = null;
        $task->save();
        $task->forceDelete();

        return 'ok';
    }

    public function init(Task $ticket)
    {
        $status = Status::where('name', 'Informado')->first();
        $ticket->status()->dissociate();
        $ticket->status()->associate($status);
        $ticket->save();

        return response()->json(['status' => '200', 'estado' => $ticket->status()->first()->toJson()]);
    }


    public function inprocess(Task $ticket)
    {

        $this->authorize('processtask', $ticket);

        $status = Status::where('name', 'En proceso')->first();
        $ticket->status_id = $status->id;
        $ticket->save();
        if (!is_null($ticket->assigneds->first())) {
            $job = new TaskProcessJob($ticket->assigneds()->get(), $ticket, auth()->user());
            dispatch($job);
        }
        if (!is_null($ticket->informeds->first())) {
            $job = new TaskProcessJob($ticket->informeds()->get(), $ticket, auth()->user());
            dispatch($job);
        }

        return response()->json(['status' => '200', 'estado' => $ticket->status()->first()->toJson()]);
    }

    public function finish(Task $ticket)
    {

        $this->authorize('finishtask', $ticket);

        $status = Status::where('name', 'Finalizado')->first();
        $ticket->status_id = $status->id;
        $ticket->save();

        if (!is_null($ticket->assigneds->first())) {
            $job = new TaskFinishJob($ticket->assigneds()->get(), $ticket, auth()->user());
            dispatch($job);
        }
        if (!is_null($ticket->informeds->first())) {
            $job = new TaskFinishJob($ticket->informeds()->get(), $ticket, auth()->user());
            dispatch($job);
        }

        return response()->json(['status' => '200', 'estado' => $ticket->status()->first()->toJson()]);
    }

    /*
    *
    * Devuelve los usuarios para reemplazos de tareas
    *
    * */

    protected function getUsers()
    {
        $users = [];
        $tmp = User::with('area')->where('id', '!=', auth()->user()->id)->userForArea()->get();
        if (auth()->user()->hasArea('Compras')) {
            $tmp = User::with('area')->where('id', '!=', auth()->user()->id)->userToAreaCompras()->get();
        }


        foreach ($tmp as $k => $v) {
            $value = null;
            if ($v->chief === 1) {
                $value = " (Jefe de ".$v->area->name." )";
            }
            $users[$v->id] = $v->name.$value;
        }

        return $users;
    }

    /*
     *
     *  Devuelo los usuarios asignados por area
     *
     * */

    protected function getUsersAssigneds()
    {
        $assigneds = [];

        $tmp = User::with('area')->userForArea()->get();

        if (auth()->user()->hasArea('Compras')) {
            $tmp = User::with('area')->userToAreaCompras()->get();
        }

        foreach ($tmp as $k => $v) {
            $value = null;
            if ($v->chief === 1) {
                $value = " (Jefe de ".$v->area->name." )";
            }
            $assigneds[$v->id] = $v->name.$value;
        }

        return $assigneds;
    }

    public function destroy($id)
    {
        $task = Task::find($id);
        $this->authorize('destroy', $task);

        if (!is_null($task->files()->get())) {
            foreach ($task->files()->get() as $f) {
                $file = TaskFile::where('id', $f->id)->first();
                Storage::delete($file->filename);
                $file->delete();
            }
        }
        $task->assigneds()->detach();
        $task->informeds()->detach();
        $task->project_id = null;
        $task->user_id = null;
        $task->area_id = null;
        $task->save();
        $task->forceDelete();

        return 'ok';
    }

    public function denegate(Task $ticket)
    {
        $this->authorize('denegatetask', $ticket);

        $status = Status::where('name', 'Denegado')->first();
        $ticket->status()->dissociate();
        $ticket->status()->associate($status);
        $ticket->save();

        return response()->json(['status' => '200', 'estado' => $ticket->status()->first()->toJson()]);
    }

}
