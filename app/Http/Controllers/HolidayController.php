<?php

namespace App\Http\Controllers;

use App\Holiday;
use App\Http\Requests\HolidayRequest;
use App\Jobs\HolidayJob;
use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Calendar;
use Illuminate\Support\Facades\Input;

class HolidayController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $this->authorize('index', Holiday::class);

        $users = $this->getUsers();
        
        $holidays = Holiday::query()->with(['user', 'replacements']);
        
        //filtar por usuario
        if (!is_null($request->get('user'))) {
            $request->flash(['user']);
            $holidays->where('user_id', $request->get('user'));
        } else {
            //filtar por fecha de inicio y fecha de finalizacion
            if (!is_null($request->get('date_init')) && !is_null($request->get('date_finish'))) {
                $request->flash(['date_init', 'date_finish']);
                $finish = Carbon::createFromFormat('d/m/Y', $request->get('date_finish'));
                $init = Carbon::createFromFormat('d/m/Y', $request->get('date_init'));
                $holidays->where('date_init', $init->format('Y-m-d'));
                $holidays->where('date_finish', [$finish->format('Y-m-d')]);

            }
        }
    
        $holidays = $holidays->where('finish_holiday', false)->orderBy('date_init')->paginate(10);
        
        return view('holiday.index', compact('holidays','users'))->with(Input::all());
    }

    public function store(HolidayRequest $request)
    {
        $this->authorize('store', Holiday::class);

        $date_init = Carbon::createFromFormat('d/m/Y', $request->input('date_init'));
        $date_finish = Carbon::createFromFormat('d/m/Y', $request->input('date_finish'));
        $date_reinstate = Carbon::createFromFormat('d/m/Y', $request->input('date_reinstate'));
        $holiday = Holiday::create([
            'date_init'      => $date_init,
            'date_finish'    => $date_finish,
            'date_reinstate' => $date_reinstate,
            'observations'   => $request->input('observations'),
            'user_id'        => $request->input('user'),
        ]);

        $holiday->url = route('holiday.show', $holiday->id);
        $holiday->save();

        if (!is_null($request->input('replacements'))) {
            $holiday->replacements()->attach($request->input('replacements'));
            $holiday->save();
        }

        $job = new HolidayJob($holiday, auth()->user());
        dispatch($job);

        $notification = ['alert-type' => 'success', 'message' => 'Vacaciones creada!'];

        return redirect()->action('HolidayController@index')->with($notification);
    }


    public function finished()
    {
        $this->authorize('index', Holiday::class);

        $holidays = Holiday::with(['user', 'replacements'])->where('finish_holiday', true)->get();

        return view('holiday.finished', compact('holidays'));
    }

    public function create()
    {
        $this->authorize('store', Holiday::class);

        $users = $this->getUsers();
        $replacements = $this->getUsers();

        return view('holiday.create', compact('users', 'replacements'));
    }

    public function edit(Holiday $holiday)
    {
        $this->authorize('update', Holiday::class);

        $users = $this->getUsers();
        $replacements = $this->getUsers();

        return view('holiday.edit', compact('holiday', 'users', 'replacements'));
    }

    public function update(Holiday $holiday, Request $request)
    {
        $this->authorize('update', Holiday::class);
        $date_init = Carbon::createFromFormat('d/m/Y', $request->input('date_init'));
        $date_finish = Carbon::createFromFormat('d/m/Y', $request->input('date_finish'));
        $date_reinstate = Carbon::createFromFormat('d/m/Y', $request->input('date_reinstate'));
        $holiday->date_init = $date_init;
        $holiday->date_finish = $date_finish;
        $holiday->date_reinstate = $date_reinstate;
        $holiday->observations = $request->input('observations');
        $holiday->finish_holiday = $request->has('finish_holiday');
        $holiday->user_id = $request->input('user');

        if (!is_null($request->input('replacements'))) {
            $holiday->replacements()->detach();
            $holiday->save();
            $holiday->replacements()->attach($request->input('replacements'));
        }
        $holiday->save();
        if (!$holiday->finish_holiday) {
            $job = new HolidayJob($holiday, auth()->user());
            dispatch($job);
        }


        $notification = [
            'alert-type' => 'success',
            'message'    => 'Vacaciones actualizado!'
        ];

        return redirect()->route('holiday.index')->with($notification);

    }

    public function destroy(Holiday $holiday)
    {
        $this->authorize('destroy', Holiday::class);

        $holiday->delete();

        return 'ok';
    }


    public function finish(Holiday $holiday)
    {
        $this->authorize('finish', Holiday::class);

        $holiday->finish_holiday = true;
        $holiday->save();

        return 'ok';
    }


    public function notfinish(Holiday $holiday)
    {
        $this->authorize('finish', Holiday::class);

        $holiday->finish_holiday = false;
        $holiday->save();

        return 'ok';
    }

    public function show(Holiday $holiday)
    {

        $holiday = Holiday::with(['user', 'replacements'])->where('id', $holiday->id)->where('finish_holiday',
            false)->get();
        $calendar = $this->getDataUnique($holiday);
        $holiday = $holiday->first();

        return view('holiday.show', compact('holiday', 'calendar'));
    }


    protected function getUsers()
    {
        $users = [];
        $tmp = User::orderBy('name')->get();

        foreach ($tmp as $k => $v) {
            $users[$v->id] = $v->name;
        }

        return $users;
    }

    public function showCalendar()
    {
        $holidays = Holiday::with(['user', 'replacements'])->where('finish_holiday', false)->get();
        $calendar = $this->getData($holidays);

        return view('holiday.calendar', compact('calendar'));
    }

    public function getData($data)
    {
        $events = [];
        if ($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = Calendar::event(
                    $value->user->name,
                    true,
                    date('Y/m/d', strtotime(str_replace('/', '-', $value->date_init))),
                    date('Y/m/d', strtotime(str_replace('/', '-', $value->date_finish))),
                    null,
                    // Add color and link on event
                    [
                        'color' => '#28a745',

                    ]
                );
            }
        }
        $calendar = Calendar::addEvents($events);
        $calendar->setOptions(
            [
                'header'     => [
                    'left'   => 'prev,next today',
                    'center' => 'title',
                    'right'  => 'year,month,agendaWeek,agendaDay',
                ],
                'eventLimit' => true,
                'lang'       => 'es'
            ]
        );

        return $calendar;
    }


    public function getDataUnique($data)
    {
        $events = [];
        if ($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = Calendar::event(
                    $value->user->name,
                    true,
                    date('Y/m/d', strtotime(str_replace('/', '-', $value->date_init))),
                    date('Y/m/d', strtotime(str_replace('/', '-', $value->date_finish))),
                    null,
                    [
                        'color' => '#28a745',

                    ]
                );
            }
        }
        $calendar = Calendar::addEvents($events);
        $calendar->setOptions(
            [
                'header'      => [

                    'left'   => 'prev,next today',
                    'center' => 'title',
                    'right'  => 'month',
                ],
                'eventLimit'  => false,
                'lang'        => 'es',
                'defaultView' => 'month',
                'defaultDate' => date('Y/m/d', strtotime(str_replace('/', '-', $value->date_init))),

            ]
        );

        return $calendar;
    }




}
