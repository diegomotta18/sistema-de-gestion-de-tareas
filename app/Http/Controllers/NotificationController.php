<?php

namespace App\Http\Controllers;

use App\Notifications\CommentNotification;
use App\Notifications\ExpiredNotification;
use App\Notifications\HolidayCreatedNotification;
use App\Notifications\InformatedNotification;
use App\Notifications\NovedadNotification;
use App\Notifications\TaskFinishNotification;
use App\Notifications\TaskNotification;
use App\Notifications\TaskProcessNotification;
use App\Notifications\TaskRespondedNotification;
use Illuminate\Notifications\DatabaseNotification;

class NotificationController extends Controller
{
    //
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        return view('notifications.index',[
            'notifications' => auth()->user()->notifications->whereIn('type',array(TaskNotification::class,InformatedNotification::class,CommentNotification::class,NovedadNotification::class,TaskProcessNotification::class,TaskFinishNotification::class,TaskRespondedNotification::class,HolidayCreatedNotification::class)),

        ]);
    }

    public function expireds(){

        return view('tasks.expired',[
            'notifications' => auth()->user()->notifications->whereIn('type',array(ExpiredNotification::class)),

        ]);
    }

    public function expiredsReadAll(){

        $notifications = auth()->user()->notifications->whereIn('type',array(ExpiredNotification::class));
        foreach ($notifications as $n){
            DatabaseNotification::find($n->id)->markAsRead();
        }

        return 'ok';
    }

    public function expiredsDestroyAll(){

        $notifications = auth()->user()->notifications->whereIn('type',array(ExpiredNotification::class));
        foreach ($notifications as $n){
            DatabaseNotification::find($n->id)->delete();
        }

        return 'ok';
    }

    public function readAll(){
        $notifications =  auth()->user()->unreadNotifications->whereIn('type',array(TaskNotification::class,InformatedNotification::class,CommentNotification::class,NovedadNotification::class,TaskProcessNotification::class,TaskFinishNotification::class,TaskRespondedNotification::class,HolidayCreatedNotification::class));
        foreach ($notifications as $n){
            DatabaseNotification::find($n->id)->markAsRead();
        }

        return 'ok';
    }

    public function readAllBox(){
        $notifications =  auth()->user()->notifications->whereIn('type',array( \App\Notifications\MessageNotification::class, \App\Notifications\MsgThreadNotification::class));
        foreach ($notifications as $n){
            DatabaseNotification::find($n->id)->markAsRead();
        }

        return 'ok';
    }



    public function destroyAll(){
        $notifications =  auth()->user()->notifications->whereIn('type',array(TaskNotification::class,InformatedNotification::class,CommentNotification::class,NovedadNotification::class,TaskProcessNotification::class,TaskFinishNotification::class,TaskRespondedNotification::class));
        foreach ($notifications as $n){
            DatabaseNotification::find($n->id)->delete();
        }

        return $notifications;
    }

    public function read($id){
        DatabaseNotification::find($id)->markAsRead();
        return 'ok';
    }

    public function destroy($id){
        DatabaseNotification::find($id)->delete();

        return 'ok';
    }
}
