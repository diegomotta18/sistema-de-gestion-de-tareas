<?php

namespace App\Http\Requests;

use App\Rules\DateToday;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;

        //return Gate::allows('update',this->event)
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required|max:255',
            'description' => 'required|max:25000',
            'place' => 'required|max:255',
            'date_init' => ['required','date_format:d/m/Y', new DateToday()],
            'date_finish' => 'required|date_format:d/m/Y|after_or_equal:date_init',
            'assigneds',

        ];
    }
}
