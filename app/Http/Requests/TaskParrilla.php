<?php

namespace App\Http\Requests;

use App\Rules\BeforeToday;
use App\Rules\DateToday;
use Illuminate\Foundation\Http\FormRequest;

class TaskParrilla extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title'  => 'required|max:255',
            'description'  => 'required|max:15000',
            'date_init',
            'date_finish',
            'assigneds',
            'area' => 'required',
            'project',
            'replacements',
            'informeds',
            'users_see' ,
            'hide',
            'files',
            'parrilla'
        ];
    }
}
