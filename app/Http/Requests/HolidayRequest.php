<?php

namespace App\Http\Requests;

use App\Rules\BeforeToday;
use Illuminate\Foundation\Http\FormRequest;

class HolidayRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'date_init' => 'required',
            'date_finish'=> ['required','date_format:d/m/Y', new BeforeToday($this->input('date_init'))],
            'date_reinstate' => ['required','date_format:d/m/Y', new BeforeToday($this->input('date_finish'))],
            'observations' =>'max:15000',
            'finish_holiday',
            'user' => 'required',
        ];
    }
}
