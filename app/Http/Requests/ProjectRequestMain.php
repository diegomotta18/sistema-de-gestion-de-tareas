<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 7/6/18
 * Time: 10:32
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class ProjectRequestMain extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'description' => 'required|max:15000',
            'areas' => 'required',

        ];
    }
}