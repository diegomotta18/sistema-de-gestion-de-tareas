<?php

namespace App\Http\Requests;

use App\Rules\BeforeToday;
use App\Rules\DateToday;
use Illuminate\Foundation\Http\FormRequest;

class TaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title'  => 'required|max:255',
            'description'  => 'required|max:15000',
            'date_init' => 'required',
            'date_finish' => ['required','date_format:d/m/Y', new BeforeToday($this->input('date_init'))],
            'assigneds' ,
            'replacements',
            'informeds',
            'users_see' ,
            'hide',
            'files',
            'hora'
        ];
    }
}
