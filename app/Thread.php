<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\HasDatabaseNotifications;
use Illuminate\Notifications\Notifiable;

class Thread extends Model
{
    //
    use SoftDeletes;

    protected $table = 'threads';
    protected $datas = [ 'deleted_at','created_at', 'updated_at'];

    protected $fillable = [
        'title','closed'
    ];


    public function messages()
    {
        return $this->hasMany(MsgThread::class);
    }

    public function members(){
        return $this->belongsToMany(User::class, 'thread_user');
    }

    public function boxthread(){
        return $this->hasMany(BoxThread::class);
    }

}
