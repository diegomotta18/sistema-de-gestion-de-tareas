<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class MsgThreadNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $message;
    protected $boxthread;

    public function __construct($message, $boxthread)
    {
        //
        $this->message = $message;
        $this->boxthread = $boxthread;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
//        return $this->message->toArray();
        return [
            'title' =>  'Recibiste un nuevo mensaje',
            'subject' => $this->message->thread->title,
            'user' => $this->message->user->name,
            'msg_id' => $this->message->id,
            'link' => route('threads.show',$this->boxthread),
            'date' => $this->message->created_at,

        ];
    }
}
