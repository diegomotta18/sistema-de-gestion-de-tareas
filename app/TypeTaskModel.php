<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeTaskModel extends Model
{
    //
    use SoftDeletes;

    protected $table = 'type_task';

    protected $datas = [ 'deleted_at','created_at', 'updated_at'];

    protected $fillable = [
        'type','id'
    ];

    public function task(){
        return $this->hasOne(Task::class);
    }


}
