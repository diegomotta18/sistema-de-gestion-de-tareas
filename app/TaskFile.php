<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskFile extends Model
{
    //
    protected $table = 'task_file';
    protected $datas = [ 'deleted_at','created_at', 'updated_at'];

    protected $fillable = [
        'task_id','filename', 'title', 'extension'
    ];
}
