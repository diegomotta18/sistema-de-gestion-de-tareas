<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentFile extends Model
{
    //
    protected $table = 'comment_file';
    protected $datas = [ 'deleted_at','created_at', 'updated_at'];

    protected $fillable = [
        'comment_id','filename', 'title', 'extension','url'
    ];
}
