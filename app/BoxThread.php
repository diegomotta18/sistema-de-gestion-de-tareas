<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class BoxThread extends Model
{
    //
    use Notifiable,SoftDeletes;

    protected $table = 'box_threads';
    protected $datas = [ 'deleted_at','created_at', 'updated_at'];

    protected $fillable = [
       'id', 'thread_id','box_id','closed'
    ];

    public function box()
    {
        return $this->belongsTo(Box::class);
    }

    public function thread()
    {
        return $this->belongsTo(Thread::class);
    }


    public function messages()
    {
        return $this->belongsToMany(MsgThread::class,'box_thread_msg_thread');
    }


    public function lastMessage(){
        return $this->messages->first();
    }

    public function  checkFile(){
        foreach ($this->messages as $message){
            if(isset($message->files) && $message->files->count() > 0){
                return true;
            }
        }
    }
}
