<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginModel extends Model
{
    protected $table = 'login';
    protected $primaryKey = 'id_user';
    protected $fillable = [ 'id_user', 'start_date'];
    public $timestamps = false;

    public function user() {
        return $this->belongsTo(User::class, 'id_user', 'id');
    }

    public function getStartDateAttribute($value) {
        return date('d-m-Y H:i', strtotime($value)) . ' hs';
    }

    public function getEndDateAttribute($value) {
        return $value ? date('d-m-Y H:i', strtotime($value)) . ' hs' : '-';
    }}
