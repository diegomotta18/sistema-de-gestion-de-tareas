<?php

use Faker\Generator as Faker;
use Illuminate\Support\Carbon;

$factory->define(\App\Task::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'description' => $faker->text(200),
        'date_init' => Carbon::create('2018', '07', '10'),
        'date_finish' =>Carbon::create('2018', '07', '11'),
        'project_id' => 1,
        'area_id' => 1,
        'task_id' => null,
        'status_id' => 1,
    ];

});
