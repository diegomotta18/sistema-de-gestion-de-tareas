<?php

use Faker\Generator as Faker;

$factory->define(\App\TypeTaskModel::class, function (Faker $faker) {
    return [
        'type' => $faker->title,
    ];

});
