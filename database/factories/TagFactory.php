<?php

use Faker\Generator as Faker;
use Illuminate\Support\Carbon;

$factory->define(\App\Tag::class, function (Faker $faker) {
    return [
        'name' => $faker->title,
        'color' => $faker->colorName,

    ];

});