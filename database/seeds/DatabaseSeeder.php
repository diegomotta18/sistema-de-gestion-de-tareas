<?php

use App\Status;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(AreaSeeder::class);
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(EstadoSeeder::class);

        $this->call(ProjectSeeder::class);
        $this->call(NoticeSeeder::class);
        $this->call(TagSeeder::class);
        $this->call(TaskSeeder::class);

    }
}
