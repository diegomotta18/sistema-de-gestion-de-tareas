<?php

use App\User;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user1 = User::find(1);
        $task = \App\Task::find(1);
        $commet = new \App\Comment();
        $commet->comment= "un primer comentario comentario";
        $commet->task_id = $task->id;
        $commet->user_id = $user1->id;
        $commet->save();
        $commet->parent_id = $commet->id;
        $commet->save();

        $commet = new \App\Comment();
        $commet->comment= "un segundo comentario";
        $commet->task_id = $task->id;
        $commet->user_id = $user1->id;

        $commet->save();

        $commet->parent_id = $commet->id;
        $commet->save();
    }
}
