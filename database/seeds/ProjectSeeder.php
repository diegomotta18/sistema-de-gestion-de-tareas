<?php

use App\Status;
use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $area = \App\Area::where('name', 'sistemas')->first();
        $user1 = \App\User::where('id', 1)->first();
        $user2 = \App\User::where('id', 2)->first();
        factory(\App\Project::class)->create([
            'name' => 'proyecto 1',
            'area_id' => $area->id,
            'user_id' => $user1->id,
            'description' => 'Non incidunt impedit numquam sint. Maxime odit ab nisi quis corporis adipisci. Earum sit debitis hic iure.'
        ]);
        factory(\App\Project::class)->create([
            'name' => 'proyecto 2',
            'area_id' => $area->id,
            'user_id' => $user1->id,
            'description' => 'Non incidunt impedit numquam sint. Maxime odit ab nisi quis corporis adipisci. Earum sit debitis hic iure.'
        ]);
        factory(\App\Project::class)->create([
            'name' => 'proyecto 3',
            'area_id' => $area->id,
            'user_id' => $user2->id,
            'description' => 'Non incidunt impedit numquam sint. Maxime odit ab nisi quis corporis adipisci. Earum sit debitis hic iure.'
        ]);
        factory(\App\Project::class)->create([
            'name' => 'proyecto 4',
            'area_id' => $area->id,
            'user_id' => $user2->id,
            'description' => 'Non incidunt impedit numquam sint. Maxime odit ab nisi quis corporis adipisci. Earum sit debitis hic iure.'
        ]);




    }
}
