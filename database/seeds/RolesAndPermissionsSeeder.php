<?php

use App\Box;
use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $area_sistemas = \App\Area::whereName('Sistemas')->first();
        $comprasmisiones = \App\Area::whereName('Compras')->first();
        $direccion = \App\Area::whereName('Dirección')->first();
        $legal = \App\Area::whereName('Legales')->first();
        $prensa = \App\Area::whereName('Prensa')->first();
        $rrhh = \App\Area::whereName('RRHH')->first();
        $tecnica = \App\Area::whereName('Técnica')->first();
        $turismomisiones = \App\Area::whereName('Turismo')->first();
        $ventas = \App\Area::whereName('Ventas')->first();
        $diseno = \App\Area::whereName('Diseño')->first();
        $intendencia = \App\Area::whereName('Intendencia')->first();

//        $areas = [$area_sistemas, $comprasmisiones, $direccion, $legal, $prensa, $rrhh, $tecnica, $turismomisiones, $ventas];

        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions

        // create roles and assign existing permissions
        $role1 = Role::create(['name' => 'god']);
        $role1 = Role::create(['name' => 'admin']);
        $role2 = Role::create(['name' => 'user']);

        $user = User::create([
            'name' => 'Diego Motta',
            'email' => 'diegomotta18@gmail.com',
            'password' => bcrypt('admin#*'),
            'chief' => true,
            'area_id' =>  $area_sistemas->id
        ]);

        $user->assignRole('god');

        $areas = \App\Area::all();

        foreach ($areas as $area){
            $user->areas()->attach($area);
            $user->save();
        }

        Box::create([
            'user_id' => $user->id
        ]);

        $user1 = User::create([
            'name' => 'direccion',
            'email' => 'direccion@direccion.com',
            'password' => bcrypt('admin#*'),
            'chief' => true,
            'area_id' => $direccion->id
        ]);

        $user1->assignRole('admin');
        foreach ($areas as $area){
            $user1->areas()->attach($area);
            $user1->save();
        }

        Box::create([
            'user_id' => $user1->id
        ]);




        $user2 = User::create([
            'name' => 'Compras',
            'email' => 'compras@compras.com',
            'password' => bcrypt('admin#*'),
            'chief' => true,
            'area_id' => $comprasmisiones->id
        ]);

        $user2->areas()->attach($comprasmisiones);
        $user2->assignRole('admin');
        $user2->save();

        Box::create([
            'user_id' => $user2->id
        ]);

        $user3 = User::create([
            'name' => 'recursos',
            'email' => 'rrhh@rrhh.com',
            'password' => bcrypt('admin#*'),
            'chief' => true,
            'area_id' => $rrhh->id
        ]);

        $user3->areas()->attach($rrhh);
        $user3->assignRole('admin');

        $user3->save();

        Box::create([
            'user_id' => $user3->id
        ]);


        $user4 = User::create([
            'name' => 'legales',
            'email' => 'legales@legales.com',
            'password' => bcrypt('admin#*'),
            'chief' => true,
            'area_id' => $legal->id
        ]);

        $user4->areas()->attach($legal);
        $user4->assignRole('admin');

        $user4->save();

        Box::create([
            'user_id' => $user4->id
        ]);

    }
}
