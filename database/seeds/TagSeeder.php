<?php

use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Tag::class)->create(['name'=>'Urgente', 'color' => '#ed5565']);

        factory(\App\Tag::class)->create(['name'=>'Informar', 'color' => '#23c6c8']);

    }
}
