<?php

use Illuminate\Database\Seeder;

class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Status::class)->create([
            'name' => 'Informado',
            'color' => '#5bc0de'
        ]);


        factory(\App\Status::class)->create([
            'name' => 'Respondido',
            'color' => '#f8ac59'

        ]);

        factory(\App\Status::class)->create([
            'name' => 'En proceso',
            'color' => '#d1dade'

        ]);

        factory(\App\Status::class)->create([
            'name' => 'Finalizado',
            'color' => '#1ab394'

        ]);


    }
}
