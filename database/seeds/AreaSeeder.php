<?php

use Illuminate\Database\Seeder;

class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Area::class)->create([
            'name' => 'Compras',
            'icon' => 'fa-shopping-cart',
        ]);

        factory(\App\Area::class)->create([
            'name' => 'Dirección',
            'icon' =>'fa-building'
        ]);

        factory(\App\Area::class)->create([
            'name' => 'Legales',
            'icon' => 'fa-balance-scale',

        ]);

        factory(\App\Area::class)->create([
            'name' => 'Prensa',
            'icon' => 'fa-columns',

        ]);

        factory(\App\Area::class)->create([
            'name' => 'RRHH',
            'icon' => 'fa-users'
        ]);

        factory(\App\Area::class)->create([
            'name' => 'Sistemas',
            'icon' => 'fa-code'
        ]);

        factory(\App\Area::class)->create([
            'name' => 'Técnica',
            'icon' => 'fa-cogs'
        ]);

        factory(\App\Area::class)->create([
            'name' => 'Turismo',
            'icon' => 'fa-globe'
        ]);

        factory(\App\Area::class)->create([
            'name' => 'Ventas',
            'icon' => 'fa-shopping-cart'
        ]);

        factory(\App\Area::class)->create([
            'name' => 'Diseño',
            'icon'=> 'fa-edit'
        ]);

        factory(\App\Area::class)->create([
            'name' => 'Administración',
            'icon' => 'fa-calculator'
        ]);

        factory(\App\Area::class)->create([
            'name' => 'Intendencia',
            'icon' =>'fa-tasks'
        ]);
    }
}
