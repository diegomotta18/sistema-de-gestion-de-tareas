<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLicenseObservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_observations', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('observations')->nullable();
            $table->unsignedInteger('license_id')->nullable();
            $table->foreign('license_id')->references('id')->on('licenses');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('license_observations');
    }
}
