<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoxThreadMsgThreadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('box_thread_msg_thread', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('box_thread_id')->nullable();
            $table->foreign('box_thread_id')->references('id')->on('box_threads');
            $table->unsignedInteger('msg_thread_id')->nullable();
            $table->foreign('msg_thread_id')->references('id')->on('msg_threads');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('box_thread_msg_thread');
    }
}
