<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParrillaToTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table) {
            //
            $table->boolean('parrilla')->nullable();
            $table->date('date_init')->nullable()->change();
            $table->date('date_finish')->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            //
            $table->dropColumn('parrilla');
            $table->date('date_init')->nullable(false)->change();
            $table->date('date_finish')->nullable(false)->change();

        });
    }
}
