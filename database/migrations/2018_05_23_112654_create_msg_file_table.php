<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsgFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msg_file', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('msg_thread_id')->nullable();
            $table->foreign('msg_thread_id')->references('id')->on('msg_threads');
            $table->string('filename');
            $table->string('title')->nullable();
            $table->string('extension')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('msg_file');
    }
}
