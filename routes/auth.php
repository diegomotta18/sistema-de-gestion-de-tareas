<?php

use App\Http\Middleware\LastSeen;

/*
 * JSON Routes
 */
Route::get('/areasj', [
    'uses' => 'AreaController@allAreas',
]);
Route::get('/areas/{area}/usersj', [
    'uses' => 'AreaController@usersForArea',
]);

Route::get('/areasforuserj', [
   'uses' => 'AreaController@areaForUser',
]);

Route::get('/areas/{id}/projectsj', [
    'uses' => 'AreaController@projectForArea',
]);

/**
 * Usuarios
 */
Route::get('/usuarios', [
    'uses' => 'UserController@index',
    'as' => 'users.index'
])->middleware(LastSeen::class);

Route::get('/usersj', [
    'uses' => 'UserController@allUser',
    'as' => 'users.all'
]);

Route::get('/usuarios/create', [
    'uses' => 'UserController@create',
    'as' => 'users.create'
]);

Route::post('/usuarios', [
    'uses' => 'UserController@store',
    'as' => 'users.store'
]);

Route::get('/usuarios/{user}/edit', [
    'uses' => 'UserController@edit',
    'as' => 'users.edit'
]);

Route::put('/usuarios/{user}/update', [
    'uses' => 'UserController@update',
    'as' => 'users.update'
]);

Route::delete('/usuarios/{id}/destroy', [
    'uses' => 'UserController@destroy',
    'as' => 'users.destroy'
])->where('id', '[0-9]+');

Route::get('/usuarios/logins', [
    'uses' => 'UserController@loginsUser',
    'as' => 'users.logins'
]);

/**
 * Proyectos
 */
Route::get('projects/index/{area?}', [
    'uses' => 'ProjectController@index',
    'as' => 'projects.index'

])->middleware(LastSeen::class);

Route::get('/areas/{area}/projects/create', [
    'uses' => 'ProjectController@create',
    'as' => 'projects.create'
])->middleware(LastSeen::class);

Route::post('/areas/{area}/projects/', [
    'uses' => 'ProjectController@store',
    'as' => 'projects.store'
])->middleware(LastSeen::class);

Route::get('project/createmain/{area?}', [
    'uses' => 'ProjectController@createmain',
    'as' => 'projects.createmain'
])->middleware(LastSeen::class);

Route::post('project/storemain/{area?}', [
    'uses' => 'ProjectController@storemain',
    'as' => 'projects.storemain'
])->middleware(LastSeen::class);

Route::get('/areas/{area}/projects/{project}/edit', [
    'uses' => 'ProjectController@edit',
    'as' => 'projects.edit'
])->middleware(LastSeen::class);

Route::put('/areas/{area}/projects/{project}/update', [
    'uses' => 'ProjectController@update',
    'as' => 'projects.update'
])->middleware(LastSeen::class);

Route::delete('/projects/{id}/destroy', [
    'uses' => 'ProjectController@destroy',
    'as' => 'projects.destroy'
])->where('id', '[0-9]+')->middleware(LastSeen::class);

/**
 * Estados
 */
Route::get('/statuses', [
    'uses' => 'StatusController@index',
    'as' => 'statuses.index'
]);

Route::get('/statuses/create', [
    'uses' => 'StatusController@create',
    'as' => 'statuses.create'
]);

Route::get('/statuses/{status}/edit', [
    'uses' => 'StatusController@edit',
    'as' => 'statuses.edit'
]);

Route::put('/statuses/{status}/update', [
    'uses' => 'StatusController@update',
    'as' => 'statuses.update'
]);

Route::post('/statuses/store', [
    'uses' => 'StatusController@store',
    'as' => 'statuses.store'
]);

Route::delete('/statuses/{status}/destroy', [
    'uses' => 'StatusController@destroy',
    'as' => 'statuses.destroy'
]);

/**
 * Estados
 */
Route::get('/typelicenses', [
    'uses' => 'TypeLicenseController@index',
    'as' => 'typelicense.index'
]);

Route::get('/typelicenses/create', [
    'uses' => 'TypeLicenseController@create',
    'as' => 'typelicense.create'
]);

Route::get('/typelicenses/{typelicense}/edit', [
    'uses' => 'TypeLicenseController@edit',
    'as' => 'typelicense.edit'
]);

Route::put('/typelicenses/{typelicense}/update', [
    'uses' => 'TypeLicenseController@update',
    'as' => 'typelicense.update'
]);

Route::post('/typelicenses/store', [
    'uses' => 'TypeLicenseController@store',
    'as' => 'typelicense.store'
]);

Route::delete('/typelicenses/{typelicense}/destroy', [
    'uses' => 'TypeLicenseController@destroy',
    'as' => 'typelicense.destroy'
]);

/**
 * Areas
 */
Route::get('/areas', [
    'uses' => 'AreaController@index',
    'as' => 'areas.index'
]);

Route::get('/areas/{area}/edit', [
    'uses' => 'AreaController@edit',
    'as' => 'areas.edit'
]);

Route::put('/areas/{id}/update', [
    'uses' => 'AreaController@update',
    'as' => 'areas.update'
]);

Route::get('/areas/create', [
    'uses' => 'AreaController@create',
    'as' => 'areas.create'
]);

Route::post('/areas/store', [
    'uses' => 'AreaController@store',
    'as' => 'areas.store'
]);

Route::get('/areas/{area}', [
    'uses' => 'AreaController@show',
    'as' => 'areas.show'
]);

Route::get('/areas/{area}/tasks', [
    'uses' => 'AreaController@tasks',
    'as' => 'areas.show.tasks'
])->middleware(LastSeen::class);

/**
 * Tareas
 */
Route::get('/areas/{area}/projects/{project}/tasks/create', [
    'uses' => 'TaskController@create',
    'as' => 'tasks.project.create'
])->middleware(LastSeen::class);

Route::get('/areas/{area}/projects/tasks/create', [
    'uses' => 'TaskController@create',
    'as' => 'tasks.area.create'
])->middleware(LastSeen::class);

Route::get('dashboard/tasks/create/{area?}/{project?}', [
    'uses' => 'TaskController@create',
    'as' => 'tasks.dashboard.create'
])->middleware(LastSeen::class);

Route::get('dashboard/tasks/{task}', [
    'uses' => 'TaskController@editwithoutarea',
    'as' => 'tasks.dashboard.edit'
])->middleware(LastSeen::class);

Route::get('/areas/{area}/tasks/{task}/edit/', [
    'uses' => 'TaskController@editwitharea',
    'as' => 'task.area.edit'
])->middleware(LastSeen::class);

Route::get('/areas/{area?}/projects/{project?}/tasks/{task}/edit', [
    'uses' => 'TaskController@editwithareaproject',
    'as' => 'task.project.edit'
])->middleware(LastSeen::class);

Route::get('/areas/{area}/projects/{project}/tasks/{task}/show', [
    'uses' => 'TaskController@show_with_area_project',
    'as' => 'task.project.show'
])->middleware(LastSeen::class);

Route::get('/areas/{area}/tasks/{task}/show', [
    'uses' => 'TaskController@show_with_area',
    'as' => 'task.area.show'
]);

Route::get('/tasks/{task}/show', [
    'uses' => 'TaskController@show',
    'as' => 'task.home.show'
])->middleware(LastSeen::class);

Route::get('/search/{area?}', 'HomeController@index')->name('taskhome.index');

Route::get('/areas/{area}/projects/{project}/tasks', [
    'uses' => 'ProjectController@show',
    'as' => 'projects.show'
])->middleware(LastSeen::class);

Route::put('tasks/{task}/{area?}/{project?}', [
    'uses' => 'TaskController@update',
    'as' => 'tasks.update'
])->middleware(LastSeen::class);

Route::post('tasks/{area?}/{project?}', [
    'uses' => 'TaskController@store',
    'as' => 'tasks.store'
])->middleware(LastSeen::class);

Route::get('tasks/createTaskMain/{area?}', [
    'uses' => 'TaskController@createTaskMain',
    'as' => 'tasks.maincreate'
])->middleware(LastSeen::class);

Route::post('/tasksmain/{area?}', [
    'uses' => 'TaskController@saveTaskMain',
    'as' => 'tasks.mainstore'
])->middleware(LastSeen::class);

Route::get('tasks/files/{file}', [
    'uses' => 'TaskController@download',
    'as' => 'tasks.download'
])->middleware(LastSeen::class);

Route::delete('tasks/{id}/destroy', [
    'uses' => 'TaskController@destroy',
    'as' => 'tasks.destroy'
])->middleware(LastSeen::class);

Route::put('task/finish/{id}', [
    'uses' => 'TaskController@finishTask',
    'as' => 'task.finishtask'
])->middleware(LastSeen::class);

Route::put('task/init/{id}', [
    'uses' => 'TaskController@initTask',
    'as' => 'task.inittask'
])->middleware(LastSeen::class);

Route::put('task/process/{id}', [
    'uses' => 'TaskController@processTask',
    'as' => 'task.processtask'
])->middleware(LastSeen::class);

Route::put('task/reply/{id}', [
    'uses' => 'TaskController@replyTask',
    'as' => 'task.replytask'
])->middleware(LastSeen::class);

Route::put('task/pigeonhole/{id}', [
    'uses' => 'TaskController@pigeonhole',
    'as' => 'task.pigeonhole'
])->middleware(LastSeen::class);

Route::put('task/denegate/{id}', [
    'uses' => 'TaskController@denegateTask',
    'as' => 'task.denegatetask'
])->middleware(LastSeen::class);

Route::get('task/{task}/changeurl', [
    'uses' => 'TaskController@changeUrl',
    'as' => 'task.changeUrl'
])->middleware(LastSeen::class);

Route::put('task/{task}/addUrl', [
    'uses' => 'TaskController@addUrl',
    'as' => 'task.addurl'
])->middleware(LastSeen::class);

/*
 * Subtareas
 */
Route::get('/areas/{area}/projects/{project}/tasks/{task}/subtasks/create', [
    'uses' => 'SubTaskController@create',
    'as' => 'subtasks.create'
]);

Route::get('/areas/{area}/projects/{project}/tasks/{task}/subtaskt/{subtask}/edit', [
    'uses' => 'SubTaskController@editwithareaproject',
    'as' => 'subtasks.editwithareaproject'
]);

Route::get('/areas/{area}/projects/{project}/tasks/{task}/subtaskt/{subtask}', [
    'uses' => 'SubTaskController@show',
    'as' => 'subtasks.show'
]);

Route::post('subtasks/{area?}/{project?}/{task?}', [
    'uses' => 'SubTaskController@store',
    'as' => 'subtasks.store'
]);

Route::put('subtasks/{subtask}/{task}/{area?}/{project?}', [
    'uses' => 'SubTaskController@update',
    'as' => 'subtasks.update'
]);

Route::delete('subtasks/{id}/destroy', [
    'uses' => 'SubTaskController@destroy',
    'as' => 'subtasks.destroy'
]);

Route::put('subtask/finish/{id}', [
    'uses' => 'SubTaskController@finishTask',
    'as' => 'subtask.finishtask'
]);

Route::put('subtask/init/{id}', [
    'uses' => 'SubTaskController@initTask',
    'as' => 'subtask.inittask'
]);

Route::put('subtask/reply/{id}', [
    'uses' => 'SubTaskController@replyTask',
    'as' => 'subtask.replytask'
]);

Route::put('subtask/process/{id}', [
    'uses' => 'SubTaskController@processTask',
    'as' => 'subtask.processtask'
]);

Route::get('/find/taskj/{id}', [
    'uses' => 'TaskController@getTask',
    'as' => 'task.find'
]);

Route::put('subtask/denegate/{id}', [
    'uses' => 'SubTaskController@denegateTask',
    'as' => 'subtask.denegatetask'
]);

/**
 * Noticias
 */
Route::get('/notices', [
    'uses' => 'NoticeController@index',
    'as' => 'notices.index'
]);

Route::get('/notices/create', [
    'uses' => 'NoticeController@create',
    'as' => 'notices.create'
]);

Route::get('/notices/{notice}', [
    'uses' => 'NoticeController@show',
    'as' => 'notices.show'
])->middleware(LastSeen::class);

Route::post('/notices', [
    'uses' => 'NoticeController@store',
    'as' => 'notices.store'
]);

Route::delete('/notices/{id}/destroy', [
    'uses' => 'NoticeController@destroy',
    'as' => 'notices.destroy'
])->where('id', '[0-9]+');

/**
 * Comentarios
 */
Route::get('tasksComments/{id}', [
    'uses' => 'CommentController@commentsTask',
    'as' => 'tasks.comments'
])->middleware(LastSeen::class);

Route::post('taskStoreComment/{id}', [
    'uses' => 'CommentController@commentInTaskStore',
])->middleware(LastSeen::class);

Route::put('taskUpdateComment/{id}', [
    'uses' => 'CommentController@commentInTaskUpdate',
])->middleware(LastSeen::class);

/**
 * Notificaciones
 */
Route::get('notifications', [
    'uses' => 'NotificationController@index',
    'as' => 'notifications.index'
])->middleware(LastSeen::class);

Route::get('expireds', [
    'uses' => 'NotificationController@expireds',
    'as' => 'notifications.expireds'
])->middleware(LastSeen::class);

Route::put('expireds/readall', [
    'uses' => 'NotificationController@expiredsReadAll',
    'as' => 'expireds.readall'
])->middleware(LastSeen::class);

Route::delete('expireds/destroyall', [
    'uses' => 'NotificationController@expiredsDestroyAll',
    'as' => 'expireds.destroyAll'
]);

Route::delete('notifications/destroyall', [
    'uses' => 'NotificationController@destroyAll',
    'as' => 'notifications.destroyAll'
])->middleware(LastSeen::class);

Route::delete('notifications/{notification}', [
    'uses' => 'NotificationController@destroy',
    'as' => 'notifications.destroy'
])->middleware(LastSeen::class);

Route::put('notifications/readall', [
    'uses' => 'NotificationController@readAll',
    'as' => 'notifications.allread'
])->middleware(LastSeen::class);

Route::put('notifications/readallbox', [
    'uses' => 'NotificationController@readAllBox',
    'as' => 'notifications.allreadbox'
])->middleware(LastSeen::class);

Route::put('notifications/{notification}', [
    'uses' => 'NotificationController@read',
    'as' => 'notifications.read'
]);

/**
 * Archivos
 */
Route::post('tasks/deletefile/{area}/{project?}/{key}', [
    'uses' => 'TaskController@deleteFile',
    'as' => 'file.destroy'
])->middleware(LastSeen::class);

/**
 * Casilla de mensajes
 */
Route::get('box/threads/create', [
    'uses' => 'ThreadController@create',
    'as' => 'threads.create'
]);

Route::post('box/threads/reply/{thread}', [
    'uses' => 'ThreadController@reply',
    'as' => 'threads.reply'
]);

Route::get('box/threads', [
    'uses' => 'BoxThreadController@index',
    'as' => 'threads.index'
]);

Route::get('box/threads/{boxthread}', [
    'uses' => 'BoxThreadController@show',
    'as' => 'threads.show'
]);

Route::post('box/threads/store', [
    'uses' => 'ThreadController@store',
    'as' => 'threads.store'
]);

Route::get('box/threads/files/{file}', [
    'uses' => 'BoxThreadController@download',
    'as' => 'threads.download'
]);

Route::delete('box/threads/{id}', [
    'uses' => 'BoxThreadController@destroy',
    'as' => 'threads.destroy'
]);

/**
 * Etiquetas
 */
Route::get('tags', [
    'uses' => 'TagController@index',
    'as' => 'tags.index'
]);

Route::post('tags', [
    'uses' => 'TagController@store',
    'as' => 'tags.store'
]);

Route::put('tags/{id}/update', [
    'uses' => 'TagController@update',
    'as' => 'tags.update'
]);

Route::get('tags/allTags', [
    'uses' => 'TagController@allTags',
    'as' => 'tags.alltags'
]);

/**
 * Enviados
 */
Route::get('box/sends', [
    'uses' => 'SendController@index',
    'as' => 'sends.index'
]);

Route::get('projectsj/{area?}', [
    'uses' => 'HomeController@getProjects',
    'as' => 'projects.all'
]);

/**
 * Eventos
 */
Route::get('events', [
    'uses' => 'TaskController@allTaskEvent',
    'as' => 'events.index'
])->middleware(LastSeen::class);

Route::get('events/morning', [
    'uses' => 'TaskController@allTaskEventMorning',
    'as' => 'events.morning'
])->middleware(LastSeen::class);

Route::get('events/afternoon', [
    'uses' => 'TaskController@allTaskEventAfternoon',
    'as' => 'events.afternoon'
])->middleware(LastSeen::class);

/**
 * Propuestos
 */
Route::get('proposeds', [
    'uses' => 'TaskController@allTaskProposeds',
    'as' => 'proposeds.index'
])->middleware(LastSeen::class);

Route::get('proposeds/morning', [
    'uses' => 'TaskController@allTaskProposedsMorning',
    'as' => 'proposeds.morning'
])->middleware(LastSeen::class);

Route::get('proposeds/afternoon', [
    'uses' => 'TaskController@allTaskProposedsAfternoon',
    'as' => 'proposeds.afternoon'
])->middleware(LastSeen::class);

/*
 * Parrilla
 */
Route::put('parrilla/{id}/update', [
    'uses' => 'ParrillaController@update',
    'as' => 'parrilla.update'
])->middleware(LastSeen::class);

Route::get('parrilla', [
    'uses' => 'ParrillaController@index',
    'as' => 'parrilla.index'
])->middleware(LastSeen::class);

Route::get('parrilla/create', [
    'uses' => 'ParrillaController@create',
    'as' => 'parrilla.create'
])->middleware(LastSeen::class);

Route::get('parrilla/{task}/edit', [
    'uses' => 'ParrillaController@edit',
    'as' => 'parrilla.edit'
])->middleware(LastSeen::class);

Route::get('parrilla/{task}', [
    'uses' => 'ParrillaController@show',
    'as' => 'parrilla.show'
])->middleware(LastSeen::class);

Route::post('parrilla/store', [
    'uses' => 'ParrillaController@store',
    'as' => 'parrilla.store'
])->middleware(LastSeen::class);

/*
 * Tickets
 */
Route::get('tickets/create', [
    'uses' => 'TicketController@create',
    'as' => 'tickets.create'
]);

Route::post('tickets/store', [
    'uses' => 'TicketController@store',
    'as' => 'tickets.store'
]);

Route::get('tickets/{ticket}/edit', [
    'uses' => 'TicketController@edit',
    'as' => 'tickets.edit'
]);

Route::get('tickets', [
    'uses' => 'TicketController@index',
    'as' => 'tickets.index'
]);

Route::get('tickets/{ticket}', [
    'uses' => 'TicketController@show',
    'as' => 'tickets.show'
]);

Route::put('tickets/{ticket}/update', [
    'uses' => 'TicketController@update',
    'as' => 'tickets.update'
]);

Route::put('tickets/{ticket}/updateadmin', [
    'uses' => 'TicketController@updateAdmin',
    'as' => 'tickets.updateAdmin'
]);

Route::delete('tickets/{ticket}', [
    'uses' => 'TicketController@delete',
    'as' => 'tickets.delete'
]);

Route::put('tickets/init/{ticket}', [
    'uses' => 'TicketController@init',
    'as' => 'tickets.inprocess'
]);

Route::put('tickets/inprocess/{ticket}', [
    'uses' => 'TicketController@inprocess',
    'as' => 'tickets.inprocess'
]);

Route::put('tickets/finish/{ticket}', [
    'uses' => 'TicketController@finish',
    'as' => 'tickets.finish'
]);

Route::get('project/{project}/agileboard', [
    'uses' => 'ProjectController@agileboard',
    'as' => 'project.agileboard'
]);

/*
 * Excepciones
 */
Route::get('exceptions/{id}/show', [
    'uses' => 'ExceptionsController@show',
    'as' => 'exceptions.show'
]);

Route::get('exceptions/index', [
    'uses' => 'ExceptionsController@index',
    'as' => 'exceptions.index'
]);

Route::delete('exceptions/{id}/destroy', [
    'uses' => 'ExceptionsController@destroy',
    'as' => 'exceptions.destroy'
]);

Route::delete('exceptions/destroyall', [
    'uses' => 'ExceptionsController@destroyall',
    'as' => 'excepcions.destroyall'
]);

/*
 * Vacaciones
 */
Route::post('holidays/store', [
    'uses' => 'HolidayController@store',
    'as' => 'holidays.store'
]);

Route::get('holidays/calendar', [
    'uses' => 'HolidayController@showCalendar',
    'as' => 'holiday.calendar'
]);

Route::get('holidays', [
    'uses' => 'HolidayController@index',
    'as' => 'holiday.index'
]);
Route::get('holidays/finished', [
    'uses' => 'HolidayController@finished',
    'as' => 'holiday.finished'
]);

Route::get('holidays/create', [
    'uses' => 'HolidayController@create',
    'as' => 'holiday.create'
]);

Route::get('holidays/{holiday}/edit', [
    'uses' => 'HolidayController@edit',
    'as' => 'holiday.edit'
]);

Route::get('holidays/{holiday}', [
    'uses' => 'HolidayController@show',
    'as' => 'holiday.show'
]);

Route::put('holidays/{holiday}/update', [
    'uses' => 'HolidayController@update',
    'as' => 'holiday.update'
]);

Route::put('holidays/{holiday}/finish', [
    'uses' => 'HolidayController@finish',
    'as' => 'holiday.finish'
]);

Route::put('holidays/{holiday}/notfinish', [
    'uses' => 'HolidayController@notfinish',
    'as' => 'holiday.notfinish'
]);

Route::delete('holidays/{holiday}/delete', [
    'uses' => 'HolidayController@destroy',
    'as' => 'holiday.destroy'
]);

/*
 * Licencias
 */
Route::put('licenses/{license}/finish', [
    'uses' => 'LicenseController@finish',
    'as' => 'licenses.finish'
]);

Route::put('licenses/{license}/notfinish', [
    'uses' => 'LicenseController@notfinish',
    'as' => 'licenses.notfinish'
]);

Route::put('licenses/observation/{id}/update', [
    'uses' => 'LicenseController@updateObservation',
    'as' => 'licenses.observation.update'
]);

Route::post('licenses/store', [
    'uses' => 'LicenseController@store',
    'as' => 'licenses.store'
]);

Route::post('licenses/{id}/observations/store', [
    'uses' => 'LicenseController@storeObservation',
    'as' => 'licenses.store.observations'
]);

Route::get('licenses/{id}/observations', [
    'uses' => 'LicenseController@getObservations',
    'as' => 'licenses.observations'
]);

Route::get('licenses', [
    'uses' => 'LicenseController@index',
    'as' => 'licenses.index'
]);

Route::get('licenses/finished', [
    'uses' => 'LicenseController@finished',
    'as' => 'licenses.finished'
]);

Route::get('licenses/create', [
    'uses' => 'LicenseController@create',
    'as' => 'licenses.create'
]);

Route::get('licenses/{license}/edit', [
    'uses' => 'LicenseController@edit',
    'as' => 'licenses.edit'
]);

Route::get('licenses/{license}', [
    'uses' => 'LicenseController@show',
    'as' => 'licenses.show'
]);

Route::put('licenses/{license}/update', [
    'uses' => 'LicenseController@update',
    'as' => 'licenses.update'
]);

Route::delete('licenses/{license}/delete', [
    'uses' => 'LicenseController@destroy',
    'as' => 'licenses.destroy'
]);

/*
 * Francos / day off
 */
Route::get('freedays', [
    'uses' => 'FreedayController@index',
    'as' => 'freedays.index'
]);

Route::get('freedays/{dayoff}/show', [
    'uses' => 'FreedayController@show',
    'as' => 'freedays.show'
]);

Route::get('freedays/{dayoff}/edit', [
    'uses' => 'FreedayController@edit',
    'as' => 'freedays.edit'
]);

Route::get('freedays/create', [
    'uses' => 'FreedayController@create',
    'as' => 'freedays.create'
]);

Route::post('freedays/store', [
    'uses' => 'FreedayController@store',
    'as' => 'freedays.store'
]);

Route::put('freedays/{dayoff}/update', [
    'uses' => 'FreedayController@update',
    'as' => 'freedays.update'
]);

Route::delete('freedays/{dayoff}/destroy', [
    'uses' => 'FreedayController@destroy',
    'as' => 'freedays.destroy'
]);

/*
 * Acciones diarias /  actions
 */
Route::get('actions', [
    'uses' => 'ActionController@index',
    'as' => 'actions.index'
]);

Route::get('actions/{action}/edit', [
    'uses' => 'ActionController@edit',
    'as' => 'actions.edit'
]);

Route::get('actions/create', [
    'uses' => 'ActionController@create',
    'as' => 'actions.create'
]);

Route::post('actions/store', [
    'uses' => 'ActionController@store',
    'as' => 'actions.store'
]);

Route::put('actions/{action}/update', [
    'uses' => 'ActionController@update',
    'as' => 'actions.update'
]);

Route::delete('actions/{action}/destroy', [
    'uses' => 'ActionController@destroy',
    'as' => 'actions.destroy'
]);

Route::catch(function () {
    throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Pagina no encontrada');
});
