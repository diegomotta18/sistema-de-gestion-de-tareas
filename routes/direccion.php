<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 5/9/18
 * Time: 10:46
 */

Route::get('/direccion', [
    'uses' => 'DirectionController@index',
    'as'   => 'direccion.index'
]);


Route::get('/direccion/{area}', [
    'uses' => 'HomeController@index',
    'as'   => 'direccion.area'
]);