<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\LastSeen;

Auth::routes();


Route::get('/', 'HomeController@index')->name('home')->middleware('auth',LastSeen::class);

Route::get('/check/{email}',[
    'uses' => 'WebController@getCheckVisit',
    'as' => 'check.visit'
]);