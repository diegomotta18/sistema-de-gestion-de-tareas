<?php

return [
    /*
     |--------------------------------------------------------------------------
     | CAS Server: Settings
     |--------------------------------------------------------------------------
     |
     | Set type of this web page. Possible options are: 'server' and 'client'.
     |
     | You must specify either 'server' or 'client'.
     |
     */

    'type' => 'server',

    /*
     |--------------------------------------------------------------------------
     | CAS Server: Allowed Origins
     |--------------------------------------------------------------------------
     |
     | Set origins without trailing comma, example: http://foo.bar
     |
     */

    'allowed' => [
        // dev sites
        'http://sgmol.test',
        'http://turnos.test',
        'http://apiyoutube.test',

        // sites
        'https://sgmol.misionesonline.net',
        'https://yt.misionesonline.net',
        'https://testturnos.misionesonline.net'
    ],

    /*
     |--------------------------------------------------------------------------
     | General Settings:
     |--------------------------------------------------------------------------
     |
     | These settings should be changed if this page is working as CAS server.
     |
     */

    'usersModel' => \App\User::class,

    /*
     |--------------------------------------------------------------------------
     | CAS Client: Config Settings
     |--------------------------------------------------------------------------
     |
     | These settings should be changed if this page is working as CAS client.
     |
     */

    'casUrl' => env('CAS_HOST', null),
    'casKey' => env('CAS_KEY', null)
];